import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import {ModalController} from '@ionic/angular';
import {UnlockContactPage} from '../unlock-contact/unlock-contact.page';
import {UnlockContact1Page} from '../unlock-contact1/unlock-contact1.page';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/storage';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-profile1',
  templateUrl: './profile1.page.html',
  styleUrls: ['./profile1.page.scss'],
})
export class Profile1Page implements OnInit {

  pageroute;
  walletBalance;
  userId;
  userName;
  userImage;
  userAddress;
  length;
  docUrl1;
  docUrl2;
  userPosts:any=[];
  posts:any=[];
  professionalUser:any={};
  selectedUser:any={};
  passionistUser:any={};
  aboutUser:boolean=false;
  proffessional:boolean=false;
  passionist:boolean=false;
  professional:boolean=false;
  bookmarkedPost;
  userData:any={};
  host_id;
  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 10,
    initialSlide:0,
    slideShow:true,
    autoplay:true
  };
  slideOpts1 = {
    slidesPerView: 1,
    spaceBetween: 10,
    initialSlide:0,
    slideShow:true,
    autoplay:true
  };

  constructor(public router: Router,public route: ActivatedRoute,private photoViewer: PhotoViewer,
    public modalController: ModalController,public storage: Storage,private socialSharing: SocialSharing,
    public afa: AngularFireAuth, public fs: AngularFirestore,public authService: AuthService) {
           console.log('heyyyyyyyy')
      this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          this.pageroute=params.pageroute;
        }else if(params){
          this.professional=params.professional;
          this.pageroute=params.pageroute;
          console.log(this.pageroute);
          this.host_id = params.hostId;
          this.userId=params.userId;
          console.log(this.host_id+"hostid");
      this.getProfile();
        }       
      });
   }


   viewPhoto(x){
    console.log(x);
    this.photoViewer.show(x);
  }

  //  getProfile(){
  //    if((this.userId!=''||this.userId!=undefined||this.userId!=null) &&
  //    (this.host_id!=''||this.host_id!=undefined||this.host_id!=null)){
  //     var obj={"hostId":this.host_id,"userId":this.userId};
  //     this.authService.getUserProfile(obj).subscribe((data:any)=>{
  //      console.log(data);
  //      this.professionalUser=data.data; 
  //      console.log(this.professionalUser);
  //      this.docUrl1=this.professionalUser.documents[0].docUrl;
  //      this.docUrl2=this.professionalUser.documents[1].docUrl;
  //      console.log(this.docUrl1);
  //      console.log(this.docUrl2);
  //     });
  //    }else {
  //     var obj1={"userId":this.host_id};
  //     this.authService.getUserProfileById(obj1).subscribe((res:any)=>{
  //       console.log(res);
  //      this.professionalUser=res.data; 
  //      console.log(this.professionalUser);
  //      this.docUrl1=this.professionalUser.documents[0].docUrl;
  //      this.docUrl2=this.professionalUser.documents[1].docUrl;
  //      console.log(this.docUrl1);
  //      console.log(this.docUrl2);
  //     });
  //    }   
  //  }

  getProfile(){
    var obj1={"hostId":this.host_id,'userId':this.userId};
    this.authService.getUserProfile(obj1).subscribe((res:any)=>{
      console.log(res);
     this.professionalUser=res.data; 
          this.docUrl1=this.professionalUser.documents[0].docUrl;
       this.docUrl2=this.professionalUser.documents[1].docUrl;
       console.log(this.docUrl1);
       console.log(this.docUrl1);
     this.userPosts=res.posts;
     
    //  console.log(this.professionalUser);
    //  this.docUrl1=this.professionalUser.documents[0].docUrl;
    //  this.docUrl2=this.professionalUser.documents[1].docUrl;
    //  console.log(this.docUrl1);
    //  console.log(this.docUrl2);
    });
  }

   getUserPost(){
    this.userPosts=[];
    var obj={'userId':this.host_id};
      this.authService.getAllPost(obj).subscribe((data:any)=>{
        console.log(data);
        this.posts=data.postArray;
        this.posts.forEach(element => {
          if(element.posttype=='0' || element.posttype=='1') {
            this.userPosts.push(element);
            console.log(this.userPosts);
          }
        });
    }); 
   }

  ngOnInit() {
    // this.loadData();
    console.log(this.userId);
    console.log(this.host_id);
  }


  // loadData(){
  //   this.storage.get('user').then((user)=>{
  //     this.userData=user;
  //     this.userId=user._id;
  //     this.userName=user.fullName;
  //     this.userImage=user.user_img;
  //     this.walletBalance=user.wallet;
  //     if(user.userType==this.selectedUser.userType){
  //       this.professionalUser=this.selectedUser;
  //       this.professionalUser.isChat=true;
  //       this.docUrl1=this.professionalUser.documents[0].docUrl;
  //       this.docUrl2=this.professionalUser.documents[1].docUrl;
  //       this.getUserPost();
  //     }else {
  //       this.getProfile(); 
  //       this.getUserPost();
  //     }
  //   });
  // }

  presentModal(){
    if(this.walletBalance>5){
      this.presentUnlock();
    }else {
      this.subscription();
    }
  }

  gotochat(){ 
    this.fs.collection('friends').doc(this.userId).collection('chats').doc(
      this.professionalUser._id).set({
      recieverName: this.professionalUser.fullName,
      recieverId: this.professionalUser._id,
      recieverImage:this.professionalUser.user_img,
      senderId: this.userId,
      senderName:this.userName,
      senderImage:this.userImage,
      Timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }, { merge: true });

    this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(
      this.userId).set({
    senderName: this.professionalUser.fullName,
    senderId:this.professionalUser._id,
    senderImage:this.professionalUser.user_img,
    recieverId: this.userId,
    recieverName:this.userName,
    recieverImage:this.userImage,
    Timestamp: firebase.firestore.FieldValue.serverTimestamp()
}, { merge: true }); 
    let vc={recieverId:this.host_id,
    senderId:this.userId,
    recieverName:this.professionalUser.fullName,
    recieverImg:this.professionalUser.user_img};
    let navigationExtras = {
   
    queryParams: {
      special: JSON.stringify(vc)
    }
    
  };
    this.router.navigate(['chat'], navigationExtras);
     
      }

  async presentUnlock() {
    console.log('helloooooooo');
    const modal = await this.modalController.create({
      component: UnlockContactPage,
      cssClass: 'unlockContactModal',
      componentProps: {'professionalUser':this.professionalUser}
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.fs.collection('friends').doc(this.userId).collection('chats').doc(
        this.professionalUser._id).set({
        recieverName: this.professionalUser.fullName,
        recieverId: this.professionalUser._id,
        recieverImage:this.professionalUser.user_img,
        senderId: this.userId,
        senderName:this.userName,
        senderImage:this.userImage,
        Timestamp: firebase.firestore.FieldValue.serverTimestamp()
    }, { merge: true });
  
      this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
      senderName: this.professionalUser.fullName,
      senderId:this.professionalUser._id,
      senderImage:this.professionalUser.user_img,
      recieverId: this.userId,
      recieverName:this.userName,
      recieverImage:this.userImage,
      Timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }, { merge: true });
  let navigationExtras:NavigationExtras = {
    queryParams: {
      special: JSON.stringify({url:this.router.url,recieverId: this.professionalUser._id,
        senderId: this.userId,
        recieverName: this.professionalUser.fullName,
        senderName:this.userName,
        recieverImg: this.professionalUser.user_img})
      
    }
  };
  this.router.navigate(['chat'],navigationExtras);
    }
    
  }

  async subscription() {
    console.log('heyyyyyyyyy');
    const modal = await this.modalController.create({
      component: UnlockContact1Page,
      cssClass: 'unlockContact1Modal',
      componentProps:{'professionalUser':this.professionalUser}
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.fs.collection('friends').doc(this.userId).collection('chats').doc(
        this.professionalUser._id).set({
        recieverName: this.professionalUser.fullName,
        recieverId: this.professionalUser._id,
        recieverImage:this.professionalUser.user_img,
        senderId: this.userId,
        senderName:this.userName,
        senderImage:this.userImage,
        isBlocked:false,
        Timestamp: firebase.firestore.FieldValue.serverTimestamp()
    }, { merge: true });
  
      this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(
        this.userId).set({
      senderName: this.professionalUser.fullName,
      senderId:this.professionalUser._id,
      senderImage:this.professionalUser.user_img,
      recieverId: this.userId,
      recieverName:this.userName,
      recieverImage:this.userImage,
      isBlocked:false,
      Timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }, { merge: true });
  let navigationExtras:NavigationExtras = {
    queryParams: {
      special: JSON.stringify({url:this.router.url,recieverId: this.professionalUser._id,
        senderId: this.userId,
        recieverName: this.professionalUser.fullName,
        senderName:this.userName,
        recieverImg: this.professionalUser.user_img})
      
    }
  };
  this.router.navigate(['chat'],navigationExtras);
    }
  }

  bookmark(item,i){
    this.posts[i].isBookmarked=true;
    console.log(item);
    var obj={'postId':item._id,'postUserId':item.userId,'postUrl':item.postUrl,
    'userId':this.userId,'postTitle':item.postTitle,'postDescription':item.postDescription,
    'posttype':item.posttype,'postCategory':item.postCategory,'isBookmkared':true}
    this.authService.bookmark(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        this.authService.presentToast('Bookmark Added');
      
      }
    });
  }

  deleteBookmark(item,i){
     this.posts[i].isBookmarked=false;
    var obj={'userId':this.userId}
    this.authService.getBookmark(obj).subscribe((data:any)=>{
      this.bookmarkedPost=data.bookmarkArray;
      console.log(this.bookmarkedPost);
      this.bookmarkedPost.forEach(element => {
        if(item._id==element.postId){
          var obj1={'bookmarkId':element._id,'userId':this.userId}
          this.authService.deleteBookmark(obj1).subscribe((res:any)=>{
            console.log(res);
            this.authService.presentToast('Bookmark Removed');
       
          });
        }
      });
    });
  }

 
  likePost(item,i){
    // this.posts[i].isliked=true;
    // this.posts[i].likeCount=this.posts[i].likeCount+1;
    //   var likedPostUser={'likedPostUserid':this.userId,'userName':this.userName,
    //   'userImage':this.userImage};
    //   // this.likeArray.push(likedPostUser);
    //   var obj={'likeArray':likedPostUser,'postId':item._id,
    //   'userName':this.userName,'hostId':item.userId,'userId':this.userId,};
    //   this.authService.like(obj).subscribe((data:any)=>{
    //     console.log(data);
    //     if(data.success){
    //       this.loadData();
    //     }
    //   })  
    this.posts[i].isliked=true;
    this.posts[i].likeCount=item.likeCount+1;
    var likeArray=item.likes;
    var likedPostUser={'likedPostUserid':this.userId,'userName':this.userData.firstName,
    'userImage':this.userData.user_img};
    likeArray.push(likedPostUser);
    console.log(likeArray);
    var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":this.posts[i].likeCount,
    'userName':this.userData.firstName,'hostId':item.userId,'userId':this.userData._id,};
    this.authService.like(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        // this.loadData();
      }
    }); 
  }

  deleteLike(item,j){
    // this.posts[j].isliked=false;
    // this.posts[j].likeCount=this.posts[j].likeCount-1;
    // item.isliked=false;
    // var likeArray=item.likes;
    // var index=0;
    // for(var i=0;i<likeArray.length;i++){
    //   if(likeArray[i].likedPostUser==this.userId){
    //     index=i--;
    //     break;
    //   }
    // }
    // likeArray.splice(index,1);
    // console.log(likeArray);
    // var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id};
    // this.authService.like(obj).subscribe((res)=>{
    //   console.log(res);
    //   this.loadData();
    // });
    this.posts[j].isliked=false;
    this.posts[j].likeCount=item.likeCount-1;
    item.isliked=false;
    var likeArray=item.likes;
    var index=0;
    for(var i=0;i<likeArray.length;i++){
      if(likeArray[i].likedPostUser==this.userId){
        index=i--;
        break;
      }
    }
    likeArray.splice(index,1);
    console.log(likeArray);
    var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":this.posts[j].likeCount};
    this.authService.deleteLike(obj).subscribe((res)=>{
      console.log(res);
      // this.loadData();
  });
  }

  share(item){
    var message= item.postTitle+' \n'+item.postDescription;
    var postUrl= item.postUrl;
    this.socialSharing.share(message,null,null,postUrl);
  }


  back(){
    this.router.navigateByUrl(this.pageroute);
  }

  notification() {
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['notification'],navigationExtras);
  }

}
