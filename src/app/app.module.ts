import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SmsRetriever } from '@ionic-native/sms-retriever/ngx';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { HttpClientModule } from  '@angular/common/http';
import { FCM } from '@ionic-native/fcm/ngx';
import { Storage,IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule} from '@angular/fire/firestore';
import 'firebase/storage';
import { AngularFireStorageModule } from '@angular/fire/storage';
// import firebase from 'firebase';
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AuthService } from './auth.service';
import { AuthGuardService } from './auth-guard.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { InViewportDirective } from 'ng-in-viewport';

import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


var config = {
  apiKey: "AIzaSyAS1j-crG3dBKxiSlFITRMBuy4ko_LKfN8",
  authDomain: "hobbyit-74e3d.firebaseapp.com",
  databaseURL: "https://hobbyit-74e3d.firebaseio.com",
  projectId: "hobbyit-74e3d",
  storageBucket: "hobbyit-74e3d.appspot.com",
  messagingSenderId: "861528652215",
  appId: "1:861528652215:web:c8d008aba537e10f411032",
  measurementId: "G-NEN7C6QP5W"
};

@NgModule({
  declarations: [AppComponent,InViewportDirective,ProgressBarComponent],
  exports: [
    InViewportDirective,
  ],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),HttpClientModule, AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    AngularFireModule.initializeApp(config),AngularFirestoreModule.enablePersistence(),
    AngularFireDatabaseModule,AngularFireAuthModule,AngularFireStorageModule,
    AppRoutingModule,IonicStorageModule.forRoot()],
  providers: [
    StatusBar,AuthService,AuthGuardService,Keyboard,PhotoViewer,
    SplashScreen,File, FilePath,Camera,NativeGeocoder,SocialSharing,
    SmsRetriever,FirebaseAuthentication,FCM,Geolocation,GooglePlus,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
