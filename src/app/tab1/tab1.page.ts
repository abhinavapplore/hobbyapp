import { Component,ViewChild, ViewChildren, QueryList } from '@angular/core';
import { MenuController, ModalController, ActionSheetController,Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';
import {QuestionPage} from '../question/question.page';
import { InViewportMetadata } from 'ng-in-viewport';
import { StoryPage } from '../story/story.page';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Observable, from } from 'rxjs';
import * as moment from 'moment';
import 'firebase/storage';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})


export class Tab1Page {

  public src: string;
  public id: string;

  @ViewChildren('player')videoPlayers: QueryList<any>;

  userId;
  posts:any=[];
  bookmarkedPost;
  likeArray:any=[];
  userName;
  userImg;
  userData;
  videoUrl;
  imgUrl;
  uploaded;
  uploadPercent: Observable<number>;
  currentPlaying=null;
  play:boolean=true;
  storyPost:any=[];
  userStory:any=[];
  selectedUser:any=[];
  unlockedUser:any=[];
  postArr:any=[];
  imgVidPost:any=[];
  hostId;
@ViewChildren('slidewithnav')slides;
  constructor(private menu: MenuController,public router: Router,private camera: Camera,
    public storage: Storage,public authService: AuthService,private photoViewer: PhotoViewer,
    public modalController: ModalController,public route: ActivatedRoute,
    public actionSheetController: ActionSheetController,public platform:Platform,
    private angularstorage: AngularFireStorage,private file: File, 
    private filePath: FilePath,private socialSharing: SocialSharing) {
      this.route.queryParams.subscribe(params => {
        console.log(params);
          // if(params.addPost){
          //   this.loadData();
          // }
        });
    }

    ngOnInIt(){
      
    }

    viewPhoto(x){
      console.log(x);
      this.photoViewer.show(x);
    }

    ionViewDidEnter() {
 
    this.loadData();
    // var obj={"userId":this.userId};
    // this.authService.story(obj).subscribe((data:any)=>{
    //   console.log(data);
    // });
  }
  ionViewWillLeave(){
    this.slides.stopAutoplay();
    }

  loadData(){
    this.imgVidPost=[];
    this.storage.get('user').then((user)=>{
      this.userData=user;
      this.authService.loading("Loading posts...");
      console.log(user);
      this.userId=user._id;
      this.userName=user.fullName;
      this.userImg=user.user_img
      var obj={"userId":this.userData._id};
      this.authService.getUnlockUser(obj).subscribe((data:any)=>{
        console.log(data);
        this.unlockedUser=data.data;
        this.unlockedUser.forEach(element => {
        this.hostId=element.hostUserId;
        var obj={'userId':this.hostId};
        this.authService.getPost(obj).subscribe((data:any)=>{
        console.log(data);
        this.postArr.push(data.data);
        });
        this.postArr.forEach(element => {
          if(element.posttype=='2'){
            this.storyPost.push(element);
          }
        });
        });
      });
      var obj={'userId':this.userId}
      this.authService.getPost(obj).subscribe((data:any)=>{
        console.log(data);
        this.posts=data.data;
        this.posts.forEach(element => {
          element.newDate1=moment(element.createdDate).startOf('hour').fromNow();
          if(element.posttype=='2' && element.userId==this.userId) {
            this.userStory.push(element);
          }
          else if(element.posttype=='0' || element.posttype=='1') {
            this.imgVidPost.push(element);
          }
        });
      });
      this.authService.dismissLoading();
    });
  }

  gotoUserProfile(x){
    console.log(x);
    if(x.userId==this.userId){
      this.router.navigateByUrl('profile2');
    }else {
      var obj={"userId":x.userId};
      this.authService.getUserProfileById(obj).subscribe((data:any)=>{
        this.selectedUser=data.data;
        console.log(data);
        let navigationExtras:NavigationExtras = {
          queryParams: {
            pageroute: this.router.url,
            user_id:x.userId,
            professionalUser:JSON.stringify(this.selectedUser),
            userType:data.userType
          }
        };
        this.router.navigate(['profile1'],navigationExtras);
      });
    }
  }

  slideOpts = {
    slidesPerView: 4,
    spaceBetween: 5,
    initialSlide:0,
  };
  slideOpts1 = {
    slidesPerView: 1,
    spaceBetween: 5,
    initialSlide:0,
    slideShow:true,
    autoplay:true
  };

  bookmark(item,i){
    this.posts[i].isBookmarked=true;
    console.log(item);
    var obj={'postId':item._id,'postUserId':item.userId,'postUrl':item.postUrl,
    'userId':this.userId,'postTitle':item.postTitle,'postDescription':item.postDescription,
    'posttype':item.posttype,'postCategory':item.postCategory,'isBookmkared':true}
    this.authService.bookmark(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        this.authService.presentToast('Bookmark Added');
      
      }
    });
  }

  deleteBookmark(item,i){
     this.posts[i].isBookmarked=false;
    var obj={'userId':this.userId}
    this.authService.getBookmark(obj).subscribe((data:any)=>{
      this.bookmarkedPost=data.bookmarkArray;
      console.log(this.bookmarkedPost);
      this.bookmarkedPost.forEach(element => {
        if(item._id==element.postId){
          var obj1={'bookmarkId':element._id,'userId':this.userId}
          this.authService.deleteBookmark(obj1).subscribe((res:any)=>{
            console.log(res);
            this.authService.presentToast('Bookmark Removed');
       
          });
        }
      });
    });
  }

 
  likePost(item,i){
      
    this.posts[i].isliked=true;
    this.posts[i].likeCount=item.likeCount+1;
    var likeArray=item.likes;
    var likedPostUser={'likedPostUserid':this.userId,'userName':this.userData.firstName,
    'userImage':this.userImg};
    likeArray.push(likedPostUser);
    console.log(likeArray);
    var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":this.posts[i].likeCount,
    'userName':this.userData.firstName,'hostId':item.userId,'userId':this.userData._id,};
    this.authService.like(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        // this.loadData();
      }
    }); 
  }

  deleteLike(item,j){
   
    this.posts[j].isliked=false;
    this.posts[j].likeCount=item.likeCount-1;
    item.isliked=false;
    var likeArray=item.likes;
    var index=0;
    for(var i=0;i<likeArray.length;i++){
      if(likeArray[i].likedPostUser==this.userId){
        index=i--;
        break;
      }
    }
    likeArray.splice(index,1);
    console.log(likeArray);
    var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":this.posts[j].likeCount};
    this.authService.deleteLike(obj).subscribe((res)=>{
      console.log(res);
      // this.loadData();
  });
  }


  goToLike(item){
    console.log(item);
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url,
        selectedPost: JSON.stringify(item)
      }
    };
    this.router.navigate(['likes'],navigationExtras);
  }


  async openStory(item){
    item.newDate=moment(item.createdDate).startOf('hour').fromNow();
    const modal = await this.modalController.create({
      component: StoryPage,
      cssClass: 'storyModal',
      componentProps:{postData:item}
    });
    return await modal.present();
  }

  async addStory(i) {
    if(this.userStory.length!=0){
      this.userStory[0].newDate=moment(this.userStory[0].createdDate).startOf('hour').fromNow();
      const modal = await this.modalController.create({
        component: StoryPage,
        cssClass: 'storyModal',
        componentProps:{postData:this.userStory}
      });
      return await modal.present();
    }else if(this.userStory.length==0) {
      const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                 this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                  
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                 this.takePicture(this.camera.PictureSourceType.CAMERA,i);
                 
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
      await actionSheet.present();
    }
  }

  async takePicture(sourceType,i) {
    if(this.platform.is('ios')){

      const options: CameraOptions = {
      quality: 100,
      targetWidth: 900,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      encodingType: this.camera.EncodingType.JPEG
      };
      
      const tempImage = await this.camera.getPicture(options);
      const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
      
      // Now, the opposite. Extract the full path, minus filename.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
      const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
      
      // Get the Data directory on the device.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
      const newBaseFilesystemPath = this.file.dataDirectory;
      await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
      newBaseFilesystemPath, tempFilename);
      
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
      const storedPhoto = newBaseFilesystemPath + tempFilename;
      this.file.resolveLocalFilesystemUrl(storedPhoto)
      .then(entry => {
      ( < FileEntry > entry).file(file => this.readFile(file, i))
      })
      .catch(err => {
      console.log(err);
      // this.presentToast('Error while reading file.');
      });
      
      }else{
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          encodingType: this.camera.EncodingType.JPEG,
          
        };
        this.camera.getPicture(options).then((imageData) => {
          this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
            entry.file(file => {
              console.log(file);
              this.readFile(file,i);
            });
          });
        }, (err) => {
          // Handle error
       
        });
      } 
  }


  readFile(file: any,i) {
    const reader = new FileReader();
    reader.onload = () => {
    // const formData = new FormData();
    const imgBlob = new Blob([reader.result], {
    type: file.type
    });
    // formData.append('file', imgBlob, file.name);
    // this.uploadImageData(formData);
    if(i===1){
    this.upload2Firebase(imgBlob);
    }
    else{
    console.log("if second image");
    }
    
    };
    reader.readAsArrayBuffer(file);
    }


    async upload2Firebase(image) {
      this.authService.loading('Loading Image..');
      const file = image;
      const filePath = this.makeid(5);
      const fileRef = this.angularstorage.ref(filePath);
      //const newFile = new File(file);
      // let newFile= file.getURL().getFile();
      
      
      const task = this.angularstorage.upload(filePath, file);
      console.log(filePath);
      console.log(file);
      // observe percentage changes
      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      
      await task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(value => {
 
      this.imgUrl = value;
      this.uploaded = true;
      var obj={'userId':this.userData._id,'posttype':'2',
      'userImg':this.userData.user_img,'postUrl':this.imgUrl,
      'userName':this.userData.fullName}
        this.authService.addNewPost(obj).subscribe((data:any)=>{
        console.log(data);
        if(data.success){
          this.authService.presentToast('Story added Successfully');
          this.authService.dismissLoading();
          this.loadData();
        }
      })      
      }))
      )
      .subscribe()
      
      }

    makeid(length) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
      }

  didScroll($event) {
        if(this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
          return;
        } else if(this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
          // item is out of view, pause it
          this.currentPlaying.pause();
          this.currentPlaying=null;
        }
        this.videoPlayers.forEach(player => {
          console.log(player);
          if(this.currentPlaying) {
            return;
          }
          const nativeElement = player.nativeElement;
          const inView = this.isElementInViewport(nativeElement);
          if(inView){
            this.currentPlaying = nativeElement;
            this.currentPlaying.muted = true;
            this.currentPlaying.play();
            console.log(this.currentPlaying);
            console.log(player);
            // var viewPostUser={'userId':this.userId};
            // var obj={'viewArray':JSON.stringify(viewPostUser),'postId':item._id,
            // 'userName':this.userName,'hostId':item.userId};
            // this.authService.view(obj).subscribe((data:any)=>{
            //   console.log(data);
            //   if(data.success){
            //     this.loadData();
            //   }
            // })      
          }
        })
  }

  openFullScreen(elem) {
    if(elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if(elem.webkitEnterFullscreen) {
      elem.webkitEnterFullscreen();
      elem.enterFullscreen();
    }
  }

  isElementInViewport(el) {
    const rect=el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
  notification() {
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['notification'],navigationExtras);
  }

  searchUser() {
    this.router.navigateByUrl('tabs/tab4');
  }

  doRefresh($event){
    console.log($event);
    this.loadData();
    setTimeout(() => {
      console.log('Async operation has ended');
      $event.target.complete();
    }, 2000);
  }

  async presentModal(item) {
    const modal = await this.modalController.create({
      component: QuestionPage,
      cssClass: 'socialSharingModal',
      componentProps:{postData:item}
    });
    return await modal.present();
  }

  share(item){
    var message= item.postTitle+' \n'+item.postDescription;
    var postUrl= item.postUrl;
    this.socialSharing.share(message,null,null,postUrl);
  }

}
