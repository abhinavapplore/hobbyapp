import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../auth.service'
import { Storage } from '@ionic/storage';
import {AngularFirestore} from "@angular/fire/firestore";

@Component({
  selector: 'app-blocked',
  templateUrl: './blocked.page.html',
  styleUrls: ['./blocked.page.scss'],
})
export class BlockedPage implements OnInit {
  userData;
  allBlockedUser=[];
  constructor(public router:Router,public authService:AuthService,public storage:Storage,
    public fs: AngularFirestore) { }

  ngOnInit() {
    this.storage.get('user').then((user)=>{
      this.userData = user;
    this.loadData();
  });
}

  back(){
    this.router.navigateByUrl('tabs/tab1');
  }

  loadData(){
this.allBlockedUser=[];
    var obj={"userId":this.userData._id};
      this.authService.blockUserList(obj).subscribe((data:any)=>{
        if(data.success){
console.log(data);
this.allBlockedUser=data.data;
        }else{
          console.log(data);
        }
      });

  }

  unblock(item,z){
this.allBlockedUser.splice(z,1);
    var data={'blockedId':item._id}
    this.authService.unblockUser(data).subscribe((data:any)=>{
      if(data.success){
        this.authService.presentToast(item.hostId.fullName+' unblocked.');
        this.fs.collection('friends').doc(this.userData._id).collection('chats').doc(
          item.hostId._id).set({
          isblocked:false
      }, { merge: true });
    
        this.fs.collection('friends').doc(item.hostId._id).collection('chats').doc(
          this.userData._id).set({
            isblocked:false
    }, { merge: true });
      }else{
        this.authService.presentToast('Please try in sometime.')
      }
    })
  }

}
