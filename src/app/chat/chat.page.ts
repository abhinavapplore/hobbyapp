import { Component, OnInit ,ViewChild,} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFirestore} from "@angular/fire/firestore";
import {Storage} from '@ionic/storage';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { AuthService } from '../auth.service';
import { NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})

export class ChatPage implements OnInit {
  @ViewChild('content',{static:false}) content;
  text: string;
  chatRef: any;
  uid: string;
  senderId: any;
  recieverId: any;
  displayName: any;
  userName: any;
  recieverName: any;
  showList:boolean=false;
  isSearch:boolean=false;
  professional:boolean=false;
  professionalUser:any={};
  searchTerm: any;
  search: any;
  chatUserName:boolean=false;
  title:any;
  recieverImg:any;
  userImage;
  pageRoute;
  senderName;
  senderImg;

  constructor(public router: Router,public af: AngularFireAuth, 
    public fs: AngularFirestore,public storage: Storage,public route: ActivatedRoute,public alertController: AlertController,
    public authService: AuthService) {
    this.storage.get('user').then((user) => {
  
this.route.queryParams.subscribe(params => {
  if(params && params.special && params.pageRoute){
    this.chatUserName=false;
    this.title="Get Help";
  }
 else if (params && params.special) {
  var todayDate=moment().format("DD-MM-YYYY");
    var details = JSON.parse(params.special);
    this.chatUserName=true;
    this.recieverId = details.recieverId;
    this.senderId = details.senderId;
    this.recieverName = details.recieverName;
    this.senderName = details.senderName;
    console.log(this.recieverName);
    document.getElementById("demo").innerHTML = this.recieverName.split(" ", 1); ;
    console.log(typeof(this.recieverName));
    this.recieverImg=details.recieverImg,
    this.pageRoute=details.url
       this.uid = user._id;
    console.log("Reciever/sender" + this.recieverId + " " + this.senderId + "" + this.uid);
      // this.chatRef = this.fs.collection('chats', ref => ref.orderBy('Timestamp')).valueChanges();
       // tslint:disable-next-line: max-line-length
    this.chatRef = this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).collection('messagetrail', ref => ref.orderBy('Timestamp')).valueChanges();
console.log(this.chatRef);


      //  this.fs.collection("friends").doc(this.senderId).collection('chats').doc(this.recieverId)
      //  .get().subscribe((doc)=>{
      //    if (doc.exists) {
      //      console.log("Document data:", doc.data());
      //      this.userName=doc.data().recieverName;
      //    } else {
      //      // doc.data() will be undefined in this case
      //      console.log("No such document!");
      //    }
      //  })
    }
    }); 
  });
   }

   async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to block ' + this.recieverName,
     
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            this.blockUser();
          }
        }
      ]
    });

    await alert.present();
  }

   ngOnInit() {
     var obj={"userId":this.senderId}
    this.authService.getUserProfileById(obj).subscribe((data:any)=>{
      console.log(data);
      this.senderImg=data.data.user_Img;
    })
    this.scrollToBottomOnInit();

  }


  blockUser(){
    var obj={'userId':this.senderId,'hostId':this.recieverId};
    this.authService.blockUser(obj).subscribe((data:any)=>{
   if(data.success){
    this.fs.collection('friends').doc(this.senderId).collection('chats').doc(
      this.recieverId).set({
      isblocked:true
  }, { merge: true });

    this.fs.collection('friends').doc(this.recieverId).collection('chats').doc(
      this.senderId).set({
        isblocked:true
}, { merge: true });
   }else{
     this.authService.presentToast("Please try in sometime.")
   }
    });
  }

  // viewProfile() {
  //     let navigationExtras:NavigationExtras = {
  //       queryParams: {
  //         pageroute: this.router.url,
  //         user_id:this.recieverId,
  //         professionalUser:JSON.stringify(this.professionalUser),
  //       }
  //     };
  //     this.router.navigate(['profile1'],navigationExtras);
  // }

  viewProfile() {
   
      let navigationExtras:NavigationExtras = {
        queryParams: {
         
          pageroute: this.router.url,
          hostId:this.recieverId,
          userId:this.senderId
       
        }
      };
      this.router.navigate(['profile1'],navigationExtras);
  }


  // viewProfile(){
  //  var obj={"userId":this.recieverId};
  //  this.authService.getUserProfileById(obj).subscribe((data:any)=>{
  //    if(data.success){
  //      console.log(data);
  //      this.professionalUser=data.data;
  //      let navigationExtras = {
  //       queryParams: {
  //         pageroute: this.router.url,
  //         user_id:this.senderId,
  //         professionalUser:JSON.stringify(this.professionalUser),
  //       }
  //     };
  //     this.router.navigate(['profile1'], navigationExtras);
  //    }
  //  })
    
    
    
  // }
  
  scrollToBottomOnInit() {
    setTimeout(() => {
        if (this.content.scrollToBottom) {
            this.content.scrollToBottom(400);
        }
    }, 500);
}

  send(text){
    if (this.text != ''){
      var formattedTime=moment().format('HH:mm');
      var formattedDate=moment().format("DD-MM-YYYY");
      // this.fs.collection('chats').add({
        
      //   Name: "test user",
      //   Message: text,
      //   UserID: this.uid,
      //   Timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      // });
      // this.text = '';
      console.log("lola");
      console.log(this);
      this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).collection('messagetrail').add({
        //Name: this.hostdisplayname,
        Message: text,
        UserID: this.uid,
        Timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        recieverId: this.recieverId,
        senderId: this.senderId,
        msgTime:formattedTime,
        msgDate:formattedDate,
        msgSent:true,
        msgRead:false
    });
      this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).update({
        lastMsg: this.text,
        msgTime:formattedTime,
        msgDate:formattedDate,
  });
      this.fs.collection('friends').doc(this.recieverId).collection('chats').doc(this.senderId).collection('messagetrail').add({
      Name: this.recieverName,
      Message: text,
      UserID: this.uid,
      Timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      recieverId: this.senderId,
      senderId: this.recieverId,
      msgTime:formattedTime,
      msgDate:formattedDate,
      msgSent:true,
      msgRead:false
  });
      this.fs.collection('friends').doc(this.recieverId).collection('chats').doc(this.senderId).update({
    lastMsg: this.text,
    msgTime:formattedTime,
    msgDate:formattedDate,
});
      this.text = '';
  
    this.scrollToBottomOnInit();
    var obj={'hostId':this.recieverId,"userId":this.senderId,"fullName":this.senderName,
    "recieverImg":this.recieverImg,"senderImg":this.senderImg,"hostName":this.recieverName}
    this.authService.messageNotification(obj).subscribe((data:any)=>{
      if(data.success){
        console.log(data);
      }
    });
  }
}

// goToProfile1() {
//   console.log(x);
//     let navigationExtras:NavigationExtras = {
//       queryParams: {
//         professional:this.professional,
//         pageroute: this.router.url,
//         user_id:x._id,
//         professionalUser:JSON.stringify(x),
//       }
//     };
//     this.router.navigate(['profile1'],navigationExtras);
// }


  back(){
    this.router.navigateByUrl('tabs/tab3');
}

}
