import { Component, OnInit ,Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, AlertController } from '@ionic/angular';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment'
declare var RazorpayCheckout: any;



@Component({
  selector: 'app-unlock-contact',
  templateUrl: './unlock-contact.page.html',
  styleUrls: ['./unlock-contact.page.scss'],
})
export class UnlockContactPage implements OnInit {

  @Input('professionalUser') professionalUser;

  selected:boolean=false;
  selected1:boolean=false;
  selectedAmount=5;
  walletBalance;
  todayDate;
  fullName;
  userId;
  user:any={}



  constructor(public modalController: ModalController,public authService: AuthService,
    public storage: Storage,public route: ActivatedRoute,public alertController: AlertController) { 

  }

  ngOnInit() {
    this.storage.get('user').then((user)=>{
      this.user=user;
      this.userId=user._id;
      this.walletBalance=user.wallet;
      this.fullName=user.fullName;
      console.log(this.professionalUser);
      
    });
  }

  yes(){
      if(this.selectedAmount>this.walletBalance){
        var options = {
          description: 'Add money to wallet',
          image: 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
          currency: 'INR',
          key: 'rzp_test_sbuNGT8VxLZqff',
          amount: this.selectedAmount*100,
          theme: {
            color: '#02CBEE'
          },
          modal: {
            ondismiss: function(){
              this.presentAlert('Transaction Cancelled');
            }
          }
        };
    
        var self=this;
    
        var successCallback = function (payment_id) {
          var obj = {'userId':self.userId,'title':self.professionalUser.fullName,
          'transactionImage':self.professionalUser.user_img,
          'amount':self.selectedAmount,"deviceId":self.professionalUser.deviceId,
          'transactionDate':moment().format('DD/MMM/YYYY'),'isAddingMoney':false,
          'transactionTime':moment().format("h:mm ' A"),'fullName':self.fullName,
          'walletbalance':self.walletBalance,'hostId':self.professionalUser._id,
          "recieverImg":self.professionalUser.user_img,"senderImg":self.user.user_img}
          self.authService.unlockUser(obj).subscribe((data:any) => {
            console.log(data);
            if(data.success){
              
              self.presentAlertSuccess(data.success,'Transaction Completed.'); 
              this.authService.presentToast('User Unlocked.');  
              this.modalController.dismiss(data.success);
                      
            }else {
              this.authService.presentToast('Please try in sometime.');
              this.modalController.dismiss();
            }
          })
          
        };
    
        var cancelCallback = function (error) {
          self.presentAlert('Transaction Cancelled');
        };
    
        RazorpayCheckout.open(options, successCallback, cancelCallback);
        
      }
      else {
         console.log("goes in else");
        var obj = {'userId':this.user._id,'title':this.professionalUser.fullName,
        'transactionImage':this.professionalUser.user_img,
        'amount':this.selectedAmount,"deviceId":this.professionalUser.deviceId,
        'transactionDate':moment().format('DD/MMM/YYYY'),'isAddingMoney':false,
        'transactionTime':moment().format("h:mm ' A"),'fullName':this.user.fullName,
        'walletbalance':this.user.wallet,'hostId':this.professionalUser._id,
        "recieverImg":this.professionalUser.user_img,"senderImg":this.user.user_img}
        this.authService.unlockUser(obj).subscribe((data:any) => {
          console.log(data);
          if(data.success){
            
            this.authService.presentToast('User Unlocked.');  
            this.modalController.dismiss(data.success);           
          }else {
            this.authService.presentToast('Please try in sometime.');
            this.modalController.dismiss();
          }
        })
      }
     
  }

  async presentAlertSuccess(success,msg) {
    const alert = await this.alertController.create({
      cssClass: 'razorpayCancel',
      message: msg,
      buttons: [
         {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            this.modalController.dismiss(success);  
           
          }
        }
      ]
    });

    await alert.present();
  }
  
 
  async presentAlert(msg) {
    const alert = await this.alertController.create({
      cssClass: 'razorpayCancel',
      message: msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Try Again',
          handler: () => {
            console.log('Confirm Okay');
            this.yes();
          }
        }
      ]
    });

    await alert.present();
  }

  // yes(){
  //   this.modalController.dismiss();
  // }
  no(){
    this.modalController.dismiss();
  }

  closeModal(){
    this.modalController.dismiss();
  }

}
