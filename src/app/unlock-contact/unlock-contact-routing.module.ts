import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnlockContactPage } from './unlock-contact.page';

const routes: Routes = [
  {
    path: '',
    component: UnlockContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnlockContactPageRoutingModule {}
