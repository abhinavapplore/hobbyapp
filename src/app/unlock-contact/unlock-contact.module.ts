import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnlockContactPageRoutingModule } from './unlock-contact-routing.module';

import { UnlockContactPage } from './unlock-contact.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnlockContactPageRoutingModule
  ],
  declarations: [UnlockContactPage]
})
export class UnlockContactPageModule {}
