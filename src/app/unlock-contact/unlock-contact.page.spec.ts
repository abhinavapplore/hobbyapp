import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UnlockContactPage } from './unlock-contact.page';

describe('UnlockContactPage', () => {
  let component: UnlockContactPage;
  let fixture: ComponentFixture<UnlockContactPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnlockContactPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UnlockContactPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
