import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject } from  'rxjs';
import { Platform, MenuController, AlertController, ModalController,ActionSheetController, NavController } from '@ionic/angular';
import { Storage } from  '@ionic/storage';
import { User } from  './user';
import { AuthResponse } from  './auth-response';
import { LoadingController ,ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  res:any;
  load: any;
  uploadPercent: Observable<number>;
  uploaded1:boolean=false;
  uploaded2:boolean=false;
  userAvatarStr;
  imgUrl;
  imgUrl1;
  AUTH_SERVER_ADDRESS:  string  =  'http://52.66.209.53';
authSubject  =  new  BehaviorSubject(false);

  constructor(
    public loadingController: LoadingController, public toastController:ToastController,
   public menuCtrl: MenuController,public alertController: AlertController,public modalController:ModalController,
   public platform:Platform, public navCtrl:NavController,
    private angularstorage: AngularFireStorage,private file: File, 
    private filePath: FilePath, public httpClient:HttpClient, 
    public actionSheetController: ActionSheetController, public storage: Storage,
    private camera: Camera) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }
 
  ifLoggedIn() {
    this.storage.get('user').then((response) => {
      if (response) {
        this.authSubject.next(true);
      }
    });
   }
   
  updateUserImage(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/updateUserImage`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  addMoney(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/addMoney`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
         
      })

    );
  }

  addNewPost(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/addPost`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
         
      })

    );
  }

  getPost(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getPost`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  getAllPost(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getAllPost`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  updatePost(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/updatePost`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  deletePost(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/deletePost`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  story(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/story`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  bookmark(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/bookmark`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
       
      })

    );
  }

  deleteBookmark(obj1): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/deleteBookmark`, obj1).pipe(
      tap(async (res:  AuthResponse ) => {
     
      })

    );
  }

  getBookmark(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getBookmark`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  like(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/like`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
       
      })

    );
  }

  deleteLike(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/deleteLike`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
       
      })

    );
  }

  view(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/views`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
       
      })

    );
  }

  getAllUser(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getAllUser`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  filterByDistance(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/filterByDistance`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
       
      })

    );
  }

  transaction(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/transaction`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })

    );
  }

  unlockUser(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/unlockUser`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })
    );
  }

  getUnlockUser(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getUnlockUser`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
          
      })
    );
  }

  updateProfile(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/updateProfile`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
         
      })

    );
  }

  updateAddress(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/updateAddress`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );
  }

  getUserProfile(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getUserProfile`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
         
      })

    );
  }

  getUserProfileById(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getUserProfileById`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
         
      })

    );
  }

  

  getTransaction(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getTransaction`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );
  }

  messageNotification(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/messageNotification`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );
  }

  getNotification(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getNotification`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );

    
  }
  

  blockUser(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/addBlockedUser`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );

    
  }

  blockUserList(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/getBlockedUser`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );

    
  }

  unblockUser(obj): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/deleteBlockedUser`, obj).pipe(
      tap(async (res:  AuthResponse ) => {
        
      })

    );

    
  }
  
   async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  register(user: User): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/api/signupUser`, user).pipe(
      tap(async (res:  AuthResponse ) => {
        if (res.user) {
          await this.storage.set('user',res.user);
          await this.storage.set("ACCESS_TOKEN", res.token);
          await this.storage.set("user_id", res.user.id);
        console.log(res.token);
        console.log(res.status);
        console.log(res.user);
          this.authSubject.next(true);
        }
      })

    );
  }

  

  login(user: User): Observable<AuthResponse> {
    return this.httpClient.post(`${this.AUTH_SERVER_ADDRESS}/api/loginUser`, user, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    }).pipe(
      tap(async (res: AuthResponse) => {
          if (res.success == true) {
           await this.storage.set('user', res.user);   
          this.authSubject.next(true);
        }
      })
    );
  }

  async logout() {

    await this.storage.remove("user");
    this.authSubject.next(false);
    this.navCtrl.navigateRoot('login');
  }

  isLoggedIn() {
    this.storage.get('user').then((user) => {
   if(user){
     this.authSubject.next(true);
   }
  });
  }
  isAuthenticated() {
    return this.authSubject.value;
  }


  async loading(msg) {
    this.load = true;
    return await this.loadingController.create({
      message: msg,
      spinner: 'dots',
      // duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.load) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismissLoading() {
    this.load = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  toggleMenu(){
    this.menuCtrl.toggle();
  }

  async presentAlertSuccess(msg) {
    const alert = await this.alertController.create({
      cssClass: 'razorpayCancel',
      message: msg,
      buttons: [
         {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            return true;
          }
        }
      ]
    });

    await alert.present();
  }
  
 
  async presentAlert(msg) {
    const alert = await this.alertController.create({
      cssClass: 'razorpayCancel',
      message: msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  

  
  

  async presentActionSheet1(i) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
              text: 'Load from Library',
              handler: () => {
                const imgUrl = this.takePicture2(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                return imgUrl;
              }
          },
          {
              text: 'Use Camera',
              handler: () => {
                const imgUrl = this.takePicture2(this.camera.PictureSourceType.CAMERA,i);
                return imgUrl;
              }
          },
          {
              text: 'Cancel',
              role: 'cancel'
          }
      ]
  });
    await actionSheet.present();
  }

  async takePicture2(sourceType,i) {
    if(this.platform.is('ios')){

      const options: CameraOptions = {
      quality: 100,
      targetWidth: 900,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      encodingType: this.camera.EncodingType.JPEG
      };
      
      const tempImage = await this.camera.getPicture(options);
      const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
      
      // Now, the opposite. Extract the full path, minus filename.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
      const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
      
      // Get the Data directory on the device.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
      const newBaseFilesystemPath = this.file.dataDirectory;
      await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
      newBaseFilesystemPath, tempFilename);
      
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
      const storedPhoto = newBaseFilesystemPath + tempFilename;
      this.file.resolveLocalFilesystemUrl(storedPhoto)
      .then(entry => {
      ( < FileEntry > entry).file(file => this.readFile1(file, i))
      })
      .catch(err => {
      console.log(err);
      // this.presentToast('Error while reading file.');
      });
      
      }else{
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          encodingType: this.camera.EncodingType.JPEG,
          
        };
        this.camera.getPicture(options).then((imageData) => {
          this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
            entry.file(file => {
              console.log(file);
            const imgUrl =  this.readFile1(file,i);
            return imgUrl;
            });
          });
        }, (err) => {
          // Handle error
        });
      }
      
   
  }

 async readFile1(file: any,i) {
    const reader = new FileReader();
    reader.onload = () => {
       // const formData = new FormData();
        const imgBlob = new Blob([reader.result], {
            type: file.type
        });
      //  formData.append('file', imgBlob, file.name);
       // this.uploadImageData(formData);
       if(i===1){
      const imgUrl =  this.upload2Firebase2(imgBlob);
       return imgUrl;
       }
       else{
        console.log("if second image");
       }
     
    };
    reader.readAsArrayBuffer(file);
  }

  async upload2Firebase2(image) {
  
    const file = image;
    const filePath = this.makeid(5);
    const fileRef = this.angularstorage.ref(filePath);
    //const newFile = new File(file);
   // let newFile= file.getURL().getFile();
  
  
    const task = this.angularstorage.upload(filePath, file);
    console.log(filePath);
    console.log(file);
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
  
    await task.snapshotChanges().pipe(
        finalize(() =>  fileRef.getDownloadURL().subscribe(value => {
          this.uploaded1 = true;
          this.imgUrl =  value;
          console.log(this.imgUrl)
       
       return this.imgUrl
        }))
     )
    .subscribe()
   
  }

  

  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }
  
  makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

}
