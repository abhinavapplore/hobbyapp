import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-modalpage',
  templateUrl: './modalpage.page.html',
  styleUrls: ['./modalpage.page.scss'],
})
export class ModalpagePage implements OnInit {

  isProfessional;
  userHobby:any=[];
  hobbyLength;
  hobbySelection;

  constructor(public router: Router,public storage: Storage,
    public modalController: ModalController) { }

  ngOnInit() {
    this.storage.get("userHobby").then((res)=>{
      console.log(res);
      this.userHobby=res;
      // this.hobbyLength=this.userHobby.length;
    });
  }

  next(x,y){
    this.isProfessional=x;
    console.log(this.isProfessional);
    if(this.isProfessional=='yes'){
      this.storage.set('userType','professional').then((res)=>{
        console.log(res);
        if(res!=null){
          this.modalController.dismiss();
          this.router.navigateByUrl('skills');
        }
      })
    }else {
      this.storage.set('userType','passionist').then((res)=>{
        console.log(res);
        if(res!=null){
          this.modalController.dismiss();
          let navigationExtras:NavigationExtras = {
            queryParams: {
              special: this.router.url,
            }
          };
          this.router.navigate(['profile'],navigationExtras);
        }
      })
    }
     
  }


  // closeModal(){
  //   this.hobbySelection=false;
  //   this.modalController.dismiss(this.hobbySelection);
  // }


}
