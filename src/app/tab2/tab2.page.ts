import { Component, OnInit,ViewChildren,QueryList } from '@angular/core';
import { MenuController ,ModalController, NavController} from '@ionic/angular';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import {QuestionPage} from '../question/question.page';
import { Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChildren('player')videoPlayers: QueryList<any>;
  selected="myPost";
  userId;
  bookmark:any=[];
  posts:any=[];
  postId;
  bookmarkPostId;
  postUserId;
  savedPost;
  selectedTab;
  imgVidPosts:any=[];
  currentPlaying=null;
  user:any={};
  userPost:any=[];

  constructor(private menu: MenuController,public authService: AuthService,
     public navCtrl:NavController,public storage: Storage,public router: Router,
     private socialSharing: SocialSharing,public modalController: ModalController,) {}
  
  ionViewDidEnter() {
    this.imgVidPosts=[];
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user
      this.userId=user._id;
     this.loadData();
   
  });
  }

  loadData(){
    this.bookmark=[];
    this.imgVidPosts=[];
    this.authService.loading("Loading your posts.");
    var obj={'userId':this.userId};
    this.authService.getPost(obj).subscribe((res:any)=>{
      console.log(res);
      this.authService.getBookmark(obj).subscribe((data:any)=>{
        console.log(data);
        this.bookmark=data.bookmarkArray;
        console.log(this.bookmark);
        this.posts=data.postArray;
        this.posts.forEach(element => {
          if(element.posttype=='0' || element.posttype=='1') {
            this.imgVidPosts.push(element);
          }
        });
    }); 
    });
    this.authService.dismissLoading();
  }

  deleteBookmark(item){
    this.imgVidPosts=[];
      this.bookmark.forEach(element => {
        if(item.postId==element.postId){
          var obj1={'bookmarkId':element._id,'userId':this.userId}
          this.authService.deleteBookmark(obj1).subscribe((res:any)=>{
            console.log(res);
            this.authService.presentToast('Bookmark Removed');
            this.loadData();
          });
        }
      });
  }

  deletePost(item){
        this.imgVidPosts=[];
        var obj={'postId':item._id,'userId':this.userId}
        this.authService.deletePost(obj).subscribe((res:any)=>{
          console.log(res);
          this.authService.presentToast('Post Removed');
          this.loadData();
        });
}


likePost(item,i){
  // this.posts[i].isliked=true;
  // this.posts[i].likeCount=this.posts[i].likeCount+1;
  //   var likedPostUser={'likedPostUserid':this.userId,'userName':this.user.firstName,
  //   'userImage':this.user.user_img};
  //   var obj={'likeArray':likedPostUser,'postId':item._id,"likeCount":item.likeCount,
  //   'userName':this.user.firstName,'hostId':item.userId,'userId':this.user._id,};
  //   this.authService.like(obj).subscribe((data:any)=>{
  //     console.log(data);
  //     if(data.success){
  //       this.loadData();
  //     }
  //   });
    this.posts[i].isliked=true;
    this.posts[i].likeCount=item.likeCount+1;
    var likeArray=item.likes;
    var likedPostUser={'likedPostUserid':this.userId,'userName':this.user.firstName,
    'userImage':this.user.user_img};
    likeArray.push(likedPostUser);
    console.log(likeArray);
    var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":this.posts[i].likeCount,
    'userName':this.user.firstName,'hostId':item.userId,'userId':this.user._id};
    this.authService.like(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        this.loadData();
      }
    });      
}

deleteLike(item,j){
  // this.posts[j].isliked=false;
  // this.posts[j].likeCount=this.posts[j].likeCount-1;
  // item.isliked=false;
  // var likeArray=item.likes;
  // var index=0;
  // for(var i=0;i<likeArray.length;i++){
  //   if(likeArray[i].likedPostUser==this.userId){
  //     index=i--;
  //     break;
  //   }
  // }
  // likeArray.splice(index,1);
  // console.log(likeArray);
  // var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":item.likeCount};
  // this.authService.deleteLike(obj).subscribe((res)=>{
  //   console.log(res);
  //   this.loadData();
  // });
    this.posts[j].isliked=false;
    this.posts[j].likeCount=item.likeCount-1;
    item.isliked=false;
    var likeArray=item.likes;
    var index=0;
    for(var i=0;i<likeArray.length;i++){
      if(likeArray[i].likedPostUser==this.userId){
        index=i--;
        break;
      }
    }
    likeArray.splice(index,1);
    console.log(likeArray);
    var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id,"likeCount":this.posts[j].likeCount};
    this.authService.deleteLike(obj).subscribe((res)=>{
      console.log(res);
      this.loadData();
    });
}

goToLike(item){
  console.log(item);
  let navigationExtras:NavigationExtras = {
    queryParams: {
      special: this.router.url,
      selectedPost: JSON.stringify(item)
    }
  };
  this.router.navigate(['likes'],navigationExtras);
}


  segmentChanged($event){
    console.log($event);
    this.selectedTab=$event.detail.value;
    if(this.selectedTab=="myPost"){
      this.savedPost=false;
    }else {
      this.savedPost=true;
    }
  }

  didScroll($event) {
    if(this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
      return;
    } else if(this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
      // item is out of view, pause it
      this.currentPlaying.pause();
      this.currentPlaying=null;
    }
    this.videoPlayers.forEach(player => {
      console.log(player);
      if(this.currentPlaying) {
        return;
      }
      const nativeElement = player.nativeElement;
      const inView = this.isElementInViewport(nativeElement);
      if(inView){
        this.currentPlaying = nativeElement;
        this.currentPlaying.muted = true;
        this.currentPlaying.play();
        
      }
    })
}

openFullScreen(elem) {
if(elem.requestFullscreen) {
  elem.requestFullscreen();
} else if(elem.webkitEnterFullscreen) {
  elem.webkitEnterFullscreen();
  elem.enterFullscreen();
}
}

isElementInViewport(el) {
const rect=el.getBoundingClientRect();
return (
  rect.top >= 0 &&
  rect.left >= 0 &&
  rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
  rect.right <= (window.innerWidth || document.documentElement.clientWidth)
);
}

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  async presentModal(item) {
    const modal = await this.modalController.create({
      component: QuestionPage,
      cssClass: 'socialSharingModal',
      componentProps:{postData:item}
    });
    return await modal.present();
  }

  editPost(item){
    let navigationExtras:NavigationExtras = {
      queryParams: {
      
        pageRoute: this.router.url,
        post:JSON.stringify(item),
        editPost:true,
       
      }
    };
    this.navCtrl.navigateRoot(['post'],navigationExtras);
  }

  share(item){
    var message= item.postTitle+' \n'+item.postDescription;
    var postUrl= item.postUrl;
    this.socialSharing.share(message,null,null,postUrl);
  }


}
