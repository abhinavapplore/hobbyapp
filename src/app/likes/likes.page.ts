import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-likes',
  templateUrl: './likes.page.html',
  styleUrls: ['./likes.page.scss'],
})
export class LikesPage implements OnInit {

  pageroute;
  selectedPost:any={};
  likesArray:any=[];
  professionalUser:any={};

  constructor(public router: Router,public route: ActivatedRoute,public authService: AuthService) { 
    this.route.queryParams.subscribe(params => {
      console.log(params);
        this.selectedPost=JSON.parse(params.selectedPost);
        console.log(this.selectedPost);
        this.likesArray=this.selectedPost.likes;
        console.log(this.likesArray);
        this.pageroute=params.specical;
      });
  }

  ngOnInit() {

  }

  goToProfile1(item){
    var obj={"userId":item.likedPostUserid};
    this.authService.getUserProfileById(obj).subscribe((data:any)=>{
      console.log(data);
      this.professionalUser=data.data;
      let navigationExtras:NavigationExtras = {
        queryParams: {
          pageroute: this.router.url,
          user_id:item.likedPostUserid,
          professionalUser: JSON.stringify(this.professionalUser)
        }
      };
      this.router.navigate(['profile1'],navigationExtras);
    });
  }

  back(){
    this.router.navigateByUrl(this.pageroute);
  }

}
