import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';


import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';
import { Platform, AlertController, NavController, LoadingController,ToastController} from '@ionic/angular';
import { FCM } from '@ionic-native/fcm/ngx';
import { SmsRetriever } from '@ionic-native/sms-retriever/ngx';
declare var SMSReceive: any;

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {
  verificationId;
  sendAgainBool:boolean=false;
  phone;
  backButton;
  deviceId;
  otp;
  userDetails;
  selection1: any;
  selection2: any;
  selection3: any;
  selection4: any;
  values: any = [];
    pass1: any;
  pass2: any;
  pass3: any;
  pass4: any;
  pass5: any;
  pass6: any;
  @ViewChild('passcode1', {static: false}) passcode1;
  @ViewChild('passcode2', {static: false}) passcode2;
  @ViewChild('passcode3', {static: false}) passcode3;
  @ViewChild('passcode4', {static: false}) passcode4;
  @ViewChild('passcode5', {static: false}) passcode5;
  @ViewChild('passcode6', {static: false}) passcode6;
  
  selection: any;
  loginForm: boolean;
  systemPin: any;
  one: string;
  two: string;
  three: string;
  four: string;
  selection5: any;
  selection6: any;
  five: string;
  six: string;
  isotp: boolean=false;
  mobno: any;
  user_type;
  OTP: string = '';
  showOTPInput: boolean = false;
  constructor(public router: Router, public route: ActivatedRoute, public alertController: AlertController,
     public http: HttpClient, public httpClient: HttpClient, public storage: Storage,
     public fauth: FirebaseAuthentication,private smsRetriever: SmsRetriever, public platform:Platform,
     public loadingController: LoadingController,private toastCtrl: ToastController,
     private  authService:  AuthService,private fcm: FCM,public navCtrl: NavController) {
      this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        var details = JSON.parse(params.special); 
      
          SMSReceive.startWatch(
            () => {
              document.addEventListener('onSMSArrive', (e: any) => {
                var IncomingSMS = e.data;
                this.processSMS(IncomingSMS);
              });
            },
            () => { console.log('watch start failed') }
          )
        
       this.userDetails = details;
       this.mobno=details.mobile;
        var phoneString = details.mobile.toString();
        this.phone = "+91" + phoneString;
        console.log(this.phone);
        console.log(typeof(this.phone));
      const phoneNumberString = this.phone;

          console.log("testing" + phoneNumberString);
          this.fauth.verifyPhoneNumber(phoneNumberString, 60).then(res => {
            //console.log("Hello Hello" + res);
            this.verificationId = res;
          });
          this.fauth.onAuthStateChanged().subscribe(response => {
            console.log(response);
          
          });
      }
   
    });
  }

  ngOnInit() {
    this.fcm.getToken().then(token => {
      console.log(token);
      this.deviceId=token;
     
     });
  }

  
  // start() {
  //   SMSReceive.startWatch(
  //     () => {
  //       console.log('watch started');
  //       document.addEventListener('onSMSArrive', (e: any) => {
  //         console.log('onSMSArrive()');
  //         var IncomingSMS = e.data;
  //         console.log(JSON.stringify(IncomingSMS));
  //       });
  //     },
  //     () => { console.log('watch start failed') }
  //   )
  // }

  // SMSReceive.stopWatch(
  //   () => { console.log('watch stopped') },
  //   () => { console.log('watch stop failed') }
  //   )

    async presentToast(message, show_button, position, duration) {
      const toast = await this.toastCtrl.create({
        message: message,
        position: position,
        duration: duration,
      });
      toast.present();
    }

    next() {
      this.showOTPInput = true;
      
    }

    
  
    stop() {
      SMSReceive.stopWatch(
        () => { console.log('watch stopped') },
        () => { console.log('watch stop failed') }
      )
    }
  
    processSMS(data) {
      // Check SMS for a specific string sequence to identify it is you SMS
      // Design your SMS in a way so you can identify the OTP quickly i.e. first 6 letters
      // In this case, I am keeping the first 6 letters as OTP
      const message = data.body;
      if (message && message.indexOf('is your verification code') != -1) {
        this.pass1 = data.body.slice(0, 1);
        this.pass2 = data.body.slice(1, 2);
        this.pass3 = data.body.slice(2, 3);
        this.pass4 = data.body.slice(3, 4);
        this.pass5= data.body.slice(4, 5);
        this.pass6 = data.body.slice(5, 6);
        
        this.verify(this.pass1,this.pass2,this.pass3,this.pass4,this.pass5,this.pass6);
      }
    }
  
    // register() {
    //   if (this.OTP != '') {
    //     this.presentToast('You are successfully registered', false, 'top', 1500);
    //   } else {
    //     this.presentToast('Your OTP is not valid', false, 'bottom', 1500);
    //   }
    // }


  ionViewDidLeave(){
   // alert("this");
    document.getElementById("timer").innerHTML="";

  }

  async checkVersion(concat) {
    const ready = !!await this.platform.ready();
    if (ready) {
        // try to add your code here

        this.fauth.signInWithVerificationId(this.verificationId, concat).then((res) => {

          if (res == "OK"){
         
        const emailId = this.makeid(5);
      
         var obj={contactNum:this.mobno,emailId:emailId, deviceId:this.deviceId,
          emailIdRegistered:false};
      this.authService.login(obj).subscribe((res) => {
        if(res.success == false){
          this.storage.set("contactNum",this.mobno).then((res)=>{
                      console.log(res);
                      if(res!=null){
                        this.stop();
                        this.navCtrl.navigateRoot('hobby');
                      }      
                    });
        }else{
          this.storage.set("ACCESS_TOKEN", res.token);
          this.storage.set("user", res.user);
          console.log(res.user);
          this.stop();
          this.navCtrl.navigateRoot('tabs/tab4');
        }
      });
    }
    
        }).catch(function(error){
          var self=this;
          console.log(error);
          if(error=="This credential is already associated with a different user account."){
            self.authService.presentToast("This credential is already associated with a different user account.");
            const emailId = self.makeid(5);
      
            var obj={contactNum:self.mobno,emailId:emailId, deviceId:self.deviceId,
             emailIdRegistered:false};
             self.authService.login(obj).subscribe((res) => {
           if(res.success == false){
            self.storage.set("contactNum",self.mobno).then((res)=>{
                         console.log(res);
                         if(res!=null){
                          self.stop();
                          self.navCtrl.navigateRoot('hobby');
                         }      
                       });
                      }
                    })
        
          }else{
            console.log("incorrect pin");
            // self.presentAlert1();
            // self.authService.presentToast("This credential is already associated with a different user account.");
            // self.navCtrl.navigateRoot('login');
            self.authService.presentToast("This credential is already associated with a different user account.");
            const emailId = self.makeid(5);
      
            var obj={contactNum:self.mobno,emailId:emailId, deviceId:self.deviceId,
             emailIdRegistered:false};
             self.authService.login(obj).subscribe((res) => {
           if(res.success == false){
            self.storage.set("contactNum",self.mobno).then((res)=>{
                         console.log(res);
                         if(res!=null){
                          self.stop();
                          self.navCtrl.navigateRoot('hobby');
                         }      
                       });
                      }
                    })
          }
       
        });
    }
}

  verify(pass1,pass2,pass3,pass4,pass5,pass6){
  
   var self = this;
   var sPass1 = pass1.toString();
   var sPass2 = pass2.toString();
   var sPass3 = pass3.toString();
   var sPass4 = pass4.toString();
   var sPass5 = pass5.toString();
   var sPass6 = pass6.toString();
   var concat = sPass1 + sPass2 + sPass3 + sPass4 + sPass5 + sPass6;
    // this.checkVersion(concat);
    this.myFakeFuntion();
    
  }


  

  getAppHash(){
    this.smsRetriever.getAppHash()
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
  }

  retriveSMS(){
    this.smsRetriever.startWatching()
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
  }

  async presentAlert1() {
    const alert = await this.alertController.create({
      header: 'Incorrect Otp',
      subHeader: 'Please enter correct otp to login.',
      
      buttons: ['OK'] 
    });
  
    await alert.present();
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Use Another Number',
      subHeader: 'Looks like this number is already registered.',
      
      buttons: ['OK'] 
    });
  
    await alert.present();
    
  }
 

  async presentAlert2() {
    const alert = await this.alertController.create({
      header: 'Server Error',
      subHeader: 'Please try after sometime.',
      
      buttons: ['OK'] 
    });
  
    await alert.present();
    
  }


 registerUser(form){
    this.presentLoading()
   
    
    this.authService.register(form.value).subscribe((res) => {
      if (res.success == false){
        alert("Invalid Parameters");
      }else{
        this.router.navigateByUrl('tabs/tab4');

      }
     
    });
   
  }

  resendsms()
  {
   
    var timeLeft = 30;
    var elem = document.getElementById('some_div');
    
    var timerId = setInterval(countdown, 1000);
    
    function countdown() {
      if (timeLeft == -1) {
        clearTimeout(timerId);
        // doSomething();
        this.sendAgainBool=true;
      } else {
        setTimeout(()=>{
          elem.innerHTML = timeLeft + ' seconds remaining';
        },2000)
        
        timeLeft--;
      }
    }

    this.sendAgainBool=false;
    this.fauth.verifyPhoneNumber(this.phone, 60).then(res => {
      //console.log("Hello Hello" + res);
      this.verificationId = res;
    });
    // firebase.auth().signInWithPhoneNumber(this.phone, this.recaptchaVerifier)
    //   .then( confirmationResult => {
    //     console.log(confirmationResult);
    //     this.confirmResult = confirmationResult;
        
       
        
    //   })
    // .catch(function (error) {
    //   console.error("SMS not sent", error);
    // });
  }

  back(){
    this.router.navigateByUrl('signup');
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Processing',
      duration: 5000
    });
    await loading.present();

    
  }

  onKeyUp(event, index){  
    console.log(event);
    if (event.target.value.length != 1){
      this.setFocus(index - 2);  
    }else{
      this.values.push(event.target.value);  
      this.setFocus(index);   
    }
    if(index===6){
      // var sPass1 = this.pass1.toString();
      // var sPass2 = this.pass2.toString();
      // var sPass3 = this.pass3.toString();
      // var sPass4 = this.pass4.toString();
      // var sPass5 = this.pass5.toString();
      // var sPass6 = this.pass6.toString();
      this.isotp=true;
      // this.verify(sPass1,sPass2,sPass3,sPass4,sPass5,sPass6);
    }
    event.stopPropagation();
  }

  onKeySixSignup(){
    var sPass1 = this.pass1.toString();
    var sPass2 = this.pass2.toString();
    var sPass3 = this.pass3.toString();
    var sPass4 = this.pass4.toString();
    var sPass5 = this.pass5.toString();
    var sPass6 = this.pass6.toString();
    this.isotp=true;
    this.verify(sPass1,sPass2,sPass3,sPass4,sPass5,sPass6);
  }

  submit(e: Event){
   
    this.values = [];
    this.passcode1.value = '';
    this.passcode2.value = '';
    this.passcode3.value = '';
    this.passcode4.value = '';
    this.passcode5.value = '';
    this.passcode6.value = '';
    
    e.stopPropagation();
    
}

setFocus(index){
       
  switch (index){
    case 0:
    this.passcode1.setFocus();
    break;
    case 1:
    this.passcode2.setFocus();
    break;
    case 2:
    this.passcode3.setFocus();
    break;
    case 3:
    this.passcode4.setFocus();
    break;
    case 4:
    this.passcode5.setFocus();
    break;
    case 5:
    this.passcode6.setFocus();
    break;
   
    }
}
 
  numberOnlyValidation(event: any) {
    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  myFakeFuntion(){
    console.log("hey there");
    const emailId = this.makeid(5);
      
         var obj={contactNum:this.mobno,emailId:emailId, deviceId:this.deviceId,
          emailIdRegistered:false};
      this.authService.login(obj).subscribe((res) => {
        if(res.success == false){
          this.storage.set("contactNum",this.mobno).then((res)=>{
                      console.log(res);
                      if(res!=null){
                        this.stop();
                        this.navCtrl.navigateRoot('hobby');
                      }      
                    });
  }else{
    this.storage.set("ACCESS_TOKEN", res.token);
    this.storage.set("user", res.user);
    console.log(res.user);
    this.stop();
    this.navCtrl.navigateRoot('tabs/tab4');
  }
});
  }
 

}





  
  


