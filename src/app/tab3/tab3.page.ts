import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFirestore} from "@angular/fire/firestore";
import * as firebase from 'firebase';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  uid;
  allUsers:any;
  openchats:any=[];
  searchTerm;
  emptydata:boolean;
  showList:boolean=false;
  isSearch:boolean=false;
  search:any=[];
  senderName;

  constructor(public router: Router,private menu: MenuController,
    public authService: AuthService,public storage: Storage,public af: AngularFireAuth, 
    public fs: AngularFirestore,) {}

  ngOnInIt(){
      
  }



  ionViewDidEnter(){
    this.storage.get('user').then((user)=>{
      this.uid=user._id;
      this.getChats();
      this.senderName=user.senderName;
    })
   
  }

  getChats(){

    this.authService.loading("Loading chats...");
    this.allUsers = this.fs.collection('friends').doc(this.uid).collection('chats', ref => ref.orderBy('Timestamp')).snapshotChanges();
   
    // This works:
    console.log(this.allUsers);
   var todayDate=moment().format("DD-MM-YYYY");
    if(this.allUsers.length!=0){
      this.allUsers.forEach( user => {
        if(user.length==0){
          this.emptydata =true;
        
        }else{
          this.openchats=[];
          user.forEach( userData =>{
            
            let data = userData.payload.doc.data();
            let id = userData.payload.doc.id;
            let time = moment(userData.payload.doc.Timestamp).format("h:mm A'");
            console.log(typeof(userData.payload.doc.Timestamp));
            data.formatTime=time;
            this.openchats.push(data);
            console.log("this is openchat array");
            console.log(this.openchats);
            console.log( "ID: ", id, " Data: " , data );
            if(this.openchats.length != 0){
              this.emptydata =false;
              
              for(var i=0;i<this.openchats.length;i++){
                // var msgDate=moment(this.openchats[i].msgDate);
                if(this.openchats[i].msgDate!=todayDate){
                  this.openchats[i].showTime=false;
                }else{
                 
                  this.openchats[i].showTime=true;
                  // this.openchats[element].msgTime=this.openchats[element].msgDate;
              
                }
            }
          
            
            }else{
              
              this.emptydata =true;
            }
           
            });
           
        }
     
    });
   
    }else{
      this.authService.dismissLoading();
  
      this.emptydata =true;
    }
    this.authService.dismissLoading();
  }

  gotochat(item){
  
    let recId=item.recieverId;
    
    let vc={recieverId:recId,
    senderId:this.uid,
    recieverName:item.recieverName,
    recieverImg:item.recieverImage,
    senderName:this.senderName};
    let navigationExtras = {
   
    queryParams: {
      special: JSON.stringify(vc),
      
    }
    
  };
    this.router.navigate(['chat'], navigationExtras);
     
      }


      getUser(ev: any){
        var arr = [];
        
          for(let i =0 ; i<this.openchats.length;i++){
           
            var input = this.openchats[i];
            arr.push(input);
         
            let val = ev.target.value;
          
             if (val && val.trim() != '') {
               
              
               this.search = arr.filter(function(item) {
           return item.recieverName.toLowerCase().indexOf(val.toLowerCase()) > -1;
         });
          
            // Show the results
      if(this.search.length!==0){
        this.showList = true;
       }else{
        this.showList = false;
       }
          
            } else {
              
              
              this.showList = false;
            }
         
          }
       
         
      }
      selectChat(item)
      {  
        
        this.showList = false;
        this.searchTerm = "";
        this.isSearch=false;
      console.log(item.evn_id);
      console.log(item.evn_name);
      console.log(item);
      this.gotochat(item);
        
      
      
        
      }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
  notification() {
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['notification'],navigationExtras);
  }

}
