import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import { NavigationExtras } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import {ActionSheetController, Platform, ModalController} from '@ionic/angular';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from  '@angular/common/http';

const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {

  pageRoute;
  imgUrl;
  postDescription;
  postTitle;
  userId;
  category= "";
  postType;
  cat:boolean=false;
  post:any={};
  userData:any={};
  editPost:boolean=false;
  postData:boolean=false;
  uploaded:boolean=false;
  uploaded1:boolean=false;
  imgUrl1;
  newImgUrl;
  addPost:boolean=false;
  uploadedVideo: string;
  selectedVideo:any= "";
  uploadPercent: Observable<number>;

  cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
   }

   gelleryOptions: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    }

  constructor(public platform:Platform,public route: ActivatedRoute,
    public modalController: ModalController,public authService: AuthService,
   private angularstorage: AngularFireStorage,private file: File, 
   private filePath: FilePath, public httpClient:HttpClient, 
   public actionSheetController: ActionSheetController, public storage: Storage,
   private camera: Camera,public router: Router) { 
    this.route.queryParams.subscribe(params => {
      this.postType=params.postType;
      this.category= "";
      console.log(params);
        if(params.editPost && params.editPost!=undefined){
          this.editPost=true;
          this.addPost=false;     
          this.pageRoute=params.pageRoute;
          this.post=JSON.parse(params.post);
          this.imgUrl=this.post.postUrl;
        }else if(params.addPost && params.postDetail){
          this.editPost=false;              
          this.pageRoute=params.pageRoute;
          this.post=JSON.parse(params.postDetail);
          this.imgUrl=this.post.postUrl;
          this.category=this.post.postCategory;
        }else if(params.addPost && params.addPost!=undefined){
          this.addPost=true;   
          this.editPost=false;  
          this.pageRoute=params.pageRoute;
          this.imgUrl=params.imgUrl;
        }
       
  });
  }

  ngOnInit() {
    this.uploaded1=false;
    this.storage.get('user').then((user)=>{
      this.userId=user._id;
      this.userData=user;     
    })
  }

  saveData(x){
    if(x.length==0){
      console.log('Do Nothing');
    }else {
      if(x=='title'){
        this.post.postTitle=this.postTitle;
        this.postData=true;
    }else if(x=='description'){
      this.post.postDescription=this.postDescription;
      this.postData=true;
    }
  }
}

  addNewPost(){
    var obj={'postUrl':this.imgUrl,'postDescription':this.post.postDescription,
    'userId':this.userData._id,'posttype':this.postType,'postTitle':this.post.postTitle,
    'postCategory':this.category,'userImg':this.userData.user_img,
    'userName':this.userData.fullName}
      this.authService.addNewPost(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        this.authService.presentToast('Post added Successfully');
        let navigationExtras:NavigationExtras = {
          queryParams: {
            addPost: true,
          }
        };
        this.router.navigate([this.pageRoute],navigationExtras);
      }
    })
  }

  updatePost(a,b,c,d,e){
      console.log(a);
      console.log(b);
      console.log(c);
      console.log(d);
      if(this.uploaded1){
        this.post.postUrl=e;
        this.post.postCategory=b;
        this.post.postTitle=c;
        this.post.postDescription=d;
        this.post.userId=this.userId;
        var obj={'post':this.post}
      this.authService.updatePost(obj).subscribe((data:any)=>{
        console.log(data);
        if(data.success){
          this.router.navigateByUrl(this.pageRoute);
          this.authService.presentToast('Post Updated.');
        }else {
          this.authService.presentToast('Something went wrong.');
        }
      });
      }else {
        this.post.postUrl=a;
        this.post.postCategory=b;
        this.post.postTitle=c;
        this.post.postDescription=d;
        this.post.userId=this.userId;
        var obj={'post':this.post}
        this.authService.updatePost(obj).subscribe((data:any)=>{
          console.log(data);
          if(data.success){
            this.router.navigateByUrl(this.pageRoute);
            this.authService.presentToast('Post Updated.');
          }else {
            this.authService.presentToast('Something went wrong.');
          }
        });
      }  
  }

  chooseCategory(){
    if(this.editPost){  
        let navigationExtras:NavigationExtras = {
          queryParams: {
            editPost: this.editPost,
            imgUrl:this.imgUrl,
            postDetail:JSON.stringify(this.post),
            category:true,
            pageRoute:this.pageRoute
          }
        };
        this.router.navigate(['skills'],navigationExtras);
    }else if(this.addPost){
      if(this.postData){
        let navigationExtras:NavigationExtras = {
          queryParams: {
            addPost: this.addPost,
            imgUrl:this.imgUrl,
            postDetail:JSON.stringify(this.post),
            category:true,
            postData:this.postData,
            pageRoute:this.pageRoute,
            postType:this.postType
          }
        };
        this.router.navigate(['skills'],navigationExtras);
      }else{
        let navigationExtras:NavigationExtras = {
          queryParams: {
            addPost: this.addPost,
            imgUrl:this.imgUrl,
            postDetail:JSON.stringify(this.post),
            category:true,
            postData:this.postData,
            pageRoute:this.pageRoute,
            postType:this.postType
          }
        };
        this.router.navigate(['skills'],navigationExtras);
      }
     
    } 
  }

  async updateImage(i) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
              text: 'Load from Library',
              handler: () => {
               this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                
              }
          },
          {
              text: 'Use Camera',
              handler: () => {
               this.takePicture(this.camera.PictureSourceType.CAMERA,i);
               
              }
          },
          {
              text: 'Cancel',
              role: 'cancel'
          }
      ]
  });
    await actionSheet.present();
  }

  async updateVideo(i){
    this.uploaded1=false;
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [ {
        text: 'Upload Video',
        handler: () => {
         this.takePicture1(this.camera.PictureSourceType.PHOTOLIBRARY,i);
         
        }
    },
          {
              text: 'Cancel',
              role: 'cancel'
          }
      ]
  });
    await actionSheet.present();
   
  }

  async takePicture(sourceType,i) {
    if(this.platform.is('ios')){

      const options: CameraOptions = {
      quality: 100,
      targetWidth: 900,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      encodingType: this.camera.EncodingType.JPEG
      };
      
      const tempImage = await this.camera.getPicture(options);
      const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
      
      // Now, the opposite. Extract the full path, minus filename.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
      const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
      
      // Get the Data directory on the device.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
      const newBaseFilesystemPath = this.file.dataDirectory;
      await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
      newBaseFilesystemPath, tempFilename);
      
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
      const storedPhoto = newBaseFilesystemPath + tempFilename;
      this.file.resolveLocalFilesystemUrl(storedPhoto)
      .then(entry => {
      ( < FileEntry > entry).file(file => this.readFile(file, i))
      })
      .catch(err => {
      console.log(err);
      // this.presentToast('Error while reading file.');
      });
      
      }else{
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          encodingType: this.camera.EncodingType.JPEG,
          
        };
        this.camera.getPicture(options).then((imageData) => {
          this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
            entry.file(file => {
              console.log(file);
              this.readFile(file,i);
            });
          });
        }, (err) => {
          // Handle error
       
        });
      }
      
   
  }

  async takePicture1(sourceType,i) {

    const options: CameraOptions = {
      mediaType: this.camera.MediaType.VIDEO,
      sourceType: sourceType
    }

    this.camera.getPicture(options)
    .then( async (videoUrl) => {
      if (videoUrl) {
        this.authService.loading('Please Wait');
        this.uploadedVideo = null;
        
        var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
        var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);

        dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
        
        try {
          var dirUrl = await this.file.resolveDirectoryUrl(dirpath);
          var retrievedFile = await this.file.getFile(dirUrl, filename, {});

        } catch(err) {
          this.authService.dismissLoading();
          return this.authService.presentToast("Error Something went wrong.");
        }
        
        retrievedFile.file( data => {
          console.log(data);
            this.authService.dismissLoading();
            if (data.size > MAX_FILE_SIZE) return this.authService.presentToast("Error You cannot upload more than 5mb.");
            if (data.type !== ALLOWED_MIME_TYPE) return this.authService.presentToast("Error Incorrect file type.");

            this.selectedVideo = retrievedFile.nativeURL;
            console.log(this.selectedVideo);
              this.uploadFile(retrievedFile);
      
              
           
        
        });
      }
    },
    (err) => {
      console.log(err);
    });
}

async uploadFile(f: FileEntry) {
  const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
  const type = this.getMimeType(f.name.split('.').pop());
  const buffer = await this.file.readAsArrayBuffer(path, f.name);
  const fileBlob = new Blob([buffer], type);

  const randomId = Math.random()
    .toString(36)
    .substring(2, 8);
this.upload2FirebaseVideo(fileBlob);
}

async upload2FirebaseVideo(video) {
  this.authService.loading('Loading Video..');
  const file = video;
  const filePath = this.makeid(5);
  const fileRef = this.angularstorage.ref(filePath);
  //const newFile = new File(file);
  // let newFile= file.getURL().getFile();
  
  
  const task = this.angularstorage.upload(filePath, file);
  console.log(filePath);
  console.log(file);
  // observe percentage changes
  this.uploadPercent = task.percentageChanges();
  // get notified when the download URL is available
  
  await task.snapshotChanges().pipe(
  finalize(() => fileRef.getDownloadURL().subscribe(value => {

  this.imgUrl = value;
  this.newImgUrl=value;
  this.uploaded1=true;
  this.uploaded = true;
  console.log(this.imgUrl);
  this.authService.dismissLoading();
  }))
  )
  .subscribe()
  
  }

getMimeType(fileExt) {
  if (fileExt == 'wav') return { type: 'audio/wav' };
  else if (fileExt == 'jpg') return { type: 'image/jpg' };
  else if (fileExt == 'mp4') return { type: 'video/mp4' };
  else if (fileExt == 'MOV') return { type: 'video/quicktime' };
}

  readFile(file: any,i) {
    const reader = new FileReader();
    reader.onload = () => {
    const imgBlob = new Blob([reader.result], {
    type: file.type
    });
    if(i===1){
    this.upload2Firebase(imgBlob);
    }
    else{
    console.log("if second image");
    }
    
    };
    reader.readAsArrayBuffer(file);
    }

    async upload2Firebase(image) {
      this.authService.loading('Loading Image..');
      const file = image;
      const filePath = this.makeid(5);
      const fileRef = this.angularstorage.ref(filePath);
      const task = this.angularstorage.upload(filePath, file);
      console.log(filePath);
      console.log(file);
      // observe percentage changes
      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      
      await task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(value => {
      this.post.imgUrl=value;
      this.imgUrl = value;
      this.uploaded = true;
      this.authService.dismissLoading();
   
      }))
      )
      .subscribe()
      
      }

    makeid(length) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
      }

  back(){
    this.router.navigateByUrl(this.pageRoute);
  }

}
