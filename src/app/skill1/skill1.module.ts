import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Skill1PageRoutingModule } from './skill1-routing.module';

import { Skill1Page } from './skill1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Skill1PageRoutingModule
  ],
  declarations: [Skill1Page]
})
export class Skill1PageModule {}
