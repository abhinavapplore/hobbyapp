import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Skill1Page } from './skill1.page';

describe('Skill1Page', () => {
  let component: Skill1Page;
  let fixture: ComponentFixture<Skill1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Skill1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Skill1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
