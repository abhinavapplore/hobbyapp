import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Skill1Page } from './skill1.page';

const routes: Routes = [
  {
    path: '',
    component: Skill1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Skill1PageRoutingModule {}
