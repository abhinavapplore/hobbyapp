import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnlockContact1PageRoutingModule } from './unlock-contact1-routing.module';

import { UnlockContact1Page } from './unlock-contact1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnlockContact1PageRoutingModule
  ],
  declarations: [UnlockContact1Page]
})
export class UnlockContact1PageModule {}
