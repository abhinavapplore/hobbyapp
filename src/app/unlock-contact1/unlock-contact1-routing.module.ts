import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnlockContact1Page } from './unlock-contact1.page';

const routes: Routes = [
  {
    path: '',
    component: UnlockContact1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnlockContact1PageRoutingModule {}
