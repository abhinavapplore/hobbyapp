import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment'
declare var RazorpayCheckout: any;


@Component({
  selector: 'app-unlock-contact1',
  templateUrl: './unlock-contact1.page.html',
  styleUrls: ['./unlock-contact1.page.scss'],
})
export class UnlockContact1Page implements OnInit {

  @Input('professionalUser') professionalUser;

  selected:boolean=false;
  selected1:boolean=false;
  proffessionalUser:any={};
  selectedAmount;
  walletBalance;
  todayDate;
  newAmount;
  userId;
  credit;
  userName;
  hostName;
  user:any={};
  constructor(public modalController: ModalController,public authService: AuthService,
    public storage: Storage,public route: ActivatedRoute,
    public alertController: AlertController) { 
    }

  ngOnInit() {
    console.log(this.professionalUser);
    console.log('hey.................')
    this.storage.get('user').then((user)=>{
      this.user=user;
      this.userId=user._id;
      this.userName=user.fullName;
      this.walletBalance=user.wallet;
      this.hostName=this.professionalUser.fullName
    });
  }

  select(x){
    this.selected=true;
    this.selected1=false;
    this.selectedAmount=parseInt(x);
    this.newAmount=this.selectedAmount*100;
    if(this.selectedAmount==10){
      this.credit=5;
    }else{
      this.credit=10;
    }
  }
  select1(x){
    this.selected1=true;
    this.selected=false;
    this.selectedAmount=parseInt(x);
    this.newAmount=this.selectedAmount*100;
    if(this.selectedAmount==15){
      this.credit=10;
    }else{
      this.credit=5;
    }
  }
  unSelect(){
    this.selected=false;
    this.selected1=false;
    this.selectedAmount='';
    this.credit=0;
  }
  unSelect1(){
    this.selected=false;
    this.selected1=false;
    this.selectedAmount='';
    this.credit=0;
  }

  buy(){
      this.todayDate=moment().format('DD/MMM/YYYY')
      console.log(this.todayDate);
      if(this.selectedAmount != ''||this.selectedAmount != undefined||this.selectedAmount !=null){
        var options = {
          description: 'Add money to wallet',
          image: 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
          currency: 'INR',
          key: 'rzp_test_sbuNGT8VxLZqff',
          amount: this.newAmount,
          theme: {
            color: '#02CBEE',
          },
          modal: {
            ondismiss: function(){
              this.presentAlert('Transaction Cancelled');
            }
          }
        };
    
        var self=this;
    
        var successCallback = function (payment_id) {
          var obj = {'userId':self.userId,'title':self.professionalUser.fullName,
          'transactionImage':self.professionalUser.user_img,
          'amount':self.credit,"deviceId":self.professionalUser.deviceId,
          'transactionDate':moment().format('DD/MMM/YYYY'),'isAddingMoney':true,
          'transactionTime':moment().format("h:mm ' A"),'fullName':self.userName,
          'addingAmount':self.selectedAmount,'walletbalance':self.walletBalance,
          'hostId':self.professionalUser._id,
          "recieverImg":self.professionalUser.user_img,"senderImg":self.user.user_img
        }
          self.authService.unlockUser(obj).subscribe((res:any) => {
            console.log(res);
            if(res.success){
              self.presentAlertSuccess(res.success,'Transaction Completed.');  
              self.authService.presentToast('User Unlocked.');  
              self.modalController.dismiss(res.success);           
          }else {
            self.authService.presentToast('Please try in sometime.');
            self.modalController.dismiss();
          }
          })
          
        };
    
        var cancelCallback = function (error) {
          console.log(error);
          self.presentAlert('Payment Cancelled.');
        };
    
        RazorpayCheckout.open(options, successCallback, cancelCallback);
        //  
      
      }     

    
  }

  async presentAlertSuccess(success,msg) {
    const alert = await this.alertController.create({
      cssClass: 'razorpayCancel',
      message: msg,
      buttons: [
         {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            this.modalController.dismiss(success); 
           
          }
        }
      ]
    });

    await alert.present();
  }
  
 
  async presentAlert(msg) {
    const alert = await this.alertController.create({
      cssClass: 'razorpayCancel',
      message: msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Try Again',
          handler: () => {
            console.log('Confirm Okay');
            this.buy();
           
          }
        }
      ]
    });

    await alert.present();
  }

  closeModal(){
    this.modalController.dismiss();
  }

}
