import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UnlockContact1Page } from './unlock-contact1.page';

describe('UnlockContact1Page', () => {
  let component: UnlockContact1Page;
  let fixture: ComponentFixture<UnlockContact1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnlockContact1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UnlockContact1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
