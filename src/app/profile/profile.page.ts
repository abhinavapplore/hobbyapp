import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import {ActionSheetController, Platform, ModalController, NavController} from '@ionic/angular';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { IonInput, IonList, IonSlides } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionPage } from '../question/question.page';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/storage';
import * as firebase from 'firebase';
import { FCM } from '@ionic-native/fcm/ngx';
declare const google;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  skillLevel;
  showContactField:boolean;
  level='Beginner';
  latitude;
  longitude;
  map;
  firstName="";
  lastName="";
  email="";
  dob;
  gender='male';
  otherLang;
  selectedLanguage=[];
  uploaded1:boolean=false;
  imgUrl1="";
  uploaded2:boolean=false;
  imgUrl2="";
  uploaded3:boolean=false;
  imgUrl3="";
  checked:boolean=false;
  hobbyArray:any=[];
  hobbyArray1:any=[];
  userImg: any = '';
  base64Img = '';
  userSkills;
  pageRoute;
  deviceKey;
  slidesRef: IonSlides;
  todayDate;
  userType;
  mobile;
  languageArray=[{'language':'English','isSelected':false},{'language':'Hindi','isSelected':false},
  {'language':'Spanish','isSelected':false},{'language':'French','isSelected':false},
  {'language':'other','isSelected':false}];
  
  uploadPercent: Observable<number>;

  cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
   }

   gelleryOptions: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    }
    @ViewChild('slideWithNav', {static: false}) set content(slideWithNav: IonSlides) {
      if (slideWithNav){
          this.slidesRef = slideWithNav;
          //Here you can set any properties you would like
         
      }
  }
  constructor(public geolocation: Geolocation,public authService: AuthService,
    public platform:Platform, public modalController: ModalController,
    private angularstorage: AngularFireStorage,private file: File, 
    private filePath: FilePath, public httpClient:HttpClient,public fcm:FCM,
    public actionSheetController: ActionSheetController, public storage: Storage,
    private camera: Camera,public router: Router,public route: ActivatedRoute,
    public afa: AngularFireAuth, public fs: AngularFirestore,
    public navCtrl: NavController) {
      this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          this.pageRoute=params.special;
        }
      });
     }
  
  ngOnInit() {
    this.fcm.getToken().then(token => {
      this.deviceKey=token;
      console.log(this.deviceKey);
      console.log('device key....')     
    });
    this.storage.get("userType").then((data)=>{
      this.storage.get('isEmailLogin').then((res)=>{
        if(res===true){
          this.storage.get('emailId').then((item)=>{
            this.email=item;
            this.showContactField=true;
          })
         
        }else{
          this.storage.get("contactNum").then((res)=>{
          this.showContactField=false;
          this.mobile=res
          });
        }
      })
      this.userType=data;    
      console.log(this.userType); 
    });
  }

  ionViewDidEnter(){
    this.storage.get('userHobby').then((res)=>{
    this.hobbyArray=res;
  
    });
    this.storage.get("userSkills").then((data)=>{
      this.userSkills=data;
      // for(var i=0; i<this.userSkills.length;i++){
      //   if(i==this.userSkills.length-1){

      //   }else if(){

      //   }
      // }
      // this.skillString=
    });
    this.geolocation.getCurrentPosition().then((resp) => {
    // resp.coords.latitude
    this.latitude= resp.coords.latitude;
    console.log(this.latitude);
    // resp.coords.longitude
    this.longitude=resp.coords.longitude;
    console.log(this.longitude);
    this.testmap();
         });
       }

  skill($event,index){
    console.log($event);
    this.skillLevel=$event.detail.value;
    this.hobbyArray[index].skillLevel=this.skillLevel;
    console.log(this.hobbyArray);
    console.log(this.skillLevel);
    if(this.skillLevel==0 || this.skillLevel==1 || this.skillLevel==2){
      this.level='Beginner';
      this.hobbyArray[index].level=this.level;
    }else if(this.skillLevel==3 || this.skillLevel==4){
      this.level='Intermediate';
      this.hobbyArray[index].level=this.level;
    }else{
      this.level='Expert';
      this.hobbyArray[index].level=this.level;
    }
    
    if(this.skillLevel!='' && this.skillLevel!=undefined){
      if(index==this.hobbyArray.length-1){
        this.slidesRef.slideTo(index[0]);
      }
      else{
        index=index+1;
        console.log(typeof(index));
        console.log(index);
        this.slidesRef.slideTo(index);
        
      }    
    }
  }

  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 10,
    initialSlide:0,
  };


  testmap() {
    var myLatlng = new google.maps.LatLng(this.latitude,this.longitude);
    console.log(myLatlng)
    var mapOptions = {
    zoom: 12,
    center: myLatlng,
    mapTypeControl: false,
    scaleControl: false,
    zoomControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    styles: [{
    stylers: [{
    saturation: -100
           }]
         }],  
        }
    this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
    //Add User Location Marker To Map
    var marker = new google.maps.Marker({
    position: myLatlng, 
    draggable:true   
       });
    // To add the marker to the map, call setMap();
    marker.setMap(this.map)

    marker.addListener('dragend', function(event){
      console.log(event);
      this.latitude = event.latLng.lat();
      console.log(this.latitude);
      this.longitude = event.latLng.lng();
      console.log(this.longitude);
    });
    
    //Map Click Event Listner
    this.map.addListener('click', function() {
    //add functions here
    });
  
     

     }

     userGender(x,y){
       this.gender=x;
       console.log(this.gender);
     }

     selectedLang(item,i){
       console.log(item);
       console.log(i);
       for(var j=0;j<this.languageArray.length;j++){
        if(!this.languageArray[j].isSelected && i==j){
          this.languageArray[j].isSelected=true;
          console.log(this.languageArray);
        }
        }
       }

        async presentActionSheet1(i,j) {
          if(j=='profile'){
            const actionSheet = await this.actionSheetController.create({
              header: "Select Image source",
              buttons: [{
                      text: 'Load from Library',
                      handler: () => {
                       this.takePicture1(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                        
                      }
                  },
                  {
                      text: 'Use Camera',
                      handler: () => {
                       this.takePicture1(this.camera.PictureSourceType.CAMERA,i);
                       
                      }
                  },
                  {
                      text: 'Cancel',
                      role: 'cancel'
                  }
              ]
          });
            await actionSheet.present();
          }else if(j=='doc1'){
            const actionSheet = await this.actionSheetController.create({
              header: "Select Image source",
              buttons: [{
                      text: 'Load from Library',
                      handler: () => {
                       this.takePicture2(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                        
                      }
                  },
                  {
                      text: 'Use Camera',
                      handler: () => {
                       this.takePicture2(this.camera.PictureSourceType.CAMERA,i);
                       
                      }
                  },
                  {
                      text: 'Cancel',
                      role: 'cancel'
                  }
              ]
          });
            await actionSheet.present();
          }else {
            const actionSheet = await this.actionSheetController.create({
              header: "Select Image source",
              buttons: [{
                      text: 'Load from Library',
                      handler: () => {
                       this.takePicture3(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                        
                      }
                  },
                  {
                      text: 'Use Camera',
                      handler: () => {
                       this.takePicture3(this.camera.PictureSourceType.CAMERA,i);
                       
                      }
                  },
                  {
                      text: 'Cancel',
                      role: 'cancel'
                  }
              ]
          });
            await actionSheet.present();
          }
          }
          
      

        async takePicture1(sourceType,i) {
          if(this.platform.is('ios')){
      
            const options: CameraOptions = {
            quality: 100,
            targetWidth: 900,
            targetHeight: 600,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            encodingType: this.camera.EncodingType.JPEG
            };
            
            const tempImage = await this.camera.getPicture(options);
            const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
            
            // Now, the opposite. Extract the full path, minus filename.
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
            const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
            
            // Get the Data directory on the device.
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
            const newBaseFilesystemPath = this.file.dataDirectory;
            await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
            newBaseFilesystemPath, tempFilename);
            
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
            const storedPhoto = newBaseFilesystemPath + tempFilename;
            this.file.resolveLocalFilesystemUrl(storedPhoto)
            .then(entry => {
            ( < FileEntry > entry).file(file => this.readFile(file, i))
            })
            .catch(err => {
            console.log(err);
            // this.presentToast('Error while reading file.');
            });
            
            }else{
              const options: CameraOptions = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: sourceType,
                encodingType: this.camera.EncodingType.JPEG,
                
              };
              this.camera.getPicture(options).then((imageData) => {
                // this._file.resolveLocalFilesystemUrl(
                //   imageData,
                //   (entry: FileEntry) => {console.log(entry)},
                //   err => console.log(err)
                // );
                this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
                  entry.file(file => {
                    console.log(file);
                    this.readFile(file,i);
                  });
                });
              }, (err) => {
                // Handle error
             
              });
            }
            
         
        }

        async takePicture2(sourceType,i) {
          if(this.platform.is('ios')){
      
            const options: CameraOptions = {
            quality: 100,
            targetWidth: 900,
            targetHeight: 600,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            encodingType: this.camera.EncodingType.JPEG
            };
            
            const tempImage = await this.camera.getPicture(options);
            const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
            
            // Now, the opposite. Extract the full path, minus filename.
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
            const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
            
            // Get the Data directory on the device.
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
            const newBaseFilesystemPath = this.file.dataDirectory;
            await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
            newBaseFilesystemPath, tempFilename);
            
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
            const storedPhoto = newBaseFilesystemPath + tempFilename;
            this.file.resolveLocalFilesystemUrl(storedPhoto)
            .then(entry => {
            ( < FileEntry > entry).file(file => this.readFile2(file, i))
            })
            .catch(err => {
            console.log(err);
            // this.presentToast('Error while reading file.');
            });
            
            }else{
              const options: CameraOptions = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: sourceType,
                encodingType: this.camera.EncodingType.JPEG,
                
              };
              this.camera.getPicture(options).then((imageData) => {
                // this._file.resolveLocalFilesystemUrl(
                //   imageData,
                //   (entry: FileEntry) => {console.log(entry)},
                //   err => console.log(err)
                // );
                this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
                  entry.file(file => {
                    console.log(file);
                    this.readFile2(file,i);
                  });
                });
              }, (err) => {
                // Handle error
             
              });
            }
            
         
        }
        

        async takePicture3(sourceType,i) {
          if(this.platform.is('ios')){
      
            const options: CameraOptions = {
            quality: 100,
            targetWidth: 900,
            targetHeight: 600,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            encodingType: this.camera.EncodingType.JPEG
            };
            
            const tempImage = await this.camera.getPicture(options);
            const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
            
            // Now, the opposite. Extract the full path, minus filename.
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
            const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
            
            // Get the Data directory on the device.
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
            const newBaseFilesystemPath = this.file.dataDirectory;
            await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
            newBaseFilesystemPath, tempFilename);
            
            // Result example: file:///var/mobile/Containers/Data/Application
            // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
            const storedPhoto = newBaseFilesystemPath + tempFilename;
            this.file.resolveLocalFilesystemUrl(storedPhoto)
            .then(entry => {
            ( < FileEntry > entry).file(file => this.readFile3(file, i))
            })
            .catch(err => {
            console.log(err);
            // this.presentToast('Error while reading file.');
            });
            
            }else{
              const options: CameraOptions = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: sourceType,
                encodingType: this.camera.EncodingType.JPEG,
                
              };
              this.camera.getPicture(options).then((imageData) => {
                // this._file.resolveLocalFilesystemUrl(
                //   imageData,
                //   (entry: FileEntry) => {console.log(entry)},
                //   err => console.log(err)
                // );
                this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
                  entry.file(file => {
                    console.log(file);
                    this.readFile3(file,i);
                  });
                });
              }, (err) => {
                // Handle error
             
              });
            }
            
         
        }
        
        
        
        readFile(file: any,i) {
        const reader = new FileReader();
        reader.onload = () => {
        // const formData = new FormData();
        const imgBlob = new Blob([reader.result], {
        type: file.type
        });
        // formData.append('file', imgBlob, file.name);
        // this.uploadImageData(formData);
        if(i===1){
        this.upload2Firebase1(imgBlob);
        }
        else{
        console.log("if second image");
        }
        
        };
        reader.readAsArrayBuffer(file);
        }

        readFile2(file: any,i) {
          const reader = new FileReader();
          reader.onload = () => {
          // const formData = new FormData();
          const imgBlob2 = new Blob([reader.result], {
          type: file.type
          });
          // formData.append('file', imgBlob, file.name);
          // this.uploadImageData(formData);
          if(i===1){
          this.upload2Firebase2(imgBlob2);
          }
          else{
          console.log("if second image");
          }
          
          };
          reader.readAsArrayBuffer(file);
          }

          readFile3(file: any,i) {
            const reader = new FileReader();
            reader.onload = () => {
            // const formData = new FormData();
            const imgBlob3 = new Blob([reader.result], {
            type: file.type
            });
            // formData.append('file', imgBlob, file.name);
            // this.uploadImageData(formData);
            if(i===1){
            this.upload2Firebase3(imgBlob3);
            }
            else{
            console.log("if second image");
            }
            
            };
            reader.readAsArrayBuffer(file);
            }
        
        private createFileName() {
        var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
        return newFileName;
        }
        
        makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
        }
        
        // uploadFile(event) {
        
        // const file = event.target.files[0];
        // const filePath = this.makeid(5);
        // const fileRef = this.angularstorage.ref(filePath);
        
        
        // const task = this.angularstorage.upload(filePath, file);
        
        // // observe percentage changes
        // this.uploadPercent = task.percentageChanges();
        // // get notified when the download URL is available
        
        // task.snapshotChanges().pipe(
        // finalize(() => fileRef.getDownloadURL().subscribe(value => {
         
        // this.imgUrl1 = value;
        // this.uploaded1 = true;
        
        
        // }))
        // )
        // .subscribe()
        // }
        
        async upload2Firebase1(image) {
        this.authService.loading('Loading Profile..');
        const file = image;
        const filePath = this.makeid(5);
        const fileRef = this.angularstorage.ref(filePath);
        //const newFile = new File(file);
        // let newFile= file.getURL().getFile();
        
        
        const task = this.angularstorage.upload(filePath, file);
        console.log(filePath);
        console.log(file);
        // observe percentage changes
        this.uploadPercent = task.percentageChanges();
        // get notified when the download URL is available
        
        await task.snapshotChanges().pipe(
        finalize(() => fileRef.getDownloadURL().subscribe(value => {
   
        this.imgUrl1 = value;
        this.uploaded1 = true;
        console.log(this.imgUrl1);
        this.authService.dismissLoading();
     
        }))
        )
        .subscribe()
        
        }

        async upload2Firebase2(image) {
          this.authService.loading('Loading Document..');
          const file = image;
          const filePath = this.makeid(5);
          const fileRef = this.angularstorage.ref(filePath);
          //const newFile = new File(file);
          // let newFile= file.getURL().getFile();
          
          
          const task = this.angularstorage.upload(filePath, file);
          console.log(filePath);
          console.log(file);
          // observe percentage changes
          this.uploadPercent = task.percentageChanges();
          // get notified when the download URL is available
          
          await task.snapshotChanges().pipe(
          finalize(() => fileRef.getDownloadURL().subscribe(value => {
     
          this.imgUrl2 = value;
          this.uploaded2 = true;
          console.log(this.imgUrl1);
          this.authService.dismissLoading();
       
          }))
          )
          .subscribe()
          
          }

          async upload2Firebase3(image) {
            this.authService.loading('Loading Document..');
            const file = image;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            
            
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            
            await task.snapshotChanges().pipe(
            finalize(() => fileRef.getDownloadURL().subscribe(value => {
       
            this.imgUrl3 = value;
            this.uploaded3 = true;
            console.log(this.imgUrl1);
            this.authService.dismissLoading();
         
            }))
            )
            .subscribe()
            
            }

            checkbox($event){
              console.log($event);
           
                this.checked=$event.detail.checked;
            
             
            }

        submit(){
          if(this.email==""){
            this.authService.presentToast("Email is required.");
          }else if(this.firstName==""){
            this.authService.presentToast("Firstname is required.");
          }else if(this.lastName==""){
            this.authService.presentToast("Lastname is required.");
          }else if(this.imgUrl1==""){
            this.authService.presentToast("Profile picture is required.");
          }else if(!this.checked){
            this.authService.presentToast("Please Agree To Terms & Conditions.");
          }else{
            if(this.checked){
              if(moment(this.dob)>moment()){
                this.authService.presentToast("Dob cannot be future date.");
              }else {
                this.authService.loading('Creating your profile');
               
                  // this.storage.get("userType").then((data)=>{
          
                    
                    var userName=this.firstName + " " + this.lastName;
                    var lName=this.lastName.charAt(0);
                    var dateOfBirth=this.dob;  
                    var gender=this.gender;
                    var latitude=this.latitude;
                    var longitude=this.longitude;
                    var lat=JSON.parse(latitude);
                    var lng=JSON.parse(longitude);
                    var latlng=lat+','+lng;
                    console.log(latlng);
                    this.authService.httpClient.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng+'&sensor=true&key=AIzaSyApm82MFXMcROWrHaGTj-auUcyOYQwBwsE').subscribe((addResponse:any)=>{
                    console.log(addResponse);
                    var str=addResponse.plus_code.compound_code;
                    var newstr = str.slice(7);  
                    var address=newstr.replace(/\w+[.!?]?$/, '');
                    var language=this.languageArray;
                    var hobby=this.hobbyArray;
                    var userImage=this.imgUrl1;
                    var userDocuents=[{'docUrl':this.imgUrl2},{'docUrl':this.imgUrl3}];
                    var obj={'fullName':userName,"firstName":this.firstName,"lastName":lName,'dob':dateOfBirth,'contactNum':this.mobile,
                    'gender':gender,'userLatitude':latitude,'userLongitude':longitude,
                    'language':language,'userHobby':hobby,'user_img':userImage,
                    'documents':userDocuents,"userType":this.userType,'emailId':this.email,
                    emailIdRegistered:true,'userSkills':this.userSkills,'address':address,
                    'deviceId':this.deviceKey};
                    this.authService.register(obj).subscribe((res:any)=>{
                        if(res.success == false){
                           console.log('Something went wrong');
                           this.authService.dismissLoading();
                           this.authService.presentToast("Please try again.")
                          }else{
                         
                            firebase.auth().createUserWithEmailAndPassword(this.email,'12345678').then((userFire)=>{
                             console.log(userFire);
                        });
                        this.storage.set("ACCESS_TOKEN", res.token);
                            this.storage.set("user", res.user);
                            console.log(res.user); 
                        this.fs.collection('friends').doc(res.user._id).set({
                          userEmail:res.user.emailId,
                          Name: res.user.fullName,
                          displayName: res.user.fullName,
                          photoURL: res.user.user_img,
                          UserID: res.user._id,
                          Timestamp: firebase.firestore.FieldValue.serverTimestamp()
                      }, { merge: true });
                            this.authService.dismissLoading();
                      this.navCtrl.navigateRoot('tabs/tab1');
                          }          
                    });
                  });
                // });
           
            }
           
          }else {
            this.authService.presentToast('Please agree to terms and conditions.')
          }
          }
         
          
        }
    
      back(){
        this.router.navigateByUrl('hobby');
      }
    
     

}
