import { Component, OnInit,ViewChildren } from '@angular/core';
import {ModalController, MenuController} from '@ionic/angular';
import {FiltermodalPage} from '../filtermodal/filtermodal.page'
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import {AuthService} from '../auth.service';
import { Storage } from '@ionic/storage';
declare const google;

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page {
  @ViewChildren('slidewithnav')slides;
  userId;
  latitude;
  longitude;
  userType;
  searchTerm;
  selectedTab;
  professionalUsers:any=[];
  passionistUsers:any=[];
  filterPassionistUser:any=[];
  filterProfessionalUser:any=[];
  allUser:any=[];
  searchArray:any=[];
  showList:boolean=false;
  isSearch:boolean=false;
  professional:boolean=true;
  search:boolean=false;
  slideOpts = {
    slidesPerView: 2,
    spaceBetween: 5,
    initialSlide:0,
  };
  slideOpts1 = {
    slidesPerView: 2,
    spaceBetween: 5,
    initialSlide:0,
  };
  slideOpts2 = {
    slidesPerView: 1,
    initialSlide:0,
    // slideShow:true,
    // autoplay:true
  };


  constructor(public modalController: ModalController,public router: Router,
    private menu: MenuController,public authService: AuthService,
    public storage: Storage,public route: ActivatedRoute) {
      this.professionalUsers=[];
      this.passionistUsers=[];
      this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          this.userType=params.userType;
          if(this.userType=='proffessionals'){
            this.professionalUsers=JSON.parse(params.special);
            console.log(this.professionalUsers);
          }else if(this.userType=='passionists'){
            this.passionistUsers=JSON.parse(params.special);
            console.log(this.passionistUsers);
          }
        }
      });
     }

     ngOnInIt(){
      
    }

  ionViewDidEnter() {
   
    this.loadData();
  }

  ionViewWillLeave(){
    this.slides.stopAutoplay();
    }

  loadData(){
    this.professionalUsers=[];
    this.passionistUsers=[];
    this.storage.get('user').then((user)=>{
      this.authService.loading("Loading profiles...");
      this.userId=user._id;
      this.latitude=user.userLatitude;
      this.longitude=user.userLongitude;
      var obj={'userId':this.userId,'userLat':user.userLatitude,'userLong':user.userLongitude};
      this.authService.getAllUser(obj).subscribe((data:any)=>{
        console.log('heyyyyyyyyyyy');
        console.log(data);
        // this.allUser.push(data.professional);
        // this.allUser.push(data.passionist);
        this.professionalUsers=data.professional;
        this.professionalUsers.forEach(element => {
          console.log(element.address);
         var shortAdd = element.address.split(',');
         element.shortAddress = shortAdd[0];
        });
        this.passionistUsers=data.passionist;
        this.passionistUsers.forEach(element => {
          console.log(element.address);
          var shortAdd = element.address.split(',');
          element.shortAddress = shortAdd[0];
         });
        this.authService.dismissLoading();
      });
     
    });
  }

  doRefresh($event){
    console.log($event);
    this.loadData();
    setTimeout(() => {
      console.log('Async operation has ended');
      $event.target.complete();
    }, 2000);
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  // selectedSegment(x){
  //  if(x=="proffessionals"){
  //    this.professional=true;
  //  }else {
  //    this.professional=false;
  //  }
  // }


  segmentChanged($event){
    console.log($event);
    this.selectedTab=$event.detail.value;
    if(this.selectedTab=="professionals"){
      this.professional=true;
    }else {
      this.professional=false;
    }
  }

  openSearch(){
    this.search=true;
    console.log(this.search);
  }
  closeSearch(){
    this.search=false;
  }

  goToProfile1(x) {
    console.log(x);
      let navigationExtras:NavigationExtras = {
        queryParams: {
          professional:this.professional,
          pageroute: this.router.url,
          hostId:x._id,
          userId:this.userId
       
        }
      };
      this.router.navigate(['profile1'],navigationExtras);
  }

  getUser(ev: any){
    var arr = [];
    
      for(let i =0 ; i<this.professionalUsers.length;i++){
       
        var input = this.professionalUsers[i];
        arr.push(input);
     
        let val = ev.target.value;
      
         if (val && val.trim() != '') {
           
          
           this.searchArray = arr.filter(function(item) {
       return item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1;
     });
      
        // Show the results
  if(this.searchArray.length!==0){
    this.showList = true;
   }else{
    this.showList = false;
   }
      
        } else {
          
          
          this.showList = false;
        }
    }   
  }

  getUser1(ev: any){
    var arr = [];
    
      for(let i =0 ; i<this.passionistUsers.length;i++){
       
        var input = this.passionistUsers[i];
        arr.push(input);
     
        let val = ev.target.value;
      
         if (val && val.trim() != '') {
           
          
           this.searchArray = arr.filter(function(item) {
       return item.fullName.toLowerCase().indexOf(val.toLowerCase()) > -1;
     });
      
        // Show the results
  if(this.searchArray.length!==0){
    this.showList = true;
   }else{
    this.showList = false;
   }
      
        } else {
          
          
          this.showList = false;
        }
    }   
  }


  selectProfile(x)
  {  
    
    this.showList = false;
    this.searchTerm = "";
    this.isSearch=false;
    console.log(x);
    this.goToProfile1(x);
    
  }



  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltermodalPage,
      cssClass: 'filterModal',
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
          if (data) {
            console.log(data);
            this.professionalUsers=[];
            this.passionistUsers=[];
            this.professionalUsers=data[0].filterProfessionalUser;
            this.passionistUsers=data[1].filterPassionistUser;
          }else{
            console.log("goes in else");
            // this.router.navigateByUrl('profile');
          }
  }


}
