import { Component, OnInit, Input } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-story',
  templateUrl: './story.page.html',
  styleUrls: ['./story.page.scss'],
})
export class StoryPage implements OnInit {
  @Input ('postData') postData;
  userData:any={};
  $:any;

  constructor(public authService: AuthService,public storage: Storage,
    private modalController: ModalController) {
      setTimeout(()=>{
        this.closeModal();
      },10000)
     }

  ngOnInit() {
    this.loadData(); 
    console.log(this.postData);  
  }

  loadData() {
      this.storage.get('user').then((user)=>{
      this.userData=user;
  });
}

closeModal() {
  this.modalController.dismiss();
}



}
