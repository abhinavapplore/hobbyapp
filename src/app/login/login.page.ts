import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FCM } from '@ionic-native/fcm/ngx';
import { NavController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  deviceKey;
  mobile;
  allCountry=[];
  userData: any = {};
  initial:boolean=true;
  loginMobile;

  constructor(public authService: AuthService,public storage: Storage,public fcm:FCM,
    public router: Router,public route: ActivatedRoute,public httpClient: HttpClient,public googlePlus:GooglePlus,
    public navCtrl: NavController) { }

  ngOnInit() {
    console.log('hello');
    this.fcm.getToken().then(token => {
      this.deviceKey=token;
      console.log(this.deviceKey);
      console.log('device key....')
    });
    // this.httpClient.get<any>('https://cors-anywhere.herokuapp.com/http://countryapi.gear.host/v1/Country/getCountries').subscribe((res) => {
    //   console.log(res);
    //   this.allCountry=res.Response;
    // });
  }

  signup(){  
      if(this.mobile.length!=10){
      this.authService.presentToast('Please Enter Valid Mobile Number')
    }else {
      this.storage.set('isEmailLogin',false).then((result)=>{
        var vc = {
          mobile:this.mobile
        } 
        let navigationExtras = {
          queryParams: {
            special: JSON.stringify(vc)
          }
        };
      
        this.router.navigate(['verify'], navigationExtras);
      })
   
  }
    
  }

  signUpMobile(){
    this.initial=false;
    this.loginMobile=true;
  }

  signUpEmail(){
    this.initial=false;
    this.loginMobile=false;
  }

  signup1(){
    console.log('heyyyyyyyyyy');
    if(this.mobile.length!=10){
      this.authService.presentToast('Please Enter Valid Mobile Number')
    }else {
      var obj={'contactNum':this.mobile,'deviceId':this.deviceKey}
      this.authService.login(obj).subscribe((data)=>{
        console.log(data);
        if(data.success){
          this.storage.set('user',data.user);
          this.navCtrl.navigateRoot('tabs/tab4');
        }else {
          this.storage.set("contactNum",this.mobile).then((res)=>{
            console.log(res);
            if(res!=null){
              this.navCtrl.navigateRoot('hobby');
            }      
          });
        }
      });
    }
  }

  googleSignIn() {
    this.googlePlus.login({}).then(result=>{
      this.authService.loading('Fetching Your details');
      console.log(result);
   console.log(result.email);
   console.log(result.email);
   var emailId= result.email;
   
   var firstName = result.givenName;
   var lastName= result.familyName;
   var s = result.familyName;
var ar = [];
ar = s.split();
   var fullName = result.givenName+" "+ ar[0];
         var obj={emailId:emailId,emailIdRegistered:true};
  this.authService.login(obj).subscribe((res) => {
    if(res.success == false){
      
      this.storage.set('emailId',emailId).then((email)=>{
this.storage.set('isEmailLogin',true).then((response)=>{
  this.router.navigateByUrl('hobby');
})
      })
    this.authService.dismissLoading();
    }else{
    
      this.storage.set("user", res.user);
      console.log(res.user);
      this.authService.dismissLoading();
      this.router.navigateByUrl('tabs/tab4');
    }
  });
    }).catch(err => this.userData = `Error ${JSON.stringify(err)}`);
  }

 

  

  // googleSignIn() {
  //   console.log("hey");
  //   this.googlePlus.login({})
  //     .then(result => this.userData = result)
  //     .catch(err => this.userData = `Error ${JSON.stringify(err)}`);
  // }

}
