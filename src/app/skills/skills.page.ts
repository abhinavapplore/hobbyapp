import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalpagePage } from '../modalpage/modalpage.page'
import { Storage } from '@ionic/storage';
import { NavigationExtras } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.page.html',
  styleUrls: ['./skills.page.scss'],
})
export class SkillsPage implements OnInit {

  data:boolean=false;
 selected:boolean=false;
 selected2:boolean=false;
 selected3:boolean=false;
 selected4:boolean=false;
 selected5:boolean=false;
 selected6:boolean=false;
 selected7:boolean=false;
 selected8:boolean=false;
 currentSelected: Number = null;
 hobby1;
 hobby2;
 hobby3;
 hobby4;
 hobby5;
 hobby6;
 hobby7;
 hobby8;
 count=0;
 SkillArray:any=[];
 selectedHobby:any=[];
 category:boolean=false;
 selectedCategory;
 imgUrl;
 editPost:boolean=false;
 postDetail;
 addPost;
 pageRoute;
 skillImage;
 postType;

  constructor(public router: Router,public modalController: ModalController,
    public storage: Storage,public route: ActivatedRoute,public authService: AuthService) {
      this.route.queryParams.subscribe(params => {
        //ADD POST AND POST DETAILS
          if(params.editPost){
            this.editPost=true;
            this.imgUrl=params.imgUrl;
            this.postDetail=JSON.parse(params.postDetail);
            this.category=params.category;
            this.pageRoute=params.pageRoute;
            // this.postType=params.postType
          }else if(params.addPost && params.postData){
              this.addPost=true;
              this.imgUrl=params.imgUrl;
             this.postDetail=JSON.parse(params.postDetail);
             this.category=params.category;
             this.pageRoute=params.pageRoute;
             this.postType=params.postType
            }else if(params.addPost && !params.postData){
              this.addPost=true;
              this.imgUrl=params.imgUrl;
             this.category=params.category;
             this.pageRoute=params.pageRoute;
             this.postType=params.postType
            }
            
      });
     }

  ngOnInit() {
    this.storage.get('userHobby').then((hobby)=>{
      console.log(hobby);
      this.selectedHobby=hobby;
    });
  }

  onItemClicked(idx,x) {
    this.currentSelected = idx;
    this.count++;
    this.selectedCategory=x.hobby;
    this.skillImage=x.hobbyImage;
    console.log(this.selectedCategory);
    console.log(this.skillImage);
}


  selectSkill1(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected=true;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectSkill2(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected2=true;
    this.selected=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectSkill3(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected3=true;
    this.selected=false;
    this.selected2=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectSkill4(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected4=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectSkill5(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected5=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectSkill6(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected6=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectSkill7(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected7=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected8=false;
  }
  selectSkill8(i,j){
    this.count++;
    this.selectedCategory=i;
    this.skillImage=j;
    console.log(this.selectedCategory);
    this.selected8=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
  }

  next(){
    if(this.selectedCategory!='' && this.selectedCategory!=undefined){
      this.SkillArray.push({'hobby':this.selectedCategory,'hobbyImage':this.skillImage});
      this.storage.set("userSkills",this.SkillArray).then((res)=>{
        console.log(res);
        if(this.SkillArray.length!=0){
          let navigationExtras:NavigationExtras = {
            queryParams: {
              special: this.route.url,
            }
          };
          this.router.navigate(['profile'],navigationExtras);
          }
          else {
            this.authService.presentToast('Please select your skill to continue.');
          }
        });
    }
  }

  selectCategory1(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected=true;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectCategory2(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected2=true;
    this.selected=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectCategory3(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected3=true;
    this.selected=false;
    this.selected2=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectCategory4(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected4=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectCategory5(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected5=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectCategory6(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected6=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected7=false;
    this.selected8=false;
  }
  selectCategory7(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected7=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected8=false;
  }
  selectCategory8(i){
    this.count++;
    this.selectedCategory=i;
    console.log(this.selectedCategory);
    this.postDetail.postCategory= this.selectedCategory;
    this.selected8=true;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
  }
  unSelectCategory(){
    this.count=0;
    this.selected=false;
    this.selected2=false;
    this.selected3=false;
    this.selected4=false;
    this.selected5=false;
    this.selected6=false;
    this.selected7=false;
    this.selected8=false;
  }

  goToPost(){
    if(this.editPost){
      this.postDetail.postUrl=this.imgUrl;
      let navigationExtras:NavigationExtras = {
        queryParams: {
         post:JSON.stringify(this.postDetail),
          categoryType: this.selectedCategory,
          imgUrl: this.imgUrl,
          editPost:true,
          pageRoute:this.pageRoute,
          
        }
      };
      this.router.navigate(['post'],navigationExtras);
    }else if(this.addPost){
      this.postDetail.postUrl=this.imgUrl;
      let navigationExtras:NavigationExtras = {
        queryParams: {
          postDetail:JSON.stringify(this.postDetail),
          categoryType: this.selectedCategory,
          imgUrl: this.imgUrl,
          addPost:true,
          pageRoute:this.pageRoute,
          postType:this.postType
          
        }
      };
      this.router.navigate(['post'],navigationExtras);
    }
   
    
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalpagePage,
      cssClass: 'modal',
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
          if (data) {
            this.router.navigateByUrl('profile');
          }else{
            this.router.navigateByUrl('profile');
          }
  }

  back(){
    let navigationExtras:NavigationExtras = {
      queryParams: {
        hobbySelection:true
      }
    };
    this.router.navigate(['hobby'],navigationExtras);
      // this.router.navigateByUrl('hobby');
      this.storage.remove("userHobby");
      console.log(this.selectedHobby);
  }

}
