import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
declare var RazorpayCheckout: any;

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {

  pageRoute;
  userId;
  walletBalance;
  transactionData:any=[];
  userData:any={};

  constructor(public router: Router,public route: ActivatedRoute,public storage: Storage,
    public alertController: AlertController,public authService: AuthService) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pageRoute=params.special;
      }
    });
   }

  ngOnInit() {
    this.storage.get('user').then((user)=>{
      this.userData=user;
      this.userId=user._id;
      this.loadData();
    });
  }

  loadData(){
    this.transactionData=[];
    var obj={"userId":this.userId};
    this.authService.getTransaction(obj).subscribe((data:any)=>{
      console.log(data);
      this.walletBalance=data.data.wallet;
      this.userData=data.data;
      this.transactionData=data.transactionData;  
    });
  }

  notification() {
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['notification'],navigationExtras);
  }
  back(){
    this.router.navigateByUrl(this.pageRoute);
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'walletAlert',
      header: 'Enter Amount',
      inputs: [
        {
          name: 'amount',
          type: 'text',
          placeholder: '₹ 0.00'
        }
      ],
      buttons: [
        {
          text: 'Add Funds',
          role: 'submit',
          cssClass: 'button',
          handler: (alertData) => {
            console.log(alertData);
            var amount=alertData.amount;
            this.payWithRazor(amount);
          }
        }
      ]
    });

    await alert.present();
    const result = await alert.onDidDismiss();  
    console.log(result);
  }

  payWithRazor(x) {
    var y=parseInt(x);
    var options = {
      description: 'Add money to wallet',
      image: 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
      currency: 'INR',
      key: 'rzp_test_sbuNGT8VxLZqff',
      amount: y*100,
      theme: {
        color: '#02CBEE',
      },
      modal: {
        ondismiss: function(){
          this.authService.presentAlert('Transaction Cancelled');
        }
      }
    };

    var self=this;

    var successCallback = function (payment_id) {
      // var obj = {'userId':self.userId,'amount':y}
      // self.authService.addMoney(obj).subscribe((data:any) => {
      //   console.log(data);
      // })
      var obj = {'userId':self.userId,'title':self.userData.fullName,
          'transactionImage':'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/credit-card.svg?alt=media&token=6b8e1044-7823-472f-a114-b8f36d74c3b2',
          'transactionAmount':y,
          'transactionDate':moment().format('DD/MMM/YYYY'),'isAddingMoney':true,
          'transactionTime':moment().format("h:mm ' A"),
        }
          self.authService.addMoney(obj).subscribe((data:any) => { 
            if(data.success){    
              console.log(data);  
             
              self.userData.wallet=self.walletBalance+y;
              self.storage.set('user',self.userData) ;   
              self.loadData(); 

            }
          })
     const successTransaction = self.authService.presentAlertSuccess('Transaction Completed');
     if(successTransaction){
       self.loadData();
     }
    };

    var cancelCallback = function (error) {
      self.authService.presentAlert('Transaction Cancelled');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

  // 'transactionImage':

}
