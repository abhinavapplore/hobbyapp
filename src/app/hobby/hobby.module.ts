import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HobbyPageRoutingModule } from './hobby-routing.module';

import { HobbyPage } from './hobby.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HobbyPageRoutingModule
  ],
  declarations: [HobbyPage]
})
export class HobbyPageModule {}
