import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalpagePage } from '../modalpage/modalpage.page'
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-hobby',
  templateUrl: './hobby.page.html',
  styleUrls: ['./hobby.page.scss'],
})
export class HobbyPage implements OnInit {
 data:boolean=false;
 selected:boolean=false;
 selected2:boolean=false;
 selected3:boolean=false;
 selected4:boolean=false;
 selected5:boolean=false;
 selected6:boolean=false;
 selected7:boolean=false;
 selected8:boolean=false;
 isUpdate:boolean=false;
 hobby1;
 hobby2;
 hobby3;
 hobby4;
 hobby5;
 hobby6;
 hobby7;
 hobby8;
 hobby1Image;
 hobby2Image;
 hobby3Image;
 hobby4Image;
 hobby5Image;
 hobby6Image;
 hobby7Image;
 hobby8Image;
 pageroute;
 count=0;
 hobbyArray:any=[];
 userData:any={};

  constructor(public router: Router,public modalController: ModalController,
    public storage: Storage,public route: ActivatedRoute,public authService: AuthService) {
      this.route.queryParams.subscribe(params => {
        if (params && params.isUpdate) {
          this.isUpdate=params.isUpdate;
          this.pageroute=params.pageroute;
          console.log(this.isUpdate);
        }else if(params && params.hobbySelection){
          this.selected=false;
          this.selected2=false;
          this.selected3=false;
          this.selected4=false;
          this.selected5=false;
          this.selected6=false;
          this.selected7=false;
          this.selected8=false;
          this.count=0;
          this.storage.remove("userHobby");
          this.hobbyArray=[];
        }
      });
     }

  ngOnInit() {
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.userData=user;
    })
  }

  selectHobby(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected=false;
      this.count=4;
    }else {
      this.selected=true;
      this.hobby1=i;
      this.hobby1Image=j;
    }
  }
  unSelectHobby(){
    this.count--;
    console.log(this.count);
    this.selected=false;
    this.hobby1='';
  }
  selectHobby2(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected2=false;
      this.count=4;
    }else {
      this.selected2=true;
      this.hobby2=i;
      this.hobby2Image=j;
    }
  }
  unSelectHobby2(){
    this.count--;
    console.log(this.count);
    this.selected2=false;
    this.hobby2='';
  }
  selectHobby3(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected3=false;
      this.count=4;
    }else {
      this.selected3=true;
      this.hobby3=i;
      this.hobby3Image=j;
    }
  }
  unSelectHobby3(){
    this.count--;
    console.log(this.count);
    this.selected3=false;
    this.hobby3='';
  }
  selectHobby4(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected4=false;
      this.count=4;
    }else {
      this.selected4=true;
      this.hobby4=i;
      this.hobby4Image=j;
    }
  }
  unSelectHobby4(){
    this.count--;
    console.log(this.count);
    this.selected4=false;
    this.hobby4='';
  }
  selectHobby5(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected5=false;
      this.count=4;
    }else {
      this.selected5=true;
      this.hobby5=i;
      this.hobby5Image=j;
    }
  }
  unSelectHobby5(){
    this.count--;
    console.log(this.count);
    this.selected5=false;
    this.hobby5='';
  }
  selectHobby6(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected6=false;
      this.count=4;
    }else {
      this.selected6=true;
      this.hobby6=i;
      this.hobby6Image=j;
    }
  }
  unSelectHobby6(){
    this.count--;
    console.log(this.count);
    this.selected6=false;
    this.hobby6='';
  }
  selectHobby7(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected7=false;
      this.count=4;
    }else {
      this.selected7=true;
      this.hobby7=i;
      this.hobby7Image=j;
    }
  }
  unSelectHobby7(){
    this.count--;
    console.log(this.count);
    this.selected7=false;
    this.hobby7='';
  }
  selectHobby8(i,j){
    this.count++;
    console.log(this.count);
    if(this.count>4){
      this.authService.presentToast('Only 4 hobby can be selected.');
      this.selected8=false;
      this.count=4;
    }else {
      this.selected8=true;
      this.hobby8=i;
      this.hobby8Image=j;
    }   
  }
  unSelectHobby8(){
    this.count--;
    console.log(this.count);
    this.selected8=false;
    this.hobby8='';
  }
  next(){
    if(this.hobby1!='' && this.hobby1!=undefined){
      this.hobbyArray.push({'hobby':this.hobby1,'hobbyImage':this.hobby1Image});
    }
    if(this.hobby2!='' && this.hobby2!=undefined){
      this.hobbyArray.push({'hobby':this.hobby2,'hobbyImage':this.hobby2Image});
    }
    if(this.hobby3!='' && this.hobby3!=undefined){
      this.hobbyArray.push({'hobby':this.hobby3,'hobbyImage':this.hobby3Image});
    }
    if(this.hobby4!='' && this.hobby4!=undefined){
      this.hobbyArray.push({'hobby':this.hobby4,'hobbyImage':this.hobby4Image});
    }
    if(this.hobby5!='' && this.hobby5!=undefined){
      this.hobbyArray.push({'hobby':this.hobby5,'hobbyImage':this.hobby5Image});
    }
    if(this.hobby6!='' && this.hobby6!=undefined){
      this.hobbyArray.push({'hobby':this.hobby6,'hobbyImage':this.hobby6Image});
    }
    if(this.hobby7!='' && this.hobby7!=undefined){
      this.hobbyArray.push({'hobby':this.hobby7,'hobbyImage':this.hobby7Image});
    }
    if(this.hobby8!='' && this.hobby8!=undefined){
      this.hobbyArray.push({'hobby':this.hobby8,'hobbyImage':this.hobby8Image});
    }
    // this.hobbyArray=[];
    this.storage.set("userHobby",this.hobbyArray).then((res)=>{
    console.log(res);
    if(!this.isUpdate && this.hobbyArray.length!=0){
      this.presentModal();
    }else if(this.isUpdate && this.hobbyArray.length!=0){
      var obj={"key":"userHobby","value":this.hobbyArray,"userId":this.userData._id}
      this.authService.updateProfile(obj).subscribe((data:any)=>{
        console.log(data);
        if(this.isUpdate){
          this.router.navigateByUrl(this.pageroute);
        }else {
          this.router.navigateByUrl('login');
        }
      });
    }
   
    
      
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalpagePage,
      cssClass: 'modal',
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
          if (data) {
            console.log(data);
          }
  }

  back(){
    if(this.isUpdate){
      this.router.navigateByUrl(this.pageroute);
    }else {
      this.router.navigateByUrl('login');
      // this.selected=false;
      // this.selected2=false;
      // this.selected3=false;
      // this.selected4=false;
      // this.selected5=false;
      // this.selected6=false;
      // this.selected7=false;
      // this.selected8=false;
    }
  }

}
