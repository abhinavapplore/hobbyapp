import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HobbyPage } from './hobby.page';

describe('HobbyPage', () => {
  let component: HobbyPage;
  let fixture: ComponentFixture<HobbyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HobbyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HobbyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
