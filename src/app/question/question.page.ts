import { Component, OnInit, ViewChild, ElementRef, NgZone, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {
@Input ('postData') postData;
message;
postUrl;
  constructor(public modalController: ModalController,
    private socialSharing: SocialSharing,public authService:AuthService) { }

  ngOnInit() {
    this.message= this.postData.postTitle+' \n'+this.postData.postDescription;
    this.postUrl= this.postData.postUrl;
    console.log(this.message);
    console.log(this.postUrl);
  }

  facebookShare(){
   
     this.socialSharing.shareViaFacebook(this.message,this.postUrl,null).then((success) =>{
      console.log(success);
  })
  .catch((err)=>{
     this.authService.presentToast("Could not share information");
     console.log(err);
   });
   }
  
   twitterShare(){
  
    this.socialSharing.shareViaTwitter(this.message,this.postUrl, null).then((success) =>{
      console.log(success);
     
  })
  .catch((err)=>{
    this.authService.presentToast("Could not share information");
    console.log(err);
   });
  }
  
  whatsappShare(){
    
     this.socialSharing.shareViaWhatsApp(this.message,this.postUrl, null).then((success) =>{
       console.log(success);
  })
  .catch((err)=>{
    this.authService.presentToast("Could not share information");
    console.log(err);
   });
   }
  
   instagramShare(){
    
     this.socialSharing.shareViaInstagram(this.message,this.postUrl).then((success) =>{
      console.log(success);
  })
  .catch((err)=>{
    this.authService.presentToast("Could not share information");
    console.log(err);
   });
   }

   closeModal(){
     this.modalController.dismiss();
   }
  

}
