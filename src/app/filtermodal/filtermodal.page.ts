import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {AuthService} from '../auth.service';
import { Storage } from '@ionic/storage';
import { NavigationExtras, Router } from '@angular/router';
import { JsonPipe } from '@angular/common';


@Component({
  selector: 'app-filtermodal',
  templateUrl: './filtermodal.page.html',
  styleUrls: ['./filtermodal.page.scss'],
})
export class FiltermodalPage implements OnInit {

  distance:number=0;
  filterProfessionalUser:any=[];
  filterPassionistUser:any=[];
  allFilterUser:any=[];

  constructor(public modalController: ModalController,public authService: AuthService,
    public storage: Storage,public router: Router) { }

  ngOnInit() {
    console.log(this.distance);
  }

  skill($event,index){
    console.log($event);
    this.distance=$event.detail.value;
    console.log(this.distance);
  }

 

  apply(){
    this.storage.get('user').then((user)=>{
      var obj={'userId':user._id,'latitude':JSON.parse(user.userLatitude),
      'longitude':JSON.parse(user.userLongitude),'distance':this.distance};
      this.authService.filterByDistance(obj).subscribe((data:any)=>{
        if(data.success){
          console.log(data);
          data.data.forEach(element => {
            if(element._id==user._id){
              console.log('user id match');
            }else if(element._id!=user._id && element.userType=="professional" ) {
              this.filterProfessionalUser.push(element);
              
              // if(this.filterProfessionalUser.length!=0){
              //   this.storage.set('filterProfessionalUser',this.filterProfessionalUser);
              // }
              
              // let navigationExtras:NavigationExtras = {
              //   queryParams: {
              //     special: JSON.stringify(this.filterProfessionalUser),
              //     userType: "professional"
              //   }
              // };
              // this.router.navigate(['tabs/tab4'],navigationExtras);
              // this.modalController.dismiss();
            }else if(element._id!=user._id && element.userType=="passionist" ) {
              this.filterPassionistUser.push(element);
              
              // if(this.filterPassionistUser.length!=0){
              //   this.storage.set('filterPassionistUser',this.filterPassionistUser);
              // }
              
              // let navigationExtras:NavigationExtras = {
              //   queryParams: {
              //     special: JSON.stringify(this.filterPassionistUser),
              //     userType: "passionists"
              //   }
              // };
              // this.router.navigate(['tabs/tab4'],navigationExtras);
              // this.modalController.dismiss();
            }
          }); 
          this.allFilterUser.push({'filterProfessionalUser':this.filterProfessionalUser});
          this.allFilterUser.push({'filterPassionistUser':this.filterPassionistUser});
          this.modalController.dismiss(this.allFilterUser);        
        }else {
          this.modalController.dismiss();
          this.authService.presentToast('No user found');
        }
      });
    });
    
  }

  closeModal(){
    this.modalController.dismiss();
  }

}
