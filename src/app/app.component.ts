import { Component } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavigationExtras } from '@angular/router';
import { AuthService } from './auth.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  userName;
  userImg;
  userId;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router,
    private menu: MenuController,
    public storage: Storage,private fcm: FCM,
    public authService: AuthService,public navCtrl:NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.loadUser();

this.fcm.onNotification().subscribe(data => {
  if(data.wasTapped){
    console.log("Received in background");
    if(data.notificationType=="unlockUser"){
      this.router.navigateByUrl('notification');
    }else if(data.notificationType=='postNotification'){
      this.router.navigateByUrl('tabs/tab1')
    }else {
      var vc={
        senderId:data.userId,
        recieverId: data.hostId,
        senderName:data.userName,
        recieverName:data.hostName,
        recieverImg:data.hostImg,
      }
      let navigationExtras:NavigationExtras = {
        queryParams: {
          special: vc
        }
      };
      this.router.navigate(['chat'],navigationExtras);
    }
  } else {
    console.log("Received in foreground");
  };
});

this.fcm.onTokenRefresh().subscribe(token => {
  var obj={"key":"deviceId","value":token,"userId":this.userId};
  this.authService.updateProfile(obj).subscribe((data:any)=>{
    if(data.success){
      console.log(data);
    }else {
      console.log(data);
    }
  })
});

      // this.router.navigateByUrl('tabs/tab1');
      // this.storage.get('user').then((user)=>{
      //   this.userId=user._id;
      //   var obj={"userId":this.userId};

      //   this.userName=user.fullName;
      //   this.userImg=user.user_img;
      //   if(user==null || user==undefined){
      //     this.router.navigateByUrl('login');
      //   }else {
      //     this.router.navigateByUrl('tabs/tab1');
      //   }
      // });
      this.authService.authSubject.subscribe(state=>{
        if(state){
           this.navCtrl.navigateRoot('tabs/tab4');
          // this.navCtrl.navigateRoot('likes');
        }else{
          this.navCtrl.navigateRoot('login');
        }
      })
    });
  }


  loadUser(){
    this.storage.get('user').then((user)=>{
      if(user==null||user==undefined){
        this.navCtrl.navigateRoot('login');
      }else {
        this.userId=user._id;
    
        this.userName=user.fullName;
          this.userImg=user.user_img;
      }
    });
  }

  wallet(){
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['wallet'],navigationExtras);
    this.menu.close();
  }

  profile(){
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['profile2'],navigationExtras);
    this.menu.close();
  }

  feedback(){
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['feedback'],navigationExtras);
    this.menu.close();
  }

  home(){
    this.menu.close();
    this.router.navigateByUrl('tabs/tab4');
  }

  logout() {
    this.menu.close();
   this.authService.logout();
 
  }

  userList(){
    this.menu.close();
    this.router.navigateByUrl('blocked');
  }

}
