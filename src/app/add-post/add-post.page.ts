import { Component, OnInit, Sanitizer } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import {ActionSheetController, Platform, ModalController} from '@ionic/angular';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.page.html',
  styleUrls: ['./add-post.page.scss'],
})
export class AddPostPage implements OnInit {

  pageRoute;
  selected:boolean=false;
  imgSelected;
  photoArray:any=[];
  data:boolean=false;
  uploaded:boolean=false;
  imgUrl;

  uploadPercent: Observable<number>;

  cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
   }

   gelleryOptions: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    }

  constructor(public route: ActivatedRoute, public platform:Platform,
     public modalController: ModalController,public authService: AuthService,
    private angularstorage: AngularFireStorage,private file: File, 
    private filePath: FilePath, public httpClient:HttpClient, 
    public actionSheetController: ActionSheetController, public storage: Storage,
    private camera: Camera,public router: Router,
    public domSanitizer: DomSanitizer) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pageRoute=params.special;
      }
    });
  }

  ngOnInit() {
  }

  

  back(){
    this.router.navigateByUrl(this.pageRoute);
  }

  selectedImg(x) {
    this.imgSelected=x;
    
  }

  next(){
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['post'],navigationExtras);
  }

}
