import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import {ActionSheetController, Platform, ModalController} from '@ionic/angular';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import 'firebase/storage';
declare const google;

@Component({
  selector: 'app-profile2',
  templateUrl: './profile2.page.html',
  styleUrls: ['./profile2.page.scss'],
})
export class Profile2Page implements OnInit {

  pageRoute;
  userData:any={};
  userAddress;
  aboutUser:boolean=false;
  updateAddress:boolean=false;
  about;
  imgUrl1;
  uploaded1;
  uploaded2;
  uploaded3;
  latitude;
  longitude;
  map;
  doc1;
  doc2;
   
  uploadPercent: Observable<number>;

  cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
   }

   gelleryOptions: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    }

  constructor(public router: Router,public route: ActivatedRoute,public storage: Storage,
    public authService: AuthService,public platform:Platform, public modalController: ModalController,
    private angularstorage: AngularFireStorage,private file: File, 
    private filePath: FilePath, public httpClient:HttpClient, private photoViewer: PhotoViewer,
    public actionSheetController: ActionSheetController,public geolocation: Geolocation,
    private camera: Camera,) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pageRoute=params.special;
        this.loadData();
      }
    });
   }

  slideOpts = {
    slidesPerView: 2,
    spaceBetween: 10,
    initialSlide:0,
  };

  ngOnInit() {
    console.log(this.about);
   this.loadData();
  }

  loadData(){
    this.storage.get('user').then((user)=>{
      var obj={"userId":user._id};
      this.authService.getUserProfileById(obj).subscribe((data:any)=>{
        console.log(data);
        this.userData=data.data;
        
        this.doc1=this.userData.documents[0].docUrl;
        this.doc2=this.userData.documents[1].docUrl;
      })
      
    });
  }

  ionViewDidEnter(){
    this.getUserProfileById();
  }

  viewPhoto(x){
    console.log(x);
    this.photoViewer.show(x);
  }

  notification() {
    let navigationExtras:NavigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
    this.router.navigate(['notification'],navigationExtras);
  }

  updateAboutUser(){
    var obj={"key":"about","value":this.about,"userId":this.userData._id};
    this.authService.updateProfile(obj).subscribe((data:any)=>{
      console.log(data);
      this.getUserProfileById();
    })
  }

  async presentActionSheet1(i) {
      const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                 this.takePicture1(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                  
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                 this.takePicture1(this.camera.PictureSourceType.CAMERA,i);
                 
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
      await actionSheet.present();  
    }
    


  async takePicture1(sourceType,i) {
    if(this.platform.is('ios')){

      const options: CameraOptions = {
      quality: 100,
      targetWidth: 900,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      encodingType: this.camera.EncodingType.JPEG
      };
      
      const tempImage = await this.camera.getPicture(options);
      const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
      
      // Now, the opposite. Extract the full path, minus filename.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
      const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
      
      // Get the Data directory on the device.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
      const newBaseFilesystemPath = this.file.dataDirectory;
      await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
      newBaseFilesystemPath, tempFilename);
      
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
      const storedPhoto = newBaseFilesystemPath + tempFilename;
      this.file.resolveLocalFilesystemUrl(storedPhoto)
      .then(entry => {
      ( < FileEntry > entry).file(file => this.readFile(file, i))
      })
      .catch(err => {
      console.log(err);
      // this.presentToast('Error while reading file.');
      });
      
      }else{
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          encodingType: this.camera.EncodingType.JPEG,
          
        };
        this.camera.getPicture(options).then((imageData) => {
          // this._file.resolveLocalFilesystemUrl(
          //   imageData,
          //   (entry: FileEntry) => {console.log(entry)},
          //   err => console.log(err)
          // );
          this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
            entry.file(file => {
              console.log(file);
              this.readFile(file,i);
            });
          });
        }, (err) => {
          // Handle error
       
        });
      }  
  }

  readFile(file: any,i) {
    const reader = new FileReader();
    reader.onload = () => {
    // const formData = new FormData();
    const imgBlob = new Blob([reader.result], {
    type: file.type
    });
    // formData.append('file', imgBlob, file.name);
    // this.uploadImageData(formData);
    if(i===1){
    this.upload2Firebase1(imgBlob);
    }else if(i===2){
      this.upload2Firebase2(imgBlob);
    }else if(i===3){
      this.upload2Firebase3(imgBlob);
    }
    else{
    console.log("if second image");
    }
    
    };
    reader.readAsArrayBuffer(file);
    }

    makeid(length) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
      }
 
      
      async upload2Firebase1(image) {
      this.authService.loading('Loading Profile..');
      const file = image;
      const filePath = this.makeid(5);
      const fileRef = this.angularstorage.ref(filePath);
      //const newFile = new File(file);
      // let newFile= file.getURL().getFile();
      
      
      const task = this.angularstorage.upload(filePath, file);
      console.log(filePath);
      console.log(file);
      // observe percentage changes
      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      
      await task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(value => {
 
      this.imgUrl1 = value;
      console.log(this.imgUrl1);
      this.uploaded1 = true;
      var obj={"key":"user_img","value":this.imgUrl1,"userId":this.userData._id}
      this.authService.updateProfile(obj).subscribe((data:any)=>{
        console.log(data);
        if(data.success){
          this.authService.dismissLoading();
        }
      });  
      }))
      )
      .subscribe()
      
      }

      async upload2Firebase2(image) {
        this.authService.loading('Loading Profile..');
        const file = image;
        const filePath = this.makeid(5);
        const fileRef = this.angularstorage.ref(filePath);
        //const newFile = new File(file);
        // let newFile= file.getURL().getFile();
        
        
        const task = this.angularstorage.upload(filePath, file);
        console.log(filePath);
        console.log(file);
        // observe percentage changes
        this.uploadPercent = task.percentageChanges();
        // get notified when the download URL is available
        
        await task.snapshotChanges().pipe(
        finalize(() => fileRef.getDownloadURL().subscribe(value => {
   
        this.doc1 = value;
        console.log(this.doc1);
        this.uploaded2 = true;
        var obj={"key":"documents['doc1']","value":this.doc1,"userId":this.userData._id}
        this.authService.updateProfile(obj).subscribe((data:any)=>{
          console.log(data);
          if(data.success){
            this.authService.dismissLoading();
          }
        });  
        }))
        )
        .subscribe()
        
        }

        async upload2Firebase3(image) {
          this.authService.loading('Loading Profile..');
          const file = image;
          const filePath = this.makeid(5);
          const fileRef = this.angularstorage.ref(filePath);
          //const newFile = new File(file);
          // let newFile= file.getURL().getFile();
          
          
          const task = this.angularstorage.upload(filePath, file);
          console.log(filePath);
          console.log(file);
          // observe percentage changes
          this.uploadPercent = task.percentageChanges();
          // get notified when the download URL is available
          
          await task.snapshotChanges().pipe(
          finalize(() => fileRef.getDownloadURL().subscribe(value => {
     
          this.doc2 = value;
          console.log(this.doc2);
          this.uploaded3 = true;
          var obj={"key":"documents['doc2']","value":this.doc2,"userId":this.userData._id}
          this.authService.updateProfile(obj).subscribe((data:any)=>{
            console.log(data);
            if(data.success){
              this.authService.dismissLoading();
            }
          });  
          }))
          )
          .subscribe()
          
          }


      testmap() {
        var myLatlng = new google.maps.LatLng(this.latitude,this.longitude);
        console.log(myLatlng)
        var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeControl: false,
        scaleControl: false,
        zoomControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: false,
        styles: [{
        stylers: [{
        saturation: -100
               }]
             }],  
            }
        this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        //Add User Location Marker To Map
        var marker = new google.maps.Marker({
        position: myLatlng, 
        draggable:true   
           });
        // To add the marker to the map, call setMap();
        marker.setMap(this.map)
    
        marker.addListener('dragend', function(event){
          console.log(event);
          this.latitude = event.latLng.lat();
          console.log(this.latitude);
          this.longitude = event.latLng.lng();
          console.log(this.longitude);
        });
        
        //Map Click Event Listner
        this.map.addListener('click', function() {
        //add functions here
        });
      }

      updateAdd(){
        this.updateAddress=true;        
      }

      update(){
        this.geolocation.getCurrentPosition().then((resp) => {
          // resp.coords.latitude
          // this.latitude= resp.coords.latitude;
          // console.log(this.latitude);
          // resp.coords.longitude
          // this.longitude=resp.coords.longitude;
          // console.log(this.longitude);
          this.latitude=80
          this.longitude=90
          this.testmap();
          var obj={"latitude":this.latitude,"longitude":this.longitude}
          this.authService.updateAddress(obj).subscribe((data:any)=>{
            console.log(data);
            this.getUserProfileById();
            this.updateAddress=false;
          })
        });
      }


  editInterest(){
    let navigationExtras:NavigationExtras = {
      queryParams: {
        isUpdate: true,
        pageroute: this.router.url,
        
      }
    };
    this.router.navigate(['hobby'],navigationExtras);
  }

  getUserProfileById(){
    var obj={"userId":this.userData._id}
    this.authService.getUserProfileById(obj).subscribe((data:any)=>{
      console.log(data);
    })
  }


  


  back(){
    this.router.navigateByUrl(this.pageRoute);
  }

}
