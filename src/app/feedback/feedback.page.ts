import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

  pageRoute;

  constructor(public router: Router,public route: ActivatedRoute,
    public authSerivice: AuthService) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pageRoute=params.special;
      }
    });
  }

  ngOnInit() {
  }

  submit(){
    this.authSerivice.presentToast('Thanks for your feedback.');
    this.router.navigateByUrl(this.pageRoute);
  }

  back() {
    this.router.navigateByUrl(this.pageRoute);
  }

  notification(){
    let navigationExtras = {
      queryParams: {
        special: this.router.url
      }
    };
  
    this.router.navigate(['notification'], navigationExtras);
  }

}
