import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  pageRoute;
  userId;
  walletBalance;
  notificationData:any=[];
  userData:any={};

  constructor(public router: Router,public route: ActivatedRoute,
    public storage: Storage,public authService: AuthService) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pageRoute=params.special;
      }
    });
  }

  ngOnInit() {
    this.storage.get('user').then((user)=>{
      this.userData=user;
      this.userId=user._id;
      this.loadData();
    });
  }

  loadData(){
    this.notificationData=[];
    var obj={"userId":this.userId};
    this.authService.getNotification(obj).subscribe((data:any)=>{
      console.log(data);
      if(data.success){
        this.walletBalance=data.data.wallet;
        this.notificationData=data.notificationData;
      }      
    });
  }

  back(){
    this.router.navigateByUrl(this.pageRoute);
  }

}
