import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import {ActionSheetController, Platform, ModalController, NavController, 
  AlertController, LoadingController} from '@ionic/angular';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { AuthService } from '../auth.service';
import * as firebase from 'firebase/app';
import { Keyboard } from '@ionic-native/keyboard/ngx';

// import { VideoEditor,CreateThumbnailOptions } from '@ionic-native/video-editor/ngx';

const baseUrl = "https://example.com";
const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
//changes

files = [];
cloudFiles = [];
uploadProgress = 0;
//end
  uploaded:boolean=false;
  imgUrl;
  selectedVideo:any= "";
  uploadedVideo: string;
  isUploading: boolean = false;
  footerAction:boolean = true;
  loader;
  videoUrl;
  capturedSnapURL;

  uploadPercent: Observable<number>;

  cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
   }

   gelleryOptions: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    }

    

  constructor(public platform:Platform,
    public modalController: ModalController,public authService: AuthService,
   private angularstorage: AngularFireStorage,private file: File, 
   private filePath: FilePath, public httpClient:HttpClient, private keyboard: Keyboard,
   public actionSheetController: ActionSheetController, public storage: Storage,
   private camera: Camera,public router: Router,public navCtrl: NavController,
   public alertCtrl: AlertController,private loadingCtrl: LoadingController,
  
) {
    this.keyboard.onKeyboardWillShow().subscribe(data => {
      this.footerAction=false;
      
    });
    this.keyboard.onKeyboardWillHide().subscribe(data => {
      this.footerAction=true;
      
    });
   }
  async addPost(i) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
              text: 'Load from Library',
              handler: () => {
               this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,i);
                
              }
          },
          {
              text: 'Use Camera',
              handler: () => {
               this.takePicture(this.camera.PictureSourceType.CAMERA,i);
               
              }
          },
          {
            text: 'Upload Video',
            handler: () => {
             this.takePicture1(this.camera.PictureSourceType.PHOTOLIBRARY,i);
             
            }
        },
          {
              text: 'Cancel',
              role: 'cancel'
          }
      ]
  });
    await actionSheet.present();
  }

  async takePicture(sourceType,i) {
    if(this.platform.is('ios')){

      const options: CameraOptions = {
      quality: 100,
      targetWidth: 900,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      encodingType: this.camera.EncodingType.JPEG
      };
      
      const tempImage = await this.camera.getPicture(options);
      const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
      
      // Now, the opposite. Extract the full path, minus filename.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
      const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
      
      // Get the Data directory on the device.
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
      const newBaseFilesystemPath = this.file.dataDirectory;
      await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
      newBaseFilesystemPath, tempFilename);
      
      // Result example: file:///var/mobile/Containers/Data/Application
      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
      const storedPhoto = newBaseFilesystemPath + tempFilename;
      this.file.resolveLocalFilesystemUrl(storedPhoto)
      .then(entry => {
      ( < FileEntry > entry).file(file => this.readFile(file, i))
      })
      .catch(err => {
      console.log(err);
      // this.presentToast('Error while reading file.');
      });
      
      }else{
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          encodingType: this.camera.EncodingType.JPEG,
          
        };
        this.camera.getPicture(options).then((imageData) => {
          this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
            entry.file(file => {
              console.log(file);
              // let options1 = {
              //   uri: uri,
              //   folderName: 'Protonet',
              //   quality: 90,
              //   width: 1280,
              //   height: 1280
              //  } as ImageResizerOptions;
              //  this.imageResizer
              // .resize(options1)
              // .then((file: string) => console.log('FilePath', filePath))
              // .catch(e => console.log(e));
              this.readFile(file,i);
            });
          });
        }, (err) => {
          // Handle error
       
        });
      }
      
   
  }

  

  async takePicture1(sourceType,i) {

    const options: CameraOptions = {
      mediaType: this.camera.MediaType.VIDEO,
      sourceType: sourceType
    }

    this.camera.getPicture(options)
    .then( async (videoUrl) => {
      if (videoUrl) {
        this.authService.loading('Please Wait');
        this.uploadedVideo = null;
        
        var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
        var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);

        dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
        
        try {
          var dirUrl = await this.file.resolveDirectoryUrl(dirpath);
          var retrievedFile = await this.file.getFile(dirUrl, filename, {});

        } catch(err) {
          this.authService.dismissLoading();
          return this.authService.presentToast("Error Something went wrong.");
        }
        
        retrievedFile.file( data => {
          console.log(data);
            this.authService.dismissLoading();
            if (data.size > MAX_FILE_SIZE) return this.authService.presentToast("Error You cannot upload more than 5mb.");
            if (data.type !== ALLOWED_MIME_TYPE) return this.authService.presentToast("Error Incorrect file type.");

            this.selectedVideo = retrievedFile.nativeURL;
            console.log(this.selectedVideo);
              this.uploadFile(retrievedFile);
      
              
           
        
        });
      }
    },
    (err) => {
      console.log(err);
    });
}


loadFiles() {
  this.cloudFiles = [];

  const storageRef = firebase.storage().ref('files');
  storageRef.listAll().then(result => {
    result.items.forEach(async ref => {
      this.cloudFiles.push({
        name: ref.name,
        full: ref.fullPath,
        url: await ref.getDownloadURL(),
        ref: ref
      });
    });
  });
}



deleteFile(ref: firebase.storage.Reference) {
  ref.delete().then(() => {
    this.loadFiles();
  });
}

async uploadFile(f: FileEntry) {
  const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
  const type = this.getMimeType(f.name.split('.').pop());
  const buffer = await this.file.readAsArrayBuffer(path, f.name);
  const fileBlob = new Blob([buffer], type);

  const randomId = Math.random()
    .toString(36)
    .substring(2, 8);
this.upload2FirebaseVideo(fileBlob);
  // const uploadTask = this.angularstorage.upload(
  //   `files/${new Date().getTime()}_${randomId}`,
  //   fileBlob
  // );

  // uploadTask.percentageChanges().subscribe(change => {
  //   this.uploadProgress = change;
  // });

  // uploadTask.then(async res => {
  //   const toast = await this.authService.toastController.create({
  //     duration: 3000,
  //     message: 'File upload finished!'
  //   });
  //   toast.present();
  // });
}

getMimeType(fileExt) {
  if (fileExt == 'wav') return { type: 'audio/wav' };
  else if (fileExt == 'jpg') return { type: 'image/jpg' };
  else if (fileExt == 'mp4') return { type: 'video/mp4' };
  else if (fileExt == 'MOV') return { type: 'video/quicktime' };
}

    // if(this.platform.is('ios')){

    //   const options: CameraOptions = {
    //   quality: 100,
    //   targetWidth: 900,
    //   targetHeight: 600,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   sourceType: sourceType,
    //   encodingType: this.camera.EncodingType.JPEG
    //   };
      
    //   const tempImage = await this.camera.getPicture(options);
    //   const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
      
    //   // Now, the opposite. Extract the full path, minus filename.
    //   // Result example: file:///var/mobile/Containers/Data/Application
    //   // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
    //   const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
      
    //   // Get the Data directory on the device.
    //   // Result example: file:///var/mobile/Containers/Data/Application
    //   // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
    //   const newBaseFilesystemPath = this.file.dataDirectory;
    //   await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
    //   newBaseFilesystemPath, tempFilename);
      
    //   // Result example: file:///var/mobile/Containers/Data/Application
    //   // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
    //   const storedPhoto = newBaseFilesystemPath + tempFilename;
    //   this.file.resolveLocalFilesystemUrl(storedPhoto)
    //   .then(entry => {
    //   ( < FileEntry > entry).file(file => this.readFile(file, i))
    //   })
    //   .catch(err => {
    //   console.log(err);
    //   // this.presentToast('Error while reading file.');
    //   });
      
    //   }else{
    //     const options: CameraOptions = {
    //       quality: 100,
    //       destinationType: this.camera.DestinationType.FILE_URI,
    //       sourceType: sourceType,
    //       encodingType: this.camera.EncodingType.JPEG,
    //       mediaType:2
          
    //     };
    //     this.camera.getPicture(options).then((imageData) => {
    //       this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
    //         entry.file(file => {
    //           console.log(file);
    //           this.readFile(file,i);
    //         });
    //       });
    //     }, (err) => {
    //       // Handle error
       
    //     });
    //   }
      
   
  

  dismissLoader() {
    this.loader.dismiss();
  }

  // presentAlert(title, message) {
  //   let alert = this.alertCtrl.create({
  //     title: title,
  //     subTitle: message,
  //     buttons: ['Dismiss']
  //   });
  //   alert.present();
  // }

  readFile(file: any,i) {
    const reader = new FileReader();
    reader.onload = () => {
    // const formData = new FormData();
    const imgBlob = new Blob([reader.result], {
    type: file.type
    });
    // formData.append('file', imgBlob, file.name);
    // this.uploadImageData(formData);
    if(i===1){
    this.upload2Firebase(imgBlob);
    }
    else{
    console.log("if second image");
    }
    
    };
    reader.readAsArrayBuffer(file);
    }

    async upload2FirebaseVideo(video) {
      this.authService.loading('Loading Video..');
      const file = video;
      const filePath = this.makeid(5);
      const fileRef = this.angularstorage.ref(filePath);
      //const newFile = new File(file);
      // let newFile= file.getURL().getFile();
      
      
      const task = this.angularstorage.upload(filePath, file);
      console.log(filePath);
      console.log(file);
      // observe percentage changes
      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      
      await task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(value => {
 
      this.videoUrl = value;
      // var Options:CreateThumbnailOptions = {fileUri:value,width:160, height:206, atTime:1, outputFileName: 'sample', quality:50 };
      //         this.videoEditor.createThumbnail(Options).then(result=>{
      //             //result-path of thumbnail
      //            console.log(result);      
      //         }).catch(e=>{
      //           console.log(e);  
      //          // alert('fail video editor');
      //         });
      this.uploaded = true;
      console.log(this.videoUrl);
      this.authService.dismissLoading();
      // this.storage.set('imgUrl',this.imgUrl);
      // this.storage.set('isAddPost',true);
      let navigationExtras:NavigationExtras = {
        queryParams: {
          pageRoute: this.router.url,
          imgUrl: this.videoUrl,
          addPost:true,
          postType:"1"
          
        }
      };
      this.navCtrl.navigateRoot(['post'],navigationExtras);
   
      }))
      )
      .subscribe()
      
      }

    async upload2Firebase(image) {
      this.authService.loading('Loading Image..');
      const file = image;
      const filePath = this.makeid(5);
      const fileRef = this.angularstorage.ref(filePath);
      //const newFile = new File(file);
      // let newFile= file.getURL().getFile();
      
      
      const task = this.angularstorage.upload(filePath, file);
      console.log(filePath);
      console.log(file);
      // observe percentage changes
      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      
      await task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(value => {
 
      this.imgUrl = value;
      this.uploaded = true;
      this.authService.dismissLoading();
      // this.storage.set('imgUrl',this.imgUrl);
      // this.storage.set('isAddPost',true);
      let navigationExtras:NavigationExtras = {
        queryParams: {
          pageRoute: this.router.url,
          imgUrl: this.imgUrl,
          addPost:true,
          postType:"0"
        }
      };
      this.navCtrl.navigateRoot(['post'],navigationExtras);
   
      }))
      )
      .subscribe()
      
      }

    makeid(length) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
      }

}
