(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile1-profile1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile1/profile1.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile1/profile1.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-col [size]=2  (click)=\"back()\">\n        <ion-icon style=\"color:white;font-size:30px\" name=\"chevron-back\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text>Profile</ion-text>\n      </ion-col>\n      <ion-col [size]=2 (click)=\"notification()\">\n        <ion-icon style=\"color:white;font-size:30px\" name=\"notifications-outline\"></ion-icon>\n      </ion-col>    \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <div class=\"div\">\n\n      <ion-row class=\"row\">\n        <ion-thumbnail class=\"thumbnai3\">\n          <ion-img class=\"img\" [src]=\"professionalUser.user_img\"></ion-img>\n        </ion-thumbnail>\n      </ion-row>\n      <ion-row class=\"row1\">\n        <ion-text>Hi,I'm {{professionalUser.firstName}} {{professionalUser.lastName}}</ion-text>\n      </ion-row>\n      <ion-row class=\"row2\">\n        <ion-text>Joined in {{professionalUser.createdDate | date:' MMMM yyyy'}}</ion-text>\n      </ion-row>\n      <ion-row *ngIf='professionalUser.isVerified' class=\"row3\">\n        <ion-col [size]=9>\n          <ion-icon class=\"icon1\" name=\"ribbon-outline\"></ion-icon>\n          <ion-text>Identity Verified</ion-text>\n        </ion-col>\n        <ion-col [size]=3>\n          <ion-icon class=\"icon\" name=\"star\"></ion-icon>\n          <ion-text>{{professionalUser.rating}}/5</ion-text>\n        </ion-col>     \n      </ion-row>\n      <ion-row *ngIf='professionalUser.isVerified' class=\"row4\">\n        <ion-icon name=\"thumbs-up-outline\"></ion-icon>\n        <ion-text>40+ Ratings</ion-text>\n      </ion-row>\n      <ion-row *ngIf='professionalUser.about!=null || professionalUser.about!=undefined'\n       class=\"row5\">\n        <ion-text>About</ion-text>\n      </ion-row>\n      <ion-row *ngIf='professionalUser.about!=null || professionalUser.about!=undefined' \n      class=\"row6\">\n        <ion-text >{{professionalUser.about}} </ion-text>\n      </ion-row>\n\n      <div class=\"div3\" *ngIf=\"docUrl1!='' || docUrl2!=''\">\n        <ion-row class=\"row5\">\n          <ion-text>Certificates</ion-text>\n        </ion-row>\n        <ion-row class=\"row7\">\n          <ion-thumbnail *ngIf=\"docUrl1!='' || docUrl1!=null || docUrl1!=undefined\" \n          class=\"thumbnail\">\n            <ion-img class=\"img\" [src]=\"docUrl1\"></ion-img>\n          </ion-thumbnail>\n          <ion-thumbnail *ngIf=\"docUrl2!='' || docUrl2!=null || docUrl2!=undefined\"\n           class=\"thumbnail\">\n            <ion-img class=\"img\" [src]=\"docUrl2\"></ion-img>\n          </ion-thumbnail>\n        </ion-row>\n      </div>\n      \n      <ion-row class=\"row5\" *ngIf=\"professionalUser.userSkills!=null\">\n        <ion-text>Skills</ion-text>\n      </ion-row>\n      <ion-row class=\"row8\" *ngIf=\"professionalUser.userSkills!=null\">\n        <ion-slides [options]=\"slideOpts\" style=\"margin: 0;\">\n          <ion-slide *ngFor='let x of professionalUser.userSkills'>\n            <ion-card class=\"container\">\n              <ion-thumbnail class=\"thumbnail2\">\n                <ion-img class=\"imgSelected\" [src]=\"x.hobbyImage\"></ion-img>\n              </ion-thumbnail>        \n              <div class=\"centered\">\n                <ion-text>{{x.hobby}}</ion-text>\n              </div>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-row>\n\n\n      <ion-row class=\"row5\" *ngIf=\"userPosts.length!=0\">\n        <ion-text>Posts</ion-text>\n      </ion-row>\n\n      <ion-row style=\"width: 100%;\" *ngIf=\"userPosts.length!=0\"> \n\n      <ion-slides [options]=\"slideOpts1\" pager=true>\n        <ion-slide  *ngFor='let item of userPosts; let i=index'>\n        <ion-card style=\"width: 100%;height: 280px;\">\n          <ion-row style=\"height: 200px;\">\n            <ion-thumbnail (click)=\"viewPhoto(item.postUrl)\" *ngIf=\"item.posttype=='0'\" style=\"height: 200px;\n            width: 410px;\">\n              <ion-img [src]='item.postUrl'></ion-img>\n            </ion-thumbnail>\n            <div *ngIf=\"item.posttype=='1'\">\n              <video #player playsinline preload=\"auto\" controls>\n                <source [src]=\"item.postUrl\" type=\"video/mp4\"/>\n              </video>\n            </div>        \n          </ion-row>\n    \n          <ion-row>\n            <ion-col [size]=6 style=\"display: flex;\n            flex-direction: row;\">\n    \n            <div style=\"display: flex;\n              flex-direction: row;\n              justify-content: center;\n              align-items: center;\n              font-family: 'Poppins-Bold';\n              font-size: 16px;\n              color: #767474;width: 50px;\">\n                <ion-icon style=\"font-size: 30px;\" *ngIf='!item.isliked' (click)='likePost(item,i)' class=\"icon1\" \n                name=\"heart-outline\"></ion-icon>\n                <ion-icon style=\"font-size: 30px;color: red;\" *ngIf='item.isliked' (click)='deleteLike(item,i)' class=\"icon3\" \n                name=\"heart\"></ion-icon>\n                <ion-text style=\"margin-left: 15%;\">{{item.likeCount}}</ion-text>\n            </div>\n    \n              <div style=\"display: flex;\n              flex-direction: row;\n              justify-content: center;\n              align-items: center;\n              font-family: 'Poppins-Bold';\n              font-size: 16px;margin-left: 2%;\n              color: #767474;width: 50px;\" *ngIf=\"item.posttype=='1'\">\n                <img src=\"../../assets/images/home/eye.svg\">\n                <ion-text style=\"margin-left: 15%;\">{{item.viewCount}}</ion-text>\n            </div>\n            <img style=\"margin-left: 5%;\" (click)=share(item) \n            src=\"../../assets/images/home/share.svg\">\n            </ion-col>\n            <ion-col [size]=6 style=\"display: flex;flex-direction: row;justify-content: flex-end;\">\n              <ion-icon style=\"font-size: 30px;\" color=black *ngIf='!item.isBookmarked' (click)='bookmark(item,i)' \n              class=\"icon1\" name=\"bookmark-outline\"></ion-icon>\n              <ion-icon style=\"font-size: 30px;\" color='primary' *ngIf='item.isBookmarked' (click)='deleteBookmark(item,i)' \n              class=\"icon1\" name=\"bookmark\"></ion-icon>\n            </ion-col>    \n          </ion-row>\n    \n          <ion-row style=\"margin-top: 2%;\" *ngIf=\"item.posttype=='0'\">\n            <ion-text style=\"margin-left: 2%;\" class=\"text2\">{{item.postTitle}}</ion-text>\n          </ion-row>\n        </ion-card>                 \n      </ion-slide>\n      </ion-slides>\n      </ion-row>\n      \n      <div style=\"display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\" *ngIf='!professionalUser.isChat'>\n        <ion-row class=\"row11\">\n          <div class=\"button\" (click)=\"presentModal()\">\n            <ion-icon name=\"lock-closed-outline\"></ion-icon>\n            <ion-text>MESSAGE NOW</ion-text>\n          </div>\n        </ion-row>\n      </div>\n\n      <div style=\"display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\" *ngIf='professionalUser.isChat'>\n        \n        <ion-row class=\"row11\">\n          <div class=\"button\" (click)=\"gotochat()\">\n            <ion-text>MESSAGE NOW</ion-text>\n          </div>\n        </ion-row>\n      </div>\n\n  </div>\n\n  \n  \n  <ion-row style=\"padding-top: 15%;\">\n    <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n  </ion-row>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/profile1/profile1-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/profile1/profile1-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: Profile1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile1PageRoutingModule", function() { return Profile1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile1.page */ "./src/app/profile1/profile1.page.ts");




const routes = [
    {
        path: '',
        component: _profile1_page__WEBPACK_IMPORTED_MODULE_3__["Profile1Page"]
    }
];
let Profile1PageRoutingModule = class Profile1PageRoutingModule {
};
Profile1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Profile1PageRoutingModule);



/***/ }),

/***/ "./src/app/profile1/profile1.module.ts":
/*!*********************************************!*\
  !*** ./src/app/profile1/profile1.module.ts ***!
  \*********************************************/
/*! exports provided: Profile1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile1PageModule", function() { return Profile1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile1_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile1-routing.module */ "./src/app/profile1/profile1-routing.module.ts");
/* harmony import */ var _profile1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile1.page */ "./src/app/profile1/profile1.page.ts");







let Profile1PageModule = class Profile1PageModule {
};
Profile1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile1_routing_module__WEBPACK_IMPORTED_MODULE_5__["Profile1PageRoutingModule"]
        ],
        declarations: [_profile1_page__WEBPACK_IMPORTED_MODULE_6__["Profile1Page"]]
    })
], Profile1PageModule);



/***/ }),

/***/ "./src/app/profile1/profile1.page.scss":
/*!*********************************************!*\
  !*** ./src/app/profile1/profile1.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-row {\n  margin-top: 0px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n@media (min-height: 568px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 640px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 667px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 731px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 736px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 812px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 823px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n.img {\n  width: 380px;\n  height: 300px;\n}\n.row {\n  padding-top: 5%;\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.row1 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 36px;\n  color: #003C69;\n  text-transform: capitalize;\n}\n.row2 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row2 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n}\n.row3 {\n  padding-top: 3%;\n  width: 100%;\n}\n.row3 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n.row3 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n  padding-left: 3%;\n}\n.icon {\n  color: #FFC107;\n  font-size: 18px;\n}\n.icon1 {\n  color: #000000;\n  font-size: 18px;\n}\n.row4 {\n  padding-top: 3%;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n.row4 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n  padding-left: 3%;\n}\n.row4 ion-icon {\n  font-size: 18px;\n}\n.row5 {\n  padding-top: 5%;\n  width: 90%;\n}\n.row5 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row6 {\n  width: 90%;\n  background: #FFFFFF;\n  padding: 5%;\n  margin-top: 3%;\n  border-radius: 10px;\n}\n.row6 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #999999;\n  text-align: justify;\n}\n.row7 {\n  padding-top: 3%;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n.row7 .img {\n  width: 115px;\n  height: 82px;\n}\n.row7 .img1 {\n  width: 115px;\n  height: 82px;\n  margin-left: 5%;\n}\n.row8 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row8 .container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 22px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n  margin: 0;\n  height: 145px;\n  width: 160px;\n}\n.row8 .centered {\n  position: absolute;\n  top: 85%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.row8 .imgSelected {\n  height: 145px;\n  width: 160px;\n}\n.div2 {\n  position: relative;\n  width: 323px;\n  height: 109px;\n}\n.row9 {\n  display: flex;\n  flex-direction: column;\n  box-sizing: border-box;\n  width: 323px;\n  height: 109px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  filter: blur(4px);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 1px solid #716a6a;\n  border-radius: 10px;\n  position: relative;\n}\n.row9 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row9 .div1 ion-icon {\n  padding-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row9 .div1 ion-text {\n  padding-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row9 .img {\n  width: 50%;\n}\n.row12 {\n  position: absolute;\n  top: 20px;\n  left: 130px;\n}\n.row12 .img {\n  width: 70px;\n  height: 70px;\n}\n.row10 {\n  padding-bottom: 5%;\n  padding-top: 5%;\n  width: 90%;\n}\n.row10 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row11 {\n  padding-top: 10%;\n  width: 90%;\n  justify-content: center;\n}\n.row11 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n.row11 .button {\n  width: 207px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n.row11 ion-icon {\n  color: #FFFFFF;\n  font-size: 24px;\n}\n.thumbnail {\n  width: 115px;\n  height: 82px;\n  margin-left: 5%;\n}\n.thumbnail1 {\n  width: 115px;\n  height: 82px;\n  padding-left: 3%;\n}\n.thumbnail2 {\n  height: 145px;\n  width: 160px;\n}\n.thumbnai3 {\n  width: 380px;\n  height: 300px;\n}\n.col {\n  width: 115px;\n  height: 82px;\n}\n.row13 {\n  display: flex;\n  flex-direction: column;\n  box-sizing: border-box;\n  width: 323px;\n  height: 109px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 1px solid #716a6a;\n  border-radius: 10px;\n  position: relative;\n}\n.row13 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row13 .div1 ion-icon {\n  padding-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row13 .div1 ion-text {\n  padding-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row13 .img {\n  width: 50%;\n}\n.div3 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.row14 {\n  display: flex;\n  flex-direction: column;\n  box-sizing: border-box;\n  width: 323px;\n  height: 109px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  filter: blur(4px);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 1px solid #716a6a;\n  border-radius: 10px;\n  position: relative;\n}\n.row14 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row14 .div1 ion-icon {\n  padding-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row14 .div1 ion-text {\n  padding-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row14 .img {\n  width: 50%;\n}\n.div4 {\n  position: absolute;\n  top: 13px;\n  left: 15px;\n  padding: 2%;\n  border-radius: 7px;\n  background: #00CBEE;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.text4 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #767474;\n}\n.row15 {\n  padding-top: 5%;\n  padding-right: 3%;\n}\n.text2 {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #003C69;\n}\n.text3 {\n  font-family: \"Poppins-Medium\";\n  font-size: 12px;\n  color: #FFFFFF;\n  text-transform: uppercase;\n}\n.image {\n  width: 25px;\n}\n.icon2 {\n  color: black;\n  font-size: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZTEvcHJvZmlsZTEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFDO0VBQ0csWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLCtEQUFBO0VBQ0EsZ0RBQUE7RUFDQSw0QkFBQTtBQUNKO0FBQUk7RUFDRSxlQUFBO0FBRU47QUFBSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFFUjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUjtBQUFJO0VBQ0UsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBRU47QUFHRTtFQUVJO0lBQ0UsZ0JBQUE7RUFETjtBQUNGO0FBS0U7RUFFSTtJQUNFLGVBQUE7RUFKTjtBQUNGO0FBUUU7RUFFSTtJQUNFLGdCQUFBO0VBUE47QUFDRjtBQVdFO0VBRUk7SUFDRSxlQUFBO0VBVk47QUFDRjtBQWNFO0VBRUk7SUFDRSxnQkFBQTtFQWJOO0FBQ0Y7QUFpQkU7RUFFSTtJQUNFLGdCQUFBO0VBaEJOO0FBQ0Y7QUFvQkU7RUFFSTtJQUNFLGVBQUE7RUFuQk47QUFDRjtBQXlCRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0FBdkJKO0FBeUJFO0VBQ0ksZUFBQTtBQXRCTjtBQXdCRTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFyQk47QUF1QkU7RUFDRSxlQUFBO0VBQ0EsVUFBQTtBQXBCSjtBQXFCSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSwwQkFBQTtBQW5CUjtBQXNCRTtFQUNFLGVBQUE7RUFDQSxVQUFBO0FBbkJKO0FBb0JJO0VBQ0ksOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQWxCUjtBQXFCRTtFQUNFLGVBQUE7RUFDQSxXQUFBO0FBbEJKO0FBbUJJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQWpCUjtBQW1CSTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQWpCUjtBQW9CRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBakJKO0FBbUJFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFoQko7QUFrQkU7RUFDRSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7QUFmSjtBQWdCSTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQWRSO0FBZ0JJO0VBQ0ksZUFBQTtBQWRSO0FBaUJFO0VBQ0UsZUFBQTtFQUNBLFVBQUE7QUFkSjtBQWVJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQWJSO0FBZ0JFO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQWJKO0FBY0k7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFaUjtBQWVFO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBWko7QUFhTTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FBWFI7QUFhTTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQVhSO0FBY0U7RUFDRSxlQUFBO0VBQ0EsVUFBQTtBQVhKO0FBWUk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQVZSO0FBWU07RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFWUjtBQVlNO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUFWUjtBQWFFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQVZKO0FBWUU7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQ0FBQTtFQUVBLGlCQUFBO0VBQ0EsK0NBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFUSjtBQVVJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVJSO0FBU1E7RUFDRSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBUFY7QUFTUTtFQUNFLGdCQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVBWO0FBVU07RUFDRSxVQUFBO0FBUlI7QUFXRTtFQUNFLGtCQUFBO0VBR0EsU0FBQTtFQUNBLFdBQUE7QUFWSjtBQVdJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFUUjtBQVlFO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQVRKO0FBVUk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBUlI7QUFXRTtFQUNFLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLHVCQUFBO0FBUko7QUFTSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFQUjtBQVNJO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSw2REFBQTtFQUNBLGdEQUFBO0VBQ0EsbUJBQUE7QUFQUjtBQVNJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFQUjtBQVVFO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBUEo7QUFTQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFOSjtBQVFDO0VBQ0MsYUFBQTtFQUNBLFlBQUE7QUFMRjtBQU9DO0VBQ0MsWUFBQTtFQUNBLGFBQUE7QUFKRjtBQU1DO0VBQ0MsWUFBQTtFQUNBLFlBQUE7QUFIRjtBQUtFO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFDQSwrQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQUZOO0FBR007RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBRFY7QUFFVTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFBWjtBQUVVO0VBQ0UsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBQVo7QUFHUTtFQUNFLFVBQUE7QUFEVjtBQUlBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFESjtBQUlBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFFQSxpQkFBQTtFQUNBLCtDQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBREo7QUFFSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFBUjtBQUNRO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUNWO0FBQ1E7RUFDRSxnQkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFDVjtBQUVNO0VBQ0UsVUFBQTtBQUFSO0FBR0E7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQUY7QUFFQTtFQUNFLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFDRjtBQUNBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBRUY7QUFBQTtFQUNFLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFHRjtBQURBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FBSUY7QUFGQTtFQUNFLFdBQUE7QUFLRjtBQUZBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUxL3Byb2ZpbGUxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAudG9vbGJhcjEge1xuICAgIGhlaWdodDogNzRweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDI1cHggMjVweDtcbiAgICBpb24tcm93IHtcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICB9XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICB9XG4gICAgaW9uLXRleHQge1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgfVxuICB9XG5cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA1NjhweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDY0MHB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA2NjdweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDczMXB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA3MzZweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDgxMnB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogODIzcHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG5cblxuICAuaW1nIHtcbiAgICB3aWR0aDogMzgwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgfVxuICAucm93IHtcbiAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgfVxuICAuZGl2IHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5yb3cxIHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xuICAgICAgICBjb2xvcjogIzAwM0M2OTtcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgfVxuICB9XG4gIC5yb3cyIHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICB9XG4gIH1cbiAgLnJvdzMge1xuICAgIHBhZGRpbmctdG9wOiAzJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBpb24tY29sIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgICB9XG4gIH1cbiAgLmljb24ge1xuICAgIGNvbG9yOiAjRkZDMTA3O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAuaWNvbjEge1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAucm93NCB7XG4gICAgcGFkZGluZy10b3A6IDMlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLVJlZ3VsYXInO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDMlO1xuICAgIH1cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB9XG4gIH1cbiAgLnJvdzUge1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIH1cbiAgfVxuICAucm93NiB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIHBhZGRpbmc6IDUlO1xuICAgIG1hcmdpbi10b3A6IDMlO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICM5OTk5OTk7XG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgfVxuICB9XG4gIC5yb3c3IHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgLmltZyB7XG4gICAgICAgIHdpZHRoOiAxMTVweDtcbiAgICAgICAgaGVpZ2h0OiA4MnB4O1xuICAgICAgfVxuICAgICAgLmltZzEge1xuICAgICAgICB3aWR0aDogMTE1cHg7XG4gICAgICAgIGhlaWdodDogODJweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgICAgfVxuICB9XG4gIC5yb3c4IHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICAuY29udGFpbmVyIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMS41O1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgaGVpZ2h0OiAxNDVweDtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgfVxuICAgICAgLmNlbnRlcmVkIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDg1JTtcbiAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAgIH1cbiAgICAgIC5pbWdTZWxlY3RlZHtcbiAgICAgICAgaGVpZ2h0OiAxNDVweDtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgfVxuICB9XG4gIC5kaXYyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDMyM3B4O1xuICAgIGhlaWdodDogMTA5cHg7XG4gIH1cbiAgLnJvdzkge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHdpZHRoOiAzMjNweDtcbiAgICBoZWlnaHQ6IDEwOXB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbiAgICAtd2Via2l0LWZpbHRlcjogYmx1cig0cHgpO1xuICAgIGZpbHRlcjogYmx1cig0cHgpO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MTZhNmE7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLmRpdjEge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAyJTtcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xuICAgICAgICB9XG4gICAgICAgIGlvbi10ZXh0IHtcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgICAuaW1nIHtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgIH1cbiAgfVxuICAucm93MTIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAvLyB0b3A6IDEwNzBweDtcbiAgICAvLyBsZWZ0OiAxNTBweDtcbiAgICB0b3A6IDIwcHg7XG4gICAgbGVmdDogMTMwcHg7XG4gICAgLmltZyB7XG4gICAgICAgIHdpZHRoOiA3MHB4O1xuICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgfVxuICB9XG4gIC5yb3cxMCB7XG4gICAgcGFkZGluZy1ib3R0b206IDUlO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIH1cbiAgfVxuICAucm93MTEge1xuICAgIHBhZGRpbmctdG9wOiAxMCU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICB9XG4gICAgLmJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiAyMDdweDtcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG4gICAgaW9uLWljb24ge1xuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbiAgfVxuICAudGh1bWJuYWlsIHtcbiAgICB3aWR0aDogMTE1cHg7XG4gICAgaGVpZ2h0OiA4MnB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbn1cbi50aHVtYm5haWwxIHtcbiAgICB3aWR0aDogMTE1cHg7XG4gICAgaGVpZ2h0OiA4MnB4O1xuICAgIHBhZGRpbmctbGVmdDogMyU7XG59XG4gLnRodW1ibmFpbDIge1xuICBoZWlnaHQ6IDE0NXB4O1xuICB3aWR0aDogMTYwcHg7XG4gfVxuIC50aHVtYm5haTMge1xuICB3aWR0aDogMzgwcHg7XG4gIGhlaWdodDogMzAwcHg7XG4gfVxuIC5jb2wge1xuICB3aWR0aDogMTE1cHg7XG4gIGhlaWdodDogODJweDtcbiB9XG4gIC5yb3cxMyB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICB3aWR0aDogMzIzcHg7XG4gICAgICBoZWlnaHQ6IDEwOXB4O1xuICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG4gICAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MTZhNmE7XG4gICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLmRpdjEge1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgIHBhZGRpbmc6IDIlO1xuICAgICAgICAgIGhlaWdodDogMzVweDtcbiAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaW9uLXRleHQge1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgICAgLmltZyB7XG4gICAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICAgfVxuICB9XG4uZGl2MyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5yb3cxNCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgd2lkdGg6IDMyM3B4O1xuICAgIGhlaWdodDogMTA5cHg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xuICAgIC13ZWJraXQtZmlsdGVyOiBibHVyKDRweCk7XG4gICAgZmlsdGVyOiBibHVyKDRweCk7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcxNmE2YTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAuZGl2MSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDIlO1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLXRleHQge1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLVJlZ3VsYXInO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAgIC5pbWcge1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgfVxufVxuLmRpdjQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTNweDtcbiAgbGVmdDogMTVweDtcbiAgcGFkZGluZzogMiU7XG4gIGJvcmRlci1yYWRpdXM6IDdweDtcbiAgYmFja2dyb3VuZDogIzAwQ0JFRTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4udGV4dDQge1xuICBmb250LWZhbWlseTogJ1BvcHBpbnMtUmVndWxhcic7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3Njc0NzQ7XG59XG4ucm93MTUge1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xufVxuLnRleHQyIHtcbiAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGNvbG9yOiAjMDAzQzY5O1xufVxuLnRleHQzIHtcbiAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLU1lZGl1bSc7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaW1hZ2Uge1xuICB3aWR0aDogMjVweDtcbn1cblxuLmljb24yIHtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDI1cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/profile1/profile1.page.ts":
/*!*******************************************!*\
  !*** ./src/app/profile1/profile1.page.ts ***!
  \*******************************************/
/*! exports provided: Profile1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile1Page", function() { return Profile1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _unlock_contact_unlock_contact_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../unlock-contact/unlock-contact.page */ "./src/app/unlock-contact/unlock-contact.page.ts");
/* harmony import */ var _unlock_contact1_unlock_contact1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../unlock-contact1/unlock-contact1.page */ "./src/app/unlock-contact1/unlock-contact1.page.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/photo-viewer/ngx */ "./node_modules/@ionic-native/photo-viewer/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");














let Profile1Page = class Profile1Page {
    constructor(router, route, photoViewer, modalController, storage, socialSharing, afa, fs, authService) {
        this.router = router;
        this.route = route;
        this.photoViewer = photoViewer;
        this.modalController = modalController;
        this.storage = storage;
        this.socialSharing = socialSharing;
        this.afa = afa;
        this.fs = fs;
        this.authService = authService;
        this.userPosts = [];
        this.posts = [];
        this.professionalUser = {};
        this.selectedUser = {};
        this.passionistUser = {};
        this.aboutUser = false;
        this.proffessional = false;
        this.passionist = false;
        this.professional = false;
        this.userData = {};
        this.slideOpts = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: 0,
            slideShow: true,
            autoplay: true
        };
        this.slideOpts1 = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: 0,
            slideShow: true,
            autoplay: true
        };
        console.log('heyyyyyyyy');
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                this.pageroute = params.pageroute;
            }
            else if (params) {
                this.professional = params.professional;
                this.pageroute = params.pageroute;
                console.log(this.pageroute);
                this.host_id = params.hostId;
                this.userId = params.userId;
                console.log(this.host_id + "hostid");
                this.getProfile();
            }
        });
    }
    viewPhoto(x) {
        console.log(x);
        this.photoViewer.show(x);
    }
    //  getProfile(){
    //    if((this.userId!=''||this.userId!=undefined||this.userId!=null) &&
    //    (this.host_id!=''||this.host_id!=undefined||this.host_id!=null)){
    //     var obj={"hostId":this.host_id,"userId":this.userId};
    //     this.authService.getUserProfile(obj).subscribe((data:any)=>{
    //      console.log(data);
    //      this.professionalUser=data.data; 
    //      console.log(this.professionalUser);
    //      this.docUrl1=this.professionalUser.documents[0].docUrl;
    //      this.docUrl2=this.professionalUser.documents[1].docUrl;
    //      console.log(this.docUrl1);
    //      console.log(this.docUrl2);
    //     });
    //    }else {
    //     var obj1={"userId":this.host_id};
    //     this.authService.getUserProfileById(obj1).subscribe((res:any)=>{
    //       console.log(res);
    //      this.professionalUser=res.data; 
    //      console.log(this.professionalUser);
    //      this.docUrl1=this.professionalUser.documents[0].docUrl;
    //      this.docUrl2=this.professionalUser.documents[1].docUrl;
    //      console.log(this.docUrl1);
    //      console.log(this.docUrl2);
    //     });
    //    }   
    //  }
    getProfile() {
        var obj1 = { "hostId": this.host_id, 'userId': this.userId };
        this.authService.getUserProfile(obj1).subscribe((res) => {
            console.log(res);
            this.professionalUser = res.data;
            this.docUrl1 = this.professionalUser.documents[0].docUrl;
            this.docUrl2 = this.professionalUser.documents[1].docUrl;
            console.log(this.docUrl1);
            console.log(this.docUrl1);
            this.userPosts = res.posts;
            //  console.log(this.professionalUser);
            //  this.docUrl1=this.professionalUser.documents[0].docUrl;
            //  this.docUrl2=this.professionalUser.documents[1].docUrl;
            //  console.log(this.docUrl1);
            //  console.log(this.docUrl2);
        });
    }
    getUserPost() {
        this.userPosts = [];
        var obj = { 'userId': this.host_id };
        this.authService.getAllPost(obj).subscribe((data) => {
            console.log(data);
            this.posts = data.postArray;
            this.posts.forEach(element => {
                if (element.posttype == '0' || element.posttype == '1') {
                    this.userPosts.push(element);
                    console.log(this.userPosts);
                }
            });
        });
    }
    ngOnInit() {
        // this.loadData();
        console.log(this.userId);
        console.log(this.host_id);
    }
    // loadData(){
    //   this.storage.get('user').then((user)=>{
    //     this.userData=user;
    //     this.userId=user._id;
    //     this.userName=user.fullName;
    //     this.userImage=user.user_img;
    //     this.walletBalance=user.wallet;
    //     if(user.userType==this.selectedUser.userType){
    //       this.professionalUser=this.selectedUser;
    //       this.professionalUser.isChat=true;
    //       this.docUrl1=this.professionalUser.documents[0].docUrl;
    //       this.docUrl2=this.professionalUser.documents[1].docUrl;
    //       this.getUserPost();
    //     }else {
    //       this.getProfile(); 
    //       this.getUserPost();
    //     }
    //   });
    // }
    presentModal() {
        if (this.walletBalance > 5) {
            this.presentUnlock();
        }
        else {
            this.subscription();
        }
    }
    gotochat() {
        this.fs.collection('friends').doc(this.userId).collection('chats').doc(this.professionalUser._id).set({
            recieverName: this.professionalUser.fullName,
            recieverId: this.professionalUser._id,
            recieverImage: this.professionalUser.user_img,
            senderId: this.userId,
            senderName: this.userName,
            senderImage: this.userImage,
            Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
        }, { merge: true });
        this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
            senderName: this.professionalUser.fullName,
            senderId: this.professionalUser._id,
            senderImage: this.professionalUser.user_img,
            recieverId: this.userId,
            recieverName: this.userName,
            recieverImage: this.userImage,
            Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
        }, { merge: true });
        let vc = { recieverId: this.host_id,
            senderId: this.userId,
            recieverName: this.professionalUser.fullName,
            recieverImg: this.professionalUser.user_img };
        let navigationExtras = {
            queryParams: {
                special: JSON.stringify(vc)
            }
        };
        this.router.navigate(['chat'], navigationExtras);
    }
    presentUnlock() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('helloooooooo');
            const modal = yield this.modalController.create({
                component: _unlock_contact_unlock_contact_page__WEBPACK_IMPORTED_MODULE_4__["UnlockContactPage"],
                cssClass: 'unlockContactModal',
                componentProps: { 'professionalUser': this.professionalUser }
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.fs.collection('friends').doc(this.userId).collection('chats').doc(this.professionalUser._id).set({
                    recieverName: this.professionalUser.fullName,
                    recieverId: this.professionalUser._id,
                    recieverImage: this.professionalUser.user_img,
                    senderId: this.userId,
                    senderName: this.userName,
                    senderImage: this.userImage,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
                    senderName: this.professionalUser.fullName,
                    senderId: this.professionalUser._id,
                    senderImage: this.professionalUser.user_img,
                    recieverId: this.userId,
                    recieverName: this.userName,
                    recieverImage: this.userImage,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                let navigationExtras = {
                    queryParams: {
                        special: JSON.stringify({ url: this.router.url, recieverId: this.professionalUser._id,
                            senderId: this.userId,
                            recieverName: this.professionalUser.fullName,
                            senderName: this.userName,
                            recieverImg: this.professionalUser.user_img })
                    }
                };
                this.router.navigate(['chat'], navigationExtras);
            }
        });
    }
    subscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('heyyyyyyyyy');
            const modal = yield this.modalController.create({
                component: _unlock_contact1_unlock_contact1_page__WEBPACK_IMPORTED_MODULE_5__["UnlockContact1Page"],
                cssClass: 'unlockContact1Modal',
                componentProps: { 'professionalUser': this.professionalUser }
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.fs.collection('friends').doc(this.userId).collection('chats').doc(this.professionalUser._id).set({
                    recieverName: this.professionalUser.fullName,
                    recieverId: this.professionalUser._id,
                    recieverImage: this.professionalUser.user_img,
                    senderId: this.userId,
                    senderName: this.userName,
                    senderImage: this.userImage,
                    isBlocked: false,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
                    senderName: this.professionalUser.fullName,
                    senderId: this.professionalUser._id,
                    senderImage: this.professionalUser.user_img,
                    recieverId: this.userId,
                    recieverName: this.userName,
                    recieverImage: this.userImage,
                    isBlocked: false,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                let navigationExtras = {
                    queryParams: {
                        special: JSON.stringify({ url: this.router.url, recieverId: this.professionalUser._id,
                            senderId: this.userId,
                            recieverName: this.professionalUser.fullName,
                            senderName: this.userName,
                            recieverImg: this.professionalUser.user_img })
                    }
                };
                this.router.navigate(['chat'], navigationExtras);
            }
        });
    }
    bookmark(item, i) {
        this.posts[i].isBookmarked = true;
        console.log(item);
        var obj = { 'postId': item._id, 'postUserId': item.userId, 'postUrl': item.postUrl,
            'userId': this.userId, 'postTitle': item.postTitle, 'postDescription': item.postDescription,
            'posttype': item.posttype, 'postCategory': item.postCategory, 'isBookmkared': true };
        this.authService.bookmark(obj).subscribe((data) => {
            console.log(data);
            if (data.success) {
                this.authService.presentToast('Bookmark Added');
            }
        });
    }
    deleteBookmark(item, i) {
        this.posts[i].isBookmarked = false;
        var obj = { 'userId': this.userId };
        this.authService.getBookmark(obj).subscribe((data) => {
            this.bookmarkedPost = data.bookmarkArray;
            console.log(this.bookmarkedPost);
            this.bookmarkedPost.forEach(element => {
                if (item._id == element.postId) {
                    var obj1 = { 'bookmarkId': element._id, 'userId': this.userId };
                    this.authService.deleteBookmark(obj1).subscribe((res) => {
                        console.log(res);
                        this.authService.presentToast('Bookmark Removed');
                    });
                }
            });
        });
    }
    likePost(item, i) {
        // this.posts[i].isliked=true;
        // this.posts[i].likeCount=this.posts[i].likeCount+1;
        //   var likedPostUser={'likedPostUserid':this.userId,'userName':this.userName,
        //   'userImage':this.userImage};
        //   // this.likeArray.push(likedPostUser);
        //   var obj={'likeArray':likedPostUser,'postId':item._id,
        //   'userName':this.userName,'hostId':item.userId,'userId':this.userId,};
        //   this.authService.like(obj).subscribe((data:any)=>{
        //     console.log(data);
        //     if(data.success){
        //       this.loadData();
        //     }
        //   })  
        this.posts[i].isliked = true;
        this.posts[i].likeCount = item.likeCount + 1;
        var likeArray = item.likes;
        var likedPostUser = { 'likedPostUserid': this.userId, 'userName': this.userData.firstName,
            'userImage': this.userData.user_img };
        likeArray.push(likedPostUser);
        console.log(likeArray);
        var obj = { 'likeArray': JSON.stringify(likeArray), 'postId': item._id, "likeCount": this.posts[i].likeCount,
            'userName': this.userData.firstName, 'hostId': item.userId, 'userId': this.userData._id, };
        this.authService.like(obj).subscribe((data) => {
            console.log(data);
            if (data.success) {
                // this.loadData();
            }
        });
    }
    deleteLike(item, j) {
        // this.posts[j].isliked=false;
        // this.posts[j].likeCount=this.posts[j].likeCount-1;
        // item.isliked=false;
        // var likeArray=item.likes;
        // var index=0;
        // for(var i=0;i<likeArray.length;i++){
        //   if(likeArray[i].likedPostUser==this.userId){
        //     index=i--;
        //     break;
        //   }
        // }
        // likeArray.splice(index,1);
        // console.log(likeArray);
        // var obj={'likeArray':JSON.stringify(likeArray),'postId':item._id};
        // this.authService.like(obj).subscribe((res)=>{
        //   console.log(res);
        //   this.loadData();
        // });
        this.posts[j].isliked = false;
        this.posts[j].likeCount = item.likeCount - 1;
        item.isliked = false;
        var likeArray = item.likes;
        var index = 0;
        for (var i = 0; i < likeArray.length; i++) {
            if (likeArray[i].likedPostUser == this.userId) {
                index = i--;
                break;
            }
        }
        likeArray.splice(index, 1);
        console.log(likeArray);
        var obj = { 'likeArray': JSON.stringify(likeArray), 'postId': item._id, "likeCount": this.posts[j].likeCount };
        this.authService.deleteLike(obj).subscribe((res) => {
            console.log(res);
            // this.loadData();
        });
    }
    share(item) {
        var message = item.postTitle + ' \n' + item.postDescription;
        var postUrl = item.postUrl;
        this.socialSharing.share(message, null, null, postUrl);
    }
    back() {
        this.router.navigateByUrl(this.pageroute);
    }
    notification() {
        let navigationExtras = {
            queryParams: {
                special: this.router.url
            }
        };
        this.router.navigate(['notification'], navigationExtras);
    }
};
Profile1Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_12__["PhotoViewer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"] },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_13__["SocialSharing"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestore"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"] }
];
Profile1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile1/profile1.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile1.page.scss */ "./src/app/profile1/profile1.page.scss")).default]
    })
], Profile1Page);



/***/ })

}]);
//# sourceMappingURL=profile1-profile1-module-es2015.js.map