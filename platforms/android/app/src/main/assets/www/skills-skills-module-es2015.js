(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skills-skills-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/skills/skills.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/skills/skills.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar\">\n    <ion-row *ngIf='!category'>\n      <ion-icon (click)=\"back()\" name=\"chevron-back\"></ion-icon>\n      <ion-text>Your Skills</ion-text>\n    </ion-row>\n    <ion-row  *ngIf='category'>\n      <ion-icon (click)=\"back()\" name=\"chevron-back\"></ion-icon>\n      <ion-text>Choose Category</ion-text>\n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <div *ngIf='!category'>\n    <ion-row class=\"row1\">\n      <ion-text class=\"text1\">Select your Profession</ion-text>\n    </ion-row>\n    \n    <div class=\"div\">\n      <ion-row class=\"row\">\n          <ion-col *ngFor=\"let item of selectedHobby; let idx = index\" (click)=\"onItemClicked(idx,item)\" [size]=6 class=\"container\">\n            <img [class.selected]=\"idx === currentSelected\" class=\"imgSelected1\" [src]=\"item.hobbyImage\">\n            <div class=\"centered\">\n              <ion-text>{{item.hobby}}</ion-text>\n            </div>\n          </ion-col>\n          <!-- <ion-col (click)=\"selectSkill1('bake','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/bake.jpg?alt=media&token=5e1317e1-351a-44b8-a483-f96b40b64294')\" [size]=6 class=\"container\" *ngIf=\"!selected\">\n            <img class=\"img\" src=\"../../assets/hobbyImage/bake.jpg\">\n            <div class=\"centered\">\n              <ion-text>BAKE</ion-text>\n            </div>\n          </ion-col> -->\n    \n          <!-- <ion-col (click)=\"unSelectCategory()\" class=\"container\" *ngIf=\"selected2\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/run.jpg\">\n            <div class=\"centered\">\n              <ion-text>RUN</ion-text>\n            </div>\n          </ion-col>\n          <ion-col (click)=\"selectSkill2('run','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/run.jpg?alt=media&token=a32179fe-eed0-4ac9-bd4c-a80a40d6a5b7')\" class=\"container\" *ngIf=\"!selected2\">\n            <img class=\"img\" src=\"../../assets/hobbyImage/run.jpg\">\n            <div class=\"centered\">\n              <ion-text>RUN</ion-text>\n            </div>\n          </ion-col> -->\n        \n      </ion-row>\n    \n      <!-- <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected3\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/yoga.jpg\">\n              <div class=\"centered\">\n                <ion-text>YOGA</ion-text>\n              </div>\n        </ion-col>\n        <ion-col (click)=\"selectSkill3('yoga','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/yoga.jpg?alt=media&token=edbd4ce1-ac21-4dc3-bb86-51fa05c46f93')\" [size]=6 class=\"container\" *ngIf=\"!selected3\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/yoga.jpg\">\n          <div class=\"centered\">\n            <ion-text>YOGA</ion-text>\n          </div>\n        </ion-col>\n    \n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected4\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/gym.jpg\">\n              <div class=\"centered\">\n                <ion-text>GYM</ion-text>\n              </div>\n        </ion-col>  \n        <ion-col (click)=\"selectSkill4('gym','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/gym.jpg?alt=media&token=3658f866-3967-4b1b-8309-c5094612f416')\" [size]=6 class=\"container\" *ngIf=\"!selected4\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/gym.jpg\">\n          <div class=\"centered\">\n            <ion-text>GYM</ion-text>\n          </div>\n        </ion-col>      \n      </ion-row>\n    \n      <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected5\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/meditation.jpg\">\n              <div class=\"centered\">\n                <ion-text>MEDITATION</ion-text>\n              </div>\n        </ion-col>\n        <ion-col (click)=\"selectSkill5('meditation','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/meditation.jpg?alt=media&token=de661d01-c565-4aed-a3b4-cfd372e0bcc8')\" [size]=6 class=\"container\" *ngIf=\"!selected5\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/meditation.jpg\">\n          <div class=\"centered\">\n            <ion-text>MEDITATION</ion-text>\n          </div>\n        </ion-col>\n    \n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected6\">\n          <img class=\"imgSelected\" src=\"../../assets/hobbyImage/guitar.jpg\">\n          <div class=\"centered\">\n            <ion-text>GUITAR</ion-text>\n          </div>\n        </ion-col>\n        <ion-col (click)=\"selectSkill6('guitar','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/guitar.jpg?alt=media&token=9cb8073a-5719-424e-801d-d725342d2c25')\" [size]=6 class=\"container\" *ngIf=\"!selected6\">\n              <img class=\"img\" src=\"../../assets/hobbyImage/guitar.jpg\">\n              <div class=\"centered\">\n                <ion-text>GUITAR</ion-text>\n              </div>\n        </ion-col>\n      </ion-row>\n    \n      <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected7\">\n          <img class=\"imgSelected\" src=\"../../assets/hobbyImage/cycling.jpg\">\n          <div class=\"centered\">\n            <ion-text>CYCLING</ion-text>\n          </div>\n        </ion-col>\n        <ion-col (click)=\"selectSkill7('cycling','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/cycling.jpg?alt=media&token=af6ab57b-c855-4829-9525-c34d20b8f691')\" [size]=6 class=\"container\" *ngIf=\"!selected7\">\n              <img class=\"img\" src=\"../../assets/hobbyImage/cycling.jpg\">\n              <div class=\"centered\">\n                <ion-text>CYCLING</ion-text>\n              </div>\n        </ion-col>\n    \n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected8\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/singing.jpg\">\n              <div class=\"centered\">\n                <ion-text>SINGING</ion-text>\n              </div>\n        </ion-col>\n        <ion-col (click)=\"selectSkill8('singing','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/singing.jpg?alt=media&token=2e9b4108-6af6-4eed-bf5f-e481339e4e53')\" [size]=6 class=\"container\" *ngIf=\"!selected8\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\">\n          <div class=\"centered\">\n            <ion-text>SINGING</ion-text>\n          </div>\n        </ion-col>\n      </ion-row> -->\n    \n      <ion-row class=\"row3\" *ngIf=\"count==0\">\n        <ion-button class=\"button1\" shape=\"block\">Next</ion-button>\n      </ion-row>\n      <ion-row class=\"row3\" *ngIf=\"count!=0\" (click)=\"next()\">\n        <ion-button class=\"button2\" shape=\"block\">Next</ion-button>\n      </ion-row>\n    </div>\n  </div>\n\n\n\n\n  \n  <div *ngIf='category'>\n    <ion-row class=\"row1\">\n      <ion-text class=\"text1\">Select Category</ion-text>\n    </ion-row>\n    \n    <div class=\"div\">\n      <ion-row class=\"row\">\n          <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/bake.jpg\">\n            <div class=\"centered\">\n              <ion-text>BAKE</ion-text>\n            </div>\n          </ion-col>\n          <ion-col  (click)=\"selectCategory1('bake')\" [size]=6 class=\"container\" *ngIf=\"!selected\">\n            <img class=\"img\" src=\"../../assets/hobbyImage/bake.jpg\">\n            <div class=\"centered\">\n              <ion-text>BAKE</ion-text>\n            </div>\n          </ion-col>\n    \n          <ion-col (click)=\"unSelectCategory()\" class=\"container\" *ngIf=\"selected2\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/run.jpg\">\n            <div class=\"centered\">\n              <ion-text>RUN</ion-text>\n            </div>\n          </ion-col>\n          <ion-col  (click)=\"selectCategory2('run')\" class=\"container\" *ngIf=\"!selected2\">\n            <img class=\"img\" src=\"../../assets/hobbyImage/run.jpg\">\n            <div class=\"centered\">\n              <ion-text>RUN</ion-text>\n            </div>\n          </ion-col>\n        \n      </ion-row>\n    \n      <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected3\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/yoga.jpg\">\n              <div class=\"centered\">\n                <ion-text>YOGA</ion-text>\n              </div>\n        </ion-col>\n        <ion-col  (click)=\"selectCategory3('yoga')\" [size]=6 class=\"container\" *ngIf=\"!selected3\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/yoga.jpg\">\n          <div class=\"centered\">\n            <ion-text>YOGA</ion-text>\n          </div>\n        </ion-col>\n    \n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected4\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/gym.jpg\">\n              <div class=\"centered\">\n                <ion-text>GYM</ion-text>\n              </div>\n        </ion-col>  \n        <ion-col  (click)=\"selectCategory4('gym')\" [size]=6 class=\"container\" *ngIf=\"!selected4\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/gym.jpg\">\n          <div class=\"centered\">\n            <ion-text>GYM</ion-text>\n          </div>\n        </ion-col>      \n      </ion-row>\n    \n      <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected5\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/meditation.jpg\">\n              <div class=\"centered\">\n                <ion-text>MEDITATION</ion-text>\n              </div>\n        </ion-col>\n        <ion-col  (click)=\"selectCategory5('meditation')\" [size]=6 class=\"container\" *ngIf=\"!selected5\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/meditation.jpg\">\n          <div class=\"centered\">\n            <ion-text>MEDITATION</ion-text>\n          </div>\n        </ion-col>\n    \n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected6\">\n          <img class=\"imgSelected\" src=\"../../assets/hobbyImage/guitar.jpg\">\n          <div class=\"centered\">\n            <ion-text>GUITAR</ion-text>\n          </div>\n        </ion-col>\n        <ion-col  (click)=\"selectCategory6('guitar')\" [size]=6 class=\"container\" *ngIf=\"!selected6\">\n              <img class=\"img\" src=\"../../assets/hobbyImage/guitar.jpg\">\n              <div class=\"centered\">\n                <ion-text>GUITAR</ion-text>\n              </div>\n        </ion-col>\n      </ion-row>\n    \n      <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected7\">\n          <img class=\"imgSelected\" src=\"../../assets/hobbyImage/cycling.jpg\">\n          <div class=\"centered\">\n            <ion-text>CYCLING</ion-text>\n          </div>\n        </ion-col>\n        <ion-col  (click)=\"selectCategory7('cycling')\" [size]=6 class=\"container\" *ngIf=\"!selected7\">\n              <img class=\"img\" src=\"../../assets/hobbyImage/cycling.jpg\">\n              <div class=\"centered\">\n                <ion-text>CYCLING</ion-text>\n              </div>\n        </ion-col>\n    \n        <ion-col (click)=\"unSelectCategory()\" [size]=6 class=\"container\" *ngIf=\"selected8\">\n              <img class=\"imgSelected\" src=\"../../assets/hobbyImage/singing.jpg\">\n              <div class=\"centered\">\n                <ion-text>SINGING</ion-text>\n              </div>\n        </ion-col>\n        <ion-col  (click)=\"selectCategory8('singing')\" [size]=6 class=\"container\" *ngIf=\"!selected8\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\">\n          <div class=\"centered\">\n            <ion-text>SINGING</ion-text>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"row3\" *ngIf=\"count==0\">\n        <ion-button class=\"button1\" shape=\"block\">Next</ion-button>\n      </ion-row>\n    \n      <ion-row *ngIf=\"count!=0\" class=\"row3\" (click)=\"goToPost()\">\n        <ion-button class=\"button2\" shape=\"block\">Next</ion-button>\n      </ion-row>\n    </div>\n  </div>\n\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/skills/skills-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/skills/skills-routing.module.ts ***!
  \*************************************************/
/*! exports provided: SkillsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsPageRoutingModule", function() { return SkillsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _skills_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./skills.page */ "./src/app/skills/skills.page.ts");




const routes = [
    {
        path: '',
        component: _skills_page__WEBPACK_IMPORTED_MODULE_3__["SkillsPage"]
    }
];
let SkillsPageRoutingModule = class SkillsPageRoutingModule {
};
SkillsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SkillsPageRoutingModule);



/***/ }),

/***/ "./src/app/skills/skills.module.ts":
/*!*****************************************!*\
  !*** ./src/app/skills/skills.module.ts ***!
  \*****************************************/
/*! exports provided: SkillsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsPageModule", function() { return SkillsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _skills_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./skills-routing.module */ "./src/app/skills/skills-routing.module.ts");
/* harmony import */ var _skills_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./skills.page */ "./src/app/skills/skills.page.ts");







let SkillsPageModule = class SkillsPageModule {
};
SkillsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _skills_routing_module__WEBPACK_IMPORTED_MODULE_5__["SkillsPageRoutingModule"]
        ],
        declarations: [_skills_page__WEBPACK_IMPORTED_MODULE_6__["SkillsPage"]]
    })
], SkillsPageModule);



/***/ }),

/***/ "./src/app/skills/skills.page.scss":
/*!*****************************************!*\
  !*** ./src/app/skills/skills.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".text1 {\n  width: 253px;\n  height: 36px;\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n}\n\n.container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 20px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n}\n\n.centered {\n  position: absolute;\n  top: 140px;\n  left: 93px;\n  transform: translate(-50%, -50%);\n}\n\n.row {\n  justify-content: center;\n  width: 90%;\n}\n\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n\n.row1 {\n  justify-content: center;\n  text-align: center;\n  padding-top: 10%;\n}\n\nion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n\nion-card {\n  margin: 0;\n  height: 70%;\n}\n\n.selected {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n  filter: brightness(0.4);\n}\n\n.imgSelected1 {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n}\n\n.button1 {\n  width: 80%;\n  border-radius: 10px;\n  --background: #F3F3F3;\n  --background-activated: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #AAAAAA;\n  --color-activated: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n\n.button2 {\n  width: 80%;\n  border-radius: 10px;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n\n.row3 {\n  width: 80%;\n  justify-content: center;\n  padding-top: 10%;\n}\n\n.img {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n  filter: brightness(0.4);\n}\n\n.imgSelected {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2tpbGxzL3NraWxscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDRTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBRUo7O0FBQUU7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsZ0NBQUE7QUFHSjs7QUFERTtFQUNJLHVCQUFBO0VBQ0EsVUFBQTtBQUlOOztBQUZFO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFLTjs7QUFIRTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQU1KOztBQUpFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFPSjs7QUFMRTtFQUNFLFNBQUE7RUFDQSxXQUFBO0FBUUo7O0FBTkU7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFTSjs7QUFQRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFVSjs7QUFQRTtFQUNFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EseUVBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0FBVUo7O0FBUkU7RUFDRSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSwrREFBQTtFQUNBLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0FBV0o7O0FBVEU7RUFDRSxVQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtBQVlKOztBQVZFO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBYUo7O0FBWEU7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBY0oiLCJmaWxlIjoic3JjL2FwcC9za2lsbHMvc2tpbGxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0MSB7XG4gICAgd2lkdGg6IDI1M3B4O1xuICAgIGhlaWdodDogMzZweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gIH1cbiAgLmNlbnRlcmVkIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxNDBweDtcbiAgICBsZWZ0OiA5M3B4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB9XG4gIC5yb3d7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA5MCU7XG4gIH1cbiAgLmRpdiB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgfVxuICAucm93MSB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiAxMCU7XG4gIH1cbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfVxuICBpb24tY2FyZCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGhlaWdodDogNzAlO1xuICB9XG4gIC5zZWxlY3RlZCB7XG4gICAgaGVpZ2h0OiAxNjBweDtcbiAgICB3aWR0aDogMTYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC40KTtcbiAgfVxuICAuaW1nU2VsZWN0ZWQxe1xuICAgIGhlaWdodDogMTYwcHg7XG4gICAgd2lkdGg6IDE2MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgXG4gIH1cbiAgLmJ1dHRvbjEge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNGM0YzRjM7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAtLWNvbG9yOiAjQUFBQUFBO1xuICAgIC0tY29sb3ItYWN0aXZhdGVkOiAjZmZmZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuICAuYnV0dG9uMiB7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTs7IFxuICAgIC0tY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG4gIC5yb3czIHtcbiAgICB3aWR0aDogODAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiAxMCU7XG4gIH1cbiAgLmltZyB7XG4gICAgaGVpZ2h0OiAxNjBweDtcbiAgICB3aWR0aDogMTYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMC40KTtcbiAgfVxuICAuaW1nU2VsZWN0ZWR7XG4gICAgaGVpZ2h0OiAxNjBweDtcbiAgICB3aWR0aDogMTYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/skills/skills.page.ts":
/*!***************************************!*\
  !*** ./src/app/skills/skills.page.ts ***!
  \***************************************/
/*! exports provided: SkillsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsPage", function() { return SkillsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _modalpage_modalpage_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../modalpage/modalpage.page */ "./src/app/modalpage/modalpage.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");







let SkillsPage = class SkillsPage {
    constructor(router, modalController, storage, route, authService) {
        this.router = router;
        this.modalController = modalController;
        this.storage = storage;
        this.route = route;
        this.authService = authService;
        this.data = false;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
        this.currentSelected = null;
        this.count = 0;
        this.SkillArray = [];
        this.selectedHobby = [];
        this.category = false;
        this.editPost = false;
        this.route.queryParams.subscribe(params => {
            //ADD POST AND POST DETAILS
            if (params.editPost) {
                this.editPost = true;
                this.imgUrl = params.imgUrl;
                this.postDetail = JSON.parse(params.postDetail);
                this.category = params.category;
                this.pageRoute = params.pageRoute;
                // this.postType=params.postType
            }
            else if (params.addPost && params.postData) {
                this.addPost = true;
                this.imgUrl = params.imgUrl;
                this.postDetail = JSON.parse(params.postDetail);
                this.category = params.category;
                this.pageRoute = params.pageRoute;
                this.postType = params.postType;
            }
            else if (params.addPost && !params.postData) {
                this.addPost = true;
                this.imgUrl = params.imgUrl;
                this.category = params.category;
                this.pageRoute = params.pageRoute;
                this.postType = params.postType;
            }
        });
    }
    ngOnInit() {
        this.storage.get('userHobby').then((hobby) => {
            console.log(hobby);
            this.selectedHobby = hobby;
        });
    }
    onItemClicked(idx, x) {
        this.currentSelected = idx;
        this.count++;
        this.selectedCategory = x.hobby;
        this.skillImage = x.hobbyImage;
        console.log(this.selectedCategory);
        console.log(this.skillImage);
    }
    selectSkill1(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected = true;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectSkill2(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected2 = true;
        this.selected = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectSkill3(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected3 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectSkill4(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected4 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectSkill5(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected5 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectSkill6(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected6 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectSkill7(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected7 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected8 = false;
    }
    selectSkill8(i, j) {
        this.count++;
        this.selectedCategory = i;
        this.skillImage = j;
        console.log(this.selectedCategory);
        this.selected8 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
    }
    next() {
        if (this.selectedCategory != '' && this.selectedCategory != undefined) {
            this.SkillArray.push({ 'hobby': this.selectedCategory, 'hobbyImage': this.skillImage });
            this.storage.set("userSkills", this.SkillArray).then((res) => {
                console.log(res);
                if (this.SkillArray.length != 0) {
                    let navigationExtras = {
                        queryParams: {
                            special: this.route.url,
                        }
                    };
                    this.router.navigate(['profile'], navigationExtras);
                }
                else {
                    this.authService.presentToast('Please select your skill to continue.');
                }
            });
        }
    }
    selectCategory1(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected = true;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectCategory2(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected2 = true;
        this.selected = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectCategory3(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected3 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectCategory4(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected4 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectCategory5(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected5 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectCategory6(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected6 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    selectCategory7(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected7 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected8 = false;
    }
    selectCategory8(i) {
        this.count++;
        this.selectedCategory = i;
        console.log(this.selectedCategory);
        this.postDetail.postCategory = this.selectedCategory;
        this.selected8 = true;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
    }
    unSelectCategory() {
        this.count = 0;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
    }
    goToPost() {
        if (this.editPost) {
            this.postDetail.postUrl = this.imgUrl;
            let navigationExtras = {
                queryParams: {
                    post: JSON.stringify(this.postDetail),
                    categoryType: this.selectedCategory,
                    imgUrl: this.imgUrl,
                    editPost: true,
                    pageRoute: this.pageRoute,
                }
            };
            this.router.navigate(['post'], navigationExtras);
        }
        else if (this.addPost) {
            this.postDetail.postUrl = this.imgUrl;
            let navigationExtras = {
                queryParams: {
                    postDetail: JSON.stringify(this.postDetail),
                    categoryType: this.selectedCategory,
                    imgUrl: this.imgUrl,
                    addPost: true,
                    pageRoute: this.pageRoute,
                    postType: this.postType
                }
            };
            this.router.navigate(['post'], navigationExtras);
        }
    }
    presentModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modalpage_modalpage_page__WEBPACK_IMPORTED_MODULE_4__["ModalpagePage"],
                cssClass: 'modal',
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.router.navigateByUrl('profile');
            }
            else {
                this.router.navigateByUrl('profile');
            }
        });
    }
    back() {
        let navigationExtras = {
            queryParams: {
                hobbySelection: true
            }
        };
        this.router.navigate(['hobby'], navigationExtras);
        // this.router.navigateByUrl('hobby');
        this.storage.remove("userHobby");
        console.log(this.selectedHobby);
    }
};
SkillsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }
];
SkillsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-skills',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./skills.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/skills/skills.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./skills.page.scss */ "./src/app/skills/skills.page.scss")).default]
    })
], SkillsPage);



/***/ })

}]);
//# sourceMappingURL=skills-skills-module-es2015.js.map