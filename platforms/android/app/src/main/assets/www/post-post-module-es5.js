(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["post-post-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/post/post.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/post/post.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPostPostPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar1\">\n    <ion-row>\n      <ion-col [size]=2 (click)=\"back()\">\n        <ion-icon name=\"chevron-back\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text>Post</ion-text>\n      </ion-col> \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div *ngIf=\"addPost\" class=\"div\">\n    <ion-row class=\"row\">\n      <ion-text>This is your first post</ion-text>\n    </ion-row>\n    <ion-row class=\"row1\">\n      <ion-thumbnail *ngIf=\"postType=='0'\" class=\"thumbnail\">\n        <ion-img class=\"img\" [src]=\"imgUrl\"></ion-img>\n      </ion-thumbnail> \n      <div *ngIf=\"postType=='1'\">\n        <video width=\"315\" height=\"315\" #player playsinline preload=\"auto\" controls autoplay>\n          <source [src]=\"imgUrl\" type=\"video/mp4\"/>\n        </video>\n      </div>    \n    </ion-row>\n    <ion-row class=\"row5\" (click)=\"chooseCategory()\">\n      <ion-text *ngIf=\"category!=''\">{{category}}</ion-text>\n      <ion-text *ngIf=\"category==''\">Choose Category</ion-text>\n    </ion-row>\n    <ion-row class=\"row4\">\n      <ion-text>Post Title(optional)</ion-text>\n    </ion-row>\n    <ion-row class=\"row2\">\n      <ion-textarea (ionBlur)=\"saveData('title')\" class=\"textarea1\" placeholder='Title' \n      [(ngModel)]='postTitle'></ion-textarea>\n    </ion-row>\n    <ion-row class=\"row4\">\n      <ion-text>Write Something(optional)</ion-text>\n    </ion-row>\n    <ion-row class=\"row2\">\n      <ion-textarea (ionBlur)=\"saveData('description')\" class=\"textarea\" placeholder='About the post...' \n      [(ngModel)]='postDescription'></ion-textarea>\n    </ion-row>\n    <ion-row class=\"row3\" (click)=\"addNewPost()\">\n      <ion-button class=\"button2\" shape=\"block\">POST</ion-button>\n    </ion-row>\n  </div>\n\n  <div *ngIf=\"editPost\" class=\"div\">\n    <ion-row *ngIf=\"post.posttype=='0'\" class=\"row6\">\n      <ion-text>Edit Image</ion-text>\n      <img (click)=\"updateImage(1)\" class=\"img1\" src=\"../../assets/images/home/edit-2.svg\">\n    </ion-row>\n    <ion-row *ngIf=\"post.posttype=='1'\" class=\"row6\">\n      <ion-text>Edit Video</ion-text>\n      <img (click)=\"updateVideo(1)\" class=\"img1\" src=\"../../assets/images/home/edit-2.svg\">\n    </ion-row>\n      <ion-row class=\"row1\">\n        <ion-thumbnail *ngIf=\"post.posttype=='0'\" class=\"thumbnail\">\n          <ion-img class=\"img\" [src]=\"imgUrl\"></ion-img>\n        </ion-thumbnail>   \n        <div *ngIf=\"post.posttype=='1'\">\n          <video *ngIf=\"!uploaded1\" width=\"315\" height=\"315\" #player playsinline preload=\"auto\" controls autoplay>\n            <source [src]=\"imgUrl\" type=\"video/mp4\"/>\n          </video>\n          <video *ngIf=\"uploaded1\" width=\"315\" height=\"315\" #player playsinline preload=\"auto\" controls autoplay>\n            <source [src]=\"newImgUrl\" type=\"video/mp4\"/>\n          </video>\n        </div>  \n      </ion-row>\n      <ion-row class=\"row5\" (click)=\"chooseCategory()\">\n        <ion-text>{{post.postCategory}}</ion-text>\n        <!-- <ion-text *ngIf='cat'>{{category}}</ion-text> -->\n      </ion-row>\n      <ion-row class=\"row4\">\n        <ion-text>Post Title(optional)</ion-text>\n      </ion-row>\n      <ion-row class=\"row2\">\n        <ion-textarea class=\"textarea1\"\n        [(ngModel)]='post.postTitle'></ion-textarea>\n      </ion-row>\n      <ion-row class=\"row4\">\n        <ion-text>Write Something(optional)</ion-text>\n      </ion-row>\n      <ion-row class=\"row2\">\n        <ion-textarea class=\"textarea\"\n        [(ngModel)]='post.postDescription'></ion-textarea>\n      </ion-row>\n      <ion-row class=\"row3\" \n      (click)=\"updatePost(imgUrl,post.postCategory,post.postTitle,post.postDescription,newImgUrl)\">\n        <ion-button class=\"button2\" shape=\"block\">Update Post</ion-button>\n      </ion-row>  \n  </div>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/post/post-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/post/post-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: PostPageRoutingModule */

    /***/
    function srcAppPostPostRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PostPageRoutingModule", function () {
        return PostPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _post_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./post.page */
      "./src/app/post/post.page.ts");

      var routes = [{
        path: '',
        component: _post_page__WEBPACK_IMPORTED_MODULE_3__["PostPage"]
      }];

      var PostPageRoutingModule = function PostPageRoutingModule() {
        _classCallCheck(this, PostPageRoutingModule);
      };

      PostPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PostPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/post/post.module.ts":
    /*!*************************************!*\
      !*** ./src/app/post/post.module.ts ***!
      \*************************************/

    /*! exports provided: PostPageModule */

    /***/
    function srcAppPostPostModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PostPageModule", function () {
        return PostPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _post_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./post-routing.module */
      "./src/app/post/post-routing.module.ts");
      /* harmony import */


      var _post_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./post.page */
      "./src/app/post/post.page.ts");

      var PostPageModule = function PostPageModule() {
        _classCallCheck(this, PostPageModule);
      };

      PostPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _post_routing_module__WEBPACK_IMPORTED_MODULE_5__["PostPageRoutingModule"]],
        declarations: [_post_page__WEBPACK_IMPORTED_MODULE_6__["PostPage"]]
      })], PostPageModule);
      /***/
    },

    /***/
    "./src/app/post/post.page.scss":
    /*!*************************************!*\
      !*** ./src/app/post/post.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppPostPostPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-row {\n  margin-top: 0px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n@media (min-height: 568px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 640px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 667px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 731px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 736px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 812px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 823px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.row {\n  width: 90%;\n  justify-content: center;\n}\n.row ion-text {\n  font-family: \"Poppins-Regular\";\n  color: #003C69;\n  font-size: 14px;\n}\n.row1 {\n  width: 315px;\n  height: 315px;\n  margin-top: 5%;\n}\n.thumbnail {\n  width: 315px;\n  height: 315px;\n}\n.img {\n  width: 315px;\n  height: 315px;\n}\n.row2 {\n  justify-content: center;\n  margin-top: 5%;\n}\n.button2 {\n  width: 80%;\n  border-radius: 10px;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n}\n.row3 {\n  width: 80%;\n  justify-content: center;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.textarea {\n  width: 240px;\n  height: 100px;\n  padding-left: 7%;\n  background: #FFFFFF;\n}\n.textarea1 {\n  width: 240px;\n  height: 50px;\n  padding-left: 7%;\n  background: #FFFFFF;\n}\n.row4 {\n  width: 90%;\n  margin-top: 5%;\n  justify-content: center;\n}\n.row4 ion-text {\n  font-family: \"Poppins-Regular\";\n  color: #003C69;\n  font-size: 14px;\n  text-transform: capitalize;\n}\n.row5 {\n  border: 1px solid #003C69;\n  padding: 3%;\n  margin-top: 5%;\n  border-radius: 10px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.row5 ion-text {\n  font-family: \"Poppins-Regular\";\n  color: #003C69;\n  font-size: 14px;\n}\n.row6 {\n  width: 315px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n.row6 ion-text {\n  font-family: \"Poppins-Regular\";\n  color: #003C69;\n  font-size: 14px;\n}\n.img1 {\n  width: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcG9zdC9wb3N0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwrREFBQTtFQUNBLGdEQUFBO0VBQ0EsNEJBQUE7QUFDSjtBQUFJO0VBQ0UsZUFBQTtBQUVOO0FBQUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRVI7QUFBSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBRVI7QUFBSTtFQUNFLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUVOO0FBR0U7RUFFSTtJQUNFLGdCQUFBO0VBRE47QUFDRjtBQUtFO0VBRUk7SUFDRSxlQUFBO0VBSk47QUFDRjtBQVFFO0VBRUk7SUFDRSxnQkFBQTtFQVBOO0FBQ0Y7QUFXRTtFQUVJO0lBQ0UsZUFBQTtFQVZOO0FBQ0Y7QUFjRTtFQUVJO0lBQ0UsZ0JBQUE7RUFiTjtBQUNGO0FBaUJFO0VBRUk7SUFDRSxnQkFBQTtFQWhCTjtBQUNGO0FBb0JFO0VBRUk7SUFDRSxlQUFBO0VBbkJOO0FBQ0Y7QUF1QkU7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQXJCSjtBQXVCQTtFQUNJLFVBQUE7RUFDQSx1QkFBQTtBQXBCSjtBQXFCSTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFuQlI7QUFzQkE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUFuQko7QUFxQkE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQWxCSjtBQW9CQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0FBakJKO0FBbUJBO0VBQ0ksdUJBQUE7RUFDQSxjQUFBO0FBaEJKO0FBa0JBO0VBQ0ksVUFBQTtFQUNBLG1CQUFBO0VBQ0EsK0RBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQWZKO0FBaUJFO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBZEo7QUFnQkE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFiSjtBQWVBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBWkY7QUFjQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7QUFYSjtBQVlJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0FBVlI7QUFhQTtFQUNJLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFWSjtBQVdJO0VBQ0UsOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQVROO0FBWUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUFUSjtBQVVJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQVJSO0FBV0E7RUFDRSxXQUFBO0FBUkYiLCJmaWxlIjoic3JjL2FwcC9wb3N0L3Bvc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvb2xiYXIxIHtcbiAgICBoZWlnaHQ6IDc0cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAyNXB4IDI1cHg7XG4gICAgaW9uLXJvdyB7XG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgfVxuICAgIGlvbi1jb2wge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG4gICAgaW9uLWljb24ge1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgfVxuICAgIGlvbi10ZXh0IHtcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgICBmb250LXdlaWdodDogNzAwO1xuICAgIH1cbiAgfVxuXG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNTY4cHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA2NDBweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNjY3cHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA3MzFweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNzM2cHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA4MTJweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDgyM3B4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG4ucm93IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICB9XG59XG4ucm93MSB7XG4gICAgd2lkdGg6IDMxNXB4O1xuICAgIGhlaWdodDogMzE1cHg7XG4gICAgbWFyZ2luLXRvcDogNSU7XG59XG4udGh1bWJuYWlsIHtcbiAgICB3aWR0aDogMzE1cHg7XG4gICAgaGVpZ2h0OiAzMTVweDtcbn1cbi5pbWcge1xuICAgIHdpZHRoOiAzMTVweDtcbiAgICBoZWlnaHQ6IDMxNXB4O1xufVxuLnJvdzIge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDUlO1xufVxuLmJ1dHRvbjIge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7OyBcbiAgICAtLWNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAucm93MyB7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICB9XG4udGV4dGFyZWEge1xuICAgIHdpZHRoOiAyNDBweDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHBhZGRpbmctbGVmdDogNyU7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbn1cbi50ZXh0YXJlYTEge1xuICB3aWR0aDogMjQwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgcGFkZGluZy1sZWZ0OiA3JTtcbiAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbn1cbi5yb3c0IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgICAgfVxufVxuLnJvdzUge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDNDNjk7XG4gICAgcGFkZGluZzogMyU7XG4gICAgbWFyZ2luLXRvcDogNSU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBpb24tdGV4dCB7XG4gICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cbn1cbi5yb3c2IHtcbiAgICB3aWR0aDogMzE1cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgICAgICBjb2xvcjogIzAwM0M2OTtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgfVxufVxuLmltZzEge1xuICB3aWR0aDogMjVweDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/post/post.page.ts":
    /*!***********************************!*\
      !*** ./src/app/post/post.page.ts ***!
      \***********************************/

    /*! exports provided: PostPage */

    /***/
    function srcAppPostPostPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PostPage", function () {
        return PostPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/file-path/ngx */
      "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/fire/storage */
      "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
      /* harmony import */


      var firebase_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! firebase/storage */
      "./node_modules/firebase/storage/dist/index.esm.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var MAX_FILE_SIZE = 5 * 1024 * 1024;
      var ALLOWED_MIME_TYPE = "video/mp4";

      var PostPage = /*#__PURE__*/function () {
        function PostPage(platform, route, modalController, authService, angularstorage, file, filePath, httpClient, actionSheetController, storage, camera, router) {
          var _this = this;

          _classCallCheck(this, PostPage);

          this.platform = platform;
          this.route = route;
          this.modalController = modalController;
          this.authService = authService;
          this.angularstorage = angularstorage;
          this.file = file;
          this.filePath = filePath;
          this.httpClient = httpClient;
          this.actionSheetController = actionSheetController;
          this.storage = storage;
          this.camera = camera;
          this.router = router;
          this.category = "";
          this.cat = false;
          this.post = {};
          this.userData = {};
          this.editPost = false;
          this.postData = false;
          this.uploaded = false;
          this.uploaded1 = false;
          this.addPost = false;
          this.selectedVideo = "";
          this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          };
          this.gelleryOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL
          };
          this.route.queryParams.subscribe(function (params) {
            _this.postType = params.postType;
            _this.category = "";
            console.log(params);

            if (params.editPost && params.editPost != undefined) {
              _this.editPost = true;
              _this.addPost = false;
              _this.pageRoute = params.pageRoute;
              _this.post = JSON.parse(params.post);
              _this.imgUrl = _this.post.postUrl;
            } else if (params.addPost && params.postDetail) {
              _this.editPost = false;
              _this.pageRoute = params.pageRoute;
              _this.post = JSON.parse(params.postDetail);
              _this.imgUrl = _this.post.postUrl;
              _this.category = _this.post.postCategory;
            } else if (params.addPost && params.addPost != undefined) {
              _this.addPost = true;
              _this.editPost = false;
              _this.pageRoute = params.pageRoute;
              _this.imgUrl = params.imgUrl;
            }
          });
        }

        _createClass(PostPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.uploaded1 = false;
            this.storage.get('user').then(function (user) {
              _this2.userId = user._id;
              _this2.userData = user;
            });
          }
        }, {
          key: "saveData",
          value: function saveData(x) {
            if (x.length == 0) {
              console.log('Do Nothing');
            } else {
              if (x == 'title') {
                this.post.postTitle = this.postTitle;
                this.postData = true;
              } else if (x == 'description') {
                this.post.postDescription = this.postDescription;
                this.postData = true;
              }
            }
          }
        }, {
          key: "addNewPost",
          value: function addNewPost() {
            var _this3 = this;

            var obj = {
              'postUrl': this.imgUrl,
              'postDescription': this.post.postDescription,
              'userId': this.userData._id,
              'posttype': this.postType,
              'postTitle': this.post.postTitle,
              'postCategory': this.category,
              'userImg': this.userData.user_img,
              'userName': this.userData.fullName
            };
            this.authService.addNewPost(obj).subscribe(function (data) {
              console.log(data);

              if (data.success) {
                _this3.authService.presentToast('Post added Successfully');

                var navigationExtras = {
                  queryParams: {
                    addPost: true
                  }
                };

                _this3.router.navigate([_this3.pageRoute], navigationExtras);
              }
            });
          }
        }, {
          key: "updatePost",
          value: function updatePost(a, b, c, d, e) {
            var _this4 = this;

            console.log(a);
            console.log(b);
            console.log(c);
            console.log(d);

            if (this.uploaded1) {
              this.post.postUrl = e;
              this.post.postCategory = b;
              this.post.postTitle = c;
              this.post.postDescription = d;
              this.post.userId = this.userId;
              var obj = {
                'post': this.post
              };
              this.authService.updatePost(obj).subscribe(function (data) {
                console.log(data);

                if (data.success) {
                  _this4.router.navigateByUrl(_this4.pageRoute);

                  _this4.authService.presentToast('Post Updated.');
                } else {
                  _this4.authService.presentToast('Something went wrong.');
                }
              });
            } else {
              this.post.postUrl = a;
              this.post.postCategory = b;
              this.post.postTitle = c;
              this.post.postDescription = d;
              this.post.userId = this.userId;
              var obj = {
                'post': this.post
              };
              this.authService.updatePost(obj).subscribe(function (data) {
                console.log(data);

                if (data.success) {
                  _this4.router.navigateByUrl(_this4.pageRoute);

                  _this4.authService.presentToast('Post Updated.');
                } else {
                  _this4.authService.presentToast('Something went wrong.');
                }
              });
            }
          }
        }, {
          key: "chooseCategory",
          value: function chooseCategory() {
            if (this.editPost) {
              var navigationExtras = {
                queryParams: {
                  editPost: this.editPost,
                  imgUrl: this.imgUrl,
                  postDetail: JSON.stringify(this.post),
                  category: true,
                  pageRoute: this.pageRoute
                }
              };
              this.router.navigate(['skills'], navigationExtras);
            } else if (this.addPost) {
              if (this.postData) {
                var _navigationExtras = {
                  queryParams: {
                    addPost: this.addPost,
                    imgUrl: this.imgUrl,
                    postDetail: JSON.stringify(this.post),
                    category: true,
                    postData: this.postData,
                    pageRoute: this.pageRoute,
                    postType: this.postType
                  }
                };
                this.router.navigate(['skills'], _navigationExtras);
              } else {
                var _navigationExtras2 = {
                  queryParams: {
                    addPost: this.addPost,
                    imgUrl: this.imgUrl,
                    postDetail: JSON.stringify(this.post),
                    category: true,
                    postData: this.postData,
                    pageRoute: this.pageRoute,
                    postType: this.postType
                  }
                };
                this.router.navigate(['skills'], _navigationExtras2);
              }
            }
          }
        }, {
          key: "updateImage",
          value: function updateImage(i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this5 = this;

              var actionSheet;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.actionSheetController.create({
                        header: "Select Image source",
                        buttons: [{
                          text: 'Load from Library',
                          handler: function handler() {
                            _this5.takePicture(_this5.camera.PictureSourceType.PHOTOLIBRARY, i);
                          }
                        }, {
                          text: 'Use Camera',
                          handler: function handler() {
                            _this5.takePicture(_this5.camera.PictureSourceType.CAMERA, i);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      actionSheet = _context.sent;
                      _context.next = 5;
                      return actionSheet.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "updateVideo",
          value: function updateVideo(i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this6 = this;

              var actionSheet;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.uploaded1 = false;
                      _context2.next = 3;
                      return this.actionSheetController.create({
                        header: "Select Image source",
                        buttons: [{
                          text: 'Upload Video',
                          handler: function handler() {
                            _this6.takePicture1(_this6.camera.PictureSourceType.PHOTOLIBRARY, i);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 3:
                      actionSheet = _context2.sent;
                      _context2.next = 6;
                      return actionSheet.present();

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "takePicture",
          value: function takePicture(sourceType, i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this7 = this;

              var options, tempImage, tempFilename, tempBaseFilesystemPath, newBaseFilesystemPath, storedPhoto, _options;

              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (!this.platform.is('ios')) {
                        _context3.next = 14;
                        break;
                      }

                      options = {
                        quality: 100,
                        targetWidth: 900,
                        targetHeight: 600,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      _context3.next = 4;
                      return this.camera.getPicture(options);

                    case 4:
                      tempImage = _context3.sent;
                      tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1); // Now, the opposite. Extract the full path, minus filename.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/

                      tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1); // Get the Data directory on the device.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/

                      newBaseFilesystemPath = this.file.dataDirectory;
                      _context3.next = 10;
                      return this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);

                    case 10:
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                      storedPhoto = newBaseFilesystemPath + tempFilename;
                      this.file.resolveLocalFilesystemUrl(storedPhoto).then(function (entry) {
                        entry.file(function (file) {
                          return _this7.readFile(file, i);
                        });
                      })["catch"](function (err) {
                        console.log(err); // this.presentToast('Error while reading file.');
                      });
                      _context3.next = 16;
                      break;

                    case 14:
                      _options = {
                        quality: 100,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      this.camera.getPicture(_options).then(function (imageData) {
                        _this7.file.resolveLocalFilesystemUrl(imageData).then(function (entry) {
                          entry.file(function (file) {
                            console.log(file);

                            _this7.readFile(file, i);
                          });
                        });
                      }, function (err) {// Handle error
                      });

                    case 16:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "takePicture1",
          value: function takePicture1(sourceType, i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this8 = this;

              var options;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      options = {
                        mediaType: this.camera.MediaType.VIDEO,
                        sourceType: sourceType
                      };
                      this.camera.getPicture(options).then(function (videoUrl) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                          var _this9 = this;

                          var filename, dirpath, dirUrl, retrievedFile;
                          return regeneratorRuntime.wrap(function _callee4$(_context4) {
                            while (1) {
                              switch (_context4.prev = _context4.next) {
                                case 0:
                                  if (!videoUrl) {
                                    _context4.next = 20;
                                    break;
                                  }

                                  this.authService.loading('Please Wait');
                                  this.uploadedVideo = null;
                                  filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
                                  dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);
                                  dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
                                  _context4.prev = 6;
                                  _context4.next = 9;
                                  return this.file.resolveDirectoryUrl(dirpath);

                                case 9:
                                  dirUrl = _context4.sent;
                                  _context4.next = 12;
                                  return this.file.getFile(dirUrl, filename, {});

                                case 12:
                                  retrievedFile = _context4.sent;
                                  _context4.next = 19;
                                  break;

                                case 15:
                                  _context4.prev = 15;
                                  _context4.t0 = _context4["catch"](6);
                                  this.authService.dismissLoading();
                                  return _context4.abrupt("return", this.authService.presentToast("Error Something went wrong."));

                                case 19:
                                  retrievedFile.file(function (data) {
                                    console.log(data);

                                    _this9.authService.dismissLoading();

                                    if (data.size > MAX_FILE_SIZE) return _this9.authService.presentToast("Error You cannot upload more than 5mb.");
                                    if (data.type !== ALLOWED_MIME_TYPE) return _this9.authService.presentToast("Error Incorrect file type.");
                                    _this9.selectedVideo = retrievedFile.nativeURL;
                                    console.log(_this9.selectedVideo);

                                    _this9.uploadFile(retrievedFile);
                                  });

                                case 20:
                                case "end":
                                  return _context4.stop();
                              }
                            }
                          }, _callee4, this, [[6, 15]]);
                        }));
                      }, function (err) {
                        console.log(err);
                      });

                    case 2:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "uploadFile",
          value: function uploadFile(f) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var path, type, buffer, fileBlob, randomId;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
                      type = this.getMimeType(f.name.split('.').pop());
                      _context6.next = 4;
                      return this.file.readAsArrayBuffer(path, f.name);

                    case 4:
                      buffer = _context6.sent;
                      fileBlob = new Blob([buffer], type);
                      randomId = Math.random().toString(36).substring(2, 8);
                      this.upload2FirebaseVideo(fileBlob);

                    case 8:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "upload2FirebaseVideo",
          value: function upload2FirebaseVideo(video) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this10 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      this.authService.loading('Loading Video..');
                      file = video;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context7.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this10.imgUrl = value;
                          _this10.newImgUrl = value;
                          _this10.uploaded1 = true;
                          _this10.uploaded = true;
                          console.log(_this10.imgUrl);

                          _this10.authService.dismissLoading();
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "getMimeType",
          value: function getMimeType(fileExt) {
            if (fileExt == 'wav') return {
              type: 'audio/wav'
            };else if (fileExt == 'jpg') return {
              type: 'image/jpg'
            };else if (fileExt == 'mp4') return {
              type: 'video/mp4'
            };else if (fileExt == 'MOV') return {
              type: 'video/quicktime'
            };
          }
        }, {
          key: "readFile",
          value: function readFile(file, i) {
            var _this11 = this;

            var reader = new FileReader();

            reader.onload = function () {
              var imgBlob = new Blob([reader.result], {
                type: file.type
              });

              if (i === 1) {
                _this11.upload2Firebase(imgBlob);
              } else {
                console.log("if second image");
              }
            };

            reader.readAsArrayBuffer(file);
          }
        }, {
          key: "upload2Firebase",
          value: function upload2Firebase(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var _this12 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      this.authService.loading('Loading Image..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath);
                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context8.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this12.post.imgUrl = value;
                          _this12.imgUrl = value;
                          _this12.uploaded = true;

                          _this12.authService.dismissLoading();
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }, {
          key: "makeid",
          value: function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;

            for (var i = 0; i < length; i++) {
              result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            return result;
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl(this.pageRoute);
          }
        }]);

        return PostPage;
      }();

      PostPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["Platform"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["ModalController"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]
        }, {
          type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__["File"]
        }, {
          type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["ActionSheetController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
        }, {
          type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      PostPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-post',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./post.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/post/post.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./post.page.scss */
        "./src/app/post/post.page.scss"))["default"]]
      })], PostPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=post-post-module-es5.js.map