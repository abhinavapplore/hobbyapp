(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile2-profile2-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile2/profile2.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile2/profile2.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-col [size]=2 (click)=\"back()\">\n        <ion-icon style=\"color:white;font-size:30px\" name=\"chevron-back\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text>Profile</ion-text>\n      </ion-col>\n      <ion-col [size]=2 (click)=\"notification()\">\n        <ion-icon style=\"color:white;font-size:30px\" name=\"notifications-outline\"></ion-icon>\n      </ion-col>    \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div class=\"div\">\n    <ion-row class=\"row\">\n      <ion-avatar>\n        <ion-img [src]=\"userData.user_img\"></ion-img>\n      </ion-avatar>\n      <div class=\"centered\" (click)=\"presentActionSheet1(1)\">\n        <img class=\"img\" src=\"../../assets/images/home/edit-2.svg\">\n      </div>\n      <div style=\"width: 100%;\n      display: flex;\n      flex-direction: row;\n      justify-content: center;\n      text-transform: capitalize;\n      position: absolute;\n      top: 160px;\">\n        <ion-text>{{userData.firstName}} {{userData.lastName}}</ion-text>\n      </div>\n    </ion-row>\n    <ion-row class=\"row1\">\n      <ion-text>About</ion-text>\n    </ion-row>\n    <ion-row class=\"row6\" >\n      <ion-textarea *ngIf='userData.about!=null || userData.about!=undefined'\n       [(ngModel)]=\"userData.about\" ></ion-textarea>\n       <ion-textarea (ionChange)=\"updateAboutUser()\" *ngIf='userData.about==null || userData.about==undefined'\n       [(ngModel)]=\"about\" placeholder='Type here...'></ion-textarea>\n    </ion-row>\n    <ion-row class=\"row5\">\n      <ion-text>Media</ion-text>\n    </ion-row>\n    <div class=\"div4\">\n      <div class=\"div6\">\n        <ion-row class=\"row11\">\n          <ion-col [size]=5.5 class=\"col5\">\n             <div class=\"centered3\">\n              <ion-thumbnail style=\"display: flex;\n              flex-direction: row;\n              justify-content: center;\n              align-items: center;\" class=\"thumbnail1\" >\n               <ion-img (click)=\"viewPhoto(doc1)\" *ngIf=\"doc1!=undefined && doc1!=''\" class=\"img1\" [src]=\"doc1\"></ion-img>\n               <ion-icon (click)=\"presentActionSheet1(2,'doc1')\" *ngIf=\"doc1==undefined||doc1==''||doc1==null\" class=\"icon1\" name=\"add-outline\"></ion-icon>\n              </ion-thumbnail>          \n             </div>\n          </ion-col>\n          <ion-col [size]=5.5 class=\"col5\"  >\n               <div class=\"centered3\">\n                <ion-thumbnail style=\"display: flex;\n                flex-direction: row;\n                justify-content: center;\n                align-items: center;\" class=\"thumbnail1\"  >\n                  <ion-img (click)=\"viewPhoto(doc2)\" *ngIf=\"doc2!=undefined && doc2!=''\" class=\"img1\" [src]=\"doc2\"></ion-img>\n                  <ion-icon (click)=\"presentActionSheet1(3,'doc1')\" *ngIf=\"doc2==undefined||doc2==''||doc2==null\" class=\"icon1\" name=\"add-outline\"></ion-icon>\n                </ion-thumbnail>\n               </div>\n          </ion-col>\n        </ion-row>\n      </div>\n    </div>\n    <ion-row class=\"row2\">\n      <ion-text>Interests</ion-text>\n      <img class=\"img\" src=\"../../assets/images/home/edit-2.svg\" (click)=\"editInterest()\">\n    </ion-row>\n    <ion-row class=\"row8\">\n      <ion-slides style=\"margin: 0;\" [options]=\"slideOpts\">\n        <ion-slide *ngFor=\"let item of userData.userHobby\">\n          <ion-card class=\"container\">\n            <ion-thumbnail class=\"thumbnail\">\n              <ion-img class=\"imgSelected\" [src]=\"item.hobbyImage\"></ion-img>\n            </ion-thumbnail>           \n            <div class=\"centered\">\n              <ion-text>{{item.hobby}}</ion-text>\n            </div>\n          </ion-card>\n        </ion-slide>        \n      </ion-slides>\n    </ion-row>\n\n    <div>\n        <ion-card class=\"row9\">\n          <div class=\"div1\">\n            <ion-icon name=\"call-outline\"></ion-icon>\n            <ion-text>+91-{{userData.contactNum}}</ion-text>\n          </div>\n          <div class=\"div1\">\n            <ion-icon name=\"mail-outline\"></ion-icon>\n            <ion-text>{{userData.emailId}}</ion-text>\n          </div>\n          <div class=\"div1\">\n            <ion-icon name=\"location-outline\"></ion-icon>\n            <ion-text>{{userData.address}}</ion-text>\n          </div>\n        </ion-card>\n    </div>\n    \n      <ion-row style=\"padding-top: 15%;\">\n        <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n      </ion-row>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/profile2/profile2-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/profile2/profile2-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: Profile2PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile2PageRoutingModule", function() { return Profile2PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile2.page */ "./src/app/profile2/profile2.page.ts");




const routes = [
    {
        path: '',
        component: _profile2_page__WEBPACK_IMPORTED_MODULE_3__["Profile2Page"]
    }
];
let Profile2PageRoutingModule = class Profile2PageRoutingModule {
};
Profile2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Profile2PageRoutingModule);



/***/ }),

/***/ "./src/app/profile2/profile2.module.ts":
/*!*********************************************!*\
  !*** ./src/app/profile2/profile2.module.ts ***!
  \*********************************************/
/*! exports provided: Profile2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile2PageModule", function() { return Profile2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile2-routing.module */ "./src/app/profile2/profile2-routing.module.ts");
/* harmony import */ var _profile2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile2.page */ "./src/app/profile2/profile2.page.ts");







let Profile2PageModule = class Profile2PageModule {
};
Profile2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Profile2PageRoutingModule"]
        ],
        declarations: [_profile2_page__WEBPACK_IMPORTED_MODULE_6__["Profile2Page"]]
    })
], Profile2PageModule);



/***/ }),

/***/ "./src/app/profile2/profile2.page.scss":
/*!*********************************************!*\
  !*** ./src/app/profile2/profile2.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-row {\n  margin-top: 0px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n@media (min-height: 568px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 640px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 667px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 731px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 736px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 812px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 823px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.row {\n  width: 90%;\n  padding-top: 5%;\n  justify-content: center;\n  position: relative;\n}\n.row ion-avatar {\n  height: 125px;\n  width: 125px;\n}\n.row .centered {\n  position: absolute;\n  top: 115px;\n  left: 235px;\n}\n.row .centered1 {\n  position: absolute;\n  top: 160px;\n  left: 175px;\n}\n.row ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #000000;\n}\n.row5 {\n  padding-top: 5%;\n  width: 90%;\n}\n.row5 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row1 {\n  padding-top: 15%;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row2 {\n  padding-top: 5%;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.row2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row6 {\n  height: 180px;\n  width: 90%;\n  background: #FFFFFF;\n  padding: 5%;\n  margin-top: 3%;\n  border-radius: 5px;\n  border: 1px solid lightgrey;\n}\n.row6 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #999999;\n  text-align: justify;\n}\n.div6 {\n  box-sizing: border-box;\n  width: 370px;\n  height: 140px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  overflow: visible;\n  background-color: #ffffff;\n}\n.div4 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 1%;\n}\n.row11 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n  padding-left: 2%;\n  padding-right: 2%;\n}\n.col5 {\n  position: relative;\n  text-align: center;\n  background: #F6F7F9;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  height: 100px;\n  border-radius: 10px;\n}\n.centered1 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.icon1 {\n  color: #79797A;\n  font-size: 50px;\n}\n.row8 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row8 .container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 22px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n  margin: 0;\n  height: 145px;\n  width: 160px;\n}\n.row8 .centered {\n  position: absolute;\n  top: 85%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.row8 .imgSelected {\n  height: 145px;\n  width: 160px;\n}\n.thumbnail {\n  height: 145px;\n  width: 160px;\n}\n.thumbnail1 {\n  width: 155px;\n  height: 95px;\n  border-radius: 10px;\n}\n.row9 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  box-sizing: border-box;\n  width: 336px;\n  height: 126px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  position: relative;\n  margin-top: 5%;\n}\n.row9 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row9 .div1 ion-icon {\n  margin-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row9 .div1 ion-text {\n  margin-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row9 .img {\n  width: 50%;\n}\n.centered3 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.centered4 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.img1 {\n  width: 155px;\n  height: 95px;\n  border-radius: 10px;\n}\n.div8 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.div9 {\n  height: 100px;\n  width: 85%;\n  position: relative;\n  overflow: hidden;\n}\n.row4 {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n.row4 .col {\n  width: 160px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n.row4 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZTIvcHJvZmlsZTIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLCtEQUFBO0VBQ0EsZ0RBQUE7RUFDQSw0QkFBQTtBQUNKO0FBQUk7RUFDRSxlQUFBO0FBRU47QUFBSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFFUjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUjtBQUFJO0VBQ0UsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBRU47QUFJRTtFQUVJO0lBQ0UsZ0JBQUE7RUFGTjtBQUNGO0FBTUU7RUFFSTtJQUNFLGVBQUE7RUFMTjtBQUNGO0FBU0U7RUFFSTtJQUNFLGdCQUFBO0VBUk47QUFDRjtBQVlFO0VBRUk7SUFDRSxlQUFBO0VBWE47QUFDRjtBQWVFO0VBRUk7SUFDRSxnQkFBQTtFQWROO0FBQ0Y7QUFrQkU7RUFFSTtJQUNFLGdCQUFBO0VBakJOO0FBQ0Y7QUFxQkU7RUFFSTtJQUNFLGVBQUE7RUFwQk47QUFDRjtBQXlCRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUF2Qko7QUF5QkE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUF0Qko7QUF1Qkk7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQXJCUjtBQXVCSTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFyQk47QUF1Qk07RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDSixXQUFBO0FBckJKO0FBdUJNO0VBQ0UsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQXJCUjtBQXdCQTtFQUNJLGVBQUE7RUFDQSxVQUFBO0FBckJKO0FBc0JJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQXBCUjtBQXVCRTtFQUNNLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFwQlI7QUFxQlE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBbkJaO0FBc0JFO0VBQ00sZUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FBbkJSO0FBb0JRO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQWxCWjtBQXFCRTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7QUFsQko7QUFtQkk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFqQlI7QUFvQkU7RUFDRSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUFqQko7QUFtQkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFoQko7QUFrQkU7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFmSjtBQWlCQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FBZEo7QUFnQkU7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFiSjtBQWVFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFaSjtBQWNFO0VBQ0UsZUFBQTtFQUNBLFVBQUE7QUFYSjtBQVlJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUFWUjtBQVlNO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBVlI7QUFZTTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBVlI7QUFhRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBVko7QUFZRTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFUSjtBQVdFO0VBQ0UsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFDQSwrQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBUko7QUFTSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFQUjtBQVFRO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBTlY7QUFRUTtFQUNFLGVBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBTlY7QUFTTTtFQUNFLFVBQUE7QUFQUjtBQVVFO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBUEo7QUFTRTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQU5KO0FBUUU7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBTEo7QUFPRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQUpKO0FBTUU7RUFDRSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFISjtBQU9FO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQUpKO0FBS0k7RUFDRSxZQUFBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDZEQUFBO0VBQ0EsZ0RBQUE7RUFDQSxtQkFBQTtBQUhSO0FBS0k7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBSFIiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlMi9wcm9maWxlMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9vbGJhcjEge1xuICAgIGhlaWdodDogNzRweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDI1cHggMjVweDtcbiAgICBpb24tcm93IHtcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICB9XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICB9XG4gICAgaW9uLXRleHQge1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgfVxuICB9XG5cblxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDU2OHB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNjQwcHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDY2N3B4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNzMxcHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDczNnB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogODEycHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA4MjNweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cblxuICAuZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5yb3cge1xuICAgIHdpZHRoOiA5MCU7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBpb24tYXZhdGFyIHtcbiAgICAgICAgaGVpZ2h0OiAxMjVweDtcbiAgICAgICAgd2lkdGg6IDEyNXB4O1xuICAgIH1cbiAgICAuY2VudGVyZWQge1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiAxMTVweDtcbiAgICAgIGxlZnQ6IDIzNXB4O1xuICAgICAgfVxuICAgICAgLmNlbnRlcmVkMSB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAxNjBweDtcbiAgICBsZWZ0OiAxNzVweDtcbiAgICAgIH1cbiAgICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLVJlZ3VsYXInO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgfVxufVxuLnJvdzUge1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIH1cbiAgfVxuICAucm93MSB7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxNSU7XG4gICAgICAgIHdpZHRoOiA5MCU7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgaW9uLXRleHQge1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgICAgIH1cbiAgfVxuICAucm93MiB7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgfVxuICB9XG4gIC5yb3c2IHtcbiAgICBoZWlnaHQ6IDE4MHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICBwYWRkaW5nOiA1JTtcbiAgICBtYXJnaW4tdG9wOiAzJTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmV5O1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGNvbG9yOiAjOTk5OTk5O1xuICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIH1cbiAgfVxuICAuZGl2NiB7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICB3aWR0aDogMzcwcHg7XG4gICAgaGVpZ2h0OiAxNDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuLmRpdjQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiAxJTtcbiAgfVxuICAucm93MTEge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmctbGVmdDogMiU7XG4gICAgcGFkZGluZy1yaWdodDogMiU7XG59XG4uY29sNSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiAjRjZGN0Y5O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIH1cbiAgLmNlbnRlcmVkMSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgfVxuICAuaWNvbjEge1xuICAgIGNvbG9yOiAjNzk3OTdBO1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgfVxuICAucm93OCB7XG4gICAgcGFkZGluZy10b3A6IDMlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgLmNvbnRhaW5lciB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGhlaWdodDogMTQ1cHg7XG4gICAgICAgIHdpZHRoOiAxNjBweDtcbiAgICAgIH1cbiAgICAgIC5jZW50ZXJlZCB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiA4NSU7XG4gICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgICB9XG4gICAgICAuaW1nU2VsZWN0ZWR7XG4gICAgICAgIGhlaWdodDogMTQ1cHg7XG4gICAgICAgIHdpZHRoOiAxNjBweDtcbiAgICAgIH1cbiAgfVxuICAudGh1bWJuYWlsIHtcbiAgICBoZWlnaHQ6IDE0NXB4O1xuICAgIHdpZHRoOiAxNjBweDtcbiAgfVxuICAudGh1bWJuYWlsMSB7XG4gICAgd2lkdGg6IDE1NXB4O1xuICAgIGhlaWdodDogOTVweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB9XG4gIC5yb3c5IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICB3aWR0aDogMzM2cHg7XG4gICAgaGVpZ2h0OiAxMjZweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogNSU7XG4gICAgLmRpdjEge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAyJTtcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLXRleHQge1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtUmVndWxhcic7XG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xuICAgICAgICB9XG4gICAgfVxuICAgICAgLmltZyB7XG4gICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICB9XG4gIH1cbiAgLmNlbnRlcmVkMyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgfVxuICAuY2VudGVyZWQ0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB9XG4gIC5pbWcxIHtcbiAgICB3aWR0aDogMTU1cHg7XG4gICAgaGVpZ2h0OiA5NXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIH1cbiAgLmRpdjgge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgfVxuICAuZGl2OSB7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICB3aWR0aDogODUlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICB9XG5cblxuICAucm93NCB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgLmNvbCB7XG4gICAgICB3aWR0aDogMTYwcHg7XG4gICAgICAgIGhlaWdodDogNTVweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgfVxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/profile2/profile2.page.ts":
/*!*******************************************!*\
  !*** ./src/app/profile2/profile2.page.ts ***!
  \*******************************************/
/*! exports provided: Profile2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile2Page", function() { return Profile2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/photo-viewer/ngx */ "./node_modules/@ionic-native/photo-viewer/__ivy_ngcc__/ngx/index.js");
















let Profile2Page = class Profile2Page {
    constructor(router, route, storage, authService, platform, modalController, angularstorage, file, filePath, httpClient, photoViewer, actionSheetController, geolocation, camera) {
        this.router = router;
        this.route = route;
        this.storage = storage;
        this.authService = authService;
        this.platform = platform;
        this.modalController = modalController;
        this.angularstorage = angularstorage;
        this.file = file;
        this.filePath = filePath;
        this.httpClient = httpClient;
        this.photoViewer = photoViewer;
        this.actionSheetController = actionSheetController;
        this.geolocation = geolocation;
        this.camera = camera;
        this.userData = {};
        this.aboutUser = false;
        this.updateAddress = false;
        this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.gelleryOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
        };
        this.slideOpts = {
            slidesPerView: 2,
            spaceBetween: 10,
            initialSlide: 0,
        };
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                this.pageRoute = params.special;
                this.loadData();
            }
        });
    }
    ngOnInit() {
        console.log(this.about);
        this.loadData();
    }
    loadData() {
        this.storage.get('user').then((user) => {
            var obj = { "userId": user._id };
            this.authService.getUserProfileById(obj).subscribe((data) => {
                console.log(data);
                this.userData = data.data;
                this.doc1 = this.userData.documents[0].docUrl;
                this.doc2 = this.userData.documents[1].docUrl;
            });
        });
    }
    ionViewDidEnter() {
        this.getUserProfileById();
    }
    viewPhoto(x) {
        console.log(x);
        this.photoViewer.show(x);
    }
    notification() {
        let navigationExtras = {
            queryParams: {
                special: this.router.url
            }
        };
        this.router.navigate(['notification'], navigationExtras);
    }
    updateAboutUser() {
        var obj = { "key": "about", "value": this.about, "userId": this.userData._id };
        this.authService.updateProfile(obj).subscribe((data) => {
            console.log(data);
            this.getUserProfileById();
        });
    }
    presentActionSheet1(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: "Select Image source",
                buttons: [{
                        text: 'Load from Library',
                        handler: () => {
                            this.takePicture1(this.camera.PictureSourceType.PHOTOLIBRARY, i);
                        }
                    },
                    {
                        text: 'Use Camera',
                        handler: () => {
                            this.takePicture1(this.camera.PictureSourceType.CAMERA, i);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    takePicture1(sourceType, i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.platform.is('ios')) {
                const options = {
                    quality: 100,
                    targetWidth: 900,
                    targetHeight: 600,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: sourceType,
                    encodingType: this.camera.EncodingType.JPEG
                };
                const tempImage = yield this.camera.getPicture(options);
                const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
                // Now, the opposite. Extract the full path, minus filename.
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
                const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
                // Get the Data directory on the device.
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
                const newBaseFilesystemPath = this.file.dataDirectory;
                yield this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                const storedPhoto = newBaseFilesystemPath + tempFilename;
                this.file.resolveLocalFilesystemUrl(storedPhoto)
                    .then(entry => {
                    entry.file(file => this.readFile(file, i));
                })
                    .catch(err => {
                    console.log(err);
                    // this.presentToast('Error while reading file.');
                });
            }
            else {
                const options = {
                    quality: 100,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: sourceType,
                    encodingType: this.camera.EncodingType.JPEG,
                };
                this.camera.getPicture(options).then((imageData) => {
                    // this._file.resolveLocalFilesystemUrl(
                    //   imageData,
                    //   (entry: FileEntry) => {console.log(entry)},
                    //   err => console.log(err)
                    // );
                    this.file.resolveLocalFilesystemUrl(imageData).then((entry) => {
                        entry.file(file => {
                            console.log(file);
                            this.readFile(file, i);
                        });
                    });
                }, (err) => {
                    // Handle error
                });
            }
        });
    }
    readFile(file, i) {
        const reader = new FileReader();
        reader.onload = () => {
            // const formData = new FormData();
            const imgBlob = new Blob([reader.result], {
                type: file.type
            });
            // formData.append('file', imgBlob, file.name);
            // this.uploadImageData(formData);
            if (i === 1) {
                this.upload2Firebase1(imgBlob);
            }
            else if (i === 2) {
                this.upload2Firebase2(imgBlob);
            }
            else if (i === 3) {
                this.upload2Firebase3(imgBlob);
            }
            else {
                console.log("if second image");
            }
        };
        reader.readAsArrayBuffer(file);
    }
    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    upload2Firebase1(image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.loading('Loading Profile..');
            const file = image;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            yield task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(() => fileRef.getDownloadURL().subscribe(value => {
                this.imgUrl1 = value;
                console.log(this.imgUrl1);
                this.uploaded1 = true;
                var obj = { "key": "user_img", "value": this.imgUrl1, "userId": this.userData._id };
                this.authService.updateProfile(obj).subscribe((data) => {
                    console.log(data);
                    if (data.success) {
                        this.authService.dismissLoading();
                    }
                });
            })))
                .subscribe();
        });
    }
    upload2Firebase2(image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.loading('Loading Profile..');
            const file = image;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            yield task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(() => fileRef.getDownloadURL().subscribe(value => {
                this.doc1 = value;
                console.log(this.doc1);
                this.uploaded2 = true;
                var obj = { "key": "documents['doc1']", "value": this.doc1, "userId": this.userData._id };
                this.authService.updateProfile(obj).subscribe((data) => {
                    console.log(data);
                    if (data.success) {
                        this.authService.dismissLoading();
                    }
                });
            })))
                .subscribe();
        });
    }
    upload2Firebase3(image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.loading('Loading Profile..');
            const file = image;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            yield task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(() => fileRef.getDownloadURL().subscribe(value => {
                this.doc2 = value;
                console.log(this.doc2);
                this.uploaded3 = true;
                var obj = { "key": "documents['doc2']", "value": this.doc2, "userId": this.userData._id };
                this.authService.updateProfile(obj).subscribe((data) => {
                    console.log(data);
                    if (data.success) {
                        this.authService.dismissLoading();
                    }
                });
            })))
                .subscribe();
        });
    }
    testmap() {
        var myLatlng = new google.maps.LatLng(this.latitude, this.longitude);
        console.log(myLatlng);
        var mapOptions = {
            zoom: 12,
            center: myLatlng,
            mapTypeControl: false,
            scaleControl: false,
            zoomControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            styles: [{
                    stylers: [{
                            saturation: -100
                        }]
                }],
        };
        this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        //Add User Location Marker To Map
        var marker = new google.maps.Marker({
            position: myLatlng,
            draggable: true
        });
        // To add the marker to the map, call setMap();
        marker.setMap(this.map);
        marker.addListener('dragend', function (event) {
            console.log(event);
            this.latitude = event.latLng.lat();
            console.log(this.latitude);
            this.longitude = event.latLng.lng();
            console.log(this.longitude);
        });
        //Map Click Event Listner
        this.map.addListener('click', function () {
            //add functions here
        });
    }
    updateAdd() {
        this.updateAddress = true;
    }
    update() {
        this.geolocation.getCurrentPosition().then((resp) => {
            // resp.coords.latitude
            // this.latitude= resp.coords.latitude;
            // console.log(this.latitude);
            // resp.coords.longitude
            // this.longitude=resp.coords.longitude;
            // console.log(this.longitude);
            this.latitude = 80;
            this.longitude = 90;
            this.testmap();
            var obj = { "latitude": this.latitude, "longitude": this.longitude };
            this.authService.updateAddress(obj).subscribe((data) => {
                console.log(data);
                this.getUserProfileById();
                this.updateAddress = false;
            });
        });
    }
    editInterest() {
        let navigationExtras = {
            queryParams: {
                isUpdate: true,
                pageroute: this.router.url,
            }
        };
        this.router.navigate(['hobby'], navigationExtras);
    }
    getUserProfileById() {
        var obj = { "userId": this.userData._id };
        this.authService.getUserProfileById(obj).subscribe((data) => {
            console.log(data);
        });
    }
    back() {
        this.router.navigateByUrl(this.pageRoute);
    }
};
Profile2Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["ModalController"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__["File"] },
    { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClient"] },
    { type: _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_14__["PhotoViewer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["ActionSheetController"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__["Geolocation"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"] }
];
Profile2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile2/profile2.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile2.page.scss */ "./src/app/profile2/profile2.page.scss")).default]
    })
], Profile2Page);



/***/ })

}]);
//# sourceMappingURL=profile2-profile2-module-es2015.js.map