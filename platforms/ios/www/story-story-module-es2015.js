(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["story-story-module"],{

/***/ "./src/app/story/story-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/story/story-routing.module.ts ***!
  \***********************************************/
/*! exports provided: StoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoryPageRoutingModule", function() { return StoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _story_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./story.page */ "./src/app/story/story.page.ts");




const routes = [
    {
        path: '',
        component: _story_page__WEBPACK_IMPORTED_MODULE_3__["StoryPage"]
    }
];
let StoryPageRoutingModule = class StoryPageRoutingModule {
};
StoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StoryPageRoutingModule);



/***/ }),

/***/ "./src/app/story/story.module.ts":
/*!***************************************!*\
  !*** ./src/app/story/story.module.ts ***!
  \***************************************/
/*! exports provided: StoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoryPageModule", function() { return StoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _story_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./story-routing.module */ "./src/app/story/story-routing.module.ts");
/* harmony import */ var _story_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./story.page */ "./src/app/story/story.page.ts");







let StoryPageModule = class StoryPageModule {
};
StoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _story_routing_module__WEBPACK_IMPORTED_MODULE_5__["StoryPageRoutingModule"]
        ],
        declarations: [_story_page__WEBPACK_IMPORTED_MODULE_6__["StoryPage"]]
    })
], StoryPageModule);



/***/ })

}]);
//# sourceMappingURL=story-story-module-es2015.js.map