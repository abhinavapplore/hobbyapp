(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["unlock-contact1-unlock-contact1-module"], {
    /***/
    "./src/app/unlock-contact1/unlock-contact1-routing.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/unlock-contact1/unlock-contact1-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: UnlockContact1PageRoutingModule */

    /***/
    function srcAppUnlockContact1UnlockContact1RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UnlockContact1PageRoutingModule", function () {
        return UnlockContact1PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _unlock_contact1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./unlock-contact1.page */
      "./src/app/unlock-contact1/unlock-contact1.page.ts");

      var routes = [{
        path: '',
        component: _unlock_contact1_page__WEBPACK_IMPORTED_MODULE_3__["UnlockContact1Page"]
      }];

      var UnlockContact1PageRoutingModule = function UnlockContact1PageRoutingModule() {
        _classCallCheck(this, UnlockContact1PageRoutingModule);
      };

      UnlockContact1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UnlockContact1PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/unlock-contact1/unlock-contact1.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/unlock-contact1/unlock-contact1.module.ts ***!
      \***********************************************************/

    /*! exports provided: UnlockContact1PageModule */

    /***/
    function srcAppUnlockContact1UnlockContact1ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UnlockContact1PageModule", function () {
        return UnlockContact1PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _unlock_contact1_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./unlock-contact1-routing.module */
      "./src/app/unlock-contact1/unlock-contact1-routing.module.ts");
      /* harmony import */


      var _unlock_contact1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./unlock-contact1.page */
      "./src/app/unlock-contact1/unlock-contact1.page.ts");

      var UnlockContact1PageModule = function UnlockContact1PageModule() {
        _classCallCheck(this, UnlockContact1PageModule);
      };

      UnlockContact1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _unlock_contact1_routing_module__WEBPACK_IMPORTED_MODULE_5__["UnlockContact1PageRoutingModule"]],
        declarations: [_unlock_contact1_page__WEBPACK_IMPORTED_MODULE_6__["UnlockContact1Page"]]
      })], UnlockContact1PageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=unlock-contact1-unlock-contact1-module-es5.js.map