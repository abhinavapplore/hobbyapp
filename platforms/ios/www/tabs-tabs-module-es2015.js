(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-tabs>  \n\n  <ion-fab *ngIf=\"footerAction\" (click)=\"addPost(1)\" class=\"fab\"\n   vertical=\"bottom\" horizontal=\"center\" translucent=\"true\" > \n    <ion-fab-button >  \n      <ion-tab-button>\n        <ion-icon name=\"add-outline\"></ion-icon>\n      </ion-tab-button>\n    </ion-fab-button>\n  </ion-fab>\n  \n  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n    <ion-tab-button tab=\"tab4\">\n      <ion-icon name=\"search\"></ion-icon>\n      <ion-label>Hub</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"tab2\" class=\"comments\">\n      <ion-icon name=\"images\"></ion-icon>\n      <ion-label>Posts</ion-label>\n    </ion-tab-button>\n\n    <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n\n    <ion-tab-button tab=\"tab3\" class=\"notifs\">\n      <ion-icon name=\"people\"></ion-icon>\n      <ion-label>Connections</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"tab1\" class=\"tab tab--right\">\n      <ion-icon name=\"newspaper\"></ion-icon>\n      <ion-label>Feed</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n</ion-tabs>\n");

/***/ }),

/***/ "./src/app/tabs/tabs-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tabs/tabs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");




const routes = [
    {
        path: 'tabs',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'tab1',
                children: [
                    {
                        path: '',
                        loadChildren: () => Promise.all(/*! import() | tab1-tab1-module */[__webpack_require__.e("default~chat-chat-module~profile-profile-module~profile1-profile1-module~tab1-tab1-module~tab3-tab3-~476cac3a"), __webpack_require__.e("default~question-question-module~tab1-tab1-module~tab2-tab2-module"), __webpack_require__.e("common"), __webpack_require__.e("tab1-tab1-module")]).then(__webpack_require__.bind(null, /*! ../tab1/tab1.module */ "./src/app/tab1/tab1.module.ts")).then(m => m.Tab1PageModule)
                    }
                ]
            }, {
                path: 'tab2',
                children: [
                    {
                        path: '',
                        loadChildren: () => Promise.all(/*! import() | tab2-tab2-module */[__webpack_require__.e("default~question-question-module~tab1-tab1-module~tab2-tab2-module"), __webpack_require__.e("common"), __webpack_require__.e("tab2-tab2-module")]).then(__webpack_require__.bind(null, /*! ../tab2/tab2.module */ "./src/app/tab2/tab2.module.ts")).then(m => m.Tab2PageModule)
                    }
                ]
            },
            {
                path: 'tab3',
                children: [
                    {
                        path: '',
                        loadChildren: () => Promise.all(/*! import() | tab3-tab3-module */[__webpack_require__.e("default~chat-chat-module~profile-profile-module~profile1-profile1-module~tab1-tab1-module~tab3-tab3-~476cac3a"), __webpack_require__.e("common"), __webpack_require__.e("tab3-tab3-module")]).then(__webpack_require__.bind(null, /*! ../tab3/tab3.module */ "./src/app/tab3/tab3.module.ts")).then(m => m.Tab3PageModule)
                    }
                ]
            },
            {
                path: 'tab4',
                children: [
                    {
                        path: '',
                        loadChildren: () => Promise.all(/*! import() | tab4-tab4-module */[__webpack_require__.e("default~filtermodal-filtermodal-module~tab4-tab4-module"), __webpack_require__.e("tab4-tab4-module")]).then(__webpack_require__.bind(null, /*! ../tab4/tab4.module */ "./src/app/tab4/tab4.module.ts")).then(m => m.Tab4PageModule)
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/tab4',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/tab4',
        pathMatch: 'full'
    }
];
let TabsPageRoutingModule = class TabsPageRoutingModule {
};
TabsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], TabsPageRoutingModule);



/***/ }),

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs-routing.module */ "./src/app/tabs/tabs-routing.module.ts");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");







let TabsPageModule = class TabsPageModule {
};
TabsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
        ],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
    })
], TabsPageModule);



/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".img1 {\n  width: 20px;\n}\n\nion-tabs ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\nion-tabs ion-fab ion-fab-button {\n  box-shadow: 0px 3px 8px 1px rgba(0, 0, 0, 0.25);\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  border-radius: 30px;\n  width: 60px;\n  height: 60px;\n}\n\nion-tabs ion-fab ion-icon {\n  font-size: 40px;\n  color: white;\n  font-weight: bolder;\n}\n\nion-tabs ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n}\n\nion-tabs ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\nion-tabs ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\nion-tabs ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\nion-tabs ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\nion-tabs ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\nion-tabs ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\nion-label {\n  font-size: 8px;\n  font-family: \"Poppins-Bold\";\n}\n\n.one-edge-shadow {\n  box-shadow: 0 8px 6px -6px black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFicy90YWJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUEyQ0E7RUFDQyxXQUFBO0FBMUNEOztBQXlEQztFQUNDLDBDQUFBO0VBQTRDLGlCQUFBO0FBckQ5Qzs7QUFzREU7RUFDQywrQ0FBQTtFQUNDLCtEQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQXBESjs7QUFzREU7RUFDQyxlQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBcERIOztBQXVEQztFQUNDLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFBUSxRQUFBO0VBQ1IsV0FBQTtBQXBERjs7QUFxREU7RUFDQyxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esa0JBQUE7QUFuREg7O0FBcURFO0VBQ0Msb0NBQUE7QUFuREg7O0FBcURFO0VBQ0MsaUJBQUE7RUFDQSw2QkFBQTtBQW5ESDs7QUFxREU7RUFDQyxnQkFBQTtFQUNBLDRCQUFBO0FBbkRIOztBQXFERTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQW5ESDs7QUFvREc7RUFDQyw0QkFBQTtBQWxESjs7QUF1REE7RUFDQyxjQUFBO0VBQ0EsMkJBQUE7QUFwREQ7O0FBdURBO0VBR1ksZ0NBQUE7QUFwRFoiLCJmaWxlIjoic3JjL2FwcC90YWJzL3RhYnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLXRhYnN7XG4vLyBcdGlvbi1mYWIge1xuLy8gXHRcdG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTsgLyogZml4IG5vdGNoIGlvcyovXG4vLyBcdFx0aW9uLWZhYi1idXR0b24ge1xuLy8gXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xuLy8gXHRcdH1cbi8vIFx0fVxuLy8gXHRpb24tdGFiLWJhciB7XG4vLyBcdFx0LS1ib3JkZXI6IDA7XG4vLyBcdFx0LS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbi8vIFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyBcdFx0Ym90dG9tOiAwO1xuLy8gXHRcdGxlZnQ6MDsgcmlnaHQ6IDA7XG4vLyBcdFx0d2lkdGg6IDEwMCU7XG4vLyBcdFx0JjphZnRlcntcbi8vIFx0XHRcdGNvbnRlbnQ6IFwiIFwiO1xuLy8gXHRcdFx0d2lkdGg6IDEwMCU7XG4vLyBcdFx0XHRib3R0b206IDA7XG4vLyBcdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuLy8gXHRcdFx0aGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XG4vLyBcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyBcdFx0fVxuLy8gXHRcdGlvbi10YWItYnV0dG9uIHtcbi8vIFx0XHRcdC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbi8vIFx0XHR9XG4vLyBcdFx0aW9uLXRhYi1idXR0b24uY29tbWVudHMge1xuLy8gXHRcdFx0bWFyZ2luLXJpZ2h0OiAwcHg7XG4vLyBcdFx0XHRib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMThweDtcbi8vIFx0XHR9XG4vLyBcdFx0aW9uLXRhYi1idXR0b24ubm90aWZzIHtcbi8vIFx0XHRcdG1hcmdpbi1sZWZ0OiAwcHg7XG4vLyBcdFx0XHRib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xuLy8gXHRcdH1cbi8vIFx0XHRzdmcgeyAgICBcbi8vIFx0XHRcdHdpZHRoOiA3MnB4O1xuLy8gXHRcdFx0bWFyZ2luLXRvcDogMTJweDtcbi8vIFx0XHRcdHBhdGh7XG4vLyBcdFx0XHRcdGZpbGw6ICB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuLy8gXHRcdFx0fVx0XHRcbi8vIFx0XHR9XG4vLyBcdH1cbi8vIH1cblxuLmltZzF7XG5cdHdpZHRoOiAyMHB4O1xufVxuLy8gLmltZzJ7XG4vLyBcdHdpZHRoOiAyMnB4O1xuLy8gfVxuXG4vLyBpb24tZmFiLWJ1dHRvbiB7XG4vLyBcdC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbi8vIH1cbi8vIGlvbi1pY29uIHtcbi8vIFx0Zm9udC1zaXplOiA0MHB4O1xuLy8gXHRjb2xvcjogd2hpdGU7XG4vLyBcdGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4vLyB9XG5pb24tdGFic3tcblx0aW9uLWZhYiB7XG5cdFx0bWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cblx0XHRpb24tZmFiLWJ1dHRvbiB7XG5cdFx0XHRib3gtc2hhZG93OiAwcHggM3B4IDhweCAxcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG5cdFx0fVxuXHRcdGlvbi1pY29uIHtcblx0XHRcdGZvbnQtc2l6ZTogNDBweDtcblx0XHRcdGNvbG9yOiB3aGl0ZTtcblx0XHRcdGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cdH1cblx0aW9uLXRhYi1iYXIge1xuXHRcdC0tYm9yZGVyOiAwO1xuXHRcdC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdGJvdHRvbTogMDtcblx0XHRsZWZ0OjA7IHJpZ2h0OiAwO1xuXHRcdHdpZHRoOiAxMDAlO1xuXHRcdCY6YWZ0ZXJ7XG5cdFx0XHRjb250ZW50OiBcIiBcIjtcblx0XHRcdHdpZHRoOiAxMDAlO1xuXHRcdFx0Ym90dG9tOiAwO1xuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcblx0XHRcdGhlaWdodDogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pO1xuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdH1cblx0XHRpb24tdGFiLWJ1dHRvbiB7XG5cdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG5cdFx0fVxuXHRcdGlvbi10YWItYnV0dG9uLmNvbW1lbnRzIHtcblx0XHRcdG1hcmdpbi1yaWdodDogMHB4O1xuXHRcdFx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE4cHg7XG5cdFx0fVxuXHRcdGlvbi10YWItYnV0dG9uLm5vdGlmcyB7XG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xuXHRcdFx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMThweDtcblx0XHR9XG5cdFx0c3ZnIHsgICAgXG5cdFx0XHR3aWR0aDogNzJweDtcblx0XHRcdG1hcmdpbi10b3A6IDE5cHg7XG5cdFx0XHRwYXRoe1xuXHRcdFx0XHRmaWxsOiAgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcblx0XHRcdH1cdFx0XG5cdFx0fVxuXHR9XG59XG5pb24tbGFiZWwge1xuXHRmb250LXNpemU6IDhweDtcblx0Zm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG59XG5cbi5vbmUtZWRnZS1zaGFkb3cge1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA4cHggNnB4IC02cHggYmxhY2s7XG4gICAgICAgLW1vei1ib3gtc2hhZG93OiAwIDhweCA2cHggLTZweCBibGFjaztcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgOHB4IDZweCAtNnB4IGJsYWNrO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ "./node_modules/@ionic-native/keyboard/__ivy_ngcc__/ngx/index.js");















// import { VideoEditor,CreateThumbnailOptions } from '@ionic-native/video-editor/ngx';
const baseUrl = "https://example.com";
const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";
let TabsPage = class TabsPage {
    constructor(platform, modalController, authService, angularstorage, file, filePath, httpClient, keyboard, actionSheetController, storage, camera, router, navCtrl, alertCtrl, loadingCtrl) {
        this.platform = platform;
        this.modalController = modalController;
        this.authService = authService;
        this.angularstorage = angularstorage;
        this.file = file;
        this.filePath = filePath;
        this.httpClient = httpClient;
        this.keyboard = keyboard;
        this.actionSheetController = actionSheetController;
        this.storage = storage;
        this.camera = camera;
        this.router = router;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        //changes
        this.files = [];
        this.cloudFiles = [];
        this.uploadProgress = 0;
        //end
        this.uploaded = false;
        this.selectedVideo = "";
        this.isUploading = false;
        this.footerAction = true;
        this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.gelleryOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
        };
        this.keyboard.onKeyboardWillShow().subscribe(data => {
            this.footerAction = false;
        });
        this.keyboard.onKeyboardWillHide().subscribe(data => {
            this.footerAction = true;
        });
    }
    addPost(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: "Select Image source",
                buttons: [{
                        text: 'Load from Library',
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, i);
                        }
                    },
                    {
                        text: 'Use Camera',
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.CAMERA, i);
                        }
                    },
                    {
                        text: 'Upload Video',
                        handler: () => {
                            this.takePicture1(this.camera.PictureSourceType.PHOTOLIBRARY, i);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    takePicture(sourceType, i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.platform.is('ios')) {
                const options = {
                    quality: 100,
                    targetWidth: 900,
                    targetHeight: 600,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: sourceType,
                    encodingType: this.camera.EncodingType.JPEG
                };
                const tempImage = yield this.camera.getPicture(options);
                const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
                // Now, the opposite. Extract the full path, minus filename.
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
                const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
                // Get the Data directory on the device.
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
                const newBaseFilesystemPath = this.file.dataDirectory;
                yield this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                const storedPhoto = newBaseFilesystemPath + tempFilename;
                this.file.resolveLocalFilesystemUrl(storedPhoto)
                    .then(entry => {
                    entry.file(file => this.readFile(file, i));
                })
                    .catch(err => {
                    console.log(err);
                    // this.presentToast('Error while reading file.');
                });
            }
            else {
                const options = {
                    quality: 100,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: sourceType,
                    encodingType: this.camera.EncodingType.JPEG,
                };
                this.camera.getPicture(options).then((imageData) => {
                    this.file.resolveLocalFilesystemUrl(imageData).then((entry) => {
                        entry.file(file => {
                            console.log(file);
                            // let options1 = {
                            //   uri: uri,
                            //   folderName: 'Protonet',
                            //   quality: 90,
                            //   width: 1280,
                            //   height: 1280
                            //  } as ImageResizerOptions;
                            //  this.imageResizer
                            // .resize(options1)
                            // .then((file: string) => console.log('FilePath', filePath))
                            // .catch(e => console.log(e));
                            this.readFile(file, i);
                        });
                    });
                }, (err) => {
                    // Handle error
                });
            }
        });
    }
    takePicture1(sourceType, i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const options = {
                mediaType: this.camera.MediaType.VIDEO,
                sourceType: sourceType
            };
            this.camera.getPicture(options)
                .then((videoUrl) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                if (videoUrl) {
                    this.authService.loading('Please Wait');
                    this.uploadedVideo = null;
                    var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
                    var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);
                    dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
                    try {
                        var dirUrl = yield this.file.resolveDirectoryUrl(dirpath);
                        var retrievedFile = yield this.file.getFile(dirUrl, filename, {});
                    }
                    catch (err) {
                        this.authService.dismissLoading();
                        return this.authService.presentToast("Error Something went wrong.");
                    }
                    retrievedFile.file(data => {
                        console.log(data);
                        this.authService.dismissLoading();
                        if (data.size > MAX_FILE_SIZE)
                            return this.authService.presentToast("Error You cannot upload more than 5mb.");
                        if (data.type !== ALLOWED_MIME_TYPE)
                            return this.authService.presentToast("Error Incorrect file type.");
                        this.selectedVideo = retrievedFile.nativeURL;
                        console.log(this.selectedVideo);
                        this.uploadFile(retrievedFile);
                    });
                }
            }), (err) => {
                console.log(err);
            });
        });
    }
    loadFiles() {
        this.cloudFiles = [];
        const storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_13__["storage"]().ref('files');
        storageRef.listAll().then(result => {
            result.items.forEach((ref) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.cloudFiles.push({
                    name: ref.name,
                    full: ref.fullPath,
                    url: yield ref.getDownloadURL(),
                    ref: ref
                });
            }));
        });
    }
    deleteFile(ref) {
        ref.delete().then(() => {
            this.loadFiles();
        });
    }
    uploadFile(f) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
            const type = this.getMimeType(f.name.split('.').pop());
            const buffer = yield this.file.readAsArrayBuffer(path, f.name);
            const fileBlob = new Blob([buffer], type);
            const randomId = Math.random()
                .toString(36)
                .substring(2, 8);
            this.upload2FirebaseVideo(fileBlob);
            // const uploadTask = this.angularstorage.upload(
            //   `files/${new Date().getTime()}_${randomId}`,
            //   fileBlob
            // );
            // uploadTask.percentageChanges().subscribe(change => {
            //   this.uploadProgress = change;
            // });
            // uploadTask.then(async res => {
            //   const toast = await this.authService.toastController.create({
            //     duration: 3000,
            //     message: 'File upload finished!'
            //   });
            //   toast.present();
            // });
        });
    }
    getMimeType(fileExt) {
        if (fileExt == 'wav')
            return { type: 'audio/wav' };
        else if (fileExt == 'jpg')
            return { type: 'image/jpg' };
        else if (fileExt == 'mp4')
            return { type: 'video/mp4' };
        else if (fileExt == 'MOV')
            return { type: 'video/quicktime' };
    }
    // if(this.platform.is('ios')){
    //   const options: CameraOptions = {
    //   quality: 100,
    //   targetWidth: 900,
    //   targetHeight: 600,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   sourceType: sourceType,
    //   encodingType: this.camera.EncodingType.JPEG
    //   };
    //   const tempImage = await this.camera.getPicture(options);
    //   const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
    //   // Now, the opposite. Extract the full path, minus filename.
    //   // Result example: file:///var/mobile/Containers/Data/Application
    //   // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
    //   const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
    //   // Get the Data directory on the device.
    //   // Result example: file:///var/mobile/Containers/Data/Application
    //   // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
    //   const newBaseFilesystemPath = this.file.dataDirectory;
    //   await this.file.copyFile(tempBaseFilesystemPath, tempFilename,
    //   newBaseFilesystemPath, tempFilename);
    //   // Result example: file:///var/mobile/Containers/Data/Application
    //   // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
    //   const storedPhoto = newBaseFilesystemPath + tempFilename;
    //   this.file.resolveLocalFilesystemUrl(storedPhoto)
    //   .then(entry => {
    //   ( < FileEntry > entry).file(file => this.readFile(file, i))
    //   })
    //   .catch(err => {
    //   console.log(err);
    //   // this.presentToast('Error while reading file.');
    //   });
    //   }else{
    //     const options: CameraOptions = {
    //       quality: 100,
    //       destinationType: this.camera.DestinationType.FILE_URI,
    //       sourceType: sourceType,
    //       encodingType: this.camera.EncodingType.JPEG,
    //       mediaType:2
    //     };
    //     this.camera.getPicture(options).then((imageData) => {
    //       this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
    //         entry.file(file => {
    //           console.log(file);
    //           this.readFile(file,i);
    //         });
    //       });
    //     }, (err) => {
    //       // Handle error
    //     });
    //   }
    dismissLoader() {
        this.loader.dismiss();
    }
    // presentAlert(title, message) {
    //   let alert = this.alertCtrl.create({
    //     title: title,
    //     subTitle: message,
    //     buttons: ['Dismiss']
    //   });
    //   alert.present();
    // }
    readFile(file, i) {
        const reader = new FileReader();
        reader.onload = () => {
            // const formData = new FormData();
            const imgBlob = new Blob([reader.result], {
                type: file.type
            });
            // formData.append('file', imgBlob, file.name);
            // this.uploadImageData(formData);
            if (i === 1) {
                this.upload2Firebase(imgBlob);
            }
            else {
                console.log("if second image");
            }
        };
        reader.readAsArrayBuffer(file);
    }
    upload2FirebaseVideo(video) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.loading('Loading Video..');
            const file = video;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            yield task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(() => fileRef.getDownloadURL().subscribe(value => {
                this.videoUrl = value;
                // var Options:CreateThumbnailOptions = {fileUri:value,width:160, height:206, atTime:1, outputFileName: 'sample', quality:50 };
                //         this.videoEditor.createThumbnail(Options).then(result=>{
                //             //result-path of thumbnail
                //            console.log(result);      
                //         }).catch(e=>{
                //           console.log(e);  
                //          // alert('fail video editor');
                //         });
                this.uploaded = true;
                console.log(this.videoUrl);
                this.authService.dismissLoading();
                // this.storage.set('imgUrl',this.imgUrl);
                // this.storage.set('isAddPost',true);
                let navigationExtras = {
                    queryParams: {
                        pageRoute: this.router.url,
                        imgUrl: this.videoUrl,
                        addPost: true,
                        postType: "1"
                    }
                };
                this.navCtrl.navigateRoot(['post'], navigationExtras);
            })))
                .subscribe();
        });
    }
    upload2Firebase(image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.loading('Loading Image..');
            const file = image;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            yield task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(() => fileRef.getDownloadURL().subscribe(value => {
                this.imgUrl = value;
                this.uploaded = true;
                this.authService.dismissLoading();
                // this.storage.set('imgUrl',this.imgUrl);
                // this.storage.set('isAddPost',true);
                let navigationExtras = {
                    queryParams: {
                        pageRoute: this.router.url,
                        imgUrl: this.imgUrl,
                        addPost: true,
                        postType: "0"
                    }
                };
                this.navCtrl.navigateRoot(['post'], navigationExtras);
            })))
                .subscribe();
        });
    }
    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
};
TabsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["ModalController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__["AngularFireStorage"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__["File"] },
    { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClient"] },
    { type: _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_14__["Keyboard"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["ActionSheetController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["LoadingController"] }
];
TabsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tabs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")).default]
    })
], TabsPage);



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es2015.js.map