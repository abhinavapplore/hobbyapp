(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["blocked-blocked-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/blocked/blocked.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/blocked/blocked.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppBlockedBlockedPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-col [size]=2 (click)=\"back()\">\n        <ion-icon name=\"chevron-back\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text class=\"myTitle\">Blocked Users</ion-text>\n      </ion-col> \n    </ion-row>   \n  </ion-toolbar>\n\n\n<ion-content>\n  <div *ngIf=\"allBlockedUser.length==0\" style=\"text-align: center;\n  margin-top: 5%;\">\n    <ion-text class=\"text\">No Blocked Users.</ion-text>\n  </div>\n\n<div *ngIf=\"allBlockedUser.length!=0\">\n  <!-- <div class=\"div\" > -->\n\n    <div *ngFor=\"let item of allBlockedUser;let z=index\">\n      <ion-card *ngIf=\"!item.isblocked\">\n      \n        <ion-row class=\"row\">\n          <ion-col [size]=3 class=\"col1\">\n            <ion-avatar class=\"container\">     \n              <ion-img class=\"img1\" [src]=\"item.hostId.user_img\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col [size]=9 class=\"col3\">\n            <ion-row class=\"row3\">\n              <ion-col>\n                <ion-text class=\"text\">{{item.hostId.fullName}}</ion-text>\n              </ion-col>\n              <ion-col>\n                <ion-text class=\"text\" (click)=\"unblock(item,z)\">Unblock</ion-text>\n              </ion-col>\n            \n            </ion-row>\n          \n          </ion-col>\n        </ion-row>\n      </ion-card>  \n    </div>\n \n    \n  <!-- </div> -->\n</div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/blocked/blocked-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/blocked/blocked-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: BlockedPageRoutingModule */

    /***/
    function srcAppBlockedBlockedRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlockedPageRoutingModule", function () {
        return BlockedPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _blocked_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./blocked.page */
      "./src/app/blocked/blocked.page.ts");

      var routes = [{
        path: '',
        component: _blocked_page__WEBPACK_IMPORTED_MODULE_3__["BlockedPage"]
      }];

      var BlockedPageRoutingModule = function BlockedPageRoutingModule() {
        _classCallCheck(this, BlockedPageRoutingModule);
      };

      BlockedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], BlockedPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/blocked/blocked.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/blocked/blocked.module.ts ***!
      \*******************************************/

    /*! exports provided: BlockedPageModule */

    /***/
    function srcAppBlockedBlockedModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlockedPageModule", function () {
        return BlockedPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _blocked_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./blocked-routing.module */
      "./src/app/blocked/blocked-routing.module.ts");
      /* harmony import */


      var _blocked_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./blocked.page */
      "./src/app/blocked/blocked.page.ts");

      var BlockedPageModule = function BlockedPageModule() {
        _classCallCheck(this, BlockedPageModule);
      };

      BlockedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _blocked_routing_module__WEBPACK_IMPORTED_MODULE_5__["BlockedPageRoutingModule"]],
        declarations: [_blocked_page__WEBPACK_IMPORTED_MODULE_6__["BlockedPage"]]
      })], BlockedPageModule);
      /***/
    },

    /***/
    "./src/app/blocked/blocked.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/blocked/blocked.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppBlockedBlockedPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.myTitle {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\nion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmxvY2tlZC9ibG9ja2VkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwrREFBQTtFQUNBLGdEQUFBO0VBQ0EsNEJBQUE7QUFDSjtBQUFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUVSO0FBRUE7RUFDSSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFDSjtBQUVBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2Jsb2NrZWQvYmxvY2tlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9vbGJhcjEge1xuICAgIGhlaWdodDogNzRweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDI1cHggMjVweDtcbiAgICBpb24tY29sIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxufVxuXG4ubXlUaXRsZXtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/blocked/blocked.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/blocked/blocked.page.ts ***!
      \*****************************************/

    /*! exports provided: BlockedPage */

    /***/
    function srcAppBlockedBlockedPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlockedPage", function () {
        return BlockedPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");

      var BlockedPage = /*#__PURE__*/function () {
        function BlockedPage(router, authService, storage, fs) {
          _classCallCheck(this, BlockedPage);

          this.router = router;
          this.authService = authService;
          this.storage = storage;
          this.fs = fs;
          this.allBlockedUser = [];
        }

        _createClass(BlockedPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.storage.get('user').then(function (user) {
              _this.userData = user;

              _this.loadData();
            });
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl('tabs/tab1');
          }
        }, {
          key: "loadData",
          value: function loadData() {
            var _this2 = this;

            this.allBlockedUser = [];
            var obj = {
              "userId": this.userData._id
            };
            this.authService.blockUserList(obj).subscribe(function (data) {
              if (data.success) {
                console.log(data);
                _this2.allBlockedUser = data.data;
              } else {
                console.log(data);
              }
            });
          }
        }, {
          key: "unblock",
          value: function unblock(item, z) {
            var _this3 = this;

            this.allBlockedUser.splice(z, 1);
            var data = {
              'blockedId': item._id
            };
            this.authService.unblockUser(data).subscribe(function (data) {
              if (data.success) {
                _this3.authService.presentToast(item.hostId.fullName + ' unblocked.');

                _this3.fs.collection('friends').doc(_this3.userData._id).collection('chats').doc(item.hostId._id).set({
                  isblocked: false
                }, {
                  merge: true
                });

                _this3.fs.collection('friends').doc(item.hostId._id).collection('chats').doc(_this3.userData._id).set({
                  isblocked: false
                }, {
                  merge: true
                });
              } else {
                _this3.authService.presentToast('Please try in sometime.');
              }
            });
          }
        }]);

        return BlockedPage;
      }();

      BlockedPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"]
        }];
      };

      BlockedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-blocked',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./blocked.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/blocked/blocked.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./blocked.page.scss */
        "./src/app/blocked/blocked.page.scss"))["default"]]
      })], BlockedPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=blocked-blocked-module-es5.js.map