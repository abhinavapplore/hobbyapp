(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-post-add-post-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/add-post/add-post.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/add-post/add-post.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAddPostAddPostPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"toolbar1\">\n  <ion-row>\n    <ion-col [size]=2 (click)=\"back()\">\n      <ion-icon name=\"chevron-back\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8>\n      <ion-text>Post</ion-text>\n    </ion-col> \n  </ion-row>   \n</ion-toolbar>\n\n<ion-content>\n\n  <div class=\"div\">\n    <ion-row class=\"row\">\n      <ion-text>Select Image or Video</ion-text>\n    </ion-row>\n    <ion-row class=\"row1\"  *ngIf='data'>\n\n      <ion-col [size]=\"4\"  *ngFor='let item of photoArray; let i=index' \n      [class.selected]='imgSelected==i' class=\"container\"\n      (click)=\"selectedImg(i)\">\n      <ion-img [src]=\"item.photoURL | cdvphotolibrary\"></ion-img>\n       \n        <div class=\"div1\" *ngIf=\"imgSelected==i\">\n          <img class=\"img1\" src=\"../../assets/images/wallet/tick.png\">\n        </div>\n      </ion-col>\n\n      <!-- <ion-col [size]=4  [class.selected]='imgSelected==\"img2\"' class=\"container\"\n      (click)=\"selectedImg('img2')\">\n        <img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\">\n        <div class=\"div1\" *ngIf=\"imgSelected=='img2'\">\n          <img class=\"img1\" src=\"../../assets/images/wallet/tick.png\">\n        </div>\n      </ion-col>\n\n      <ion-col [size]=4  [class.selected]='imgSelected==\"img3\"' class=\"container\"\n      (click)=\"selectedImg('img3')\">\n        <img class=\"img\" src=\"../../assets/hobbyImage/cycling.jpg\">\n        <div class=\"div1\" *ngIf=\"imgSelected=='img3'\">\n          <img class=\"img1\" src=\"../../assets/images/wallet/tick.png\">\n        </div>\n      </ion-col> -->\n    </ion-row>\n\n    <ion-row class=\"row3\" *ngIf=\"!selected\">\n      <ion-button class=\"button1\" shape=\"block\">NEXT</ion-button>\n    </ion-row>\n    <ion-row class=\"row3\" *ngIf=\"selected\" (click)=\"next()\">\n      <ion-button class=\"button2\" shape=\"block\">NEXT</ion-button>\n    </ion-row>\n  </div>\n  \n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/add-post/add-post-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/add-post/add-post-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: AddPostPageRoutingModule */

    /***/
    function srcAppAddPostAddPostRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddPostPageRoutingModule", function () {
        return AddPostPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _add_post_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./add-post.page */
      "./src/app/add-post/add-post.page.ts");

      var routes = [{
        path: '',
        component: _add_post_page__WEBPACK_IMPORTED_MODULE_3__["AddPostPage"]
      }];

      var AddPostPageRoutingModule = function AddPostPageRoutingModule() {
        _classCallCheck(this, AddPostPageRoutingModule);
      };

      AddPostPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AddPostPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/add-post/add-post.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/add-post/add-post.module.ts ***!
      \*********************************************/

    /*! exports provided: AddPostPageModule */

    /***/
    function srcAppAddPostAddPostModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddPostPageModule", function () {
        return AddPostPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _add_post_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./add-post-routing.module */
      "./src/app/add-post/add-post-routing.module.ts");
      /* harmony import */


      var _add_post_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-post.page */
      "./src/app/add-post/add-post.page.ts");
      /* harmony import */


      var _cdvphotolibrary_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../cdvphotolibrary.pipe */
      "./src/app/cdvphotolibrary.pipe.ts");

      var AddPostPageModule = function AddPostPageModule() {
        _classCallCheck(this, AddPostPageModule);
      };

      AddPostPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _add_post_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddPostPageRoutingModule"]],
        declarations: [_add_post_page__WEBPACK_IMPORTED_MODULE_6__["AddPostPage"], _cdvphotolibrary_pipe__WEBPACK_IMPORTED_MODULE_7__["CDVPhotoLibraryPipe"]]
      })], AddPostPageModule);
      /***/
    },

    /***/
    "./src/app/add-post/add-post.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/add-post/add-post.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppAddPostAddPostPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.row {\n  width: 90%;\n  justify-content: center;\n}\n.row ion-text {\n  font-family: \"Poppins-Regular\";\n  color: #003C69;\n  font-size: 14px;\n}\n.row1 {\n  width: 90%;\n  justify-content: space-evenly;\n  padding-top: 7%;\n}\n.row1 .container {\n  position: relative;\n  width: 100px;\n  height: 100px;\n}\n.row1 .img {\n  width: 100px;\n  height: 100px;\n}\n.row1 .selected {\n  width: 100px;\n  height: 100px;\n  filter: brightness(0.5);\n}\n.div1 {\n  position: absolute;\n  top: 35px;\n  left: 35px;\n}\n.img1 {\n  width: 60%;\n}\n.button1 {\n  width: 80%;\n  border-radius: 10px;\n  --background: #F3F3F3;\n  --background-activated: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #AAAAAA;\n  --color-activated: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n.button2 {\n  width: 80%;\n  border-radius: 10px;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n}\n.row3 {\n  width: 80%;\n  justify-content: center;\n  padding-bottom: 5%;\n  padding-top: 8%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRkLXBvc3QvYWRkLXBvc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLCtEQUFBO0VBQ0EsZ0RBQUE7RUFDQSw0QkFBQTtBQUNKO0FBQUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRVI7QUFBSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBRVI7QUFBSTtFQUNFLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUVOO0FBQ0U7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQUVKO0FBQUE7RUFDSSxVQUFBO0VBQ0EsdUJBQUE7QUFHSjtBQUZJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUlSO0FBREE7RUFDSSxVQUFBO0VBQ0EsNkJBQUE7RUFDQSxlQUFBO0FBSUo7QUFISTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUFLUjtBQUhJO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUFLUjtBQUhJO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtBQUtOO0FBRkE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBS0Y7QUFIQTtFQUNFLFVBQUE7QUFNRjtBQUpBO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSx5RUFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGVBQUE7QUFPRjtBQUxBO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsK0RBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQVFGO0FBTkE7RUFDRSxVQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFTRiIsImZpbGUiOiJzcmMvYXBwL2FkZC1wb3N0L2FkZC1wb3N0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b29sYmFyMSB7XG4gICAgaGVpZ2h0OiA3NHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjVweCAyNXB4O1xuICAgIGlvbi1jb2wge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG4gICAgaW9uLWljb24ge1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgfVxuICAgIGlvbi10ZXh0IHtcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgICBmb250LXdlaWdodDogNzAwO1xuICAgIH1cbiAgfVxuICAuZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG4ucm93IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICB9XG59XG4ucm93MSB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBwYWRkaW5nLXRvcDogNyU7XG4gICAgLmNvbnRhaW5lciB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIH1cbiAgICAuaW1nIHtcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIH1cbiAgICAuc2VsZWN0ZWQge1xuICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjUpO1xuICB9XG59XG4uZGl2MSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAzNXB4O1xuICBsZWZ0OiAzNXB4O1xufVxuLmltZzEge1xuICB3aWR0aDogNjAlO1xufVxuLmJ1dHRvbjEge1xuICB3aWR0aDogODAlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAtLWJhY2tncm91bmQ6ICNGM0YzRjM7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gIC0tY29sb3I6ICNBQUFBQUE7XG4gIC0tY29sb3ItYWN0aXZhdGVkOiAjZmZmZmZmO1xuICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLmJ1dHRvbjIge1xuICB3aWR0aDogODAlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7OyBcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5yb3czIHtcbiAgd2lkdGg6IDgwJTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgcGFkZGluZy10b3A6IDglO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/add-post/add-post.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/add-post/add-post.page.ts ***!
      \*******************************************/

    /*! exports provided: AddPostPage */

    /***/
    function srcAppAddPostAddPostPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddPostPage", function () {
        return AddPostPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/file-path/ngx */
      "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/fire/storage */
      "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
      /* harmony import */


      var firebase_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! firebase/storage */
      "./node_modules/firebase/storage/dist/index.esm.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");

      var AddPostPage = /*#__PURE__*/function () {
        function AddPostPage(route, platform, modalController, authService, angularstorage, file, filePath, httpClient, actionSheetController, storage, camera, router, domSanitizer) {
          var _this = this;

          _classCallCheck(this, AddPostPage);

          this.route = route;
          this.platform = platform;
          this.modalController = modalController;
          this.authService = authService;
          this.angularstorage = angularstorage;
          this.file = file;
          this.filePath = filePath;
          this.httpClient = httpClient;
          this.actionSheetController = actionSheetController;
          this.storage = storage;
          this.camera = camera;
          this.router = router;
          this.domSanitizer = domSanitizer;
          this.selected = false;
          this.photoArray = [];
          this.data = false;
          this.uploaded = false;
          this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          };
          this.gelleryOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL
          };
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              _this.pageRoute = params.special;
            }
          });
        }

        _createClass(AddPostPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl(this.pageRoute);
          }
        }, {
          key: "selectedImg",
          value: function selectedImg(x) {
            this.imgSelected = x;
          }
        }, {
          key: "next",
          value: function next() {
            var navigationExtras = {
              queryParams: {
                special: this.router.url
              }
            };
            this.router.navigate(['post'], navigationExtras);
          }
        }]);

        return AddPostPage;
      }();

      AddPostPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["ModalController"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"]
        }, {
          type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__["AngularFireStorage"]
        }, {
          type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__["File"]
        }, {
          type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["ActionSheetController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
        }, {
          type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]
        }];
      };

      AddPostPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-post',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-post.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/add-post/add-post.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-post.page.scss */
        "./src/app/add-post/add-post.page.scss"))["default"]]
      })], AddPostPage);
      /***/
    },

    /***/
    "./src/app/cdvphotolibrary.pipe.ts":
    /*!*****************************************!*\
      !*** ./src/app/cdvphotolibrary.pipe.ts ***!
      \*****************************************/

    /*! exports provided: CDVPhotoLibraryPipe */

    /***/
    function srcAppCdvphotolibraryPipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CDVPhotoLibraryPipe", function () {
        return CDVPhotoLibraryPipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

      var CDVPhotoLibraryPipe = /*#__PURE__*/function () {
        function CDVPhotoLibraryPipe(sanitizer) {
          _classCallCheck(this, CDVPhotoLibraryPipe);

          this.sanitizer = sanitizer;
        }

        _createClass(CDVPhotoLibraryPipe, [{
          key: "transform",
          value: function transform(url) {
            return url.startsWith('cdvphotolibrary://') ? this.sanitizer.bypassSecurityTrustUrl(url) : url;
          }
        }]);

        return CDVPhotoLibraryPipe;
      }();

      CDVPhotoLibraryPipe.ctorParameters = function () {
        return [{
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]
        }];
      };

      CDVPhotoLibraryPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'cdvphotolibrary'
      })], CDVPhotoLibraryPipe);
      /***/
    }
  }]);
})();
//# sourceMappingURL=add-post-add-post-module-es5.js.map