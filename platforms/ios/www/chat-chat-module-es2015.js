(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <ion-toolbar class=\"toolbar\">\n      <ion-row >\n     \n          <ion-col style=\"display: flex;flex-direction: row;align-items: center;\" size=\"9\">\n            <ion-icon (click)=\"back()\" class=\"icon1\" name=\"chevron-back\"></ion-icon>\n          \n              <ion-avatar (click)=\"viewProfile()\">     \n                <ion-img class=\"img1\" [src]=\"recieverImg\"></ion-img>\n              </ion-avatar>\n       \n              <ion-text (click)=\"viewProfile()\" id='demo' style=\"text-transform: capitalize;\n              font-size: 20px;margin-left: 5%;\">{{receiverName}}</ion-text>\n          </ion-col>\n      \n          <ion-col style=\"display: flex;flex-direction: row-reverse;left: -5%;\" size=\"3\">\n            <ion-icon name=\"ban-outline\" class=\"icon1\" (click)=\"presentAlertConfirm()\"></ion-icon>\n          </ion-col>\n     \n      </ion-row>    \n  </ion-toolbar>\n\n<ion-content #content>\n\n  <div class =\"chat\" *ngFor=\"let chat of chatRef | async\">\n    <div *ngIf=\"uid== chat.UserID\" class=\"me\" style=\"display: flex;\">      \n      <div  class=\"me\">\n        <ion-text style=\"font-size: 20px;\">\n          {{chat.Message}} <br>\n          <ion-text  class=\"span1\"> {{chat.msgTime}} </ion-text> \n        </ion-text>     \n      </div>  \n    </div>\n\n    <div *ngIf=\"uid!=chat.UserID\" class=\"chat\" style=\"display: flex;\">\n      <div class=\"you\">  \n        <!-- <span> -->\n          <ion-text style=\"font-size: 20px;\" >{{chat.Message}} <br>\n            <ion-text  class=\"span1\"> {{chat.msgTime}} </ion-text> \n           </ion-text> \n        <!-- </span> -->\n      </div>\n    </div>\n\n  </div>\n\n  <ion-row style=\"padding-bottom:15%\">\n    <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n  </ion-row>\n  \n\n  <div class=\"div\">\n    <ion-row class=\"item\">\n      <ion-col [size]=10 class=\"col3\">\n        <ion-input placeholder=\"Send Message...\" [(ngModel)]=\"text\"></ion-input>\n      </ion-col>\n      <ion-col [size]=2 class=\"col2\" (click)='send(text)'>\n        <ion-icon class=\"icon\" name=\"paper-plane-outline\"></ion-icon>\n      </ion-col> \n    </ion-row>\n  </div>\n  </ion-content>\n\n");

/***/ }),

/***/ "./src/app/chat/chat-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/chat/chat-routing.module.ts ***!
  \*********************************************/
/*! exports provided: ChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function() { return ChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");




const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
    }
];
let ChatPageRoutingModule = class ChatPageRoutingModule {
};
ChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChatPageRoutingModule);



/***/ }),

/***/ "./src/app/chat/chat.module.ts":
/*!*************************************!*\
  !*** ./src/app/chat/chat.module.ts ***!
  \*************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-routing.module */ "./src/app/chat/chat-routing.module.ts");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");







let ChatPageModule = class ChatPageModule {
};
ChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"]
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "./src/app/chat/chat.page.scss":
/*!*************************************!*\
  !*** ./src/app/chat/chat.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".col {\n  font-family: \"Popins-Bold\";\n  font-size: 25px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.chat {\n  display: flex;\n  flex-direction: column;\n}\n\n.chat .me {\n  position: relative;\n  margin: 20px 0px 0px;\n  max-width: 90%;\n  align-self: flex-end;\n  padding: 0px 10px 0px;\n  border-radius: 15px 15px 0;\n  color: black;\n  background: white;\n  font-size: medium;\n  display: flex;\n  height: 50%;\n}\n\n.chat .meImage {\n  position: relative;\n  margin: 20px 5px 0px;\n  align-self: flex-end;\n  padding: 6px 10px 7px;\n}\n\n.chat .you {\n  position: relative;\n  max-width: 90%;\n  align-self: flex-start;\n  padding: 0px 10px 0px;\n  border-radius: 15px 15px 15px 0;\n  display: flex;\n  font-size: medium;\n  color: black;\n  background: white;\n  margin: 18px 0px 5px 0px;\n}\n\n.chat .you .name {\n  font-size: small;\n  font-weight: bold;\n}\n\n.header-fixed-top {\n  text-align: center;\n}\n\n.header-fixed-top .head-profileimg {\n  width: 35px;\n  position: absolute;\n  left: -25px;\n  top: -10px;\n}\n\n.massagechat-content .chat-internal-block {\n  width: 100%;\n  height: 100%;\n  position: relative;\n  padding: 10px 10px 50px 0px;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box {\n  overflow: auto;\n  height: 100%;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .chat-row {\n  width: 96%;\n  margin-bottom: 10px;\n  display: inline-block;\n  margin-left: 2%;\n  position: relative;\n  font-size: 14px;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .sender {\n  padding-left: 30px;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .sender profile {\n  width: 25px;\n  height: 25px;\n  position: absolute;\n  left: 0px;\n  top: 3px;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .sender .chat-msgbox {\n  background: #3E9DFF;\n  padding: 8px 10px 5px 5px;\n  border-radius: 10px;\n  color: #ffffff;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .chat-time {\n  display: inline-block;\n  width: 100%;\n  text-align: right;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .reciver {\n  padding-right: 30px;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .reciver profile {\n  width: 25px;\n  height: 25px;\n  position: absolute;\n  right: 0px;\n  border-radius: 50%;\n  top: 3px;\n}\n\n.massagechat-content .chat-internal-block .chat-massage-box .reciver .chat-msgbox {\n  background: #e4e4e4;\n  padding: 8px 5px 5px 10px;\n  border-radius: 10px;\n  color: #666666;\n}\n\n.massagechat-content .chat-internal-block .chat-txtbox {\n  width: 90%;\n  position: absolute;\n  left: 5%;\n  bottom: 10px;\n  border: solid thin #e1e1e1;\n  border-radius: 30px;\n  font-size: 12px;\n  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\n}\n\n.footer-fixed-tab {\n  background: #414141;\n  width: 100%;\n  padding: 0px 5%;\n}\n\n.footer-fixed-tab ion-toolbar {\n  --background: #414141;\n  --ion-color-base: #000000 !important;\n  color: #ffffff;\n}\n\n.footer-fixed-tab ion-toolbar .footer-btn-tabs {\n  width: 25%;\n  float: left;\n  text-align: center;\n  padding: 15px;\n  --background: transparent;\n}\n\n.footer-fixed-tab ion-toolbar .footer-btn-tabs img {\n  width: 35px;\n}\n\n.item {\n  border: 1px solid transparent;\n  width: 95%;\n  height: 110%;\n  border-radius: 5px;\n  background: white;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.div {\n  height: 40px;\n  position: fixed;\n  bottom: 3%;\n  opacity: 1;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  width: 100%;\n}\n\n.icon {\n  color: #00CBEE;\n  font-size: 30px;\n}\n\nion-avatar {\n  height: 45px;\n  width: 45px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #003C69;\n  margin-left: 5%;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n}\n\n.div1 {\n  display: flex;\n}\n\n.span1 {\n  font-size: 10px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.icon1 {\n  font-size: 30px;\n  color: white;\n}\n\n.col3 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  height: 100%;\n}\n\n.col2 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhdC9jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNRLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7QUFDUjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtBQUNKOztBQUFJO0VBQ0ksa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsMEJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FBRVI7O0FBQUk7RUFDSSxrQkFBQTtFQUNBLG9CQUFBO0VBRUEsb0JBQUE7RUFDQSxxQkFBQTtBQUNSOztBQUlJO0VBQ0ksa0JBQUE7RUFDSixjQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esd0JBQUE7QUFGSjs7QUFLUTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QUFIWjs7QUFTQTtFQUNJLGtCQUFBO0FBTko7O0FBT0k7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQUxOOztBQVNJO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0FBTk47O0FBT007RUFDRSxjQUFBO0VBQ0EsWUFBQTtBQUxSOztBQU1RO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBSlY7O0FBTVE7RUFDRSxrQkFBQTtBQUpWOztBQUtVO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0FBSFo7O0FBS1U7RUFDRSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBSFo7O0FBTVE7RUFDRSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQUpWOztBQU1RO0VBQ0UsbUJBQUE7QUFKVjs7QUFLVTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0FBSFo7O0FBS1U7RUFDRSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBSFo7O0FBT007RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsNkVBQUE7QUFMUjs7QUFVRTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFQSjs7QUFRSTtFQUNFLHFCQUFBO0VBQ0Esb0NBQUE7RUFDQSxjQUFBO0FBTk47O0FBT007RUFDRSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0FBTFI7O0FBTVE7RUFDRSxXQUFBO0FBSlY7O0FBU0U7RUFDRSw2QkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFOSjs7QUFRRTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7QUFMSjs7QUFPRTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBSk47O0FBTUU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7QUFISjs7QUFLRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0FBRko7O0FBSUU7RUFDRSxhQUFBO0FBREo7O0FBR0U7RUFDRSxlQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUFBSjs7QUFFRTtFQUNFLGVBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBQ0U7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQUVKOztBQUFFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUdKIiwiZmlsZSI6InNyYy9hcHAvY2hhdC9jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xuICAgICAgICBmb250LWZhbWlseTpcIlBvcGlucy1Cb2xkXCI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uY2hhdHtcbiAgICBkaXNwbGF5OmZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAubWV7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgbWFyZ2luOiAyMHB4IDBweCAwcHg7XG4gICAgICAgIG1heC13aWR0aDogOTAlO1xuICAgICAgICBhbGlnbi1zZWxmOiBmbGV4LWVuZDtcbiAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4IDE1cHggMDtcbiAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiBtZWRpdW07XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGhlaWdodDogNTAlO1xuICAgIH1cbiAgICAubWVJbWFnZXtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBtYXJnaW4gOjIwcHggNXB4IDBweDtcbiAgICAgICBcbiAgICAgICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG4gICAgICAgIHBhZGRpbmc6IDZweCAxMHB4IDdweDtcbiAgICAgXG4gICAgICAgXG4gICAgICAgIFxuICAgIH1cbiAgICAueW91e1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWF4LXdpZHRoOiA5MCU7XG4gICAgYWxpZ24tc2VsZjogZmxleC1zdGFydDtcbiAgICBwYWRkaW5nOiAwcHggMTBweCAwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweCAxNXB4IDE1cHggMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBtYXJnaW46IDE4cHggMHB4IDVweCAwcHg7XG4gICAgICAgXG5cbiAgICAgICAgLm5hbWV7XG4gICAgICAgICAgICBmb250LXNpemU6IHNtYWxsO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgXG59XG5cbi5oZWFkZXItZml4ZWQtdG9wIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLmhlYWQtcHJvZmlsZWltZyB7XG4gICAgICB3aWR0aDogMzVweDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IC0yNXB4O1xuICAgICAgdG9wOiAtMTBweDtcbiAgICB9XG4gIH1cbiAgLm1hc3NhZ2VjaGF0LWNvbnRlbnR7XG4gICAgLmNoYXQtaW50ZXJuYWwtYmxvY2sge1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBwYWRkaW5nOiAxMHB4IDEwcHggNTBweCAwcHg7XG4gICAgICAuY2hhdC1tYXNzYWdlLWJveCB7XG4gICAgICAgIG92ZXJmbG93OiBhdXRvO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIC5jaGF0LXJvdyB7XG4gICAgICAgICAgd2lkdGg6IDk2JTtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMiU7XG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgICAuc2VuZGVyIHtcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG4gICAgICAgICAgcHJvZmlsZSB7XG4gICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDBweDtcbiAgICAgICAgICAgIHRvcDozcHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5jaGF0LW1zZ2JveCB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjM0U5REZGO1xuICAgICAgICAgICAgcGFkZGluZzogOHB4IDEwcHggNXB4IDVweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmNoYXQtdGltZSB7XG4gICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICB9XG4gICAgICAgIC5yZWNpdmVyIHtcbiAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAzMHB4O1xuICAgICAgICAgIHByb2ZpbGUge1xuICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICByaWdodDogMHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgdG9wOjNweDtcbiAgICAgICAgICB9XG4gICAgICAgICAgLmNoYXQtbXNnYm94IHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlNGU0ZTQ7XG4gICAgICAgICAgICBwYWRkaW5nOiA4cHggNXB4IDVweCAxMHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2NjY2O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLmNoYXQtdHh0Ym94IHtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiA1JTtcbiAgICAgICAgYm90dG9tOiAxMHB4O1xuICAgICAgICBib3JkZXI6IHNvbGlkIHRoaW4gI2UxZTFlMTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBib3gtc2hhZG93OiAwIDJweCA1cHggMCByZ2JhKDAsIDAsIDAsIC4xNiksIDAgMnB4IDEwcHggMCByZ2JhKDAsIDAsIDAsIC4xMik7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLmZvb3Rlci1maXhlZC10YWIge1xuICAgIGJhY2tncm91bmQ6ICM0MTQxNDE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMHB4IDUlO1xuICAgIGlvbi10b29sYmFyIHtcbiAgICAgIC0tYmFja2dyb3VuZDogIzQxNDE0MTtcbiAgICAgIC0taW9uLWNvbG9yLWJhc2U6ICMwMDAwMDAgIWltcG9ydGFudDtcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgLmZvb3Rlci1idG4tdGFicyB7XG4gICAgICAgIHdpZHRoOiAyNSU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIGltZyB7XG4gICAgICAgICAgd2lkdGg6IDM1cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLml0ZW0ge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgIHdpZHRoOiA5NSU7XG4gICAgaGVpZ2h0OiAxMTAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuZGl2IHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogMyU7XG4gICAgb3BhY2l0eTogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmljb24ge1xuICAgICAgY29sb3I6ICMwMENCRUU7XG4gICAgICBmb250LXNpemU6IDMwcHg7XG4gIH1cbiAgaW9uLWF2YXRhciB7XG4gICAgaGVpZ2h0OiA0NXB4O1xuICAgIHdpZHRoOiA0NXB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDNDNjk7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xuICB9XG4gIC5jb2wxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICB9XG4gIC5kaXYxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG4gIC5zcGFuMSB7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICB9XG4gIC5pY29uMSB7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICAuY29sMyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG4gIC5jb2wyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuIl19 */");

/***/ }),

/***/ "./src/app/chat/chat.page.ts":
/*!***********************************!*\
  !*** ./src/app/chat/chat.page.ts ***!
  \***********************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");










let ChatPage = class ChatPage {
    constructor(router, af, fs, storage, route, alertController, authService) {
        this.router = router;
        this.af = af;
        this.fs = fs;
        this.storage = storage;
        this.route = route;
        this.alertController = alertController;
        this.authService = authService;
        this.showList = false;
        this.isSearch = false;
        this.professional = false;
        this.professionalUser = {};
        this.chatUserName = false;
        this.storage.get('user').then((user) => {
            this.route.queryParams.subscribe(params => {
                if (params && params.special && params.pageRoute) {
                    this.chatUserName = false;
                    this.title = "Get Help";
                }
                else if (params && params.special) {
                    var todayDate = moment__WEBPACK_IMPORTED_MODULE_7__().format("DD-MM-YYYY");
                    var details = JSON.parse(params.special);
                    this.chatUserName = true;
                    this.recieverId = details.recieverId;
                    this.senderId = details.senderId;
                    this.recieverName = details.recieverName;
                    this.senderName = details.senderName;
                    console.log(this.recieverName);
                    document.getElementById("demo").innerHTML = this.recieverName.split(" ", 1);
                    ;
                    console.log(typeof (this.recieverName));
                    this.recieverImg = details.recieverImg,
                        this.pageRoute = details.url;
                    this.uid = user._id;
                    console.log("Reciever/sender" + this.recieverId + " " + this.senderId + "" + this.uid);
                    // this.chatRef = this.fs.collection('chats', ref => ref.orderBy('Timestamp')).valueChanges();
                    // tslint:disable-next-line: max-line-length
                    this.chatRef = this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).collection('messagetrail', ref => ref.orderBy('Timestamp')).valueChanges();
                    console.log(this.chatRef);
                    //  this.fs.collection("friends").doc(this.senderId).collection('chats').doc(this.recieverId)
                    //  .get().subscribe((doc)=>{
                    //    if (doc.exists) {
                    //      console.log("Document data:", doc.data());
                    //      this.userName=doc.data().recieverName;
                    //    } else {
                    //      // doc.data() will be undefined in this case
                    //      console.log("No such document!");
                    //    }
                    //  })
                }
            });
        });
    }
    presentAlertConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Are you sure you want to block ' + this.recieverName,
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Okay',
                        handler: () => {
                            console.log('Confirm Okay');
                            this.blockUser();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    ngOnInit() {
        var obj = { "userId": this.senderId };
        this.authService.getUserProfileById(obj).subscribe((data) => {
            console.log(data);
            this.senderImg = data.data.user_Img;
        });
        this.scrollToBottomOnInit();
    }
    blockUser() {
        var obj = { 'userId': this.senderId, 'hostId': this.recieverId };
        this.authService.blockUser(obj).subscribe((data) => {
            if (data.success) {
                this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).set({
                    isblocked: true
                }, { merge: true });
                this.fs.collection('friends').doc(this.recieverId).collection('chats').doc(this.senderId).set({
                    isblocked: true
                }, { merge: true });
            }
            else {
                this.authService.presentToast("Please try in sometime.");
            }
        });
    }
    viewProfile() {
        let navigationExtras = {
            queryParams: {
                pageroute: this.router.url,
                user_id: this.recieverId,
                professionalUser: JSON.stringify(this.professionalUser),
            }
        };
        this.router.navigate(['profile1'], navigationExtras);
    }
    // viewProfile(){
    //  var obj={"userId":this.recieverId};
    //  this.authService.getUserProfileById(obj).subscribe((data:any)=>{
    //    if(data.success){
    //      console.log(data);
    //      this.professionalUser=data.data;
    //      let navigationExtras = {
    //       queryParams: {
    //         pageroute: this.router.url,
    //         user_id:this.senderId,
    //         professionalUser:JSON.stringify(this.professionalUser),
    //       }
    //     };
    //     this.router.navigate(['profile1'], navigationExtras);
    //    }
    //  })
    // }
    scrollToBottomOnInit() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom(400);
            }
        }, 500);
    }
    send(text) {
        if (this.text != '') {
            var formattedTime = moment__WEBPACK_IMPORTED_MODULE_7__().format('HH:mm');
            var formattedDate = moment__WEBPACK_IMPORTED_MODULE_7__().format("DD-MM-YYYY");
            // this.fs.collection('chats').add({
            //   Name: "test user",
            //   Message: text,
            //   UserID: this.uid,
            //   Timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            // });
            // this.text = '';
            console.log("lola");
            console.log(this);
            this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).collection('messagetrail').add({
                //Name: this.hostdisplayname,
                Message: text,
                UserID: this.uid,
                Timestamp: firebase__WEBPACK_IMPORTED_MODULE_6__["firestore"].FieldValue.serverTimestamp(),
                recieverId: this.recieverId,
                senderId: this.senderId,
                msgTime: formattedTime,
                msgDate: formattedDate,
                msgSent: true,
                msgRead: false
            });
            this.fs.collection('friends').doc(this.senderId).collection('chats').doc(this.recieverId).update({
                lastMsg: this.text,
                msgTime: formattedTime,
                msgDate: formattedDate,
            });
            this.fs.collection('friends').doc(this.recieverId).collection('chats').doc(this.senderId).collection('messagetrail').add({
                Name: this.recieverName,
                Message: text,
                UserID: this.uid,
                Timestamp: firebase__WEBPACK_IMPORTED_MODULE_6__["firestore"].FieldValue.serverTimestamp(),
                recieverId: this.senderId,
                senderId: this.recieverId,
                msgTime: formattedTime,
                msgDate: formattedDate,
                msgSent: true,
                msgRead: false
            });
            this.fs.collection('friends').doc(this.recieverId).collection('chats').doc(this.senderId).update({
                lastMsg: this.text,
                msgTime: formattedTime,
                msgDate: formattedDate,
            });
            this.text = '';
            this.scrollToBottomOnInit();
            var obj = { 'hostId': this.recieverId, "userId": this.senderId, "fullName": this.senderName,
                "recieverImg": this.recieverImg, "senderImg": this.senderImg, "hostName": this.recieverName };
            this.authService.messageNotification(obj).subscribe((data) => {
                if (data.success) {
                    console.log(data);
                }
            });
        }
    }
    // goToProfile1() {
    //   console.log(x);
    //     let navigationExtras:NavigationExtras = {
    //       queryParams: {
    //         professional:this.professional,
    //         pageroute: this.router.url,
    //         user_id:x._id,
    //         professionalUser:JSON.stringify(x),
    //       }
    //     };
    //     this.router.navigate(['profile1'],navigationExtras);
    // }
    back() {
        this.router.navigateByUrl('tabs/tab3');
    }
};
ChatPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["AlertController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"] }
];
ChatPage.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['content', { static: false },] }]
};
ChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./chat.page.scss */ "./src/app/chat/chat.page.scss")).default]
    })
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=chat-chat-module-es2015.js.map