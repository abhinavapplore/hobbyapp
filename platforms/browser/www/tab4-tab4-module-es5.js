(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab4-tab4-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppTab4Tab4PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header style=\"background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%) !important;\">\n  <ion-list-header style=\"left: -9px;\">\n    \n      <ion-slides pager=true [options]=\"slideOpts2\">\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/singing.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/gym.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/meditation.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/run.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n    </ion-slides>\n  \n  </ion-list-header>\n</ion-header>\n\n<ion-content>\n\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n<div class=\"div\">\n    <ion-row > \n      <ion-col [size]=2 class=\"col5\" (click)='openFirst()'>\n        <img class=\"img\" src=\"../../assets/images/menu.svg\">\n      </ion-col>\n      <ion-col [size]=10 class=\"col6\">\n        <ion-text class=\"text\">Hub</ion-text>\n      </ion-col>   \n    </ion-row>  \n    <ion-segment selected value=\"professionals\" (ionChange)=\"segmentChanged($event)\" mode=\"ios\">\n      <ion-segment-button value=\"professionals\">\n        <ion-label  class=\"mytext\">PROFESSIONALS</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"passionists\">\n        <ion-label  class=\"mytext\">PASSIONISTS</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n  </div>\n  \n  <!-- <div class=\"div1\">\n    <ion-row class=\"row\">\n      <ion-col class=\"col1\" [class.selected]='professional' (click)=\"selectedSegment('proffessionals')\">\n        <ion-text class=\"text1\">PROFESSIONALS</ion-text>\n      </ion-col>\n      <ion-col class=\"col1\" [class.selected]='!professional' (click)=\"selectedSegment('passionists')\">\n        <ion-text class=\"text1\">PASSIONISTS</ion-text>\n      </ion-col>\n    </ion-row>\n  </div>  -->\n\n\n\n\n<div *ngIf=\"professional\" class=\"div2\">\n  <ion-row  class=\"row1\" *ngIf='!search'>\n    <ion-col (click)=\"openSearch()\" [size]=2 class=\"col2\">\n      <ion-icon class=\"icon2\" name=\"search\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8 class=\"col2\">\n      <ion-input (ionFocus)=\"openSearch()\" type='text' placeholder='Search...'></ion-input>\n    </ion-col>\n    <ion-col [size]=2 class=\"col\" (click)=\"presentModal()\">\n      <ion-icon class=\"icon1\" name=\"funnel-outline\"></ion-icon>\n    </ion-col>\n  </ion-row>\n\n  <ion-row class=\"row1\" *ngIf='search'>\n    <ion-col [size]=2 class=\"col2\">\n      <ion-icon class=\"icon2\" name=\"search\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8 class=\"col2\">\n      <ion-input (ionInput)=\"getUser($event)\" type='text' placeholder='Search...'></ion-input>\n    </ion-col>\n    <ion-col [size]=2 class=\"col2\" (click)=\"closeSearch()\">\n      <ion-icon class=\"icon2\" name=\"close-outline\"></ion-icon>\n    </ion-col>\n    <ion-list *ngIf=\"showList\" class=\"list\">\n      <ion-item *ngFor=\"let users of searchArray\" (click)=\"selectProfile(users)\">\n       <ion-text class=\"text11\">\n          {{users.firstName}}          \n        </ion-text> \n      \n      </ion-item>\n    </ion-list>\n  </ion-row>\n</div>\n\n\n\n\n<div *ngIf=\"!professional\" class=\"div2\">\n  <ion-row  class=\"row1\" *ngIf='!search'>\n    <ion-col (click)=\"openSearch()\" [size]=2 class=\"col2\">\n      <ion-icon class=\"icon2\" name=\"search\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8 class=\"col2\">\n      <ion-input (ionFocus)=\"openSearch()\" type='text' placeholder='Search...'></ion-input>\n    </ion-col>\n    <ion-col [size]=2 class=\"col\" (click)=\"presentModal()\">\n      <ion-icon class=\"icon1\" name=\"funnel-outline\"></ion-icon>\n    </ion-col>\n  </ion-row>\n\n  <ion-row class=\"row1\" *ngIf='search'>\n    <ion-col [size]=2 class=\"col2\">\n      <ion-icon class=\"icon2\" name=\"search\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8 class=\"col2\">\n      <ion-input (ionInput)=\"getUser1($event)\" type='text' placeholder='Search...'></ion-input>\n    </ion-col>\n    <ion-col [size]=2 class=\"col2\" (click)=\"closeSearch()\">\n      <ion-icon class=\"icon2\" name=\"close-outline\"></ion-icon>\n    </ion-col>\n    <ion-list *ngIf=\"showList\" class=\"list\">\n      <ion-item *ngFor=\"let users of searchArray\" (click)=\"selectProfile(users)\">\n       <ion-text class=\"text11\">\n          {{users.firstName}}          \n        </ion-text> \n      \n      </ion-item>\n    </ion-list>\n  </ion-row>\n</div>\n\n<div>\n  <div *ngIf=\"professional && professionalUsers.length==0\" class=\"div6\">\n    <ion-text>No Users</ion-text>\n  </div>\n\n\n  <div *ngIf='professional && professionalUsers.length!=0'>\n\n    <div class=\"div4\">\n      <ion-row class=\"row11\">\n        <ion-text class=\"text6\">Top Rated</ion-text>\n      </ion-row>\n    </div>\n    \n    <div style=\"padding: 4%;\n    padding-bottom: 0;\">\n     \n        <ion-slides style=\"height: 200px;\" class=\"slides\" [options]=\"slideOpts\"> \n          <ion-slide (click)=\"goToProfile1(item)\"\n           *ngFor='let item of professionalUsers'>\n           <ion-card style=\"margin: 0;\n           padding: 3%;\n           display: flex;\n           flex-direction: column;\n           align-items: center;\n           justify-content: center;\n           width: 174px;\n           height: 185px;\">\n            <ion-row>\n              <ion-avatar>     \n                <ion-img class=\"img1\" [src]=\"item.user_img\"></ion-img>\n              </ion-avatar>\n             </ion-row>\n             <ion-row style=\"padding-top: 5%;text-transform: capitalize;\">\n              <ion-text class=\"text7\">{{item.firstName}} {{item.lastName}}</ion-text>\n             </ion-row>\n             <ion-row style=\"padding-top: 5%;display: flex;\n             text-align: justify;\n             flex-direction: row;\n             justify-content: center;\">\n              <ion-text class=\"text4\" *ngFor=\"let x of item.userSkills;let z=index\">\n                <span [class.spanpad]=\"z==1\" *ngIf=\"z<2\">\n                  {{x.hobby}}\n                  <span *ngIf=\"z<1 && item.userSkills.length>1\"> | </span>\n                </span>\n                </ion-text>\n             </ion-row>\n             <ion-row style=\"padding-top: 5%;display: flex;\n             flex-direction: row;\n             \">\n             <ion-col size = \"4\">\n              <img style=\"width: 20px;\" src=\"../../assets/images/unlock.svg\">\n             \n             </ion-col>\n             <ion-col style=\"padding-left: 0% !important;\" size = \"8\">\n         <ion-text style=\"font-size: 10px;\" class=\"text2\">{{item.unlockedProfessionalUser.length}} unlocks</ion-text>\n            </ion-col>\n              \n             \n             </ion-row>\n             <ion-row style=\"padding-top: 5%;display: flex;\n             flex-direction: row;\n             justify-content: center;\n             \">\n              <ion-icon class=\"icon5\" name=\"location-outline\"></ion-icon>\n              <ion-text class=\"text10\">{{item.shortAddress}}</ion-text>\n             </ion-row>\n           </ion-card>\n\n          </ion-slide>\n        </ion-slides>\n\n     \n    </div>\n    \n    <div class=\"div5\">\n      <ion-row class=\"row7\">\n        <ion-text class=\"text6\">Nearby</ion-text>\n      </ion-row>\n    </div>\n    \n    <!-- <div class=\"div3\"> -->\n      <!-- <ion-row class=\"row2\"> -->\n        <ion-card style=\"display: flex;margin-top: 2%;\" *ngFor='let item of professionalUsers' (click)=\"goToProfile1(item)\">\n          <ion-col [size]=5 class=\"col3\">\n            <ion-row>\n              <ion-avatar>     \n                <ion-img class=\"img1\" [src]=\"item.user_img\"></ion-img>\n              </ion-avatar> \n            </ion-row>\n            <ion-row class=\"row12\">\n              <ion-col size=\"4\" style=\"display: flex;\n              flex-direction: row-reverse;\">\n                <img style=\"width: 20px;\" src=\"../../assets/images/unlock.svg\">\n              </ion-col>\n\n              <ion-col size=\"8\">\n                <ion-text style=\"font-size:10px\" class=\"text2\">{{item.unlockedProfessionalUser.length}} unlocks</ion-text>\n              </ion-col>\n              \n              \n            </ion-row>\n          </ion-col>\n          <ion-col [size]=7 class=\"col4\">\n            <ion-row class=\"row6\">\n              <ion-text class=\"text3\">{{item.firstName}} {{item.lastName}}</ion-text>\n            </ion-row>\n            <ion-row style=\"display: flex;\n            flex-direction: row;\n            justify-content: flex-start;width: 100%;\">\n              <ion-text class=\"text4\" *ngFor=\"let x of item.userSkills;let z=index\">  \n                <span [class.spanpad]=\"z==1\" *ngIf=\"z<2\">\n                {{x.hobby}}\n                <span *ngIf=\"z<1 && item.userSkills.length>1\"> | </span>\n              </span>\n              </ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-icon class=\"icon4\" name=\"ribbon-outline\"></ion-icon>\n              <ion-text class=\"text5\">{{item.skillLevel}} skill level</ion-text>\n            </ion-row>\n            <ion-row class=\"row3\">\n              <ion-icon class=\"icon4\" name=\"location-outline\"></ion-icon>\n              <ion-text class=\"text5\">{{item.shortAddress}}</ion-text>\n            </ion-row>\n          </ion-col>\n        </ion-card>       \n      <!-- </ion-row> -->\n    <!-- </div> -->\n    \n    <div class=\"div5\">\n      <ion-row class=\"row7\">\n        <ion-text class=\"text6\">Recently Joined</ion-text>\n      </ion-row>\n    </div>\n    \n    <!-- <div class=\"div3\"> -->\n      <!-- <ion-row class=\"row2\"> -->\n        <ion-card style=\"display: flex;margin-top: 2%;\" *ngFor='let item of professionalUsers' (click)=\"goToProfile1(item)\"> \n          <ion-col [size]=5  class=\"col3\">\n            <ion-row>\n              <ion-avatar>     \n                <ion-img class=\"img1\" [src]=\"item.user_img\"></ion-img>\n              </ion-avatar> \n            </ion-row>\n            <ion-row class=\"row12\">\n              <ion-col size=\"4\" style=\"display: flex;\n              flex-direction: row-reverse;\">\n                <img style=\"width: 20px;\" src=\"../../assets/images/unlock.svg\">\n              </ion-col>\n\n              <ion-col size=\"8\">\n                <ion-text style=\"font-size: 10px;\" class=\"text2\">{{item.unlockedProfessionalUser.length}} unlocks</ion-text>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-col [size]=7 class=\"col4\">\n            <ion-row class=\"row6\">\n              <ion-text class=\"text3\">{{item.firstName}} {{item.lastName}}</ion-text>\n            </ion-row>\n            <ion-row style=\"display: flex;\n            flex-direction: row;\n            justify-content: flex-start;width: 100%;\">\n              <ion-text class=\"text4\" *ngFor=\"let x of item.userSkills;let z=index\"> \n                 <span [class.spanpad]=\"z==1\" *ngIf=\"z<2\">\n                {{x.hobby}}\n                <span *ngIf=\"z<1 && item.userSkills.length>1\"> | </span>\n             \n              </span>\n              </ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-icon class=\"icon4\" name=\"ribbon-outline\"></ion-icon>\n              <ion-text class=\"text5\">{{item.skillLevel}} skill level</ion-text>\n            </ion-row>\n            <ion-row class=\"row3\">\n              <ion-icon class=\"icon4\" name=\"location-outline\"></ion-icon>\n              <ion-text class=\"text5\">{{item.shortAddress}}</ion-text>\n            </ion-row>\n          </ion-col>\n        </ion-card>\n        \n      <!-- </ion-row> -->\n    <!-- </div> -->\n  \n  </div>\n</div>\n\n\n\n<div>\n  <div *ngIf='!professional && passionistUsers!=0'>\n\n    <div class=\"div4\">\n      <ion-row class=\"row11\">\n        <ion-text class=\"text6\">Top Rated</ion-text>\n      </ion-row>\n    </div>\n    \n    <div style=\"padding: 4%;\n    padding-bottom: 0;padding-top: 0;\">\n     \n        <ion-slides  style=\"height: 200px;\" [options]=\"slideOpts1\"> \n          <ion-slide  (click)=\"goToProfile1(item)\"\n           *ngFor='let item of passionistUsers'>\n           <ion-card style=\"margin: 0;\n           padding: 3%;\n           display: flex;\n           flex-direction: column;\n           align-items: center;\n           justify-content: center;width: 174px;\n           height: 185px;\">\n            <ion-row>\n              <ion-avatar>     \n                <ion-img class=\"img1\" [src]=\"item.user_img\"></ion-img>\n              </ion-avatar>\n             </ion-row>\n             <ion-row style=\"padding-top: 5%;text-transform: capitalize;\">\n              <ion-text class=\"text7\">{{item.firstName}} {{item.lastName}}</ion-text>\n             </ion-row>\n             <ion-row style=\"padding-top: 5%;display: flex;\n             text-align: justify;\n             flex-direction: row;\n             justify-content: center;\">\n              <ion-text class=\"text4\" *ngFor=\"let x of item.userHobby;let z=index\">\n                <span [class.spanpad]=\"z==1\" *ngIf=\"z<2\">\n                  {{x.hobby}}\n                  <span [class.spanpad]=\"z==1\" *ngIf=\"z<1 && item.userHobby.length>1\"> | </span>\n                </span>\n                </ion-text>\n             </ion-row>\n            \n             <ion-row style=\"padding-top: 5%;display: flex;\n             flex-direction: row;\n             justify-content: center;\n             \">\n              <ion-icon class=\"icon5\" name=\"location-outline\"></ion-icon>\n              <ion-text class=\"text10\">{{item.shortAddress}}</ion-text>\n             </ion-row>\n           </ion-card>\n\n          </ion-slide>\n        </ion-slides>\n    \n       \n    </div>\n    \n    <div class=\"div5\">\n      <ion-row class=\"row7\">\n        <ion-text class=\"text6\">Nearby</ion-text>\n      </ion-row>\n    </div>\n    \n    <!-- <div class=\"div3\" \n     > -->\n      <!-- <ion-row class=\"row2\"> -->\n        <ion-card style=\"display: flex;margin-top: 2%;\" *ngFor='let item of passionistUsers' (click)=\"goToProfile1(item)\">\n          <ion-col [size]=4 class=\"col3\">\n            <ion-row>\n              <ion-avatar>     \n                <ion-img class=\"img1\" [src]=\"item.user_img\"></ion-img>\n              </ion-avatar> \n            </ion-row>\n            <!-- <ion-row class=\"row12\">\n              <ion-icon class=\"icon3\" name=\"star\"></ion-icon>\n              <ion-text class=\"text2\">0/5</ion-text>\n            </ion-row> -->\n          </ion-col>\n          <ion-col [size]=8 class=\"col4\">\n            <ion-row class=\"row6\">\n              <ion-text class=\"text3\">{{item.firstName}} {{item.lastName}}</ion-text>\n            </ion-row>\n            <ion-row style=\"display: flex;\n              flex-direction: row;\n              justify-content: flex-start;width: 100%;\">\n              <ion-text class=\"text4\" *ngFor=\"let x of item.userHobby;let z=index\">\n                <span [class.spanpad]=\"z==1\" *ngIf=\"z<2\">\n                  {{x.hobby}}\n                  <span *ngIf=\"z<1 && item.userHobby.length>1\"> | </span>\n                </span> </ion-text>\n            </ion-row>\n            <!-- <ion-row class=\"row4\">\n              <ion-icon class=\"icon4\" name=\"ribbon-outline\"></ion-icon>\n              <ion-text class=\"text5\">5 Years Exp</ion-text>\n            </ion-row> -->\n            <ion-row class=\"row3\">\n              <ion-icon class=\"icon4\" name=\"location-outline\"></ion-icon>\n              <ion-text class=\"text5\">{{item.shortAddress}}</ion-text>\n            </ion-row>\n          </ion-col>\n        </ion-card>\n        \n      <!-- </ion-row> -->\n    <!-- </div> -->\n    \n    <div class=\"div5\">\n      <ion-row class=\"row7\">\n        <ion-text class=\"text6\">Recently Joined</ion-text>\n      </ion-row>\n    </div>\n    \n    <!-- <div class=\"div3\"> -->\n      <!-- <ion-row class=\"row2\"> -->\n        <ion-card style=\"display: flex;margin-top: 2%;\" *ngFor='let item of passionistUsers' (click)=\"goToProfile1(item)\">\n          <ion-col [size]=4  class=\"col3\">\n            <ion-row>\n              <ion-avatar>     \n                <ion-img class=\"img1\" [src]=\"item.user_img\"></ion-img>\n              </ion-avatar> \n            </ion-row>\n            <!-- <ion-row class=\"row12\">\n              <ion-icon class=\"icon3\" name=\"star\"></ion-icon>\n              <ion-text class=\"text2\">0/5</ion-text>\n            </ion-row> -->\n          </ion-col>\n          <ion-col [size]=8 class=\"col4\">\n            <ion-row class=\"row6\">\n              <ion-text class=\"text3\">{{item.firstName}} {{item.lastName}}</ion-text>\n            </ion-row>\n            <ion-row style=\"display: flex;\n            flex-direction: row;\n            justify-content: flex-start;width: 100%;\">\n              <ion-text class=\"text4\" *ngFor=\"let x of item.userHobby;let z=index\"> \n                 <span [class.spanpad]=\"z==1\" *ngIf=\"z<2\">\n                {{x.hobby}}\n                <span *ngIf=\"z<1 && item.userHobby.length>1\"> | </span>\n              </span> </ion-text>\n            </ion-row>\n            <!-- <ion-row class=\"row4\">\n              <ion-icon class=\"icon4\" name=\"ribbon-outline\"></ion-icon>\n              <ion-text class=\"text5\">5 Years Exp</ion-text>\n            </ion-row> -->\n            <ion-row class=\"row3\">\n              <ion-icon class=\"icon4\" name=\"location-outline\"></ion-icon>\n              <ion-text class=\"text5\">{{item.shortAddress}}</ion-text>\n            </ion-row>\n          </ion-col>\n        </ion-card>\n        \n      <!-- </ion-row> -->\n    <!-- </div> -->\n  \n  </div>\n  \n  <div *ngIf=\"!professional && passionistUsers.length==0\" class=\"div6\">\n    <ion-text>No Users</ion-text>\n  </div>\n</div>\n\n\n\n\n\n<ion-row style=\"padding-top: 15%;\">\n  <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n</ion-row>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/tab4/tab4-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/tab4/tab4-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: Tab4PageRoutingModule */

    /***/
    function srcAppTab4Tab4RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab4PageRoutingModule", function () {
        return Tab4PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _tab4_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tab4.page */
      "./src/app/tab4/tab4.page.ts");

      var routes = [{
        path: '',
        component: _tab4_page__WEBPACK_IMPORTED_MODULE_3__["Tab4Page"]
      }];

      var Tab4PageRoutingModule = function Tab4PageRoutingModule() {
        _classCallCheck(this, Tab4PageRoutingModule);
      };

      Tab4PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Tab4PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/tab4/tab4.module.ts":
    /*!*************************************!*\
      !*** ./src/app/tab4/tab4.module.ts ***!
      \*************************************/

    /*! exports provided: Tab4PageModule */

    /***/
    function srcAppTab4Tab4ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab4PageModule", function () {
        return Tab4PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _tab4_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tab4-routing.module */
      "./src/app/tab4/tab4-routing.module.ts");
      /* harmony import */


      var _tab4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./tab4.page */
      "./src/app/tab4/tab4.page.ts");

      var Tab4PageModule = function Tab4PageModule() {
        _classCallCheck(this, Tab4PageModule);
      };

      Tab4PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _tab4_routing_module__WEBPACK_IMPORTED_MODULE_5__["Tab4PageRoutingModule"]],
        declarations: [_tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"]]
      })], Tab4PageModule);
      /***/
    },

    /***/
    "./src/app/tab4/tab4.page.scss":
    /*!*************************************!*\
      !*** ./src/app/tab4/tab4.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppTab4Tab4PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".thumbnail {\n  height: 125px;\n  width: 390px;\n  padding: 1%;\n}\n\n.img2 {\n  height: 125px;\n  width: 390px;\n  border-radius: 5px;\n}\n\nion-segment-button {\n  --indicator-color: lightgray;\n}\n\n#top {\n  position: fixed;\n  top: 20%;\n  width: 90%;\n  z-index: 100;\n}\n\n.div {\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%) !important;\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  width: 100%;\n}\n\n.icon {\n  color: #ffffff;\n  font-size: 25px;\n}\n\n.spanpad {\n  padding-left: 2px;\n}\n\n.img {\n  width: 25px;\n}\n\n.text {\n  font-family: \"Poppins-Bold\";\n  font-size: 25px;\n  color: #FFFFFF;\n  margin-left: 5%;\n}\n\n.div1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.row {\n  padding-top: 5%;\n  width: 311px;\n}\n\n.selected {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  border-bottom: 2px solid white !important;\n  height: 143%;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.text1 {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #FFFFFF;\n}\n\n.mytext {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #000000;\n}\n\n.div2 {\n  height: 52px;\n  bottom: 3%;\n  opacity: 1;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  width: 100%;\n  margin-top: 5%;\n}\n\n.icon1 {\n  color: #02CBEE;\n  font-size: 23px;\n}\n\n.icon2 {\n  color: #000000;\n  font-size: 23px;\n}\n\n.row1 {\n  width: 340px;\n}\n\n.col {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  align-items: center;\n}\n\n.col2 {\n  background: #FFFFFF;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 40px;\n}\n\nion-avatar {\n  height: 56px;\n  width: 56px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #003C69;\n}\n\n.icon3 {\n  color: #FFC107;\n  font-size: 15px;\n}\n\n.text2 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n}\n\n.text3 {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #003C69;\n  text-transform: capitalize;\n}\n\n.text4 {\n  font-family: \"Poppins-Regular\";\n  font-size: 10px;\n  color: #767474;\n}\n\n.text2 {\n  font-family: \"Poppins-Regular\";\n  font-size: 12px;\n  color: #000000;\n}\n\n.text5 {\n  font-family: \"Poppins-Regular\";\n  font-size: 12px;\n  color: #000000;\n  margin-left: 3%;\n}\n\n.icon4 {\n  font-size: 12px;\n  color: #000000;\n}\n\n.div3 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  padding-top: 3%;\n}\n\n.row2 {\n  width: 360px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 5px;\n}\n\n.row3 {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n  width: 100%;\n}\n\n.list {\n  width: 90%;\n}\n\n.row4 {\n  padding-top: 5%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n  width: 100%;\n}\n\n.row6 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n  text-transform: capitalize;\n}\n\n.text6 {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row7 {\n  width: 340px;\n}\n\n.text11 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n}\n\n.div4 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.text7 {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #003C69;\n}\n\n.text8 {\n  font-family: \"Poppins-Regular\";\n  font-size: 10px;\n  color: #767474;\n}\n\n.text9 {\n  font-family: \"Poppins-Regular\";\n  font-size: 10px;\n  color: #000000;\n}\n\n.text10 {\n  font-family: \"Poppins-Regular\";\n  font-size: 8px;\n  color: #767474;\n}\n\n.icon5 {\n  font-size: 8px;\n  color: #767474;\n}\n\n.icon6 {\n  color: #FFC107;\n  font-size: 10px;\n}\n\n.img1 {\n  width: 56px;\n  height: 56px;\n}\n\n.row8 {\n  padding-top: 5%;\n}\n\n.row9 {\n  padding-top: 3%;\n}\n\n.div5 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  padding-top: 3%;\n}\n\n.row10 {\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n\n.slide {\n  display: flex;\n  flex-direction: column;\n}\n\n.slide1 {\n  width: 311px;\n  height: 187px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 5px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.slide2 {\n  width: 150px;\n  height: 150px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 5px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row11 {\n  padding-top: 5%;\n  width: 340px;\n}\n\n.col3 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row12 {\n  padding-top: 5%;\n}\n\n.col4 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.col5 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.col6 {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.div6 {\n  margin-top: 10%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.div6 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiNC90YWI0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFVQTtFQUNJLGFBQUE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtBQVROOztBQVlFO0VBQ0UsYUFBQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQVROOztBQVlBO0VBQ0ksNEJBQUE7QUFUSjs7QUFZQTtFQUVFLGVBQUE7RUFDRSxRQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7QUFWSjs7QUFlQTtFQUNJLHdFQUFBO0VBQ0EsZ0RBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUVBLFdBQUE7QUFiSjs7QUFlRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBWko7O0FBZUU7RUFDRSxpQkFBQTtBQVpKOztBQWNFO0VBQ0UsV0FBQTtBQVhKOztBQWFFO0VBQ0UsMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFWSjs7QUFZQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBVEo7O0FBV0U7RUFDRSxlQUFBO0VBQ0EsWUFBQTtBQVJKOztBQVVFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSx5Q0FBQTtFQUNBLFlBQUE7QUFQSjs7QUFTQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBTkY7O0FBUUE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBTEo7O0FBUUE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBTEo7O0FBU0U7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0FBTko7O0FBUUU7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBQUxKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFKSjs7QUFTQTtFQUNJLFlBQUE7QUFOSjs7QUFRQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUFMSjs7QUFPQTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFKSjs7QUFNQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FBSEo7O0FBS0U7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQUZOOztBQUlFO0VBQ0ksOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUROOztBQUdFO0VBQ0UsMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0FBQUo7O0FBRUU7RUFDRSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBQ0o7O0FBQ0E7RUFDSSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBRUo7O0FBQUE7RUFDSSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUdKOztBQURBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUFJSjs7QUFGQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQUtKOztBQUtBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFDQSwrQ0FBQTtFQUNBLGtCQUFBO0FBRko7O0FBSUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQURKOztBQUdBO0VBQ0ksVUFBQTtBQUFKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBQ0E7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUFFSjs7QUFBQTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFHSjs7QUFFQTtFQUNJLFlBQUE7QUFDSjs7QUFDQTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtBQUVKOztBQUFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFHSjs7QUFEQTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFJSjs7QUFGQTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFLSjs7QUFIQTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFNSjs7QUFKQTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUFPSjs7QUFMQTtFQUNJLGNBQUE7RUFDQSxjQUFBO0FBUUo7O0FBTkE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQVNKOztBQVBBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFVSjs7QUFSQTtFQUNJLGVBQUE7QUFXSjs7QUFUQTtFQUNJLGVBQUE7QUFZSjs7QUFWQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQWFKOztBQVhBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FBY0o7O0FBWkE7RUFPRSxhQUFBO0VBQ0Esc0JBQUE7QUFTRjs7QUFMQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQ0FBQTtFQUNBLCtDQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBUUo7O0FBTEE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFDQSwrQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQVFKOztBQURBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUFJSjs7QUFGQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFLSjs7QUFIQTtFQUNJLGVBQUE7QUFNSjs7QUFKQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBT0o7O0FBTEE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBUUo7O0FBTkE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBU0o7O0FBSkE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQU9KOztBQU5JO0VBQ0ksOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVFSIiwiZmlsZSI6InNyYy9hcHAvdGFiNC90YWI0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIC5kaXYge1xuLy8gICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSkgIWltcG9ydGFudDtcbi8vICAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4vLyAgICAgZGlzcGxheTogZmxleDtcbi8vICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuLy8gICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuLy8gICAgIGhlaWdodDogMTQwcHg7XG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuLy8gICAgIHdpZHRoOiAxMDAlO1xuLy8gICB9XG4udGh1bWJuYWlsIHtcbiAgICBoZWlnaHQ6IDEyNXB4O1xuICAgICAgd2lkdGg6IDM5MHB4O1xuICAgICAgcGFkZGluZzogMSU7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogNXB4O1xuICB9XG4gIC5pbWcyIHtcbiAgICBoZWlnaHQ6IDEyNXB4O1xuICAgICAgd2lkdGg6IDM5MHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgLy8gYm9yZGVyLXJhZGl1czogNXB4O1xuICB9XG5pb24tc2VnbWVudC1idXR0b24ge1xuICAgIC0taW5kaWNhdG9yLWNvbG9yOiBsaWdodGdyYXk7XG59XG5cbiN0b3AgXG5cbnsgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMjAlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgei1pbmRleDogMTAwO1xufVxuXG5cblxuLmRpdiB7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKSAhaW1wb3J0YW50O1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgLy8gaGVpZ2h0OiAxMzBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbiAgLmljb24ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgfVxuXG4gIC5zcGFucGFke1xuICAgIHBhZGRpbmctbGVmdDogMnB4O1xuICB9XG4gIC5pbWcge1xuICAgIHdpZHRoOiAyNXB4O1xuICB9XG4gIC50ZXh0IHtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xufSBcbi5kaXYxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgLnJvdyB7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIHdpZHRoOiAzMTFweDtcbiAgfVxuICAuc2VsZWN0ZWQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgd2hpdGUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDE0MyU7XG59IFxuLmNvbDEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn0gXG4udGV4dDEge1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbn0gXG5cbi5teXRleHR7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjMDAwMDAwO1xufVxuXG5cbiAgLmRpdjIge1xuICAgIGhlaWdodDogNTJweDtcbiAgICBib3R0b206IDMlO1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICB9XG4gIC5pY29uMSB7XG4gICAgY29sb3I6ICMwMkNCRUU7XG4gICAgZm9udC1zaXplOiAyM3B4O1xufVxuLmljb24yIHtcbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgICBmb250LXNpemU6IDIzcHg7XG59XG4vLyAucm93MSB7XG4vLyAgICAgd2lkdGg6IDM5MHB4O1xuLy8gfVxuLnJvdzEge1xuICAgIHdpZHRoOiAzNDBweDtcbn1cbi5jb2wge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29sMiB7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDQwcHg7XG59XG5pb24tYXZhdGFyIHtcbiAgICBoZWlnaHQ6IDU2cHg7XG4gICAgd2lkdGg6IDU2cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzAwM0M2OTtcbiAgfVxuICAuaWNvbjMge1xuICAgICAgY29sb3I6ICNGRkMxMDc7XG4gICAgICBmb250LXNpemU6IDE1cHg7XG4gIH1cbiAgLnRleHQyIHtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgY29sb3I6ICMwMDAwMDA7XG4gIH1cbiAgLnRleHQzIHtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6ICMwMDNDNjk7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIH1cbiAgLnRleHQ0IHtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgY29sb3I6ICM3Njc0NzQ7XG59XG4udGV4dDIge1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBjb2xvcjogIzAwMDAwMDtcbn1cbi50ZXh0NSB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIG1hcmdpbi1sZWZ0OiAzJTtcbn1cbi5pY29uNCB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGNvbG9yOiAjMDAwMDAwO1xufVxuLmRpdjMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG59XG4vLyAucm93MiB7XG4vLyAgICAgd2lkdGg6IDM5MHB4O1xuLy8gICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuLy8gICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbi8vICAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbi8vICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4vLyAgICAgcGFkZGluZzogMyU7XG4vLyB9XG4ucm93MiB7XG4gICAgd2lkdGg6IDM2MHB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4ucm93MyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmxpc3Qge1xuICAgIHdpZHRoOiA5MCU7XG59XG4ucm93NCB7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5yb3c2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4udGV4dDYge1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBjb2xvcjogIzAwM0M2OTtcbn1cbi8vIC5yb3c3IHtcbi8vICAgICB3aWR0aDogMzkwcHg7XG4vLyB9XG4ucm93NyB7XG4gICAgd2lkdGg6IDM0MHB4O1xufVxuLnRleHQxMSB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuLmRpdjQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi50ZXh0NyB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjMDAzQzY5O1xufVxuLnRleHQ4IHtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgY29sb3I6ICM3Njc0NzQ7XG59XG4udGV4dDkge1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBjb2xvcjogIzAwMDAwMDtcbn1cbi50ZXh0MTAge1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgIGZvbnQtc2l6ZTogOHB4O1xuICAgIGNvbG9yOiAjNzY3NDc0O1xufVxuLmljb241IHtcbiAgICBmb250LXNpemU6IDhweDtcbiAgICBjb2xvcjogIzc2NzQ3NDtcbn1cbi5pY29uNiB7XG4gICAgY29sb3I6ICNGRkMxMDc7XG4gICAgZm9udC1zaXplOiAxMHB4O1xufVxuLmltZzEge1xuICAgIHdpZHRoOiA1NnB4O1xuICAgIGhlaWdodDogNTZweDtcbn1cbi5yb3c4IHtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG4ucm93OSB7XG4gICAgcGFkZGluZy10b3A6IDMlO1xufVxuLmRpdjUge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG59XG4ucm93MTAge1xuICAgIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG4uc2xpZGUge1xuLy8gICB3aWR0aDogOTRweDtcbi8vICAgaGVpZ2h0OiAxMzZweDtcbi8vICAgb3ZlcmZsb3c6IHZpc2libGU7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbi8vICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4vLyAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbi8vICAgaGVpZ2h0OiAxNzBweDtcblxufVxuLnNsaWRlMSB7XG4gICAgd2lkdGg6IDMxMXB4O1xuICAgIGhlaWdodDogMTg3cHg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuXG4uc2xpZGUyIHtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG5cbi8vIC5yb3cxMXtcbi8vICAgICBwYWRkaW5nLXRvcDogNSU7XG4vLyAgICAgd2lkdGg6IDM5MHB4O1xuLy8gfVxuLnJvdzExIHtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgd2lkdGg6IDM0MHB4O1xufVxuLmNvbDMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnJvdzEyIHtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG4uY29sNCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLmNvbDUge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbDYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi8vIC5zbGlkZXMge1xuLy8gICAgIGhlaWdodDogMTEwJTtcbi8vIH1cbi5kaXY2IHtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/tab4/tab4.page.ts":
    /*!***********************************!*\
      !*** ./src/app/tab4/tab4.page.ts ***!
      \***********************************/

    /*! exports provided: Tab4Page */

    /***/
    function srcAppTab4Tab4PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab4Page", function () {
        return Tab4Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _filtermodal_filtermodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../filtermodal/filtermodal.page */
      "./src/app/filtermodal/filtermodal.page.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");

      var Tab4Page = /*#__PURE__*/function () {
        function Tab4Page(modalController, router, menu, authService, storage, route) {
          var _this = this;

          _classCallCheck(this, Tab4Page);

          this.modalController = modalController;
          this.router = router;
          this.menu = menu;
          this.authService = authService;
          this.storage = storage;
          this.route = route;
          this.professionalUsers = [];
          this.passionistUsers = [];
          this.filterPassionistUser = [];
          this.filterProfessionalUser = [];
          this.allUser = [];
          this.searchArray = [];
          this.showList = false;
          this.isSearch = false;
          this.professional = true;
          this.search = false;
          this.slideOpts = {
            slidesPerView: 3,
            spaceBetween: 5,
            initialSlide: 0
          };
          this.slideOpts1 = {
            slidesPerView: 3,
            spaceBetween: 5,
            initialSlide: 0
          };
          this.slideOpts2 = {
            slidesPerView: 1,
            initialSlide: 0
          };
          this.professionalUsers = [];
          this.passionistUsers = [];
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              _this.userType = params.userType;

              if (_this.userType == 'proffessionals') {
                _this.professionalUsers = JSON.parse(params.special);
                console.log(_this.professionalUsers);
              } else if (_this.userType == 'passionists') {
                _this.passionistUsers = JSON.parse(params.special);
                console.log(_this.passionistUsers);
              }
            }
          });
        }

        _createClass(Tab4Page, [{
          key: "ngOnInIt",
          value: function ngOnInIt() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.loadData();
          }
        }, {
          key: "loadData",
          value: function loadData() {
            var _this2 = this;

            this.professionalUsers = [];
            this.passionistUsers = [];
            this.storage.get('user').then(function (user) {
              _this2.authService.loading("Loading profiles...");

              _this2.userId = user._id;
              _this2.latitude = user.userLatitude;
              _this2.longitude = user.userLongitude;
              var obj = {
                'userId': _this2.userId,
                'userLat': user.userLatitude,
                'userLong': user.userLongitude
              };

              _this2.authService.getAllUser(obj).subscribe(function (data) {
                console.log('heyyyyyyyyyyy');
                console.log(data); // this.allUser.push(data.professional);
                // this.allUser.push(data.passionist);

                _this2.professionalUsers = data.professional;

                _this2.professionalUsers.forEach(function (element) {
                  console.log(element.address);
                  var shortAdd = element.address.split(',');
                  element.shortAddress = shortAdd[0];
                });

                _this2.passionistUsers = data.passionist;

                _this2.passionistUsers.forEach(function (element) {
                  console.log(element.address);
                  var shortAdd = element.address.split(',');
                  element.shortAddress = shortAdd[0];
                });

                _this2.authService.dismissLoading();
              });
            });
          }
        }, {
          key: "doRefresh",
          value: function doRefresh($event) {
            console.log($event);
            this.loadData();
            setTimeout(function () {
              console.log('Async operation has ended');
              $event.target.complete();
            }, 2000);
          }
        }, {
          key: "openFirst",
          value: function openFirst() {
            this.menu.enable(true, 'first');
            this.menu.open('first');
          } // selectedSegment(x){
          //  if(x=="proffessionals"){
          //    this.professional=true;
          //  }else {
          //    this.professional=false;
          //  }
          // }

        }, {
          key: "segmentChanged",
          value: function segmentChanged($event) {
            console.log($event);
            this.selectedTab = $event.detail.value;

            if (this.selectedTab == "professionals") {
              this.professional = true;
            } else {
              this.professional = false;
            }
          }
        }, {
          key: "openSearch",
          value: function openSearch() {
            this.search = true;
            console.log(this.search);
          }
        }, {
          key: "closeSearch",
          value: function closeSearch() {
            this.search = false;
          }
        }, {
          key: "goToProfile1",
          value: function goToProfile1(x) {
            console.log(x);
            var navigationExtras = {
              queryParams: {
                professional: this.professional,
                pageroute: this.router.url,
                user_id: x._id,
                professionalUser: JSON.stringify(x),
                userType: x.userType
              }
            };
            this.router.navigate(['profile1'], navigationExtras);
          }
        }, {
          key: "getUser",
          value: function getUser(ev) {
            var _this3 = this;

            var arr = [];

            var _loop = function _loop(i) {
              input = _this3.professionalUsers[i];
              arr.push(input);
              var val = ev.target.value;

              if (val && val.trim() != '') {
                _this3.searchArray = arr.filter(function (item) {
                  return item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1;
                }); // Show the results

                if (_this3.searchArray.length !== 0) {
                  _this3.showList = true;
                } else {
                  _this3.showList = false;
                }
              } else {
                _this3.showList = false;
              }
            };

            for (var i = 0; i < this.professionalUsers.length; i++) {
              var input;

              _loop(i);
            }
          }
        }, {
          key: "getUser1",
          value: function getUser1(ev) {
            var _this4 = this;

            var arr = [];

            var _loop2 = function _loop2(i) {
              input = _this4.passionistUsers[i];
              arr.push(input);
              var val = ev.target.value;

              if (val && val.trim() != '') {
                _this4.searchArray = arr.filter(function (item) {
                  return item.fullName.toLowerCase().indexOf(val.toLowerCase()) > -1;
                }); // Show the results

                if (_this4.searchArray.length !== 0) {
                  _this4.showList = true;
                } else {
                  _this4.showList = false;
                }
              } else {
                _this4.showList = false;
              }
            };

            for (var i = 0; i < this.passionistUsers.length; i++) {
              var input;

              _loop2(i);
            }
          }
        }, {
          key: "selectProfile",
          value: function selectProfile(x) {
            this.showList = false;
            this.searchTerm = "";
            this.isSearch = false;
            console.log(x);
            this.goToProfile1(x);
          }
        }, {
          key: "presentModal",
          value: function presentModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal, _yield$modal$onWillDi, data;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _filtermodal_filtermodal_page__WEBPACK_IMPORTED_MODULE_3__["FiltermodalPage"],
                        cssClass: 'filterModal'
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      _context.next = 7;
                      return modal.onWillDismiss();

                    case 7:
                      _yield$modal$onWillDi = _context.sent;
                      data = _yield$modal$onWillDi.data;

                      if (data) {
                        console.log(data);
                        this.professionalUsers = [];
                        this.passionistUsers = [];
                        this.professionalUsers = data[0].filterProfessionalUser;
                        this.passionistUsers = data[1].filterPassionistUser;
                      } else {
                        console.log("goes in else"); // this.router.navigateByUrl('profile');
                      }

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return Tab4Page;
      }();

      Tab4Page.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }];
      };

      Tab4Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab4',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./tab4.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./tab4.page.scss */
        "./src/app/tab4/tab4.page.scss"))["default"]]
      })], Tab4Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=tab4-tab4-module-es5.js.map