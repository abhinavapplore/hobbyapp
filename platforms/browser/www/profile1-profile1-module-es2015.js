(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile1-profile1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile1/profile1.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile1/profile1.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"toolbar1\">\n  <ion-row>\n    <ion-col [size]=2  (click)=\"back()\">\n      <ion-icon name=\"chevron-back\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8>\n      <ion-text>Profile</ion-text>\n    </ion-col>\n    <ion-col [size]=2 (click)=\"notification()\">\n      <ion-icon name=\"notifications-outline\"></ion-icon>\n    </ion-col>    \n  </ion-row>   \n</ion-toolbar>\n\n<ion-content>\n\n  <div class=\"div\">\n\n      <ion-row class=\"row\">\n        <ion-thumbnail class=\"thumbnai3\">\n          <ion-img class=\"img\" [src]=\"professionalUser.user_img\"></ion-img>\n        </ion-thumbnail>\n      </ion-row>\n      <ion-row class=\"row1\">\n        <ion-text>Hi,I'm {{professionalUser.firstName}} {{professionalUser.lastName}}</ion-text>\n      </ion-row>\n      <ion-row class=\"row2\">\n        <ion-text>Joined in {{professionalUser.createdDate | date:' MMMM yyyy'}}</ion-text>\n      </ion-row>\n      <ion-row *ngIf='professionalUser.isVerified' class=\"row3\">\n        <ion-col [size]=9>\n          <ion-icon class=\"icon1\" name=\"ribbon-outline\"></ion-icon>\n          <ion-text>Identity Verified</ion-text>\n        </ion-col>\n        <ion-col [size]=3>\n          <ion-icon class=\"icon\" name=\"star\"></ion-icon>\n          <ion-text>{{professionalUser.rating}}/5</ion-text>\n        </ion-col>     \n      </ion-row>\n      <ion-row *ngIf='professionalUser.isVerified' class=\"row4\">\n        <ion-icon name=\"thumbs-up-outline\"></ion-icon>\n        <ion-text>40+ Ratings</ion-text>\n      </ion-row>\n      <ion-row *ngIf='professionalUser.about!=null || professionalUser.about!=undefined'\n       class=\"row5\">\n        <ion-text>About</ion-text>\n      </ion-row>\n      <ion-row *ngIf='professionalUser.about!=null || professionalUser.about!=undefined' \n      class=\"row6\">\n        <!-- <ion-textarea *ngIf='proffessionalUser.about!=null || proffessionalUser.about!=undefined'\n       [(ngModel)]=\"proffessionalUser.about\" ></ion-textarea> -->\n        <ion-text >{{professionalUser.about}} </ion-text>\n      </ion-row>\n\n      <div class=\"div3\">\n        <ion-row *ngIf=\"docUrl1!='' && docUrl2!=''\" class=\"row5\">\n          <ion-text>Certificates</ion-text>\n        </ion-row>\n        <ion-row class=\"row7\">\n          <ion-thumbnail *ngIf=\"docUrl1!='' && docUrl1!=null && docUrl1!=undefined\" \n          class=\"thumbnail\">\n            <ion-img class=\"img\" [src]=\"docUrl1\"></ion-img>\n          </ion-thumbnail>\n          <ion-thumbnail *ngIf=\"docUrl2!='' && docUrl2!=null && docUrl2!=undefined\"\n           class=\"thumbnail\">\n            <ion-img class=\"img\" [src]=\"docUrl2\"></ion-img>\n          </ion-thumbnail>\n        </ion-row>\n      </div>\n      \n      <ion-row class=\"row5\">\n        <ion-text>Skills</ion-text>\n      </ion-row>\n      <ion-row class=\"row8\">\n        <ion-slides [options]=\"slideOpts\" style=\"margin: 0;\">\n          <ion-slide *ngFor='let x of professionalUser.userSkills'>\n            <ion-card class=\"container\">\n              <ion-thumbnail class=\"thumbnail2\">\n                <ion-img class=\"imgSelected\" [src]=\"x.hobbyImage\"></ion-img>\n              </ion-thumbnail>        \n              <div class=\"centered\">\n                <ion-text>{{x.hobby}}</ion-text>\n              </div>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-row>\n\n\n      <ion-row class=\"row5\" *ngIf=\"userPosts.length!=0\">\n        <ion-text>Posts</ion-text>\n      </ion-row>\n\n      <ion-row style=\"width: 100%;\" *ngIf=\"userPosts.length!=0\"> \n        <ion-slides [options]=\"slideOpts1\" pager=true>\n          <ion-slide *ngFor='let item of userPosts'>\n              <ion-card style=\"width: 100%;\">\n                <ion-row>\n                  <ion-col  style=\"position: relative;display: flex;flex-direction: column;\n                  justify-content: center;align-items: baseline;width: 151px;height: 177px;\">\n                    <ion-thumbnail *ngIf=\"item.posttype=='0'\" style=\"width: 151px;\n                    height: 160px;margin-left: 2%;\">\n                      <ion-img [src]='item.postUrl'></ion-img>\n                    </ion-thumbnail>\n                    <div *ngIf=\"item.posttype=='1'\">\n                      <video width=\"151\" height=\"160\" #player playsinline preload=\"auto\" controls autoplay>\n                        <source [src]=\"item.postUrl\" type=\"video/mp4\"/>\n                      </video>\n                    </div>\n                    <ion-row class=\"div4\">\n                      <ion-text class=\"text3\">{{item.postCategory}}</ion-text>\n                    </ion-row>\n                  </ion-col>\n                  <ion-col  style=\"width: 151px;height: 177px;display: flex;\n                  flex-direction: column;position: relative;\n                  justify-content: flex-start;\">\n                    <ion-row style=\"margin-left: 10%;\">\n                      <ion-text class=\"text2\">{{item.postTitle}}</ion-text>\n                    </ion-row>\n                    <ion-row class=\"row15\" style=\"margin-left: 10%;\">\n                      <ion-text class=\"text4\">{{item.postDescription}}</ion-text>\n                    </ion-row>\n                    <ion-row style=\"display: flex;\n                    position: absolute;\n                    bottom: 15px;\n                    justify-content: space-between;\n                    width: 100%;\">\n                    <ion-col style=\"display: flex;\n                    flex-direction: row;\n                    justify-content: center;\n                    align-items: center;\">\n                      <ion-icon style=\"font-size: 25px;\n                      color: black;\" *ngIf='!item.isliked' (click)='likePost(item,i)'\n                        name=\"heart-outline\"></ion-icon>\n                      <ion-icon style=\"color: #B92D2D;\n                      font-size: 25px;\" *ngIf='item.isliked' (click)='deleteLike(item,i)' class=\"icon3\" \n                        name=\"heart\"></ion-icon>\n                      <ion-text style=\"margin-left: 15%;\">{{item.likeCount}}</ion-text>\n                    </ion-col>\n                     <ion-col style=\"display: flex;\n                     flex-direction: row;\n                     justify-content: center;\n                     align-items: center;\">\n                      <img src=\"../../assets/images/home/eye.svg\">\n                        <ion-text style=\"margin-left: 15%;\">{{item.viewCount}}</ion-text>\n                    </ion-col>\n                    <ion-col style=\"display: flex;\n                    flex-direction: row;\n                    justify-content: center;\n                    align-items: center;\">\n                      <ion-icon style=\"font-size: 25px;\n                      color: black;\" *ngIf='!item.isBookmarked' (click)='bookmark(item,i)' \n                        class=\"icon1\" name=\"bookmark-outline\"></ion-icon>\n                        <ion-icon style=\"font-size: 25px;\" color='primary' *ngIf='item.isBookmarked' (click)='deleteBookmark(item,i)' \n                        class=\"icon1\" name=\"bookmark\"></ion-icon>\n                    </ion-col>     \n                    </ion-row>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-row>\n      \n      <div style=\"display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\" *ngIf='!professionalUser.isChat'>\n        <!-- <ion-row class=\"row10\">\n          <ion-text>Unlock Contact Info</ion-text>\n        </ion-row>\n    \n        <div class=\"div2\">\n          <ion-row class=\"row9\">\n            <div class=\"div1\">\n              <ion-icon name=\"call-outline\"></ion-icon>\n              <ion-text>+91-{{professionalUser.contactNum}}</ion-text>\n            </div>\n            <div class=\"div1\">\n              <ion-icon name=\"mail-outline\"></ion-icon>\n              <ion-text>{{professionalUser.emailId}}</ion-text>\n            </div>\n            <div class=\"div1\">\n              <ion-icon name=\"location-outline\"></ion-icon>\n              <ion-text>{{professionalUser.address}}</ion-text>\n            </div>\n          </ion-row>\n          <div class=\"row12\" (click)=\"presentModal()\">\n            <img class=\"img\" src=\"../../assets/images/profile/lock.svg\">\n          </div>\n        </div>\n         -->\n        <ion-row class=\"row11\">\n          <div class=\"button\" (click)=\"presentModal()\">\n            <ion-icon name=\"lock-closed-outline\"></ion-icon>\n            <ion-text>MESSAGE NOW</ion-text>\n          </div>\n        </ion-row>\n      </div>\n\n      <div style=\"display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\" *ngIf='professionalUser.isChat'>\n        <!-- <ion-row class=\"row10\">\n          <ion-text>Unlock Contact Info</ion-text>\n        </ion-row>\n    \n        <div class=\"div2\">\n          <ion-row class=\"row14\">\n            <div class=\"div1\">\n              <ion-icon name=\"call-outline\"></ion-icon>\n              <ion-text>+91-{{professionalUser.contactNum}}</ion-text>\n            </div>\n            <div class=\"div1\">\n              <ion-icon name=\"mail-outline\"></ion-icon>\n              <ion-text>{{professionalUser.emailId}}</ion-text>\n            </div>\n            <div class=\"div1\">\n              <ion-icon name=\"location-outline\"></ion-icon>\n              <ion-text>{{professionalUser.address}}</ion-text>\n            </div>\n          </ion-row>\n        </div> -->\n        \n        <ion-row class=\"row11\">\n          <div class=\"button\" (click)=\"gotochat()\">\n            <ion-text>MESSAGE NOW</ion-text>\n          </div>\n        </ion-row>\n      </div>\n\n  </div>\n\n  \n  \n  <ion-row style=\"padding-top: 15%;\">\n    <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n  </ion-row>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/profile1/profile1-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/profile1/profile1-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: Profile1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile1PageRoutingModule", function() { return Profile1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile1.page */ "./src/app/profile1/profile1.page.ts");




const routes = [
    {
        path: '',
        component: _profile1_page__WEBPACK_IMPORTED_MODULE_3__["Profile1Page"]
    }
];
let Profile1PageRoutingModule = class Profile1PageRoutingModule {
};
Profile1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Profile1PageRoutingModule);



/***/ }),

/***/ "./src/app/profile1/profile1.module.ts":
/*!*********************************************!*\
  !*** ./src/app/profile1/profile1.module.ts ***!
  \*********************************************/
/*! exports provided: Profile1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile1PageModule", function() { return Profile1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile1_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile1-routing.module */ "./src/app/profile1/profile1-routing.module.ts");
/* harmony import */ var _profile1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile1.page */ "./src/app/profile1/profile1.page.ts");







let Profile1PageModule = class Profile1PageModule {
};
Profile1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile1_routing_module__WEBPACK_IMPORTED_MODULE_5__["Profile1PageRoutingModule"]
        ],
        declarations: [_profile1_page__WEBPACK_IMPORTED_MODULE_6__["Profile1Page"]]
    })
], Profile1PageModule);



/***/ }),

/***/ "./src/app/profile1/profile1.page.scss":
/*!*********************************************!*\
  !*** ./src/app/profile1/profile1.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n.img {\n  width: 380px;\n  height: 300px;\n}\n.row {\n  padding-top: 5%;\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.row1 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 36px;\n  color: #003C69;\n  text-transform: capitalize;\n}\n.row2 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row2 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n}\n.row3 {\n  padding-top: 3%;\n  width: 100%;\n}\n.row3 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n.row3 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n  padding-left: 3%;\n}\n.icon {\n  color: #FFC107;\n  font-size: 18px;\n}\n.icon1 {\n  color: #000000;\n  font-size: 18px;\n}\n.row4 {\n  padding-top: 3%;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n.row4 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n  padding-left: 3%;\n}\n.row4 ion-icon {\n  font-size: 18px;\n}\n.row5 {\n  padding-top: 5%;\n  width: 90%;\n}\n.row5 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row6 {\n  width: 90%;\n  background: #FFFFFF;\n  padding: 5%;\n  margin-top: 3%;\n  border-radius: 10px;\n}\n.row6 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #999999;\n  text-align: justify;\n}\n.row7 {\n  padding-top: 3%;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n.row7 .img {\n  width: 115px;\n  height: 82px;\n}\n.row7 .img1 {\n  width: 115px;\n  height: 82px;\n  margin-left: 5%;\n}\n.row8 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row8 .container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 22px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n  margin: 0;\n  height: 145px;\n  width: 160px;\n}\n.row8 .centered {\n  position: absolute;\n  top: 85%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.row8 .imgSelected {\n  height: 145px;\n  width: 160px;\n}\n.div2 {\n  position: relative;\n  width: 323px;\n  height: 109px;\n}\n.row9 {\n  display: flex;\n  flex-direction: column;\n  box-sizing: border-box;\n  width: 323px;\n  height: 109px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  filter: blur(4px);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 1px solid #716a6a;\n  border-radius: 10px;\n  position: relative;\n}\n.row9 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row9 .div1 ion-icon {\n  padding-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row9 .div1 ion-text {\n  padding-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row9 .img {\n  width: 50%;\n}\n.row12 {\n  position: absolute;\n  top: 20px;\n  left: 130px;\n}\n.row12 .img {\n  width: 70px;\n  height: 70px;\n}\n.row10 {\n  padding-bottom: 5%;\n  padding-top: 5%;\n  width: 90%;\n}\n.row10 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row11 {\n  padding-top: 10%;\n  width: 90%;\n  justify-content: center;\n}\n.row11 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n.row11 .button {\n  width: 207px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n.row11 ion-icon {\n  color: #FFFFFF;\n  font-size: 24px;\n}\n.thumbnail {\n  width: 115px;\n  height: 82px;\n  margin-left: 5%;\n}\n.thumbnail1 {\n  width: 115px;\n  height: 82px;\n  padding-left: 3%;\n}\n.thumbnail2 {\n  height: 145px;\n  width: 160px;\n}\n.thumbnai3 {\n  width: 380px;\n  height: 300px;\n}\n.col {\n  width: 115px;\n  height: 82px;\n}\n.row13 {\n  display: flex;\n  flex-direction: column;\n  box-sizing: border-box;\n  width: 323px;\n  height: 109px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 1px solid #716a6a;\n  border-radius: 10px;\n  position: relative;\n}\n.row13 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row13 .div1 ion-icon {\n  padding-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row13 .div1 ion-text {\n  padding-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row13 .img {\n  width: 50%;\n}\n.div3 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.row14 {\n  display: flex;\n  flex-direction: column;\n  box-sizing: border-box;\n  width: 323px;\n  height: 109px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  filter: blur(4px);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 1px solid #716a6a;\n  border-radius: 10px;\n  position: relative;\n}\n.row14 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row14 .div1 ion-icon {\n  padding-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row14 .div1 ion-text {\n  padding-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row14 .img {\n  width: 50%;\n}\n.div4 {\n  position: absolute;\n  top: 13px;\n  left: 15px;\n  padding: 2%;\n  border-radius: 7px;\n  background: #00CBEE;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.text4 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #767474;\n}\n.row15 {\n  padding-top: 5%;\n  padding-right: 3%;\n}\n.text2 {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #003C69;\n}\n.text3 {\n  font-family: \"Poppins-Medium\";\n  font-size: 12px;\n  color: #FFFFFF;\n  text-transform: uppercase;\n}\n.image {\n  width: 25px;\n}\n.icon2 {\n  color: black;\n  font-size: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZTEvcHJvZmlsZTEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFDO0VBQ0csWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLCtEQUFBO0VBQ0EsZ0RBQUE7RUFDQSw0QkFBQTtBQUNKO0FBQUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRVI7QUFBSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBRVI7QUFBSTtFQUNFLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUVOO0FBQ0U7RUFDRSxZQUFBO0VBQ0EsYUFBQTtBQUVKO0FBQUU7RUFDSSxlQUFBO0FBR047QUFERTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFJTjtBQUZFO0VBQ0UsZUFBQTtFQUNBLFVBQUE7QUFLSjtBQUpJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0FBTVI7QUFIRTtFQUNFLGVBQUE7RUFDQSxVQUFBO0FBTUo7QUFMSTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFPUjtBQUpFO0VBQ0UsZUFBQTtFQUNBLFdBQUE7QUFPSjtBQU5JO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQVFSO0FBTkk7RUFDSSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUFRUjtBQUxFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFRSjtBQU5FO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFTSjtBQVBFO0VBQ0UsZUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBVUo7QUFUSTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQVdSO0FBVEk7RUFDSSxlQUFBO0FBV1I7QUFSRTtFQUNFLGVBQUE7RUFDQSxVQUFBO0FBV0o7QUFWSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFZUjtBQVRFO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQVlKO0FBWEk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFhUjtBQVZFO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBYUo7QUFaTTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FBY1I7QUFaTTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQWNSO0FBWEU7RUFDRSxlQUFBO0VBQ0EsVUFBQTtBQWNKO0FBYkk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQWVSO0FBYk07RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFlUjtBQWJNO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUFlUjtBQVpFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQWVKO0FBYkU7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQ0FBQTtFQUVBLGlCQUFBO0VBQ0EsK0NBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFnQko7QUFmSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFpQlI7QUFoQlE7RUFDRSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBa0JWO0FBaEJRO0VBQ0UsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBa0JWO0FBZk07RUFDRSxVQUFBO0FBaUJSO0FBZEU7RUFDRSxrQkFBQTtFQUdBLFNBQUE7RUFDQSxXQUFBO0FBZUo7QUFkSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBZ0JSO0FBYkU7RUFDRSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FBZ0JKO0FBZkk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBaUJSO0FBZEU7RUFDRSxnQkFBQTtFQUNBLFVBQUE7RUFDQSx1QkFBQTtBQWlCSjtBQWhCSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFrQlI7QUFoQkk7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDZEQUFBO0VBQ0EsZ0RBQUE7RUFDQSxtQkFBQTtBQWtCUjtBQWhCSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBa0JSO0FBZkU7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFrQko7QUFoQkE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBbUJKO0FBakJDO0VBQ0MsYUFBQTtFQUNBLFlBQUE7QUFvQkY7QUFsQkM7RUFDQyxZQUFBO0VBQ0EsYUFBQTtBQXFCRjtBQW5CQztFQUNDLFlBQUE7RUFDQSxZQUFBO0FBc0JGO0FBcEJFO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFDQSwrQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQXVCTjtBQXRCTTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUF3QlY7QUF2QlU7RUFDRSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBeUJaO0FBdkJVO0VBQ0UsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBeUJaO0FBdEJRO0VBQ0UsVUFBQTtBQXdCVjtBQXJCQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBd0JKO0FBckJBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7RUFFQSxpQkFBQTtFQUNBLCtDQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBd0JKO0FBdkJJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQXlCUjtBQXhCUTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUEwQlY7QUF4QlE7RUFDRSxnQkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUEwQlY7QUF2Qk07RUFDRSxVQUFBO0FBeUJSO0FBdEJBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQXlCRjtBQXZCQTtFQUNFLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUEwQkY7QUF4QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUEyQkY7QUF6QkE7RUFDRSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBNEJGO0FBMUJBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FBNkJGO0FBM0JBO0VBQ0UsV0FBQTtBQThCRjtBQTNCQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0FBOEJGIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZTEvcHJvZmlsZTEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC50b29sYmFyMSB7XG4gICAgaGVpZ2h0OiA3NHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjVweCAyNXB4O1xuICAgIGlvbi1jb2wge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG4gICAgaW9uLWljb24ge1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgfVxuICAgIGlvbi10ZXh0IHtcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgICBmb250LXdlaWdodDogNzAwO1xuICAgIH1cbiAgfVxuICAuaW1nIHtcbiAgICB3aWR0aDogMzgwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgfVxuICAucm93IHtcbiAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgfVxuICAuZGl2IHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5yb3cxIHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xuICAgICAgICBjb2xvcjogIzAwM0M2OTtcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgfVxuICB9XG4gIC5yb3cyIHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICB9XG4gIH1cbiAgLnJvdzMge1xuICAgIHBhZGRpbmctdG9wOiAzJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBpb24tY29sIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgICB9XG4gIH1cbiAgLmljb24ge1xuICAgIGNvbG9yOiAjRkZDMTA3O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAuaWNvbjEge1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAucm93NCB7XG4gICAgcGFkZGluZy10b3A6IDMlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLVJlZ3VsYXInO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDMlO1xuICAgIH1cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB9XG4gIH1cbiAgLnJvdzUge1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIH1cbiAgfVxuICAucm93NiB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIHBhZGRpbmc6IDUlO1xuICAgIG1hcmdpbi10b3A6IDMlO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICM5OTk5OTk7XG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgfVxuICB9XG4gIC5yb3c3IHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgLmltZyB7XG4gICAgICAgIHdpZHRoOiAxMTVweDtcbiAgICAgICAgaGVpZ2h0OiA4MnB4O1xuICAgICAgfVxuICAgICAgLmltZzEge1xuICAgICAgICB3aWR0aDogMTE1cHg7XG4gICAgICAgIGhlaWdodDogODJweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgICAgfVxuICB9XG4gIC5yb3c4IHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICAuY29udGFpbmVyIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMS41O1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgaGVpZ2h0OiAxNDVweDtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgfVxuICAgICAgLmNlbnRlcmVkIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDg1JTtcbiAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAgIH1cbiAgICAgIC5pbWdTZWxlY3RlZHtcbiAgICAgICAgaGVpZ2h0OiAxNDVweDtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgfVxuICB9XG4gIC5kaXYyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDMyM3B4O1xuICAgIGhlaWdodDogMTA5cHg7XG4gIH1cbiAgLnJvdzkge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHdpZHRoOiAzMjNweDtcbiAgICBoZWlnaHQ6IDEwOXB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbiAgICAtd2Via2l0LWZpbHRlcjogYmx1cig0cHgpO1xuICAgIGZpbHRlcjogYmx1cig0cHgpO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MTZhNmE7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLmRpdjEge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAyJTtcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xuICAgICAgICB9XG4gICAgICAgIGlvbi10ZXh0IHtcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgICAuaW1nIHtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgIH1cbiAgfVxuICAucm93MTIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAvLyB0b3A6IDEwNzBweDtcbiAgICAvLyBsZWZ0OiAxNTBweDtcbiAgICB0b3A6IDIwcHg7XG4gICAgbGVmdDogMTMwcHg7XG4gICAgLmltZyB7XG4gICAgICAgIHdpZHRoOiA3MHB4O1xuICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgfVxuICB9XG4gIC5yb3cxMCB7XG4gICAgcGFkZGluZy1ib3R0b206IDUlO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIH1cbiAgfVxuICAucm93MTEge1xuICAgIHBhZGRpbmctdG9wOiAxMCU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICB9XG4gICAgLmJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiAyMDdweDtcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG4gICAgaW9uLWljb24ge1xuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbiAgfVxuICAudGh1bWJuYWlsIHtcbiAgICB3aWR0aDogMTE1cHg7XG4gICAgaGVpZ2h0OiA4MnB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbn1cbi50aHVtYm5haWwxIHtcbiAgICB3aWR0aDogMTE1cHg7XG4gICAgaGVpZ2h0OiA4MnB4O1xuICAgIHBhZGRpbmctbGVmdDogMyU7XG59XG4gLnRodW1ibmFpbDIge1xuICBoZWlnaHQ6IDE0NXB4O1xuICB3aWR0aDogMTYwcHg7XG4gfVxuIC50aHVtYm5haTMge1xuICB3aWR0aDogMzgwcHg7XG4gIGhlaWdodDogMzAwcHg7XG4gfVxuIC5jb2wge1xuICB3aWR0aDogMTE1cHg7XG4gIGhlaWdodDogODJweDtcbiB9XG4gIC5yb3cxMyB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICB3aWR0aDogMzIzcHg7XG4gICAgICBoZWlnaHQ6IDEwOXB4O1xuICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG4gICAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MTZhNmE7XG4gICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLmRpdjEge1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgIHBhZGRpbmc6IDIlO1xuICAgICAgICAgIGhlaWdodDogMzVweDtcbiAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaW9uLXRleHQge1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgICAgLmltZyB7XG4gICAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICAgfVxuICB9XG4uZGl2MyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5yb3cxNCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgd2lkdGg6IDMyM3B4O1xuICAgIGhlaWdodDogMTA5cHg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xuICAgIC13ZWJraXQtZmlsdGVyOiBibHVyKDRweCk7XG4gICAgZmlsdGVyOiBibHVyKDRweCk7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcxNmE2YTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAuZGl2MSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDIlO1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLXRleHQge1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLVJlZ3VsYXInO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAgIC5pbWcge1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgfVxufVxuLmRpdjQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTNweDtcbiAgbGVmdDogMTVweDtcbiAgcGFkZGluZzogMiU7XG4gIGJvcmRlci1yYWRpdXM6IDdweDtcbiAgYmFja2dyb3VuZDogIzAwQ0JFRTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4udGV4dDQge1xuICBmb250LWZhbWlseTogJ1BvcHBpbnMtUmVndWxhcic7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3Njc0NzQ7XG59XG4ucm93MTUge1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xufVxuLnRleHQyIHtcbiAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGNvbG9yOiAjMDAzQzY5O1xufVxuLnRleHQzIHtcbiAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLU1lZGl1bSc7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaW1hZ2Uge1xuICB3aWR0aDogMjVweDtcbn1cblxuLmljb24yIHtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDI1cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/profile1/profile1.page.ts":
/*!*******************************************!*\
  !*** ./src/app/profile1/profile1.page.ts ***!
  \*******************************************/
/*! exports provided: Profile1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile1Page", function() { return Profile1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _unlock_contact_unlock_contact_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../unlock-contact/unlock-contact.page */ "./src/app/unlock-contact/unlock-contact.page.ts");
/* harmony import */ var _unlock_contact1_unlock_contact1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../unlock-contact1/unlock-contact1.page */ "./src/app/unlock-contact1/unlock-contact1.page.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");












let Profile1Page = class Profile1Page {
    constructor(router, route, modalController, storage, afa, fs, authService) {
        this.router = router;
        this.route = route;
        this.modalController = modalController;
        this.storage = storage;
        this.afa = afa;
        this.fs = fs;
        this.authService = authService;
        this.userPosts = [];
        this.posts = [];
        this.professionalUser = {};
        this.selectedUser = {};
        this.passionistUser = {};
        this.aboutUser = false;
        this.proffessional = false;
        this.passionist = false;
        this.professional = false;
        this.slideOpts = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: 0,
            slideShow: true,
            autoplay: true
        };
        this.slideOpts1 = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: 0,
            slideShow: true,
            autoplay: true
        };
        console.log('heyyyyyyyy');
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                this.pageroute = params.pageroute;
            }
            else if (params) {
                this.professional = params.professional;
                this.pageroute = params.pageroute;
                console.log(this.pageroute);
                this.host_id = params.user_id;
                console.log(this.host_id + "hostid");
                this.selectedUser = JSON.parse(params.professionalUser);
            }
        });
    }
    getProfile() {
        var obj = { "hostId": this.host_id, "userId": this.userId };
        this.authService.getUserProfile(obj).subscribe((data) => {
            console.log(data);
            this.professionalUser = data.data;
            console.log(this.professionalUser);
            this.docUrl1 = this.professionalUser.documents[0].docUrl;
            this.docUrl2 = this.professionalUser.documents[1].docUrl;
            console.log(this.docUrl1);
            console.log(this.docUrl2);
        });
    }
    getUserPost() {
        this.userPosts = [];
        var obj = { 'userId': this.host_id };
        this.authService.getBookmark(obj).subscribe((data) => {
            console.log(data);
            this.posts = data.postArray;
            this.posts.forEach(element => {
                if (element.posttype == '0' || element.posttype == '1') {
                    this.userPosts.push(element);
                    console.log(this.userPosts);
                }
            });
        });
    }
    ngOnInit() {
        this.storage.get('user').then((user) => {
            this.userId = user._id;
            this.userName = user.fullName;
            this.userImage = user.user_img;
            this.walletBalance = user.wallet;
            if (user.userType == this.selectedUser.userType) {
                this.professionalUser = this.selectedUser;
                this.professionalUser.isChat = true;
                this.docUrl1 = this.professionalUser.documents[0].docUrl;
                this.docUrl2 = this.professionalUser.documents[1].docUrl;
                this.getUserPost();
            }
            else {
                this.getProfile();
                this.getUserPost();
            }
        });
    }
    presentModal() {
        if (this.walletBalance > 5) {
            this.presentUnlock();
        }
        else {
            this.subscription();
        }
    }
    gotochat() {
        this.fs.collection('friends').doc(this.userId).collection('chats').doc(this.professionalUser._id).set({
            recieverName: this.professionalUser.fullName,
            recieverId: this.professionalUser._id,
            recieverImage: this.professionalUser.user_img,
            senderId: this.userId,
            senderName: this.userName,
            senderImage: this.userImage,
            Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
        }, { merge: true });
        this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
            senderName: this.professionalUser.fullName,
            senderId: this.professionalUser._id,
            senderImage: this.professionalUser.user_img,
            recieverId: this.userId,
            recieverName: this.userName,
            recieverImage: this.userImage,
            Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
        }, { merge: true });
        let vc = { recieverId: this.host_id,
            senderId: this.userId,
            recieverName: this.professionalUser.fullName,
            recieverImg: this.professionalUser.user_img };
        let navigationExtras = {
            queryParams: {
                special: JSON.stringify(vc)
            }
        };
        this.router.navigate(['chat'], navigationExtras);
    }
    presentUnlock() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('helloooooooo');
            const modal = yield this.modalController.create({
                component: _unlock_contact_unlock_contact_page__WEBPACK_IMPORTED_MODULE_4__["UnlockContactPage"],
                cssClass: 'unlockContactModal',
                componentProps: { 'professionalUser': this.professionalUser }
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.fs.collection('friends').doc(this.userId).collection('chats').doc(this.professionalUser._id).set({
                    recieverName: this.professionalUser.fullName,
                    recieverId: this.professionalUser._id,
                    recieverImage: this.professionalUser.user_img,
                    senderId: this.userId,
                    senderName: this.userName,
                    senderImage: this.userImage,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
                    senderName: this.professionalUser.fullName,
                    senderId: this.professionalUser._id,
                    senderImage: this.professionalUser.user_img,
                    recieverId: this.userId,
                    recieverName: this.userName,
                    recieverImage: this.userImage,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                let navigationExtras = {
                    queryParams: {
                        special: JSON.stringify({ url: this.router.url, recieverId: this.professionalUser._id,
                            senderId: this.userId,
                            recieverName: this.professionalUser.fullName,
                            senderName: this.userName,
                            recieverImg: this.professionalUser.user_img })
                    }
                };
                this.router.navigate(['chat'], navigationExtras);
            }
        });
    }
    subscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('heyyyyyyyyy');
            const modal = yield this.modalController.create({
                component: _unlock_contact1_unlock_contact1_page__WEBPACK_IMPORTED_MODULE_5__["UnlockContact1Page"],
                cssClass: 'unlockContact1Modal',
                componentProps: { 'professionalUser': this.professionalUser }
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.fs.collection('friends').doc(this.userId).collection('chats').doc(this.professionalUser._id).set({
                    recieverName: this.professionalUser.fullName,
                    recieverId: this.professionalUser._id,
                    recieverImage: this.professionalUser.user_img,
                    senderId: this.userId,
                    senderName: this.userName,
                    senderImage: this.userImage,
                    isBlocked: false,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                this.fs.collection('friends').doc(this.professionalUser._id).collection('chats').doc(this.userId).set({
                    senderName: this.professionalUser.fullName,
                    senderId: this.professionalUser._id,
                    senderImage: this.professionalUser.user_img,
                    recieverId: this.userId,
                    recieverName: this.userName,
                    recieverImage: this.userImage,
                    isBlocked: false,
                    Timestamp: firebase__WEBPACK_IMPORTED_MODULE_9__["firestore"].FieldValue.serverTimestamp()
                }, { merge: true });
                let navigationExtras = {
                    queryParams: {
                        special: JSON.stringify({ url: this.router.url, recieverId: this.professionalUser._id,
                            senderId: this.userId,
                            recieverName: this.professionalUser.fullName,
                            senderName: this.userName,
                            recieverImg: this.professionalUser.user_img })
                    }
                };
                this.router.navigate(['chat'], navigationExtras);
            }
        });
    }
    back() {
        this.router.navigateByUrl(this.pageroute);
    }
    notification() {
        let navigationExtras = {
            queryParams: {
                special: this.router.url
            }
        };
        this.router.navigate(['notification'], navigationExtras);
    }
};
Profile1Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestore"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"] }
];
Profile1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile1/profile1.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile1.page.scss */ "./src/app/profile1/profile1.page.scss")).default]
    })
], Profile1Page);



/***/ })

}]);
//# sourceMappingURL=profile1-profile1-module-es2015.js.map