(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["unlock-contact-unlock-contact-module"],{

/***/ "./src/app/unlock-contact/unlock-contact-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/unlock-contact/unlock-contact-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: UnlockContactPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnlockContactPageRoutingModule", function() { return UnlockContactPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _unlock_contact_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unlock-contact.page */ "./src/app/unlock-contact/unlock-contact.page.ts");




const routes = [
    {
        path: '',
        component: _unlock_contact_page__WEBPACK_IMPORTED_MODULE_3__["UnlockContactPage"]
    }
];
let UnlockContactPageRoutingModule = class UnlockContactPageRoutingModule {
};
UnlockContactPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UnlockContactPageRoutingModule);



/***/ }),

/***/ "./src/app/unlock-contact/unlock-contact.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/unlock-contact/unlock-contact.module.ts ***!
  \*********************************************************/
/*! exports provided: UnlockContactPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnlockContactPageModule", function() { return UnlockContactPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _unlock_contact_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./unlock-contact-routing.module */ "./src/app/unlock-contact/unlock-contact-routing.module.ts");
/* harmony import */ var _unlock_contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./unlock-contact.page */ "./src/app/unlock-contact/unlock-contact.page.ts");







let UnlockContactPageModule = class UnlockContactPageModule {
};
UnlockContactPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _unlock_contact_routing_module__WEBPACK_IMPORTED_MODULE_5__["UnlockContactPageRoutingModule"]
        ],
        declarations: [_unlock_contact_page__WEBPACK_IMPORTED_MODULE_6__["UnlockContactPage"]]
    })
], UnlockContactPageModule);



/***/ })

}]);
//# sourceMappingURL=unlock-contact-unlock-contact-module-es2015.js.map