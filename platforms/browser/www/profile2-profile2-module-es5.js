(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile2-profile2-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/profile2/profile2.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile2/profile2.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppProfile2Profile2PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"toolbar1\">\n  <ion-row>\n    <ion-col [size]=2 (click)=\"back()\">\n      <ion-icon name=\"chevron-back\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8>\n      <ion-text>Profile</ion-text>\n    </ion-col>\n    <ion-col [size]=2 (click)=\"notification()\">\n      <ion-icon name=\"notifications-outline\"></ion-icon>\n    </ion-col>    \n  </ion-row>   \n</ion-toolbar>\n\n<ion-content>\n\n  <div class=\"div\">\n    <ion-row class=\"row\">\n      <ion-avatar>\n        <ion-img [src]=\"userData.user_img\"></ion-img>\n      </ion-avatar>\n      <div class=\"centered\" (click)=\"presentActionSheet1(1)\">\n        <img class=\"img\" src=\"../../assets/images/home/edit-2.svg\">\n      </div>\n      <div style=\"width: 100%;\n      display: flex;\n      flex-direction: row;\n      justify-content: center;\n      text-transform: capitalize;\n      position: absolute;\n      top: 160px;\">\n        <ion-text>{{userData.firstName}} {{userData.lastName}}</ion-text>\n      </div>\n    </ion-row>\n    <ion-row class=\"row1\">\n      <ion-text>About</ion-text>\n      <!-- <img class=\"img\" src=\"../../assets/images/home/edit-2.svg\"> -->\n    </ion-row>\n    <!-- <ion-row class=\"row6\" *ngIf=\"!aboutUser\">\n      <ion-textarea [(ngModel)]=\"about\" placeholder='Type here...'></ion-textarea>\n    </ion-row> -->\n    <ion-row class=\"row6\" >\n      <ion-textarea *ngIf='userData.about!=null || userData.about!=undefined'\n       [(ngModel)]=\"userData.about\" ></ion-textarea>\n       <ion-textarea (ionChange)=\"updateAboutUser()\" *ngIf='userData.about==null || userData.about==undefined'\n       [(ngModel)]=\"about\" placeholder='Type here...'></ion-textarea>\n    </ion-row>\n    <ion-row class=\"row5\">\n      <ion-text>Media</ion-text>\n    </ion-row>\n    <div class=\"div4\">\n      <div class=\"div6\">\n        <ion-row class=\"row11\">\n          <ion-col [size]=5.5 class=\"col5\">\n             <div class=\"centered3\">\n              <ion-thumbnail style=\"    display: flex;\n              flex-direction: row;\n              justify-content: center;\n              align-items: center;\" class=\"thumbnail1\" >\n               <ion-img *ngIf=\"doc1!=undefined && doc1!=''\" class=\"img1\" [src]=\"doc1\"></ion-img>\n               <ion-icon (click)=\"updateDocuments()\" *ngIf=\"doc1==undefined||doc1==''||doc1==null\" class=\"icon1\" name=\"add-outline\"></ion-icon>\n              </ion-thumbnail>          \n             </div>\n          </ion-col>\n          <ion-col [size]=5.5 class=\"col5\">\n               <div class=\"centered3\">\n                <ion-thumbnail style=\"    display: flex;\n                flex-direction: row;\n                justify-content: center;\n                align-items: center;\" class=\"thumbnail1\"  >\n                  <ion-img  *ngIf=\"doc2!=undefined && doc1!=''\" class=\"img1\" [src]=\"doc2\"></ion-img>\n                  <ion-icon (click)=\"updateDocuments()\" *ngIf=\"doc2==undefined||doc1==''||doc1==null\" class=\"icon1\" name=\"add-outline\"></ion-icon>\n                </ion-thumbnail>\n               </div>\n          </ion-col>\n        </ion-row>\n      </div>\n    </div>\n    <ion-row class=\"row2\">\n      <ion-text>Interests</ion-text>\n      <img class=\"img\" src=\"../../assets/images/home/edit-2.svg\" (click)=\"editInterest()\">\n    </ion-row>\n    <ion-row class=\"row8\">\n      <ion-slides [options]=\"slideOpts\">\n        <ion-slide *ngFor=\"let item of userData.userHobby\">\n          <ion-card class=\"container\">\n            <ion-thumbnail class=\"thumbnail\">\n              <ion-img class=\"imgSelected\" [src]=\"item.hobbyImage\"></ion-img>\n            </ion-thumbnail>           \n            <div class=\"centered\">\n              <ion-text>{{item.hobby}}</ion-text>\n            </div>\n          </ion-card>\n        </ion-slide>        \n      </ion-slides>\n    </ion-row>\n\n    <div>\n      <!-- <ion-row class=\"row2\">\n        <ion-text>Contact Info</ion-text>\n        <img class=\"img\" (click)=\"updateAdd()\" src=\"../../assets/images/home/edit-2.svg\">\n      </ion-row> -->\n        <ion-card class=\"row9\">\n          <div class=\"div1\">\n            <ion-icon name=\"call-outline\"></ion-icon>\n            <ion-text>+91-{{userData.contactNum}}</ion-text>\n          </div>\n          <div class=\"div1\">\n            <ion-icon name=\"mail-outline\"></ion-icon>\n            <ion-text>{{userData.emailId}}</ion-text>\n          </div>\n          <div class=\"div1\">\n            <ion-icon name=\"location-outline\"></ion-icon>\n            <ion-text>{{userData.address}}</ion-text>\n          </div>\n        </ion-card>\n    </div>\n    \n\n        <!-- <div class=\"div8\" *ngIf='updateAddress' (click)=\"update()\">\n          <div class=\"div9\" id=\"map\"></div>\n          <ion-row class=\"row4\" (click)=\"buy()\">\n            <div class=\"col\">\n              <ion-text>UPDATE</ion-text>\n            </div>\n          </ion-row>\n        </div> -->\n\n\n\n      <ion-row style=\"padding-top: 15%;\">\n        <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n      </ion-row>\n  </div>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/profile2/profile2-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/profile2/profile2-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: Profile2PageRoutingModule */

    /***/
    function srcAppProfile2Profile2RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Profile2PageRoutingModule", function () {
        return Profile2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _profile2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./profile2.page */
      "./src/app/profile2/profile2.page.ts");

      var routes = [{
        path: '',
        component: _profile2_page__WEBPACK_IMPORTED_MODULE_3__["Profile2Page"]
      }];

      var Profile2PageRoutingModule = function Profile2PageRoutingModule() {
        _classCallCheck(this, Profile2PageRoutingModule);
      };

      Profile2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Profile2PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/profile2/profile2.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/profile2/profile2.module.ts ***!
      \*********************************************/

    /*! exports provided: Profile2PageModule */

    /***/
    function srcAppProfile2Profile2ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Profile2PageModule", function () {
        return Profile2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _profile2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./profile2-routing.module */
      "./src/app/profile2/profile2-routing.module.ts");
      /* harmony import */


      var _profile2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./profile2.page */
      "./src/app/profile2/profile2.page.ts");

      var Profile2PageModule = function Profile2PageModule() {
        _classCallCheck(this, Profile2PageModule);
      };

      Profile2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _profile2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Profile2PageRoutingModule"]],
        declarations: [_profile2_page__WEBPACK_IMPORTED_MODULE_6__["Profile2Page"]]
      })], Profile2PageModule);
      /***/
    },

    /***/
    "./src/app/profile2/profile2.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/profile2/profile2.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppProfile2Profile2PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.row {\n  width: 90%;\n  padding-top: 5%;\n  justify-content: center;\n  position: relative;\n}\n.row ion-avatar {\n  height: 125px;\n  width: 125px;\n}\n.row .centered {\n  position: absolute;\n  top: 115px;\n  left: 235px;\n}\n.row .centered1 {\n  position: absolute;\n  top: 160px;\n  left: 175px;\n}\n.row ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #000000;\n}\n.row5 {\n  padding-top: 5%;\n  width: 90%;\n}\n.row5 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row1 {\n  padding-top: 15%;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row2 {\n  padding-top: 5%;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.row2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row6 {\n  height: 180px;\n  width: 90%;\n  background: #FFFFFF;\n  padding: 5%;\n  margin-top: 3%;\n  border-radius: 5px;\n  border: 1px solid lightgrey;\n}\n.row6 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #999999;\n  text-align: justify;\n}\n.div6 {\n  box-sizing: border-box;\n  width: 370px;\n  height: 140px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  overflow: visible;\n  background-color: #ffffff;\n}\n.div4 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 1%;\n}\n.row11 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n  padding-left: 2%;\n  padding-right: 2%;\n}\n.col5 {\n  position: relative;\n  text-align: center;\n  background: #F6F7F9;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  height: 100px;\n  border-radius: 10px;\n}\n.centered1 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.icon1 {\n  color: #79797A;\n  font-size: 50px;\n}\n.row8 {\n  padding-top: 3%;\n  width: 90%;\n}\n.row8 .container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 22px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n  margin: 0;\n  height: 145px;\n  width: 160px;\n}\n.row8 .centered {\n  position: absolute;\n  top: 85%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.row8 .imgSelected {\n  height: 145px;\n  width: 160px;\n}\n.thumbnail {\n  height: 145px;\n  width: 160px;\n}\n.thumbnail1 {\n  width: 155px;\n  height: 95px;\n  border-radius: 10px;\n}\n.row9 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  box-sizing: border-box;\n  width: 336px;\n  height: 126px;\n  overflow: visible;\n  background-color: rgba(255, 255, 255, 0.5);\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  position: relative;\n  margin-top: 5%;\n}\n.row9 .div1 {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 2%;\n  height: 35px;\n  padding-bottom: 5%;\n  padding-top: 5%;\n}\n.row9 .div1 ion-icon {\n  margin-left: 5%;\n  font-size: 20px;\n  color: #767474;\n}\n.row9 .div1 ion-text {\n  margin-left: 5%;\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n.row9 .img {\n  width: 50%;\n}\n.centered3 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.centered4 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.img1 {\n  width: 155px;\n  height: 95px;\n  border-radius: 10px;\n}\n.div8 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.div9 {\n  height: 100px;\n  width: 85%;\n  position: relative;\n  overflow: hidden;\n}\n.row4 {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n.row4 .col {\n  width: 160px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n.row4 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZTIvcHJvZmlsZTIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLCtEQUFBO0VBQ0EsZ0RBQUE7RUFDQSw0QkFBQTtBQUNKO0FBQUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRVI7QUFBSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBRVI7QUFBSTtFQUNFLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUVOO0FBQ0U7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRUo7QUFBQTtFQUNJLFVBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQUdKO0FBRkk7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQUlSO0FBRkk7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBSU47QUFGTTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNKLFdBQUE7QUFJSjtBQUZNO0VBQ0UsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUlSO0FBREE7RUFDSSxlQUFBO0VBQ0EsVUFBQTtBQUlKO0FBSEk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBS1I7QUFGRTtFQUNNLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFLUjtBQUpRO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQU1aO0FBSEU7RUFDTSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFNUjtBQUxRO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQU9aO0FBSkU7RUFDRSxhQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0FBT0o7QUFOSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQVFSO0FBTEU7RUFDRSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUFRSjtBQU5BO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBU0o7QUFQRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQVVKO0FBUkE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQVdKO0FBVEU7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFZSjtBQVZFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFhSjtBQVhFO0VBQ0UsZUFBQTtFQUNBLFVBQUE7QUFjSjtBQWJJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUFlUjtBQWJNO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBZVI7QUFiTTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBZVI7QUFaRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBZUo7QUFiRTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFnQko7QUFkRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLDBDQUFBO0VBQ0EsK0NBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQWlCSjtBQWhCSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFrQlI7QUFqQlE7RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFtQlY7QUFqQlE7RUFDRSxlQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQW1CVjtBQWhCTTtFQUNFLFVBQUE7QUFrQlI7QUFmRTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQWtCSjtBQWhCRTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQW1CSjtBQWpCRTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFvQko7QUFsQkU7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFxQko7QUFuQkU7RUFDRSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFzQko7QUFsQkU7RUFDRSxVQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBcUJKO0FBcEJJO0VBQ0UsWUFBQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSw2REFBQTtFQUNBLGdEQUFBO0VBQ0EsbUJBQUE7QUFzQlI7QUFwQkk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBc0JSIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZTIvcHJvZmlsZTIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvb2xiYXIxIHtcbiAgICBoZWlnaHQ6IDc0cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAyNXB4IDI1cHg7XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICB9XG4gICAgaW9uLXRleHQge1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgfVxuICB9XG4gIC5kaXYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnJvdyB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGlvbi1hdmF0YXIge1xuICAgICAgICBoZWlnaHQ6IDEyNXB4O1xuICAgICAgICB3aWR0aDogMTI1cHg7XG4gICAgfVxuICAgIC5jZW50ZXJlZCB7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDExNXB4O1xuICAgICAgbGVmdDogMjM1cHg7XG4gICAgICB9XG4gICAgICAuY2VudGVyZWQxIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDE2MHB4O1xuICAgIGxlZnQ6IDE3NXB4O1xuICAgICAgfVxuICAgICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtUmVndWxhcic7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgICB9XG59XG4ucm93NSB7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgfVxuICB9XG4gIC5yb3cxIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDE1JTtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgfVxuICB9XG4gIC5yb3cyIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGlvbi10ZXh0IHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgICB9XG4gIH1cbiAgLnJvdzYge1xuICAgIGhlaWdodDogMTgwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIHBhZGRpbmc6IDUlO1xuICAgIG1hcmdpbi10b3A6IDMlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICM5OTk5OTk7XG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgfVxuICB9XG4gIC5kaXY2IHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHdpZHRoOiAzNzBweDtcbiAgICBoZWlnaHQ6IDE0MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG4uZGl2NCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDElO1xuICB9XG4gIC5yb3cxMSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbn1cbi5jb2w1IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6ICNGNkY3Rjk7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgfVxuICAuY2VudGVyZWQxIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB9XG4gIC5pY29uMSB7XG4gICAgY29sb3I6ICM3OTc5N0E7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICB9XG4gIC5yb3c4IHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICAuY29udGFpbmVyIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMS41O1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgaGVpZ2h0OiAxNDVweDtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgfVxuICAgICAgLmNlbnRlcmVkIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDg1JTtcbiAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAgIH1cbiAgICAgIC5pbWdTZWxlY3RlZHtcbiAgICAgICAgaGVpZ2h0OiAxNDVweDtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgfVxuICB9XG4gIC50aHVtYm5haWwge1xuICAgIGhlaWdodDogMTQ1cHg7XG4gICAgd2lkdGg6IDE2MHB4O1xuICB9XG4gIC50aHVtYm5haWwxIHtcbiAgICB3aWR0aDogMTU1cHg7XG4gICAgaGVpZ2h0OiA5NXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIH1cbiAgLnJvdzkge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHdpZHRoOiAzMzZweDtcbiAgICBoZWlnaHQ6IDEyNnB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICAuZGl2MSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDIlO1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgfVxuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgICAuaW1nIHtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgIH1cbiAgfVxuICAuY2VudGVyZWQzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB9XG4gIC5jZW50ZXJlZDQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDUwJTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIH1cbiAgLmltZzEge1xuICAgIHdpZHRoOiAxNTVweDtcbiAgICBoZWlnaHQ6IDk1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgfVxuICAuZGl2OCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICB9XG4gIC5kaXY5IHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHdpZHRoOiA4NSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cblxuXG4gIC5yb3c0IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAuY29sIHtcbiAgICAgIHdpZHRoOiAxNjBweDtcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XG4gICAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/profile2/profile2.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/profile2/profile2.page.ts ***!
      \*******************************************/

    /*! exports provided: Profile2Page */

    /***/
    function srcAppProfile2Profile2PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Profile2Page", function () {
        return Profile2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/file-path/ngx */
      "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/fire/storage */
      "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
      /* harmony import */


      var firebase_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! firebase/storage */
      "./node_modules/firebase/storage/dist/index.esm.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");

      var Profile2Page = /*#__PURE__*/function () {
        function Profile2Page(router, route, storage, authService, platform, modalController, angularstorage, file, filePath, httpClient, actionSheetController, geolocation, camera) {
          var _this = this;

          _classCallCheck(this, Profile2Page);

          this.router = router;
          this.route = route;
          this.storage = storage;
          this.authService = authService;
          this.platform = platform;
          this.modalController = modalController;
          this.angularstorage = angularstorage;
          this.file = file;
          this.filePath = filePath;
          this.httpClient = httpClient;
          this.actionSheetController = actionSheetController;
          this.geolocation = geolocation;
          this.camera = camera;
          this.userData = {};
          this.aboutUser = false;
          this.updateAddress = false;
          this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          };
          this.gelleryOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL
          };
          this.slideOpts = {
            slidesPerView: 2,
            spaceBetween: 10,
            initialSlide: 0
          };
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              _this.pageRoute = params.special;

              _this.loadData();
            }
          });
        }

        _createClass(Profile2Page, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log(this.about);
            this.loadData();
          }
        }, {
          key: "loadData",
          value: function loadData() {
            var _this2 = this;

            this.storage.get('user').then(function (user) {
              var obj = {
                "userId": user._id
              };

              _this2.authService.getUserProfileById(obj).subscribe(function (data) {
                console.log(data);
                _this2.userData = data.data;
                _this2.doc1 = _this2.userData.documents[0].docUrl;
                _this2.doc2 = _this2.userData.documents[1].docUrl;
              });
            });
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.getUserProfileById();
          }
        }, {
          key: "notification",
          value: function notification() {
            var navigationExtras = {
              queryParams: {
                special: this.router.url
              }
            };
            this.router.navigate(['notification'], navigationExtras);
          }
        }, {
          key: "updateAboutUser",
          value: function updateAboutUser() {
            var _this3 = this;

            var obj = {
              "key": "about",
              "value": this.about,
              "userId": this.userData._id
            };
            this.authService.updateProfile(obj).subscribe(function (data) {
              console.log(data);

              _this3.getUserProfileById();
            });
          }
        }, {
          key: "presentActionSheet1",
          value: function presentActionSheet1(i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this4 = this;

              var actionSheet;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.actionSheetController.create({
                        header: "Select Image source",
                        buttons: [{
                          text: 'Load from Library',
                          handler: function handler() {
                            _this4.takePicture1(_this4.camera.PictureSourceType.PHOTOLIBRARY, i);
                          }
                        }, {
                          text: 'Use Camera',
                          handler: function handler() {
                            _this4.takePicture1(_this4.camera.PictureSourceType.CAMERA, i);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      actionSheet = _context.sent;
                      _context.next = 5;
                      return actionSheet.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "takePicture1",
          value: function takePicture1(sourceType, i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this5 = this;

              var options, tempImage, tempFilename, tempBaseFilesystemPath, newBaseFilesystemPath, storedPhoto, _options;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!this.platform.is('ios')) {
                        _context2.next = 14;
                        break;
                      }

                      options = {
                        quality: 100,
                        targetWidth: 900,
                        targetHeight: 600,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      _context2.next = 4;
                      return this.camera.getPicture(options);

                    case 4:
                      tempImage = _context2.sent;
                      tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1); // Now, the opposite. Extract the full path, minus filename.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/

                      tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1); // Get the Data directory on the device.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/

                      newBaseFilesystemPath = this.file.dataDirectory;
                      _context2.next = 10;
                      return this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);

                    case 10:
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                      storedPhoto = newBaseFilesystemPath + tempFilename;
                      this.file.resolveLocalFilesystemUrl(storedPhoto).then(function (entry) {
                        entry.file(function (file) {
                          return _this5.readFile(file, i);
                        });
                      })["catch"](function (err) {
                        console.log(err); // this.presentToast('Error while reading file.');
                      });
                      _context2.next = 16;
                      break;

                    case 14:
                      _options = {
                        quality: 100,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      this.camera.getPicture(_options).then(function (imageData) {
                        // this._file.resolveLocalFilesystemUrl(
                        //   imageData,
                        //   (entry: FileEntry) => {console.log(entry)},
                        //   err => console.log(err)
                        // );
                        _this5.file.resolveLocalFilesystemUrl(imageData).then(function (entry) {
                          entry.file(function (file) {
                            console.log(file);

                            _this5.readFile(file, i);
                          });
                        });
                      }, function (err) {// Handle error
                      });

                    case 16:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "readFile",
          value: function readFile(file, i) {
            var _this6 = this;

            var reader = new FileReader();

            reader.onload = function () {
              // const formData = new FormData();
              var imgBlob = new Blob([reader.result], {
                type: file.type
              }); // formData.append('file', imgBlob, file.name);
              // this.uploadImageData(formData);

              if (i === 1) {
                _this6.upload2Firebase1(imgBlob);
              } else if (i === 2) {
                _this6.upload2Firebase2(imgBlob);
              } else if (i === 3) {
                _this6.upload2Firebase3(imgBlob);
              } else {
                console.log("if second image");
              }
            };

            reader.readAsArrayBuffer(file);
          }
        }, {
          key: "makeid",
          value: function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;

            for (var i = 0; i < length; i++) {
              result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            return result;
          }
        }, {
          key: "upload2Firebase1",
          value: function upload2Firebase1(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this7 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.authService.loading('Loading Profile..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context3.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this7.imgUrl1 = value;
                          console.log(_this7.imgUrl1);
                          _this7.uploaded1 = true;
                          var obj = {
                            "key": "user_img",
                            "value": _this7.imgUrl1,
                            "userId": _this7.userData._id
                          };

                          _this7.authService.updateProfile(obj).subscribe(function (data) {
                            console.log(data);

                            if (data.success) {
                              _this7.authService.dismissLoading();
                            }
                          });
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "upload2Firebase2",
          value: function upload2Firebase2(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this8 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      this.authService.loading('Loading Profile..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context4.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this8.doc1 = value;
                          console.log(_this8.doc1);
                          _this8.uploaded2 = true;
                          var obj = {
                            "key": "documents['doc1']",
                            "value": _this8.doc1,
                            "userId": _this8.userData._id
                          };

                          _this8.authService.updateProfile(obj).subscribe(function (data) {
                            console.log(data);

                            if (data.success) {
                              _this8.authService.dismissLoading();
                            }
                          });
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "upload2Firebase3",
          value: function upload2Firebase3(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this9 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      this.authService.loading('Loading Profile..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context5.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this9.doc2 = value;
                          console.log(_this9.doc2);
                          _this9.uploaded3 = true;
                          var obj = {
                            "key": "documents['doc2']",
                            "value": _this9.doc2,
                            "userId": _this9.userData._id
                          };

                          _this9.authService.updateProfile(obj).subscribe(function (data) {
                            console.log(data);

                            if (data.success) {
                              _this9.authService.dismissLoading();
                            }
                          });
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "testmap",
          value: function testmap() {
            var myLatlng = new google.maps.LatLng(this.latitude, this.longitude);
            console.log(myLatlng);
            var mapOptions = {
              zoom: 12,
              center: myLatlng,
              mapTypeControl: false,
              scaleControl: false,
              zoomControl: false,
              streetViewControl: false,
              rotateControl: false,
              fullscreenControl: false,
              styles: [{
                stylers: [{
                  saturation: -100
                }]
              }]
            };
            this.map = new google.maps.Map(document.getElementById("map"), mapOptions); //Add User Location Marker To Map

            var marker = new google.maps.Marker({
              position: myLatlng,
              draggable: true
            }); // To add the marker to the map, call setMap();

            marker.setMap(this.map);
            marker.addListener('dragend', function (event) {
              console.log(event);
              this.latitude = event.latLng.lat();
              console.log(this.latitude);
              this.longitude = event.latLng.lng();
              console.log(this.longitude);
            }); //Map Click Event Listner

            this.map.addListener('click', function () {//add functions here
            });
          }
        }, {
          key: "updateAdd",
          value: function updateAdd() {
            this.updateAddress = true;
          }
        }, {
          key: "update",
          value: function update() {
            var _this10 = this;

            this.geolocation.getCurrentPosition().then(function (resp) {
              // resp.coords.latitude
              // this.latitude= resp.coords.latitude;
              // console.log(this.latitude);
              // resp.coords.longitude
              // this.longitude=resp.coords.longitude;
              // console.log(this.longitude);
              _this10.latitude = 80;
              _this10.longitude = 90;

              _this10.testmap();

              var obj = {
                "latitude": _this10.latitude,
                "longitude": _this10.longitude
              };

              _this10.authService.updateAddress(obj).subscribe(function (data) {
                console.log(data);

                _this10.getUserProfileById();

                _this10.updateAddress = false;
              });
            });
          }
        }, {
          key: "editInterest",
          value: function editInterest() {
            var navigationExtras = {
              queryParams: {
                isUpdate: true,
                pageroute: this.router.url
              }
            };
            this.router.navigate(['hobby'], navigationExtras);
          }
        }, {
          key: "getUserProfileById",
          value: function getUserProfileById() {
            var obj = {
              "userId": this.userData._id
            };
            this.authService.getUserProfileById(obj).subscribe(function (data) {
              console.log(data);
            });
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl(this.pageRoute);
          }
        }]);

        return Profile2Page;
      }();

      Profile2Page.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["ModalController"]
        }, {
          type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]
        }, {
          type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__["File"]
        }, {
          type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["ActionSheetController"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__["Geolocation"]
        }, {
          type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"]
        }];
      };

      Profile2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./profile2.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/profile2/profile2.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./profile2.page.scss */
        "./src/app/profile2/profile2.page.scss"))["default"]]
      })], Profile2Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=profile2-profile2-module-es5.js.map