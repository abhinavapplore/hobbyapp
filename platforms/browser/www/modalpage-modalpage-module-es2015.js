(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modalpage-modalpage-module"],{

/***/ "./src/app/modalpage/modalpage-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/modalpage/modalpage-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: ModalpagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalpagePageRoutingModule", function() { return ModalpagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _modalpage_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modalpage.page */ "./src/app/modalpage/modalpage.page.ts");




const routes = [
    {
        path: '',
        component: _modalpage_page__WEBPACK_IMPORTED_MODULE_3__["ModalpagePage"]
    }
];
let ModalpagePageRoutingModule = class ModalpagePageRoutingModule {
};
ModalpagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ModalpagePageRoutingModule);



/***/ }),

/***/ "./src/app/modalpage/modalpage.module.ts":
/*!***********************************************!*\
  !*** ./src/app/modalpage/modalpage.module.ts ***!
  \***********************************************/
/*! exports provided: ModalpagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalpagePageModule", function() { return ModalpagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _modalpage_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modalpage-routing.module */ "./src/app/modalpage/modalpage-routing.module.ts");
/* harmony import */ var _modalpage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modalpage.page */ "./src/app/modalpage/modalpage.page.ts");







let ModalpagePageModule = class ModalpagePageModule {
};
ModalpagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _modalpage_routing_module__WEBPACK_IMPORTED_MODULE_5__["ModalpagePageRoutingModule"]
        ],
        declarations: [_modalpage_page__WEBPACK_IMPORTED_MODULE_6__["ModalpagePage"]]
    })
], ModalpagePageModule);



/***/ })

}]);
//# sourceMappingURL=modalpage-modalpage-module-es2015.js.map