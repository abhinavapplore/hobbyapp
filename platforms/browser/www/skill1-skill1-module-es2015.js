(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skill1-skill1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/skill1/skill1.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/skill1/skill1.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("");

/***/ }),

/***/ "./src/app/skill1/skill1-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/skill1/skill1-routing.module.ts ***!
  \*************************************************/
/*! exports provided: Skill1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skill1PageRoutingModule", function() { return Skill1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _skill1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./skill1.page */ "./src/app/skill1/skill1.page.ts");




const routes = [
    {
        path: '',
        component: _skill1_page__WEBPACK_IMPORTED_MODULE_3__["Skill1Page"]
    }
];
let Skill1PageRoutingModule = class Skill1PageRoutingModule {
};
Skill1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Skill1PageRoutingModule);



/***/ }),

/***/ "./src/app/skill1/skill1.module.ts":
/*!*****************************************!*\
  !*** ./src/app/skill1/skill1.module.ts ***!
  \*****************************************/
/*! exports provided: Skill1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skill1PageModule", function() { return Skill1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _skill1_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./skill1-routing.module */ "./src/app/skill1/skill1-routing.module.ts");
/* harmony import */ var _skill1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./skill1.page */ "./src/app/skill1/skill1.page.ts");







let Skill1PageModule = class Skill1PageModule {
};
Skill1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _skill1_routing_module__WEBPACK_IMPORTED_MODULE_5__["Skill1PageRoutingModule"]
        ],
        declarations: [_skill1_page__WEBPACK_IMPORTED_MODULE_6__["Skill1Page"]]
    })
], Skill1PageModule);



/***/ }),

/***/ "./src/app/skill1/skill1.page.scss":
/*!*****************************************!*\
  !*** ./src/app/skill1/skill1.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".text1 {\n  width: 253px;\n  height: 36px;\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n}\n\n.container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 20px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n}\n\n.centered {\n  position: absolute;\n  top: 140px;\n  left: 85px;\n  transform: translate(-50%, -50%);\n}\n\n.row {\n  justify-content: center;\n  width: 90%;\n}\n\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n\n.row1 {\n  justify-content: center;\n  text-align: center;\n  padding-top: 10%;\n}\n\nion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n\nion-card {\n  margin: 0;\n  height: 70%;\n}\n\n.img {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n  filter: brightness(0.4);\n}\n\n.imgSelected {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n}\n\n.button1 {\n  width: 80%;\n  border-radius: 10px;\n  --background: #F3F3F3;\n  --background-activated: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #AAAAAA;\n  --color-activated: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n\n.button2 {\n  width: 80%;\n  border-radius: 10px;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n\n.row3 {\n  width: 80%;\n  justify-content: center;\n  padding-bottom: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2tpbGwxL3NraWxsMS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDRTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBRUo7O0FBQUU7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsZ0NBQUE7QUFHSjs7QUFERTtFQUNJLHVCQUFBO0VBQ0EsVUFBQTtBQUlOOztBQUZFO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFLTjs7QUFIRTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQU1KOztBQUpFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFPSjs7QUFMRTtFQUNFLFNBQUE7RUFDQSxXQUFBO0FBUUo7O0FBTkU7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFTSjs7QUFQRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFVSjs7QUFSRTtFQUNFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EseUVBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0FBV0o7O0FBVEU7RUFDRSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSwrREFBQTtFQUNBLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0FBWUo7O0FBVkU7RUFDRSxVQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQWFKIiwiZmlsZSI6InNyYy9hcHAvc2tpbGwxL3NraWxsMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGV4dDEge1xuICAgIHdpZHRoOiAyNTNweDtcbiAgICBoZWlnaHQ6IDM2cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBjb2xvcjogIzAwM0M2OTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuY29udGFpbmVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICB9XG4gIC5jZW50ZXJlZCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMTQwcHg7XG4gICAgbGVmdDogODVweDtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgfVxuICAucm93e1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICB3aWR0aDogOTAlO1xuICB9XG4gIC5kaXYge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBwYWRkaW5nLXRvcDogNSU7XG4gIH1cbiAgLnJvdzEge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xuICB9XG4gIGlvbi1pY29uIHtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gIH1cbiAgaW9uLWNhcmQge1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IDcwJTtcbiAgfVxuICAuaW1nIHtcbiAgICBoZWlnaHQ6IDE2MHB4O1xuICAgIHdpZHRoOiAxNjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjQpO1xuICB9XG4gIC5pbWdTZWxlY3RlZHtcbiAgICBoZWlnaHQ6IDE2MHB4O1xuICAgIHdpZHRoOiAxNjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB9XG4gIC5idXR0b24xIHtcbiAgICB3aWR0aDogODAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjNGM0YzO1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7XG4gICAgLS1jb2xvcjogI0FBQUFBQTtcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogI2ZmZmZmZjtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cbiAgLmJ1dHRvbjIge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyNzBkZWcsICMwMkNCRUUgMCUsICM1OUUwMjIgMTAwJSk7OyBcbiAgICAtLWNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuICAucm93MyB7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/skill1/skill1.page.ts":
/*!***************************************!*\
  !*** ./src/app/skill1/skill1.page.ts ***!
  \***************************************/
/*! exports provided: Skill1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skill1Page", function() { return Skill1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let Skill1Page = class Skill1Page {
    constructor() { }
    ngOnInit() {
    }
};
Skill1Page.ctorParameters = () => [];
Skill1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-skill1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./skill1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/skill1/skill1.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./skill1.page.scss */ "./src/app/skill1/skill1.page.scss")).default]
    })
], Skill1Page);



/***/ })

}]);
//# sourceMappingURL=skill1-skill1-module-es2015.js.map