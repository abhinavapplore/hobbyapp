(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["verify-verify-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/verify/verify.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/verify/verify.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppVerifyVerifyPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div id=\"recaptcha-container\"></div>\n  <!-- <div>\n    <ion-grid>\n      <ion-row style=\"margin-top: 8%;\">\n        <ion-col [size]=\"2\" (click)=\"back()\" style=\"justify-content: center;\n        align-items: center;\n        display: flex;\">\n          <ion-icon name=\"arrow-back\" style=\"font-size: 30px;\"></ion-icon>\n        </ion-col>\n        <ion-col [size]=\"2\">\n          <p> </p>\n          </ion-col>\n        <ion-col [size]=\"2\">\n          <p> </p>\n          </ion-col>\n      \n      </ion-row>\n    </ion-grid>\n    \n  </div> -->\n  \n  <ion-grid fixed=\"true\">\n   \n\n    <ion-row class=\"row1\">\n      <ion-col>\n        <ion-text class=\"text\">Automatically fetching OTP...</ion-text>\n      </ion-col>\n    </ion-row>\n    \n\n  \n    <ion-row class=\"row2\">\n      <ion-col size=\"2\">\n        <ion-item class=\"roundedInput\" lines=\"none\">\n       \n          <ion-input type=\"tel\" maxlength=\"1\" #passcode1\n          (keyup)=\"onKeyUp($event,1)\" [(ngModel)]=\"pass1\" style=\"text-align: center;color: #1E32FA;font-size: 22px;\n          font-family: Open Sans-bold;\"\n          (keypress)=\"numberOnlyValidation($event)\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-item class=\"roundedInput\" lines=\"none\">\n       \n          <ion-input type=\"tel\" maxlength=\"1\" #passcode2\n          (keyup)=\"onKeyUp($event,2)\" [(ngModel)]=\"pass2\" style=\"text-align: center;color: #1E32FA;font-size: 22px;\n          font-family: Open Sans-bold;\"\n          (keypress)=\"numberOnlyValidation($event)\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-item class=\"roundedInput\" lines=\"none\">\n        \n          <ion-input type=\"tel\" maxlength=\"1\" #passcode3\n          (keyup)=\"onKeyUp($event,3)\" [(ngModel)]=\"pass3\" style=\"text-align: center;color: #1E32FA;    font-size: 22px;\n          font-family: Open Sans-bold;\"\n          (keypress)=\"numberOnlyValidation($event)\" ></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-item  class=\"roundedInput\" lines=\"none\">\n         \n          <ion-input type=\"tel\" maxlength=\"1\" #passcode4\n          (keyup)=\"onKeyUp($event,4)\" [(ngModel)]=\"pass4\" style=\"text-align: center;color: #1E32FA;    font-size: 22px;\n          font-family: Open Sans-bold;\"\n          (keypress)=\"numberOnlyValidation($event)\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-item class=\"roundedInput\" lines=\"none\">\n        \n          <ion-input type=\"tel\" maxlength=\"1\" #passcode5\n          (keyup)=\"onKeyUp($event,5)\" [(ngModel)]=\"pass5\" style=\"text-align: center;color: #1E32FA;    font-size: 22px;\n          font-family: Open Sans-bold;\"\n          (keypress)=\"numberOnlyValidation($event)\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-item class=\"roundedInput\" lines=\"none\">\n         \n          <ion-input (ionInput)=\"onKeySixSignup()\" style=\"background-color:  #1E32FA;\" type=\"tel\" maxlength=\"1\" #passcode6\n          (keyup)=\"onKeyUp($event,6)\" [(ngModel)]=\"pass6\" style=\"text-align: center;color: #1E32FA;    font-size: 22px;\n          font-family: Open Sans-bold;\"\n          (keypress)=\"numberOnlyValidation($event)\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"row3\">\n      <ion-spinner name=\"dots\"></ion-spinner>\n      <!-- <ion-icon name=\"ellipsis-horizontal\"></ion-icon> -->\n    </ion-row>\n\n    <ion-row *ngIf=isotp class=\"row3\" (click)=\"myFakeFuntion()\">\n      <div class=\"button\">\n        <ion-text class=\"signin\">Verify Now</ion-text>\n      </div>\n    </ion-row>\n\n    <!-- <ion-row>\n      <ion-col style=\"text-align: center;\">\n        <ion-text style=\"font-size: 16px;font-family:Open Sans-semibold;\"\n         color=\"secondary\" id=\"timer\"></ion-text>\n      </ion-col> \n    </ion-row> -->\n   \n    <!-- <ion-row>\n      <ion-col style=\"text-align: center;\">\n        <ion-button (click)=\"verify(pass1,pass2,pass3,pass4,pass5,pass6)\" \n        style=\"width:279px;height:44px;font-size: 21px;font-family:Open Sans-bold;\n        border-radius: 12px;\">Verify Now</ion-button>\n      </ion-col>\n    </ion-row> -->\n    <!-- <ion-row >\n      <ion-col id=\"timerstr\" style=\"text-align: center;\">    \n          <ion-text style=\"font-size: 16px;font-family:Open Sans-semibold;\n          color: #1E32FA;opacity: 0.7;\" >Didn't receive SMS?</ion-text>    \n      </ion-col>     \n    </ion-row>\n    <ion-row>\n      <ion-col id=\"bottomDiv\" style=\"text-align: center;\">    \n        <ion-text style=\"font-size: 16px;font-family:Open Sans-bold;\n        color: #1E32FA;\" (click)=\"resendsms()\">Resend</ion-text>     \n    </ion-col>\n    </ion-row> -->\n\n  </ion-grid>\n  \n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/verify/verify-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/verify/verify-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: VerifyPageRoutingModule */

    /***/
    function srcAppVerifyVerifyRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VerifyPageRoutingModule", function () {
        return VerifyPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _verify_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./verify.page */
      "./src/app/verify/verify.page.ts");

      var routes = [{
        path: '',
        component: _verify_page__WEBPACK_IMPORTED_MODULE_3__["VerifyPage"]
      }];

      var VerifyPageRoutingModule = function VerifyPageRoutingModule() {
        _classCallCheck(this, VerifyPageRoutingModule);
      };

      VerifyPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], VerifyPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/verify/verify.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/verify/verify.module.ts ***!
      \*****************************************/

    /*! exports provided: VerifyPageModule */

    /***/
    function srcAppVerifyVerifyModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VerifyPageModule", function () {
        return VerifyPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _verify_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./verify-routing.module */
      "./src/app/verify/verify-routing.module.ts");
      /* harmony import */


      var _verify_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./verify.page */
      "./src/app/verify/verify.page.ts");

      var VerifyPageModule = function VerifyPageModule() {
        _classCallCheck(this, VerifyPageModule);
      };

      VerifyPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _verify_routing_module__WEBPACK_IMPORTED_MODULE_5__["VerifyPageRoutingModule"]],
        declarations: [_verify_page__WEBPACK_IMPORTED_MODULE_6__["VerifyPage"]]
      })], VerifyPageModule);
      /***/
    },

    /***/
    "./src/app/verify/verify.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/verify/verify.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppVerifyVerifyPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".row3 {\n  justify-content: center;\n  padding-top: 7%;\n}\n\n.signin {\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 18px;\n}\n\n.text {\n  width: 306px;\n  height: 75px;\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 24px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n  font-style: normal;\n  text-align: center;\n}\n\n.row1 {\n  justify-content: center;\n  padding-top: 30%;\n  text-align: center;\n}\n\n.row2 {\n  padding-top: 25%;\n}\n\n.row3 {\n  justify-content: center;\n  padding-top: 5%;\n  text-align: center;\n}\n\nion-icon {\n  color: #888;\n  font-size: 35px;\n}\n\nion-input {\n  --placeholder-color:#333333;\n  --placeholder-opacity:0.8;\n  --placeholder-font-weight:400;\n  --background: #EBEBEB;\n}\n\nion-item {\n  --background: #EBEBEB;\n  --border-color:#EBEBEB;\n}\n\n.roundedInput {\n  --border-top-width: 1px;\n  --border-end-width: 1px;\n  --border-bottom-width: 1px;\n  --border-start-width: 1px;\n  --border-radius:10px;\n  --inner-border-width: 0;\n  --border-width: var(--border-top-width) var(--border-end-width) var(--border-bottom-width) var(--border-start-width);\n}\n\nion-button {\n  --background: #3B5998;\n}\n\n.image-cropper {\n  width: 30%;\n  height: 30%;\n  overflow: hidden;\n  border-radius: 50%;\n  margin-top: 5%;\n}\n\n#bottomDiv {\n  display: none;\n}\n\n#highlightRow .item-has-focus {\n  border: 1px solid blue !important;\n  box-shadow: 0 0 3px blue !important;\n  -moz-box-shadow: 0 0 3px blue !important;\n  -webkit-box-shadow: 0 0 3px blue !important;\n  outline-offset: 0px !important;\n  outline: none !important;\n  border-radius: 10px !important;\n}\n\nion-input {\n  --padding-end: 0px;\n}\n\n.custom-skeleton {\n  justify-content: center;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmVyaWZ5L3ZlcmlmeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDRSx1QkFBQTtFQUNBLGVBQUE7QUFIRjs7QUFLQTtFQUNFLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFGRjs7QUFJQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQURKOztBQUlFO0VBQ0UsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBREo7O0FBR0E7RUFDSSxnQkFBQTtBQUFKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLHlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxxQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxzQkFBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7RUFDQSxvQkFBQTtFQUNBLHVCQUFBO0VBRUEsb0hBQUE7QUFBSjs7QUFNQTtFQUNFLHFCQUFBO0FBSEY7O0FBTUE7RUFDRSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBSEY7O0FBS0E7RUFDRSxhQUFBO0FBRkY7O0FBU0k7RUFDSSxpQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esd0NBQUE7RUFDQSwyQ0FBQTtFQUNBLDhCQUFBO0VBQ0Esd0JBQUE7RUFDQSw4QkFBQTtBQU5SOztBQVNFO0VBRUUsa0JBQUE7QUFQSjs7QUFTRTtFQUNFLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFOSiIsImZpbGUiOiJzcmMvYXBwL3ZlcmlmeS92ZXJpZnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWNvbnRlbnR7XG4vLyAgICAgLS1iYWNrZ3JvdW5kOiAjRjRGNEY0O1xuLy8gfVxuXG4ucm93M3tcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiA3JTtcbn1cbi5zaWduaW4ge1xuICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi50ZXh0IHtcbiAgICB3aWR0aDogMzA2cHg7XG4gICAgaGVpZ2h0OiA3NXB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgY29sb3I6ICMwMDNDNjk7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAucm93MXtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMzAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5yb3cyIHtcbiAgICBwYWRkaW5nLXRvcDogMjUlO1xufVxuLnJvdzN7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWljb24ge1xuICAgIGNvbG9yOiAjODg4O1xuICAgIGZvbnQtc2l6ZTogMzVweDtcbn1cblxuaW9uLWlucHV0e1xuICAgIC0tcGxhY2Vob2xkZXItY29sb3I6IzMzMzMzMztcbiAgICAtLXBsYWNlaG9sZGVyLW9wYWNpdHk6MC44O1xuICAgIC0tcGxhY2Vob2xkZXItZm9udC13ZWlnaHQ6NDAwO1xuICAgIC0tYmFja2dyb3VuZDogI0VCRUJFQjtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0tYmFja2dyb3VuZDogI0VCRUJFQjtcbiAgICAtLWJvcmRlci1jb2xvcjojRUJFQkVCO1xufVxuXG4ucm91bmRlZElucHV0e1xuICAgIC0tYm9yZGVyLXRvcC13aWR0aDogMXB4O1xuICAgIC0tYm9yZGVyLWVuZC13aWR0aDogMXB4O1xuICAgIC0tYm9yZGVyLWJvdHRvbS13aWR0aDogMXB4O1xuICAgIC0tYm9yZGVyLXN0YXJ0LXdpZHRoOiAxcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOjEwcHg7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDA7XG4gICAgXG4gICAgLS1ib3JkZXItd2lkdGg6IHZhcigtLWJvcmRlci10b3Atd2lkdGgpIHZhcigtLWJvcmRlci1lbmQtd2lkdGgpIHZhcigtLWJvcmRlci1ib3R0b20td2lkdGgpIHZhcigtLWJvcmRlci1zdGFydC13aWR0aCk7XG4gICAgXG5cbiAgIFxufVxuXG5pb24tYnV0dG9ue1xuICAtLWJhY2tncm91bmQ6ICMzQjU5OTg7XG59XG5cbi5pbWFnZS1jcm9wcGVye1xuICB3aWR0aDogMzAlO1xuICBoZWlnaHQ6IDMwJTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBtYXJnaW4tdG9wOiA1JTtcbiAgfVxuI2JvdHRvbURpdntcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLy8gaW9uLWl0ZW0ge1xuLy8gICAgIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZjE0NTNkKTtcbi8vICAgfVxuICAjaGlnaGxpZ2h0Um93e1xuICAgIC5pdGVtLWhhcy1mb2N1cyB7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsdWUgIWltcG9ydGFudDtcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDNweCBibHVlICFpbXBvcnRhbnQ7XG4gICAgICAgIC1tb3otYm94LXNoYWRvdzogMCAwIDNweCBibHVlICFpbXBvcnRhbnQ7XG4gICAgICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDNweCBibHVlICFpbXBvcnRhbnQ7XG4gICAgICAgIG91dGxpbmUtb2Zmc2V0OiAwcHggIWltcG9ydGFudDtcbiAgICAgICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xuICAgICAgICBib3JkZXItcmFkaXVzOjEwcHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuICB9XG4gIGlvbi1pbnB1dCB7XG5cbiAgICAtLXBhZGRpbmctZW5kOiAwcHg7XG4gIH1cbiAgLmN1c3RvbS1za2VsZXRvbntcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/verify/verify.page.ts":
    /*!***************************************!*\
      !*** ./src/app/verify/verify.page.ts ***!
      \***************************************/

    /*! exports provided: VerifyPage */

    /***/
    function srcAppVerifyVerifyPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VerifyPage", function () {
        return VerifyPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_firebase_authentication_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/firebase-authentication/ngx */
      "./node_modules/@ionic-native/firebase-authentication/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/fcm/ngx */
      "./node_modules/@ionic-native/fcm/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_sms_retriever_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/sms-retriever/ngx */
      "./node_modules/@ionic-native/sms-retriever/__ivy_ngcc__/ngx/index.js");

      var VerifyPage = /*#__PURE__*/function () {
        function VerifyPage(router, route, alertController, http, httpClient, storage, fauth, smsRetriever, platform, loadingController, toastCtrl, authService, fcm, navCtrl) {
          var _this = this;

          _classCallCheck(this, VerifyPage);

          this.router = router;
          this.route = route;
          this.alertController = alertController;
          this.http = http;
          this.httpClient = httpClient;
          this.storage = storage;
          this.fauth = fauth;
          this.smsRetriever = smsRetriever;
          this.platform = platform;
          this.loadingController = loadingController;
          this.toastCtrl = toastCtrl;
          this.authService = authService;
          this.fcm = fcm;
          this.navCtrl = navCtrl;
          this.sendAgainBool = false;
          this.values = [];
          this.isotp = false;
          this.OTP = '';
          this.showOTPInput = false;
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              var details = JSON.parse(params.special);
              SMSReceive.startWatch(function () {
                document.addEventListener('onSMSArrive', function (e) {
                  var IncomingSMS = e.data;

                  _this.processSMS(IncomingSMS);
                });
              }, function () {
                console.log('watch start failed');
              });
              _this.userDetails = details;
              _this.mobno = details.mobile;
              var phoneString = details.mobile.toString();
              _this.phone = "+91" + phoneString;
              console.log(_this.phone);
              console.log(typeof _this.phone);
              var phoneNumberString = _this.phone;
              console.log("testing" + phoneNumberString);

              _this.fauth.verifyPhoneNumber(phoneNumberString, 60).then(function (res) {
                //console.log("Hello Hello" + res);
                _this.verificationId = res;
              });

              _this.fauth.onAuthStateChanged().subscribe(function (response) {
                console.log(response);
              });
            }
          });
        }

        _createClass(VerifyPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.fcm.getToken().then(function (token) {
              console.log(token);
              _this2.deviceId = token;
            });
          } // start() {
          //   SMSReceive.startWatch(
          //     () => {
          //       console.log('watch started');
          //       document.addEventListener('onSMSArrive', (e: any) => {
          //         console.log('onSMSArrive()');
          //         var IncomingSMS = e.data;
          //         console.log(JSON.stringify(IncomingSMS));
          //       });
          //     },
          //     () => { console.log('watch start failed') }
          //   )
          // }
          // SMSReceive.stopWatch(
          //   () => { console.log('watch stopped') },
          //   () => { console.log('watch stop failed') }
          //   )

        }, {
          key: "presentToast",
          value: function presentToast(message, show_button, position, duration) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastCtrl.create({
                        message: message,
                        position: position,
                        duration: duration
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "next",
          value: function next() {
            this.showOTPInput = true;
          }
        }, {
          key: "stop",
          value: function stop() {
            SMSReceive.stopWatch(function () {
              console.log('watch stopped');
            }, function () {
              console.log('watch stop failed');
            });
          }
        }, {
          key: "processSMS",
          value: function processSMS(data) {
            // Check SMS for a specific string sequence to identify it is you SMS
            // Design your SMS in a way so you can identify the OTP quickly i.e. first 6 letters
            // In this case, I am keeping the first 6 letters as OTP
            var message = data.body;

            if (message && message.indexOf('is your verification code') != -1) {
              this.pass1 = data.body.slice(0, 1);
              this.pass2 = data.body.slice(1, 2);
              this.pass3 = data.body.slice(2, 3);
              this.pass4 = data.body.slice(3, 4);
              this.pass5 = data.body.slice(4, 5);
              this.pass6 = data.body.slice(5, 6);
              this.verify(this.pass1, this.pass2, this.pass3, this.pass4, this.pass5, this.pass6);
            }
          } // register() {
          //   if (this.OTP != '') {
          //     this.presentToast('You are successfully registered', false, 'top', 1500);
          //   } else {
          //     this.presentToast('Your OTP is not valid', false, 'bottom', 1500);
          //   }
          // }

        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {
            // alert("this");
            document.getElementById("timer").innerHTML = "";
          }
        }, {
          key: "checkVersion",
          value: function checkVersion(concat) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this3 = this;

              var ready;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.platform.ready();

                    case 2:
                      ready = !!_context2.sent;

                      if (ready) {
                        // try to add your code here
                        this.fauth.signInWithVerificationId(this.verificationId, concat).then(function (res) {
                          if (res == "OK") {
                            var emailId = _this3.makeid(5);

                            var obj = {
                              contactNum: _this3.mobno,
                              emailId: emailId,
                              deviceId: _this3.deviceId,
                              emailIdRegistered: false
                            };

                            _this3.authService.login(obj).subscribe(function (res) {
                              if (res.success == false) {
                                _this3.storage.set("contactNum", _this3.mobno).then(function (res) {
                                  console.log(res);

                                  if (res != null) {
                                    _this3.stop();

                                    _this3.navCtrl.navigateRoot('hobby');
                                  }
                                });
                              } else {
                                _this3.storage.set("ACCESS_TOKEN", res.token);

                                _this3.storage.set("user", res.user);

                                console.log(res.user);

                                _this3.stop();

                                _this3.navCtrl.navigateRoot('tabs/tab4');
                              }
                            });
                          }
                        })["catch"](function (error) {
                          var self = this;
                          console.log(error);

                          if (error == "This credential is already associated with a different user account.") {
                            self.authService.presentToast("This credential is already associated with a different user account.");
                            var emailId = self.makeid(5);
                            var obj = {
                              contactNum: self.mobno,
                              emailId: emailId,
                              deviceId: self.deviceId,
                              emailIdRegistered: false
                            };
                            self.authService.login(obj).subscribe(function (res) {
                              if (res.success == false) {
                                self.storage.set("contactNum", self.mobno).then(function (res) {
                                  console.log(res);

                                  if (res != null) {
                                    self.stop();
                                    self.navCtrl.navigateRoot('hobby');
                                  }
                                });
                              }
                            });
                          } else {
                            console.log("incorrect pin"); // self.presentAlert1();
                            // self.authService.presentToast("This credential is already associated with a different user account.");
                            // self.navCtrl.navigateRoot('login');

                            self.authService.presentToast("This credential is already associated with a different user account.");

                            var _emailId = self.makeid(5);

                            var obj = {
                              contactNum: self.mobno,
                              emailId: _emailId,
                              deviceId: self.deviceId,
                              emailIdRegistered: false
                            };
                            self.authService.login(obj).subscribe(function (res) {
                              if (res.success == false) {
                                self.storage.set("contactNum", self.mobno).then(function (res) {
                                  console.log(res);

                                  if (res != null) {
                                    self.stop();
                                    self.navCtrl.navigateRoot('hobby');
                                  }
                                });
                              }
                            });
                          }
                        });
                      }

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "verify",
          value: function verify(pass1, pass2, pass3, pass4, pass5, pass6) {
            var self = this;
            var sPass1 = pass1.toString();
            var sPass2 = pass2.toString();
            var sPass3 = pass3.toString();
            var sPass4 = pass4.toString();
            var sPass5 = pass5.toString();
            var sPass6 = pass6.toString();
            var concat = sPass1 + sPass2 + sPass3 + sPass4 + sPass5 + sPass6; // this.checkVersion(concat);

            this.myFakeFuntion();
          }
        }, {
          key: "getAppHash",
          value: function getAppHash() {
            this.smsRetriever.getAppHash().then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          }
        }, {
          key: "retriveSMS",
          value: function retriveSMS() {
            this.smsRetriever.startWatching().then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          }
        }, {
          key: "presentAlert1",
          value: function presentAlert1() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        header: 'Incorrect Otp',
                        subHeader: 'Please enter correct otp to login.',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "presentAlert",
          value: function presentAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        header: 'Use Another Number',
                        subHeader: 'Looks like this number is already registered.',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "presentAlert2",
          value: function presentAlert2() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.alertController.create({
                        header: 'Server Error',
                        subHeader: 'Please try after sometime.',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context5.sent;
                      _context5.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "registerUser",
          value: function registerUser(form) {
            var _this4 = this;

            this.presentLoading();
            this.authService.register(form.value).subscribe(function (res) {
              if (res.success == false) {
                alert("Invalid Parameters");
              } else {
                _this4.router.navigateByUrl('tabs/tab4');
              }
            });
          }
        }, {
          key: "resendsms",
          value: function resendsms() {
            var _this5 = this;

            var timeLeft = 30;
            var elem = document.getElementById('some_div');
            var timerId = setInterval(countdown, 1000);

            function countdown() {
              if (timeLeft == -1) {
                clearTimeout(timerId); // doSomething();

                this.sendAgainBool = true;
              } else {
                setTimeout(function () {
                  elem.innerHTML = timeLeft + ' seconds remaining';
                }, 2000);
                timeLeft--;
              }
            }

            this.sendAgainBool = false;
            this.fauth.verifyPhoneNumber(this.phone, 60).then(function (res) {
              //console.log("Hello Hello" + res);
              _this5.verificationId = res;
            }); // firebase.auth().signInWithPhoneNumber(this.phone, this.recaptchaVerifier)
            //   .then( confirmationResult => {
            //     console.log(confirmationResult);
            //     this.confirmResult = confirmationResult;
            //   })
            // .catch(function (error) {
            //   console.error("SMS not sent", error);
            // });
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl('signup');
          }
        }, {
          key: "presentLoading",
          value: function presentLoading() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var loading;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.loadingController.create({
                        message: 'Processing',
                        duration: 5000
                      });

                    case 2:
                      loading = _context6.sent;
                      _context6.next = 5;
                      return loading.present();

                    case 5:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "onKeyUp",
          value: function onKeyUp(event, index) {
            console.log(event);

            if (event.target.value.length != 1) {
              this.setFocus(index - 2);
            } else {
              this.values.push(event.target.value);
              this.setFocus(index);
            }

            if (index === 6) {
              // var sPass1 = this.pass1.toString();
              // var sPass2 = this.pass2.toString();
              // var sPass3 = this.pass3.toString();
              // var sPass4 = this.pass4.toString();
              // var sPass5 = this.pass5.toString();
              // var sPass6 = this.pass6.toString();
              this.isotp = true; // this.verify(sPass1,sPass2,sPass3,sPass4,sPass5,sPass6);
            }

            event.stopPropagation();
          }
        }, {
          key: "onKeySixSignup",
          value: function onKeySixSignup() {
            var sPass1 = this.pass1.toString();
            var sPass2 = this.pass2.toString();
            var sPass3 = this.pass3.toString();
            var sPass4 = this.pass4.toString();
            var sPass5 = this.pass5.toString();
            var sPass6 = this.pass6.toString();
            this.isotp = true;
            this.verify(sPass1, sPass2, sPass3, sPass4, sPass5, sPass6);
          }
        }, {
          key: "submit",
          value: function submit(e) {
            this.values = [];
            this.passcode1.value = '';
            this.passcode2.value = '';
            this.passcode3.value = '';
            this.passcode4.value = '';
            this.passcode5.value = '';
            this.passcode6.value = '';
            e.stopPropagation();
          }
        }, {
          key: "setFocus",
          value: function setFocus(index) {
            switch (index) {
              case 0:
                this.passcode1.setFocus();
                break;

              case 1:
                this.passcode2.setFocus();
                break;

              case 2:
                this.passcode3.setFocus();
                break;

              case 3:
                this.passcode4.setFocus();
                break;

              case 4:
                this.passcode5.setFocus();
                break;

              case 5:
                this.passcode6.setFocus();
                break;
            }
          }
        }, {
          key: "numberOnlyValidation",
          value: function numberOnlyValidation(event) {
            var pattern = /[0-9.,]/;
            var inputChar = String.fromCharCode(event.charCode);

            if (!pattern.test(inputChar)) {
              // invalid character, prevent input
              event.preventDefault();
            }
          }
        }, {
          key: "makeid",
          value: function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;

            for (var i = 0; i < length; i++) {
              result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            return result;
          }
        }, {
          key: "myFakeFuntion",
          value: function myFakeFuntion() {
            var _this6 = this;

            console.log("hey there");
            var emailId = this.makeid(5);
            var obj = {
              contactNum: this.mobno,
              emailId: emailId,
              deviceId: this.deviceId,
              emailIdRegistered: false
            };
            this.authService.login(obj).subscribe(function (res) {
              if (res.success == false) {
                _this6.storage.set("contactNum", _this6.mobno).then(function (res) {
                  console.log(res);

                  if (res != null) {
                    _this6.stop();

                    _this6.navCtrl.navigateRoot('hobby');
                  }
                });
              } else {
                _this6.storage.set("ACCESS_TOKEN", res.token);

                _this6.storage.set("user", res.user);

                console.log(res.user);

                _this6.stop();

                _this6.navCtrl.navigateRoot('tabs/tab4');
              }
            });
          }
        }]);

        return VerifyPage;
      }();

      VerifyPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }, {
          type: _ionic_native_firebase_authentication_ngx__WEBPACK_IMPORTED_MODULE_3__["FirebaseAuthentication"]
        }, {
          type: _ionic_native_sms_retriever_ngx__WEBPACK_IMPORTED_MODULE_9__["SmsRetriever"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]
        }, {
          type: _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
        }];
      };

      VerifyPage.propDecorators = {
        passcode1: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['passcode1', {
            "static": false
          }]
        }],
        passcode2: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['passcode2', {
            "static": false
          }]
        }],
        passcode3: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['passcode3', {
            "static": false
          }]
        }],
        passcode4: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['passcode4', {
            "static": false
          }]
        }],
        passcode5: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['passcode5', {
            "static": false
          }]
        }],
        passcode6: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['passcode6', {
            "static": false
          }]
        }]
      };
      VerifyPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-verify',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./verify.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/verify/verify.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./verify.page.scss */
        "./src/app/verify/verify.page.scss"))["default"]]
      })], VerifyPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=verify-verify-module-es5.js.map