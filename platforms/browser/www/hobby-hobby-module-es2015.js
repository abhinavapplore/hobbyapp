(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["hobby-hobby-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/hobby/hobby.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/hobby/hobby.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-icon *ngIf='isUpdate' (click)=\"back()\" name=\"chevron-back\"></ion-icon>\n      <ion-icon *ngIf='!isUpdate' (click)=\"back()\" name=\"chevron-back\"></ion-icon>\n      <ion-text>Select Your Hobby</ion-text>\n    </ion-row>   \n  </ion-toolbar>\n\n<ion-content>\n \n  <ion-row class=\"row1\">\n    <ion-text class=\"text1\">Choose your interests. You can change \n      them later</ion-text>\n  </ion-row>\n\n  <div class=\"div\">\n    <ion-row class=\"row\">\n        <ion-col (click)=\"unSelectHobby()\" [size]=6 class=\"container\" *ngIf=\"selected\">\n          <img class=\"imgSelected\" src=\"../../assets/hobbyImage/bake.jpg\">\n          <div class=\"centered\">\n            <ion-text>BAKE</ion-text>\n          </div>\n        </ion-col>\n        <ion-col (click)=\"selectHobby('bake','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/bake.jpg?alt=media&token=5e1317e1-351a-44b8-a483-f96b40b64294')\" [size]=6 class=\"container\" *ngIf=\"!selected\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/bake.jpg\">\n          <div class=\"centered\">\n            <ion-text>BAKE</ion-text>\n          </div>\n        </ion-col>\n\n        <ion-col (click)=\"unSelectHobby2()\" class=\"container\" *ngIf=\"selected2\">\n          <img class=\"imgSelected\" src=\"../../assets/hobbyImage/run.jpg\">\n          <div class=\"centered\">\n            <ion-text>RUN</ion-text>\n          </div>\n        </ion-col>\n        <ion-col (click)=\"selectHobby2('run','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/run.jpg?alt=media&token=a32179fe-eed0-4ac9-bd4c-a80a40d6a5b7')\" class=\"container\" *ngIf=\"!selected2\">\n          <img class=\"img\" src=\"../../assets/hobbyImage/run.jpg\">\n          <div class=\"centered\">\n            <ion-text>RUN</ion-text>\n          </div>\n        </ion-col>\n      \n    </ion-row>\n\n    <ion-row class=\"row\">\n      <ion-col (click)=\"unSelectHobby3()\" [size]=6 class=\"container\" *ngIf=\"selected3\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/yoga.jpg\">\n            <div class=\"centered\">\n              <ion-text>YOGA</ion-text>\n            </div>\n      </ion-col>\n      <ion-col (click)=\"selectHobby3('yoga','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/yoga.jpg?alt=media&token=edbd4ce1-ac21-4dc3-bb86-51fa05c46f93')\" [size]=6 class=\"container\" *ngIf=\"!selected3\">\n        <img class=\"img\" src=\"../../assets/hobbyImage/yoga.jpg\">\n        <div class=\"centered\">\n          <ion-text>YOGA</ion-text>\n        </div>\n      </ion-col>\n\n      <ion-col (click)=\"unSelectHobby4()\" [size]=6 class=\"container\" *ngIf=\"selected4\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/gym.jpg\">\n            <div class=\"centered\">\n              <ion-text>GYM</ion-text>\n            </div>\n      </ion-col>  \n      <ion-col (click)=\"selectHobby4('gym','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/gym.jpg?alt=media&token=3658f866-3967-4b1b-8309-c5094612f416')\" [size]=6 class=\"container\" *ngIf=\"!selected4\">\n        <img class=\"img\" src=\"../../assets/hobbyImage/gym.jpg\">\n        <div class=\"centered\">\n          <ion-text>GYM</ion-text>\n        </div>\n      </ion-col>      \n    </ion-row>\n\n    <ion-row class=\"row\">\n      <ion-col (click)=\"unSelectHobby5()\" [size]=6 class=\"container\" *ngIf=\"selected5\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/meditation.jpg\">\n            <div class=\"centered\">\n              <ion-text>MEDITATION</ion-text>\n            </div>\n      </ion-col>\n      <ion-col (click)=\"selectHobby5('meditation','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/meditation.jpg?alt=media&token=de661d01-c565-4aed-a3b4-cfd372e0bcc8')\" [size]=6 class=\"container\" *ngIf=\"!selected5\">\n        <img class=\"img\" src=\"../../assets/hobbyImage/meditation.jpg\">\n        <div class=\"centered\">\n          <ion-text>MEDITATION</ion-text>\n        </div>\n      </ion-col>\n\n      <ion-col (click)=\"unSelectHobby6()\" [size]=6 class=\"container\" *ngIf=\"selected6\">\n        <img class=\"imgSelected\" src=\"../../assets/hobbyImage/guitar.jpg\">\n        <div class=\"centered\">\n          <ion-text>GUITAR</ion-text>\n        </div>\n      </ion-col>\n      <ion-col (click)=\"selectHobby6('guitar','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/guitar.jpg?alt=media&token=9cb8073a-5719-424e-801d-d725342d2c25')\" [size]=6 class=\"container\" *ngIf=\"!selected6\">\n            <img class=\"img\" src=\"../../assets/hobbyImage/guitar.jpg\">\n            <div class=\"centered\">\n              <ion-text>GUITAR</ion-text>\n            </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"row\">\n      <ion-col (click)=\"unSelectHobby7()\" [size]=6 class=\"container\" *ngIf=\"selected7\">\n        <img class=\"imgSelected\" src=\"../../assets/hobbyImage/cycling.jpg\">\n        <div class=\"centered\">\n          <ion-text>CYCLING</ion-text>\n        </div>\n      </ion-col>\n      <ion-col (click)=\"selectHobby7('cycling','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/cycling.jpg?alt=media&token=af6ab57b-c855-4829-9525-c34d20b8f691')\" [size]=6 class=\"container\" *ngIf=\"!selected7\">\n            <img class=\"img\" src=\"../../assets/hobbyImage/cycling.jpg\">\n            <div class=\"centered\">\n              <ion-text>CYCLING</ion-text>\n            </div>\n      </ion-col>\n\n      <ion-col (click)=\"unSelectHobby8()\" [size]=6 class=\"container\" *ngIf=\"selected8\">\n            <img class=\"imgSelected\" src=\"../../assets/hobbyImage/singing.jpg\">\n            <div class=\"centered\">\n              <ion-text>SINGING</ion-text>\n            </div>\n      </ion-col>\n      <ion-col (click)=\"selectHobby8('singing','https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/singing.jpg?alt=media&token=2e9b4108-6af6-4eed-bf5f-e481339e4e53')\" [size]=6 class=\"container\" *ngIf=\"!selected8\">\n        <img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\">\n        <div class=\"centered\">\n          <ion-text>SINGING</ion-text>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"row3\" *ngIf=\"count==0\">\n      <ion-button class=\"button1\" shape=\"block\">Next(0 Selected)</ion-button>\n    </ion-row>\n    <ion-row class=\"row3\" *ngIf=\"count!=0\" (click)=\"next()\">\n      <ion-button class=\"button2\" shape=\"block\">Next({{count}} Selected)</ion-button>\n    </ion-row>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/hobby/hobby-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/hobby/hobby-routing.module.ts ***!
  \***********************************************/
/*! exports provided: HobbyPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HobbyPageRoutingModule", function() { return HobbyPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _hobby_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hobby.page */ "./src/app/hobby/hobby.page.ts");




const routes = [
    {
        path: '',
        component: _hobby_page__WEBPACK_IMPORTED_MODULE_3__["HobbyPage"]
    }
];
let HobbyPageRoutingModule = class HobbyPageRoutingModule {
};
HobbyPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HobbyPageRoutingModule);



/***/ }),

/***/ "./src/app/hobby/hobby.module.ts":
/*!***************************************!*\
  !*** ./src/app/hobby/hobby.module.ts ***!
  \***************************************/
/*! exports provided: HobbyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HobbyPageModule", function() { return HobbyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _hobby_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./hobby-routing.module */ "./src/app/hobby/hobby-routing.module.ts");
/* harmony import */ var _hobby_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./hobby.page */ "./src/app/hobby/hobby.page.ts");







let HobbyPageModule = class HobbyPageModule {
};
HobbyPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _hobby_routing_module__WEBPACK_IMPORTED_MODULE_5__["HobbyPageRoutingModule"]
        ],
        declarations: [_hobby_page__WEBPACK_IMPORTED_MODULE_6__["HobbyPage"]]
    })
], HobbyPageModule);



/***/ }),

/***/ "./src/app/hobby/hobby.page.scss":
/*!***************************************!*\
  !*** ./src/app/hobby/hobby.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".text1 {\n  width: 253px;\n  height: 36px;\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n}\n\n.container {\n  position: relative;\n  text-align: center;\n  color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 20px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n}\n\n.centered {\n  position: absolute;\n  top: 140px;\n  left: 96px;\n  transform: translate(-50%, -50%);\n}\n\n.row {\n  justify-content: center;\n  width: 90%;\n}\n\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n\n.row1 {\n  justify-content: center;\n  text-align: center;\n  padding-top: 10%;\n}\n\nion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n\nion-card {\n  margin: 0;\n  height: 70%;\n}\n\n.img {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n  filter: brightness(0.4);\n}\n\n.imgSelected {\n  height: 160px;\n  width: 160px;\n  border-radius: 10px;\n}\n\n.button1 {\n  width: 80%;\n  border-radius: 10px;\n  --background: #F3F3F3;\n  --background-activated: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #AAAAAA;\n  --color-activated: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n\n.button2 {\n  width: 80%;\n  border-radius: 10px;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  --color: #ffffff;\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n}\n\n.row3 {\n  width: 80%;\n  justify-content: center;\n  padding-bottom: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9iYnkvaG9iYnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBQ0U7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQUVKOztBQUFFO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLGdDQUFBO0FBR0o7O0FBREU7RUFDSSx1QkFBQTtFQUNBLFVBQUE7QUFJTjs7QUFGRTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBS047O0FBSEU7RUFDRSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFNSjs7QUFKRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBT0o7O0FBTEU7RUFDRSxTQUFBO0VBQ0EsV0FBQTtBQVFKOztBQU5FO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBU0o7O0FBUEU7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBVUo7O0FBUkU7RUFDRSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLHlFQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQVdKOztBQVRFO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsK0RBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQVlKOztBQVZFO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUFhSiIsImZpbGUiOiJzcmMvYXBwL2hvYmJ5L2hvYmJ5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0MSB7XG4gICAgd2lkdGg6IDI1M3B4O1xuICAgIGhlaWdodDogMzZweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gIH1cbiAgLmNlbnRlcmVkIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxNDBweDtcbiAgICBsZWZ0OiA5NnB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB9XG4gIC5yb3d7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA5MCU7XG4gIH1cbiAgLmRpdiB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgfVxuICAucm93MSB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiAxMCU7XG4gIH1cbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfVxuICBpb24tY2FyZCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGhlaWdodDogNzAlO1xuICB9XG4gIC5pbWcge1xuICAgIGhlaWdodDogMTYwcHg7XG4gICAgd2lkdGg6IDE2MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuNCk7XG4gIH1cbiAgLmltZ1NlbGVjdGVke1xuICAgIGhlaWdodDogMTYwcHg7XG4gICAgd2lkdGg6IDE2MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIH1cbiAgLmJ1dHRvbjEge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNGM0YzRjM7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAtLWNvbG9yOiAjQUFBQUFBO1xuICAgIC0tY29sb3ItYWN0aXZhdGVkOiAjZmZmZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuICAuYnV0dG9uMiB7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTs7IFxuICAgIC0tY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG4gIC5yb3czIHtcbiAgICB3aWR0aDogODAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/hobby/hobby.page.ts":
/*!*************************************!*\
  !*** ./src/app/hobby/hobby.page.ts ***!
  \*************************************/
/*! exports provided: HobbyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HobbyPage", function() { return HobbyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _modalpage_modalpage_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../modalpage/modalpage.page */ "./src/app/modalpage/modalpage.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");







let HobbyPage = class HobbyPage {
    constructor(router, modalController, storage, route, authService) {
        this.router = router;
        this.modalController = modalController;
        this.storage = storage;
        this.route = route;
        this.authService = authService;
        this.data = false;
        this.selected = false;
        this.selected2 = false;
        this.selected3 = false;
        this.selected4 = false;
        this.selected5 = false;
        this.selected6 = false;
        this.selected7 = false;
        this.selected8 = false;
        this.isUpdate = false;
        this.count = 0;
        this.hobbyArray = [];
        this.userData = {};
        this.route.queryParams.subscribe(params => {
            if (params && params.isUpdate) {
                this.isUpdate = params.isUpdate;
                this.pageroute = params.pageroute;
                console.log(this.isUpdate);
            }
        });
    }
    ngOnInit() {
        this.storage.get('user').then((user) => {
            console.log(user);
            this.userData = user;
        });
    }
    selectHobby(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected = false;
            this.count = 4;
        }
        else {
            this.selected = true;
            this.hobby1 = i;
            this.hobby1Image = j;
        }
    }
    unSelectHobby() {
        this.count--;
        console.log(this.count);
        this.selected = false;
        this.hobby1 = '';
    }
    selectHobby2(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected2 = false;
            this.count = 4;
        }
        else {
            this.selected2 = true;
            this.hobby2 = i;
            this.hobby2Image = j;
        }
    }
    unSelectHobby2() {
        this.count--;
        console.log(this.count);
        this.selected2 = false;
        this.hobby2 = '';
    }
    selectHobby3(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected3 = false;
            this.count = 4;
        }
        else {
            this.selected3 = true;
            this.hobby3 = i;
            this.hobby3Image = j;
        }
    }
    unSelectHobby3() {
        this.count--;
        console.log(this.count);
        this.selected3 = false;
        this.hobby3 = '';
    }
    selectHobby4(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected4 = false;
            this.count = 4;
        }
        else {
            this.selected4 = true;
            this.hobby4 = i;
            this.hobby4Image = j;
        }
    }
    unSelectHobby4() {
        this.count--;
        console.log(this.count);
        this.selected4 = false;
        this.hobby4 = '';
    }
    selectHobby5(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected5 = false;
            this.count = 4;
        }
        else {
            this.selected5 = true;
            this.hobby5 = i;
            this.hobby5Image = j;
        }
    }
    unSelectHobby5() {
        this.count--;
        console.log(this.count);
        this.selected5 = false;
        this.hobby5 = '';
    }
    selectHobby6(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected6 = false;
            this.count = 4;
        }
        else {
            this.selected6 = true;
            this.hobby6 = i;
            this.hobby6Image = j;
        }
    }
    unSelectHobby6() {
        this.count--;
        console.log(this.count);
        this.selected6 = false;
        this.hobby6 = '';
    }
    selectHobby7(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected7 = false;
            this.count = 4;
        }
        else {
            this.selected7 = true;
            this.hobby7 = i;
            this.hobby7Image = j;
        }
    }
    unSelectHobby7() {
        this.count--;
        console.log(this.count);
        this.selected7 = false;
        this.hobby7 = '';
    }
    selectHobby8(i, j) {
        this.count++;
        console.log(this.count);
        if (this.count > 4) {
            this.authService.presentToast('Only 4 hobby can be selected.');
            this.selected8 = false;
            this.count = 4;
        }
        else {
            this.selected8 = true;
            this.hobby8 = i;
            this.hobby8Image = j;
        }
    }
    unSelectHobby8() {
        this.count--;
        console.log(this.count);
        this.selected8 = false;
        this.hobby8 = '';
    }
    next() {
        if (this.hobby1 != '' && this.hobby1 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby1, 'hobbyImage': this.hobby1Image });
        }
        if (this.hobby2 != '' && this.hobby2 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby2, 'hobbyImage': this.hobby2Image });
        }
        if (this.hobby3 != '' && this.hobby3 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby3, 'hobbyImage': this.hobby3Image });
        }
        if (this.hobby4 != '' && this.hobby4 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby4, 'hobbyImage': this.hobby4Image });
        }
        if (this.hobby5 != '' && this.hobby5 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby5, 'hobbyImage': this.hobby5Image });
        }
        if (this.hobby6 != '' && this.hobby6 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby6, 'hobbyImage': this.hobby6Image });
        }
        if (this.hobby7 != '' && this.hobby7 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby7, 'hobbyImage': this.hobby7Image });
        }
        if (this.hobby8 != '' && this.hobby8 != undefined) {
            this.hobbyArray.push({ 'hobby': this.hobby8, 'hobbyImage': this.hobby8Image });
        }
        this.storage.set("userHobby", this.hobbyArray).then((res) => {
            console.log(res);
            if (!this.isUpdate && this.hobbyArray.length != 0) {
                this.presentModal();
            }
            else if (this.isUpdate && this.hobbyArray.length != 0) {
                var obj = { "key": "userHobby", "value": this.hobbyArray, "userId": this.userData._id };
                this.authService.updateProfile(obj).subscribe((data) => {
                    console.log(data);
                    if (this.isUpdate) {
                        this.router.navigateByUrl(this.pageroute);
                    }
                    else {
                        this.router.navigateByUrl('login');
                    }
                });
            }
        });
    }
    presentModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modalpage_modalpage_page__WEBPACK_IMPORTED_MODULE_4__["ModalpagePage"],
                cssClass: 'modal',
            });
            yield modal.present();
        });
    }
    back() {
        if (this.isUpdate) {
            this.router.navigateByUrl(this.pageroute);
        }
        else {
            this.router.navigateByUrl('login');
        }
    }
};
HobbyPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }
];
HobbyPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-hobby',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./hobby.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/hobby/hobby.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./hobby.page.scss */ "./src/app/hobby/hobby.page.scss")).default]
    })
], HobbyPage);



/***/ })

}]);
//# sourceMappingURL=hobby-hobby-module-es2015.js.map