(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/tab2/tab2.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab2/tab2.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppTab2Tab2PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content [fullscreen]=\"true\"  [scrollEvents]=\"true\" (ionScroll)=\"didScroll($event)\">\n\n<div class=\"div\">\n  <ion-row style=\"margin-top: 5%;\">\n    <ion-col [size]=2 class=\"col5\" (click)='openFirst()'>\n      <img class=\"img\" src=\"../../assets/images/menu.svg\">\n    </ion-col>\n    <ion-col [size]=10 class=\"col6\">\n      <ion-text class=\"text\">Posts</ion-text>\n    </ion-col>   \n  </ion-row>  \n\n<!-- <div class=\"div1\">\n  <ion-row class=\"row\">\n    <ion-col class=\"col1\" [class.selected]='selected==\"myPost\"' (click)=\"selectedSegment('myPost')\">\n      <ion-text class=\"text1\">MY POSTS</ion-text>\n    </ion-col>\n    <ion-col class=\"col1\" [class.selected]='selected==\"savedPost\"' (click)=\"selectedSegment('savedPost')\">\n      <ion-text class=\"text1\">SAVED</ion-text>\n    </ion-col>\n  </ion-row>\n</div>  -->\n\n<ion-segment value=\"myPost\" (ionChange)=\"segmentChanged($event)\" mode=\"ios\">\n  <ion-segment-button value=\"myPost\">\n    <ion-label class=\"text1\">MY POSTS</ion-label>\n  </ion-segment-button>\n  <ion-segment-button value=\"savedPost\">\n    <ion-label class=\"text1\">SAVED</ion-label>\n  </ion-segment-button>\n</ion-segment>\n</div>\n\n<ion-row *ngIf=\"imgVidPosts.length==0 && !savedPost\" style=\"display: flex;\nflex-direction: row;\njustify-content: center;\nalign-items: center;margin-top: 40%;\">\n  No Posts Yet!\n</ion-row>\n\n<!-- <ion-row *ngIf=\"bookmark.length!=0 && selected=='savedPost'\" style=\"margin-top: 13%;\">\n  <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n</ion-row> -->\n\n<div *ngIf=\"imgVidPosts.length!=0 && !savedPost\">\n    <ion-card *ngFor='let item of imgVidPosts'>\n      <ion-row>\n        <ion-col [size]=5 style=\"position: relative;display: flex;flex-direction: column;\n        justify-content: center;align-items: baseline;width: 151px;height: 177px;\">\n          <ion-thumbnail *ngIf=\"item.posttype=='0'\" style=\"width: 151px;\n          height: 160px;margin-left: 2%;\">\n            <ion-img [src]='item.postUrl'></ion-img>\n          </ion-thumbnail>\n          <div *ngIf=\"item.posttype=='1'\">\n            <video width=\"151\" height=\"160\" #player playsinline preload=\"auto\" controls autoplay>\n              <source [src]=\"item.postUrl\" type=\"video/mp4\"/>\n            </video>\n          </div>\n          <ion-row class=\"div2\">\n            <ion-text class=\"text3\">{{item.postCategory}}</ion-text>\n          </ion-row>\n        </ion-col>\n        <ion-col [size]=7 style=\"width: 151px;height: 177px;display: flex;\n        flex-direction: column;position: relative;\n        justify-content: flex-start;\">\n          <ion-row style=\"margin-left: 10%;\">\n            <ion-text class=\"text2\">{{item.postTitle}}</ion-text>\n          </ion-row>\n          <ion-row class=\"row3\" style=\"margin-left: 10%;\">\n            <ion-text class=\"text4\">{{item.postDescription}}</ion-text>\n          </ion-row>\n          <ion-row style=\"display: flex;\n          position: absolute;\n          bottom: 15px;\n          margin-left: 10%;\n          justify-content: space-between;\n          width: 100%;\">\n          <ion-col>\n            <ion-icon (click)='deletePost(item)' class=\"icon1\" \n            name=\"trash-outline\"></ion-icon>\n          </ion-col>\n           <ion-col>\n            <img style=\"margin-left: 10%;\" (click)=\"share(item)\" \n            src=\"../../assets/images/home/share.svg\">\n          </ion-col>\n          <ion-col>\n            <img style=\"margin-left: 10%;\" (click)=\"editPost(item)\" class=\"img\" \n            src=\"../../assets/images/home/edit-2.svg\">\n          </ion-col>     \n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n\n<ion-row *ngIf=\"bookmark.length==0 && savedPost\" style=\"display: flex;\nflex-direction: row;\njustify-content: center;\nalign-items: center;margin-top: 40%;\">\n  No Bookmarks Yet!\n</ion-row>\n\n<div *ngIf=\"bookmark.length!=0 && savedPost\">\n    <ion-card *ngFor='let item of bookmark'>\n      <ion-row>\n        <ion-col [size]=5 style=\"position: relative;display: flex;flex-direction: column;\n        justify-content: center;align-items: baseline;width: 151px;height: 177px;\">\n          <ion-thumbnail *ngIf=\"item.posttype=='0'\" style=\"width: 151px;\n          height: 160px;margin-left: 2%;\">\n            <ion-img [src]='item.postUrl'></ion-img>\n          </ion-thumbnail>\n          <div *ngIf=\"item.posttype=='1'\">\n            <video width=\"151\" height=\"160\" #player playsinline preload=\"auto\" controls autoplay>\n              <source [src]=\"item.postUrl\" type=\"video/mp4\"/>\n            </video>\n          </div>\n          <ion-row class=\"div2\">\n            <ion-text class=\"text3\">{{item.postCategory}}</ion-text>\n          </ion-row>\n        </ion-col>\n        <ion-col [size]=7 style=\"width: 151px;height: 177px;display: flex;\n        flex-direction: column;position: relative;\n        justify-content: flex-start;\">\n          <ion-row style=\"margin-left: 10%;\">\n            <ion-text class=\"text2\">{{item.postTitle}}</ion-text>\n          </ion-row>\n          <ion-row style=\"margin-left: 10%;\" class=\"row3\">\n            <ion-text class=\"text4\">{{item.postDescription}}</ion-text>\n          </ion-row>\n          <ion-row style=\"display: flex;\n          position: absolute;\n          bottom: 15px;\n          margin-left: 10%;\n          justify-content: space-between;\n          width: 100%;\">\n          <ion-col>\n            <ion-icon style=\"margin-left: 10%;\" (click)='deleteBookmark(item)' class=\"icon1\" \n            name=\"trash-outline\"></ion-icon>\n          </ion-col>\n          <ion-col>\n            <img style=\"margin-left: 10%;\" (click)=\"share(item)\" src=\"../../assets/images/home/share.svg\">\n          </ion-col>\n           \n           \n            <!-- <img (click)=\"editPost(item)\" class=\"img\" src=\"../../assets/images/home/edit-2.svg\"> -->\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n\n<!-- <ion-row *ngIf=\"bookmark.length!=0 && selected=='savedPost'\" style=\"margin-top: 10%;\">\n  <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n</ion-row> -->\n\n<ion-row style=\"padding-top: 15%;\">\n  <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n</ion-row>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/tab2/tab2-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/tab2/tab2-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: Tab2PageRoutingModule */

    /***/
    function srcAppTab2Tab2RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2PageRoutingModule", function () {
        return Tab2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tab2.page */
      "./src/app/tab2/tab2.page.ts");

      var routes = [{
        path: '',
        component: _tab2_page__WEBPACK_IMPORTED_MODULE_3__["Tab2Page"]
      }];

      var Tab2PageRoutingModule = function Tab2PageRoutingModule() {
        _classCallCheck(this, Tab2PageRoutingModule);
      };

      Tab2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Tab2PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/tab2/tab2.module.ts":
    /*!*************************************!*\
      !*** ./src/app/tab2/tab2.module.ts ***!
      \*************************************/

    /*! exports provided: Tab2PageModule */

    /***/
    function srcAppTab2Tab2ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function () {
        return Tab2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tab2.page */
      "./src/app/tab2/tab2.page.ts");
      /* harmony import */


      var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../explore-container/explore-container.module */
      "./src/app/explore-container/explore-container.module.ts");
      /* harmony import */


      var _tab2_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./tab2-routing.module */
      "./src/app/tab2/tab2-routing.module.ts");

      var Tab2PageModule = function Tab2PageModule() {
        _classCallCheck(this, Tab2PageModule);
      };

      Tab2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"], _tab2_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab2PageRoutingModule"]],
        declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_5__["Tab2Page"]]
      })], Tab2PageModule);
      /***/
    },

    /***/
    "./src/app/tab2/tab2.page.scss":
    /*!*************************************!*\
      !*** ./src/app/tab2/tab2.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppTab2Tab2PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".img {\n  width: 25px;\n}\n\n.img1 {\n  width: 15%;\n}\n\n.icon1 {\n  color: black;\n  font-size: 25px;\n}\n\n.selected {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  border-bottom: 2px solid white !important;\n  height: 143%;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.text {\n  font-family: \"Poppins-Bold\";\n  font-size: 25px;\n  color: #FFFFFF;\n}\n\n.text1 {\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n  color: #000000;\n}\n\n.div {\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%) !important;\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  position: relative;\n  width: 100%;\n}\n\nion-segment-button {\n  --indicator-color: lightgray;\n}\n\n.div1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.row {\n  padding-top: 5%;\n  width: 311px;\n}\n\n.icon {\n  font-size: 30px;\n  color: #FFFFFF;\n}\n\n.row4 {\n  padding-top: 17%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n}\n\n.text4 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #767474;\n}\n\n.row3 {\n  padding-top: 5%;\n  padding-right: 3%;\n}\n\n.text2 {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #003C69;\n}\n\n.text3 {\n  font-family: \"Poppins-Medium\";\n  font-size: 12px;\n  color: #FFFFFF;\n  text-transform: uppercase;\n}\n\n.div2 {\n  position: absolute;\n  top: 13px;\n  left: 15px;\n  padding: 2%;\n  border-radius: 7px;\n  background: #00CBEE;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.img3 {\n  height: 177px;\n  width: 151px;\n}\n\n.thumbnail {\n  height: 177px;\n  width: 151px;\n}\n\n.col2 {\n  width: 151px;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-evenly;\n}\n\n.col {\n  position: relative;\n  text-align: center;\n  padding: 2%;\n}\n\n.row2 {\n  border-radius: 5px;\n  background: white;\n  width: 344px;\n  height: 197px;\n  padding: 1%;\n}\n\n.div4 {\n  width: 344px;\n  height: 197px;\n}\n\n.div3 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 3%;\n}\n\n.col5 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.col6 {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.row5 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n  height: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMi90YWIyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSjs7QUFDRTtFQUNFLFVBQUE7QUFFSjs7QUFBQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FBR0o7O0FBREU7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLHlDQUFBO0VBQ0EsWUFBQTtBQUlOOztBQUZFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFLSjs7QUFIRTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFNTjs7QUFKRTtFQUNFLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFPSjs7QUFMQTtFQUNJLHdFQUFBO0VBQ0EsZ0RBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUVBLGtCQUFBO0VBQ0EsV0FBQTtBQU9KOztBQUxFO0VBQ0UsNEJBQUE7QUFRSjs7QUFORTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBU0o7O0FBUEU7RUFDRSxlQUFBO0VBQ0EsWUFBQTtBQVVKOztBQVJFO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUFXTjs7QUFURTtFQUNFLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtBQVlKOztBQVRBO0VBQ0ksOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVlKOztBQVZFO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBYUo7O0FBWEE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBY0o7O0FBWkU7RUFDRSw2QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QUFlSjs7QUFiQTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFnQkY7O0FBZEE7RUFDRSxhQUFBO0VBQ0EsWUFBQTtBQWlCRjs7QUFmQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBa0JGOztBQWhCQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtBQW1CRjs7QUFqQkE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQW9CSjs7QUFsQkE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FBcUJKOztBQW5CRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0FBc0JKOztBQXBCRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBdUJKOztBQXJCRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUF3Qko7O0FBdEJBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQXlCSjs7QUF2QkE7RUFDRSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUEwQkYiLCJmaWxlIjoic3JjL2FwcC90YWIyL3RhYjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZyB7XG4gICAgd2lkdGg6IDI1cHg7XG4gIH1cbiAgLmltZzEge1xuICAgIHdpZHRoOiAxNSU7XG59XG4uaWNvbjEge1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBmb250LXNpemU6IDI1cHg7XG59XG4gIC5zZWxlY3RlZCB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIHdoaXRlICFpbXBvcnRhbnQ7XG4gICAgICBoZWlnaHQ6IDE0MyU7XG4gIH0gXG4gIC5jb2wxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59IFxuICAudGV4dCB7XG4gICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICB9ICAgXG4gIC50ZXh0MSB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjMDAwMDAwO1xufSAgXG4uZGl2IHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpICFpbXBvcnRhbnQ7XG4gICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAvLyBoZWlnaHQ6IDE0MHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICBpb24tc2VnbWVudC1idXR0b24ge1xuICAgIC0taW5kaWNhdG9yLWNvbG9yOiBsaWdodGdyYXk7XG59XG4gIC5kaXYxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgLnJvdyB7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIHdpZHRoOiAzMTFweDtcbiAgfVxuICAuaWNvbiB7XG4gICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgfVxuICAucm93NCB7XG4gICAgcGFkZGluZy10b3A6IDE3JTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnRleHQ0IHtcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtUmVndWxhcic7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjNzY3NDc0O1xuICB9XG4gIC5yb3czIHtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgcGFkZGluZy1yaWdodDogMyU7XG59XG4udGV4dDIge1xuICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6ICMwMDNDNjk7XG4gIH1cbiAgLnRleHQzIHtcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtTWVkaXVtJztcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5kaXYyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEzcHg7XG4gIGxlZnQ6IDE1cHg7XG4gIHBhZGRpbmc6IDIlO1xuICBib3JkZXItcmFkaXVzOiA3cHg7XG4gIGJhY2tncm91bmQ6ICMwMENCRUU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmltZzMge1xuICBoZWlnaHQ6IDE3N3B4O1xuICB3aWR0aDogMTUxcHg7XG59XG4udGh1bWJuYWlsIHtcbiAgaGVpZ2h0OiAxNzdweDtcbiAgd2lkdGg6IDE1MXB4O1xufVxuLmNvbDIge1xuICB3aWR0aDogMTUxcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xufVxuLmNvbCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAyJTtcbn1cbi5yb3cyIHtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgd2lkdGg6IDM0NHB4O1xuICAgIGhlaWdodDogMTk3cHg7XG4gICAgcGFkZGluZzogMSU7XG4gIH1cbiAgLmRpdjQge1xuICAgIHdpZHRoOiAzNDRweDtcbiAgICBoZWlnaHQ6IDE5N3B4O1xuICB9XG4gIC5kaXYzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gIH1cbiAgLmNvbDUge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbDYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5yb3c1IHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOmNlbnRlcjtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/tab2/tab2.page.ts":
    /*!***********************************!*\
      !*** ./src/app/tab2/tab2.page.ts ***!
      \***********************************/

    /*! exports provided: Tab2Page */

    /***/
    function srcAppTab2Tab2PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2Page", function () {
        return Tab2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _question_question_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../question/question.page */
      "./src/app/question/question.page.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/social-sharing/ngx */
      "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");

      var Tab2Page = /*#__PURE__*/function () {
        function Tab2Page(menu, authService, navCtrl, storage, router, socialSharing, modalController) {
          _classCallCheck(this, Tab2Page);

          this.menu = menu;
          this.authService = authService;
          this.navCtrl = navCtrl;
          this.storage = storage;
          this.router = router;
          this.socialSharing = socialSharing;
          this.modalController = modalController;
          this.selected = "myPost";
          this.bookmark = [];
          this.posts = [];
          this.imgVidPosts = [];
          this.currentPlaying = null;
        }

        _createClass(Tab2Page, [{
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            var _this = this;

            this.imgVidPosts = [];
            this.storage.get('user').then(function (user) {
              console.log(user);
              _this.userId = user._id;

              _this.loadData();
            });
          }
        }, {
          key: "loadData",
          value: function loadData() {
            var _this2 = this;

            this.bookmark = [];
            this.imgVidPosts = [];
            this.authService.loading("Loading your posts.");
            var obj = {
              'userId': this.userId
            };
            this.authService.getBookmark(obj).subscribe(function (data) {
              console.log(data);
              _this2.bookmark = data.bookmarkArray;
              _this2.posts = data.postArray;

              _this2.posts.forEach(function (element) {
                if (element.posttype == '0' || element.posttype == '1') {
                  _this2.imgVidPosts.push(element);
                }
              });
            });
            this.authService.dismissLoading();
          }
        }, {
          key: "deleteBookmark",
          value: function deleteBookmark(item) {
            var _this3 = this;

            this.imgVidPosts = [];
            this.bookmark.forEach(function (element) {
              if (item.postId == element.postId) {
                var obj1 = {
                  'bookmarkId': element._id,
                  'userId': _this3.userId
                };

                _this3.authService.deleteBookmark(obj1).subscribe(function (res) {
                  console.log(res);

                  _this3.authService.presentToast('Bookmark Removed');

                  _this3.loadData();
                });
              }
            });
          }
        }, {
          key: "deletePost",
          value: function deletePost(item) {
            var _this4 = this;

            this.imgVidPosts = [];
            var obj = {
              'postId': item._id,
              'userId': this.userId
            };
            this.authService.deletePost(obj).subscribe(function (res) {
              console.log(res);

              _this4.authService.presentToast('Post Removed');

              _this4.loadData();
            });
          } // selectedSegment(x){
          //   if(x=="myPost"){
          //     this.selected="myPost";
          //     this.savedPost=false;
          //   }else {
          //     this.selected="savedPost";
          //     this.savedPost=true;
          //   }
          // }

        }, {
          key: "segmentChanged",
          value: function segmentChanged($event) {
            console.log($event);
            this.selectedTab = $event.detail.value;

            if (this.selectedTab == "myPost") {
              this.savedPost = false;
            } else {
              this.savedPost = true;
            }
          }
        }, {
          key: "didScroll",
          value: function didScroll($event) {
            var _this5 = this;

            if (this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
              return;
            } else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
              // item is out of view, pause it
              this.currentPlaying.pause();
              this.currentPlaying = null;
            }

            this.videoPlayers.forEach(function (player) {
              console.log(player);

              if (_this5.currentPlaying) {
                return;
              }

              var nativeElement = player.nativeElement;

              var inView = _this5.isElementInViewport(nativeElement);

              if (inView) {
                _this5.currentPlaying = nativeElement;
                _this5.currentPlaying.muted = true;

                _this5.currentPlaying.play();
              }
            });
          }
        }, {
          key: "openFullScreen",
          value: function openFullScreen(elem) {
            if (elem.requestFullscreen) {
              elem.requestFullscreen();
            } else if (elem.webkitEnterFullscreen) {
              elem.webkitEnterFullscreen();
              elem.enterFullscreen();
            }
          }
        }, {
          key: "isElementInViewport",
          value: function isElementInViewport(el) {
            var rect = el.getBoundingClientRect();
            return rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && rect.right <= (window.innerWidth || document.documentElement.clientWidth);
          }
        }, {
          key: "openFirst",
          value: function openFirst() {
            this.menu.enable(true, 'first');
            this.menu.open('first');
          }
        }, {
          key: "presentModal",
          value: function presentModal(item) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _question_question_page__WEBPACK_IMPORTED_MODULE_5__["QuestionPage"],
                        cssClass: 'socialSharingModal',
                        componentProps: {
                          postData: item
                        }
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      return _context.abrupt("return", _context.sent);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "editPost",
          value: function editPost(item) {
            var navigationExtras = {
              queryParams: {
                pageRoute: this.router.url,
                post: JSON.stringify(item),
                editPost: true
              }
            };
            this.navCtrl.navigateRoot(['post'], navigationExtras);
          }
        }, {
          key: "share",
          value: function share(item) {
            var message = item.postTitle + ' \n' + item.postDescription;
            var postUrl = item.postUrl;
            this.socialSharing.share(message, null, null, postUrl);
          }
        }]);

        return Tab2Page;
      }();

      Tab2Page.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_7__["SocialSharing"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      Tab2Page.propDecorators = {
        videoPlayers: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"],
          args: ['player']
        }]
      };
      Tab2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./tab2.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/tab2/tab2.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./tab2.page.scss */
        "./src/app/tab2/tab2.page.scss"))["default"]]
      })], Tab2Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=tab2-tab2-module-es5.js.map