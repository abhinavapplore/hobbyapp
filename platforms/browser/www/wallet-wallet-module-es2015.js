(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["wallet-wallet-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"toolbar1\">\n  <ion-row>\n    <ion-col [size]=2 (click)=\"back()\">\n      <ion-icon name=\"chevron-back\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8>\n      <ion-text>Wallet</ion-text>\n    </ion-col>\n    <ion-col [size]=2 (click)=\"notification()\">\n      <ion-icon name=\"notifications-outline\"></ion-icon>\n    </ion-col>    \n  </ion-row>   \n</ion-toolbar>\n\n<ion-content>\n\n  <div class=\"div\">\n    <ion-row class=\"row\">\n      <ion-col [size]=3 class=\"col1\">\n        <img class=\"img\" src=\"../../assets/images/wallet/credit-card.svg\">\n      </ion-col>\n      <ion-col [size]=9 class=\"col2\">\n        <ion-text class=\"text1\">Balance</ion-text>\n        <ion-text class=\"text2\">₹ {{walletBalance}}</ion-text>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"row1\" (click)=\"presentAlert()\">\n      <ion-text>Add Funds</ion-text>\n    </ion-row>\n    <ion-row class=\"row5\">\n      <ion-text>Transactions</ion-text>\n    </ion-row>\n    <div  *ngFor=\"let item of transactionData.slice().reverse()\">\n      <ion-row class=\"row2\" *ngIf=\"item.transactionType=='Add Money' && transactionData.length!=0\">\n        <ion-col [size]=3 class=\"col1\">\n          <ion-avatar>\n            <ion-img class=\"img\" [src]=\"item.transactionImage\"></ion-img>\n          </ion-avatar>\n        </ion-col>\n        <ion-col [size]=9 class=\"col2\">\n          <ion-row class=\"row3\">\n            <ion-text class=\"text2\">Add to wallet</ion-text>\n          </ion-row>\n          <ion-row class=\"row4\">\n            <ion-text class=\"text2\">₹ {{item.transactionAmount}}</ion-text>\n          </ion-row>\n          <ion-row class=\"row4\">\n            <ion-text class=\"text3\">{{item.transactionDate}}</ion-text>\n            <ion-text class=\"text4\">{{item.transactionTime}}</ion-text>\n          </ion-row>\n          <div class=\"centered\">\n            <img class=\"img\" src=\"../../assets/images/wallet/tick.png\">\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"row2\" *ngIf=\"item.transactionType=='Unlock User'&& transactionData.length!=0\">\n        <ion-col [size]=3 class=\"col1\">\n          <ion-avatar>\n            <ion-img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\"></ion-img>\n          </ion-avatar>\n        </ion-col>\n        <ion-col [size]=9 class=\"col2\">\n          <ion-row class=\"row3\">\n            <ion-text class=\"text2\">Unlocked <span class=\"text1\">{{item.userName}}</span></ion-text>\n          </ion-row>\n          <ion-row class=\"row4\">\n            <ion-text class=\"text2\">₹ {{item.transactionAmount}}</ion-text>\n          </ion-row>\n          <ion-row class=\"row4\">\n            <ion-text class=\"text3\">{{item.transactionDate}}</ion-text>\n            <ion-text class=\"text4\">{{item.transactionTime}}</ion-text>\n          </ion-row>\n          <div class=\"centered\">\n            <img class=\"img\" src=\"../../assets/images/wallet/tick.png\">\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n\n  <div *ngIf=\"transactionData.length==0\" class=\"div1\">\n    <ion-text>No Transactions</ion-text>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/wallet/wallet-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/wallet/wallet-routing.module.ts ***!
  \*************************************************/
/*! exports provided: WalletPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageRoutingModule", function() { return WalletPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wallet.page */ "./src/app/wallet/wallet.page.ts");




const routes = [
    {
        path: '',
        component: _wallet_page__WEBPACK_IMPORTED_MODULE_3__["WalletPage"]
    }
];
let WalletPageRoutingModule = class WalletPageRoutingModule {
};
WalletPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], WalletPageRoutingModule);



/***/ }),

/***/ "./src/app/wallet/wallet.module.ts":
/*!*****************************************!*\
  !*** ./src/app/wallet/wallet.module.ts ***!
  \*****************************************/
/*! exports provided: WalletPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _wallet_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./wallet-routing.module */ "./src/app/wallet/wallet-routing.module.ts");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wallet.page */ "./src/app/wallet/wallet.page.ts");







let WalletPageModule = class WalletPageModule {
};
WalletPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _wallet_routing_module__WEBPACK_IMPORTED_MODULE_5__["WalletPageRoutingModule"]
        ],
        declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]]
    })
], WalletPageModule);



/***/ }),

/***/ "./src/app/wallet/wallet.page.scss":
/*!*****************************************!*\
  !*** ./src/app/wallet/wallet.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.row {\n  width: 344px;\n  height: 99px;\n  background: #FFFFFF;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n}\n.row .col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.row .col1 .img {\n  width: 40px;\n}\n.row .col2 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n}\n.row .col2 .text1 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 16px;\n}\n.row .col2 .text2 {\n  font-family: \"Poppins-Bold\";\n  color: #000000;\n  font-size: 28px;\n  padding-top: 3%;\n}\n.row1 {\n  width: 258px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n  margin-top: 5%;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n}\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  color: #FFFFFF;\n  font-size: 18px;\n}\n.row5 {\n  padding-top: 5%;\n  width: 90%;\n}\n.row5 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n.row2 {\n  width: 344px;\n  height: 110px;\n  background: #FFFFFF;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n  margin-top: 5%;\n}\n.row2 .col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.row2 .col1 ion-row {\n  width: 100%;\n}\n.row2 .col1 ion-avatar {\n  height: 40px;\n  width: 40px;\n}\n.row2 .col2 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n  position: relative;\n}\n.row2 .col2 .centered {\n  position: absolute;\n  top: 15px;\n  left: 200px;\n}\n.row2 .col2 .centered .img {\n  width: 40px;\n}\n.row2 .col2 .row3 {\n  width: 100%;\n}\n.row2 .col2 .row4 {\n  width: 100%;\n  padding-top: 3%;\n}\n.row2 .col2 .text1 {\n  font-family: \"Poppins-Regular\";\n  color: #000000;\n  font-size: 14px;\n}\n.row2 .col2 .text2 {\n  font-family: \"Poppins-Bold\";\n  color: #000000;\n  font-size: 14px;\n}\n.row2 .col2 .text3 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 12px;\n}\n.row2 .col2 .text4 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 12px;\n  margin-left: 10%;\n}\n.div1 {\n  margin-top: 10%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.div1 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FsbGV0L3dhbGxldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsK0RBQUE7RUFDQSxnREFBQTtFQUNBLDRCQUFBO0FBQ0o7QUFBSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFFUjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUjtBQUFJO0VBQ0UsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBRU47QUFDRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBRUo7QUFBQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFHSjtBQUZJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUlSO0FBSFE7RUFDSSxXQUFBO0FBS1o7QUFGSTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsdUJBQUE7QUFJUjtBQUhRO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUtaO0FBSFU7RUFDSSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQUtkO0FBREE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw2REFBQTtBQUlKO0FBSEk7RUFDSSwyQkFBQTtFQUNNLGNBQUE7RUFDQSxlQUFBO0FBS2Q7QUFGQTtFQUNJLGVBQUE7RUFDQSxVQUFBO0FBS0o7QUFKSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFNUjtBQUhFO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFNSjtBQUxJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQU9SO0FBTlE7RUFDSSxXQUFBO0FBUVo7QUFOUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FBUVo7QUFMSTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQU9SO0FBTlE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBUVo7QUFQWTtFQUNJLFdBQUE7QUFTaEI7QUFOUTtFQUNJLFdBQUE7QUFRWjtBQU5RO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUFRWjtBQU5RO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQVFaO0FBTlU7RUFDSSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBUWQ7QUFOWTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFRaEI7QUFOWTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQVFoQjtBQUpBO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFPSjtBQU5JO0VBQ0ksOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQVFSIiwiZmlsZSI6InNyYy9hcHAvd2FsbGV0L3dhbGxldC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9vbGJhcjEge1xuICAgIGhlaWdodDogNzRweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDI1cHggMjVweDtcbiAgICBpb24tY29sIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgIH1cbiAgICBpb24tdGV4dCB7XG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gICAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICB9XG4gIH1cbiAgLmRpdiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xufVxuLnJvdyB7XG4gICAgd2lkdGg6IDM0NHB4O1xuICAgIGhlaWdodDogOTlweDtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgLmNvbDEge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgLmltZyB7XG4gICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAuY29sMiB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAgICAgLnRleHQxIHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIC50ZXh0MiB7XG4gICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAyOHB4O1xuICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgICAgICAgICB9XG4gICAgfSAgIFxufVxuLnJvdzEge1xuICAgIHdpZHRoOiAyNThweDtcbiAgICBoZWlnaHQ6IDU1cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgICAgICAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgfVxufVxuLnJvdzUge1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIH1cbiAgfVxuICAucm93MiB7XG4gICAgd2lkdGg6IDM0NHB4O1xuICAgIGhlaWdodDogMTEwcHg7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIC5jb2wxIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGlvbi1yb3cge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLWF2YXRhciB7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICB9XG4gICAgfVxuICAgIC5jb2wyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC5jZW50ZXJlZCB7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICB0b3A6IDE1cHg7XG4gICAgICAgICAgICBsZWZ0OiAyMDBweDtcbiAgICAgICAgICAgIC5pbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgLnJvdzMge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLnJvdzQge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgICAgIH1cbiAgICAgICAgLnRleHQxIHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIC50ZXh0MiB7XG4gICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnRleHQzIHtcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50ZXh0NCB7XG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMCU7XG4gICAgICAgICAgICAgIH1cbiAgICB9ICAgXG59XG4uZGl2MSB7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/wallet/wallet.page.ts":
/*!***************************************!*\
  !*** ./src/app/wallet/wallet.page.ts ***!
  \***************************************/
/*! exports provided: WalletPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPage", function() { return WalletPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







let WalletPage = class WalletPage {
    constructor(router, route, storage, alertController, authService) {
        this.router = router;
        this.route = route;
        this.storage = storage;
        this.alertController = alertController;
        this.authService = authService;
        this.transactionData = [];
        this.userData = {};
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                this.pageRoute = params.special;
            }
        });
    }
    ngOnInit() {
        this.storage.get('user').then((user) => {
            this.userData = user;
            this.userId = user._id;
            this.loadData();
        });
    }
    loadData() {
        this.transactionData = [];
        var obj = { "userId": this.userId };
        this.authService.getTransaction(obj).subscribe((data) => {
            console.log(data);
            this.walletBalance = data.data.wallet;
            this.userData = data.data;
            this.transactionData = data.transactionData;
        });
    }
    notification() {
        let navigationExtras = {
            queryParams: {
                special: this.router.url
            }
        };
        this.router.navigate(['notification'], navigationExtras);
    }
    back() {
        this.router.navigateByUrl(this.pageRoute);
    }
    presentAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'walletAlert',
                header: 'Enter Amount',
                inputs: [
                    {
                        name: 'amount',
                        type: 'text',
                        placeholder: '₹ 0.00'
                    }
                ],
                buttons: [
                    {
                        text: 'Add Funds',
                        role: 'submit',
                        cssClass: 'button',
                        handler: (alertData) => {
                            console.log(alertData);
                            var amount = alertData.amount;
                            this.payWithRazor(amount);
                        }
                    }
                ]
            });
            yield alert.present();
            const result = yield alert.onDidDismiss();
            console.log(result);
        });
    }
    payWithRazor(x) {
        var y = parseInt(x);
        var options = {
            description: 'Add money to wallet',
            image: 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
            currency: 'INR',
            key: 'rzp_test_sbuNGT8VxLZqff',
            amount: y * 100,
            theme: {
                color: '#02CBEE',
            },
            modal: {
                ondismiss: function () {
                    this.authService.presentAlert('Transaction Cancelled');
                }
            }
        };
        var self = this;
        var successCallback = function (payment_id) {
            // var obj = {'userId':self.userId,'amount':y}
            // self.authService.addMoney(obj).subscribe((data:any) => {
            //   console.log(data);
            // })
            var obj = { 'userId': self.userId, 'title': self.userData.fullName,
                'transactionImage': 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/credit-card.svg?alt=media&token=6b8e1044-7823-472f-a114-b8f36d74c3b2',
                'transactionAmount': y,
                'transactionDate': moment__WEBPACK_IMPORTED_MODULE_6__().format('DD/MMM/YYYY'), 'isAddingMoney': true,
                'transactionTime': moment__WEBPACK_IMPORTED_MODULE_6__().format("h:mm ' A"),
            };
            self.authService.addMoney(obj).subscribe((data) => {
                if (data.success) {
                    console.log(data);
                    self.userData.wallet = self.walletBalance + y;
                    self.storage.set('user', self.userData);
                    self.loadData();
                }
            });
            const successTransaction = self.authService.presentAlertSuccess('Transaction Completed');
            if (successTransaction) {
                self.loadData();
            }
        };
        var cancelCallback = function (error) {
            self.authService.presentAlert('Transaction Cancelled');
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
    }
};
WalletPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
WalletPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-wallet',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./wallet.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./wallet.page.scss */ "./src/app/wallet/wallet.page.scss")).default]
    })
], WalletPage);



/***/ })

}]);
//# sourceMappingURL=wallet-wallet-module-es2015.js.map