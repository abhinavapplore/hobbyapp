(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notification-notification-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppNotificationNotificationPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"toolbar1\">\n  <ion-row>\n    <ion-col [size]=2 (click)=\"back()\">\n      <ion-icon name=\"chevron-back\"></ion-icon>\n    </ion-col>\n    <ion-col [size]=8>\n      <ion-text>Notifications</ion-text>\n    </ion-col> \n  </ion-row>   \n</ion-toolbar>\n\n<ion-content>\n  <div>\n    <div class=\"div\" *ngFor='let item of notificationData'>\n      <!-- <div > -->\n        <ion-row class=\"row2\" *ngIf=\"item.notificationType=='like'\" >\n          <ion-col [size]=3 class=\"col1\">\n            <ion-avatar>\n              <ion-img class=\"img\" [src]=\"item.transactionImage\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col [size]=9 class=\"col2\">\n            <ion-row class=\"row3\">\n              <ion-text class=\"text2\">Add to wallet</ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text2\">₹ {{item.transactionAmount}}</ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text3\">{{item.transactionDate}}</ion-text>\n              <ion-text class=\"text4\">{{item.transactionTime}}</ion-text>\n            </ion-row>\n            <div class=\"centered\">\n              <img class=\"img\" src=\"../../assets/images/wallet/tick.png\">\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row class=\"row2\" *ngIf=\"item.notificationType=='unlock'\">\n          <ion-col [size]=3 class=\"col1\">\n            <ion-avatar>\n              <ion-img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col [size]=9 class=\"col2\">\n            <ion-row class=\"row3\">\n              <ion-text class=\"text2\">Unlocked <span class=\"text1\">{{item.userName}}</span></ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text2\">₹ {{item.transactionAmount}}</ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text3\">{{item.transactionDate}}</ion-text>\n              <ion-text class=\"text4\">{{item.transactionTime}}</ion-text>\n            </ion-row>\n            <div class=\"centered\">\n              <img class=\"img\" src=\"../../assets/images/wallet/tick.png\">\n            </div>\n          </ion-col>\n        </ion-row>\n      <!-- </div> -->\n    </div>\n  </div>\n\n  \n\n  <div class=\"div1\" *ngIf='notificationData.length==0'>\n    <ion-row class=\"row5\" style=\"margin-top: 5%;\">\n      <ion-text class=\"text5\">No Notification</ion-text>\n    </ion-row>\n  </div>\n  \n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/notification/notification-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/notification/notification-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: NotificationPageRoutingModule */

    /***/
    function srcAppNotificationNotificationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationPageRoutingModule", function () {
        return NotificationPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _notification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./notification.page */
      "./src/app/notification/notification.page.ts");

      var routes = [{
        path: '',
        component: _notification_page__WEBPACK_IMPORTED_MODULE_3__["NotificationPage"]
      }];

      var NotificationPageRoutingModule = function NotificationPageRoutingModule() {
        _classCallCheck(this, NotificationPageRoutingModule);
      };

      NotificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NotificationPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/notification/notification.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/notification/notification.module.ts ***!
      \*****************************************************/

    /*! exports provided: NotificationPageModule */

    /***/
    function srcAppNotificationNotificationModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function () {
        return NotificationPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./notification-routing.module */
      "./src/app/notification/notification-routing.module.ts");
      /* harmony import */


      var _notification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./notification.page */
      "./src/app/notification/notification.page.ts");

      var NotificationPageModule = function NotificationPageModule() {
        _classCallCheck(this, NotificationPageModule);
      };

      NotificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationPageRoutingModule"]],
        declarations: [_notification_page__WEBPACK_IMPORTED_MODULE_6__["NotificationPage"]]
      })], NotificationPageModule);
      /***/
    },

    /***/
    "./src/app/notification/notification.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/notification/notification.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppNotificationNotificationPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.row2 {\n  width: 344px;\n  height: 110px;\n  background: #FFFFFF;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n  margin-top: 5%;\n}\n.row2 .col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.row2 .col1 ion-row {\n  width: 100%;\n}\n.row2 .col1 ion-avatar {\n  height: 50px;\n  width: 50px;\n}\n.row2 .col2 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n  position: relative;\n}\n.row2 .col2 .centered {\n  position: absolute;\n  top: 20px;\n  left: 200px;\n}\n.row2 .col2 .centered .img {\n  width: 40px;\n}\n.row2 .col2 .row3 {\n  width: 100%;\n}\n.row2 .col2 .row4 {\n  width: 100%;\n  padding-top: 3%;\n}\n.row2 .col2 .text1 {\n  font-family: \"Poppins-Regular\";\n  color: #000000;\n  font-size: 14px;\n  margin-left: 3%;\n}\n.row2 .col2 .text2 {\n  font-family: \"Poppins-Bold\";\n  color: #000000;\n  font-size: 14px;\n}\n.row2 .col2 .text3 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 12px;\n}\n.row2 .col2 .text4 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 12px;\n  margin-left: 10%;\n}\n.text5 {\n  font-family: \"Poppins-Bold\";\n  color: #000000;\n  font-size: 14px;\n}\n.row5 {\n  width: 90%;\n  justify-content: center;\n}\n.div1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsK0RBQUE7RUFDQSxnREFBQTtFQUNBLDRCQUFBO0FBQ0o7QUFBSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFFUjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUjtBQUFJO0VBQ0UsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBRU47QUFDRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBRUo7QUFBQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBR0o7QUFGSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFJUjtBQUhRO0VBQ0ksV0FBQTtBQUtaO0FBSFE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQUtaO0FBRkk7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUFJUjtBQUhRO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQUtaO0FBSlk7RUFDSSxXQUFBO0FBTWhCO0FBSFE7RUFDSSxXQUFBO0FBS1o7QUFIUTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FBS1o7QUFIUTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBS1o7QUFIVTtFQUNJLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFLZDtBQUhZO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUtoQjtBQUhZO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBS2hCO0FBREE7RUFDRSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBSUY7QUFGQTtFQUNFLFVBQUE7RUFDQSx1QkFBQTtBQUtGO0FBSEE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQU1GIiwiZmlsZSI6InNyYy9hcHAvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9vbGJhcjEge1xuICAgIGhlaWdodDogNzRweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDI1cHggMjVweDtcbiAgICBpb24tY29sIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgIH1cbiAgICBpb24tdGV4dCB7XG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gICAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICB9XG4gIH1cbiAgLmRpdiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xufVxuLnJvdzIge1xuICAgIHdpZHRoOiAzNDRweDtcbiAgICBoZWlnaHQ6IDExMHB4O1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICAuY29sMSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBpb24tcm93IHtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIGlvbi1hdmF0YXIge1xuICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgfVxuICAgIH1cbiAgICAuY29sMiB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAuY2VudGVyZWQge1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgdG9wOiAyMHB4O1xuICAgICAgICAgICAgbGVmdDogMjAwcHg7XG4gICAgICAgICAgICAuaW1nIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIC5yb3czIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIC5yb3c0IHtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDMlO1xuICAgICAgICB9XG4gICAgICAgIC50ZXh0MSB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICAgICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDMlO1xuICAgICAgICAgIH1cbiAgICAgICAgICAudGV4dDIge1xuICAgICAgICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50ZXh0MyB7XG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAudGV4dDQge1xuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTAlO1xuICAgICAgICAgICAgICB9XG4gICAgfSAgIFxufVxuLnRleHQ1IHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ucm93NSB7XG4gIHdpZHRoOiA5MCU7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLmRpdjEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/notification/notification.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/notification/notification.page.ts ***!
      \***************************************************/

    /*! exports provided: NotificationPage */

    /***/
    function srcAppNotificationNotificationPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationPage", function () {
        return NotificationPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");

      var NotificationPage = /*#__PURE__*/function () {
        function NotificationPage(router, route, storage, authService) {
          var _this = this;

          _classCallCheck(this, NotificationPage);

          this.router = router;
          this.route = route;
          this.storage = storage;
          this.authService = authService;
          this.notificationData = [];
          this.userData = {};
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              _this.pageRoute = params.special;
            }
          });
        }

        _createClass(NotificationPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.storage.get('user').then(function (user) {
              _this2.userData = user;
              _this2.userId = user._id;

              _this2.loadData();
            });
          }
        }, {
          key: "loadData",
          value: function loadData() {
            var _this3 = this;

            this.notificationData = [];
            var obj = {
              "userId": this.userId
            };
            this.authService.getNotification(obj).subscribe(function (data) {
              console.log(data);

              if (data.success) {
                _this3.walletBalance = data.data.wallet;
                _this3.notificationData = data.notificationData;
              }
            });
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl(this.pageRoute);
          }
        }]);

        return NotificationPage;
      }();

      NotificationPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }];
      };

      NotificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notification',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./notification.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./notification.page.scss */
        "./src/app/notification/notification.page.scss"))["default"]]
      })], NotificationPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=notification-notification-module-es5.js.map