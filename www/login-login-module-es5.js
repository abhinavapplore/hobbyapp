(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n  <ion-row class=\"row1\">\n    <img class=\"img\" src=\"../../assets/images/login/logo (1).svg\">\n  </ion-row>\n\n<div class=\"div\">\n  \n  <ion-row class=\"row\">\n    <ion-text class=\"text\">Enter your phone number</ion-text>\n  </ion-row>\n\n  <ion-row class=\"row2\">\n    <div class=\"div2\">\n      <ion-col [size]=3 class=\"col1\">\n        <img class=\"img1\" src=\"../../assets/images/login/india.svg\">\n        <ion-icon name=\"chevron-down-outline\"></ion-icon>\n          <!-- <ion-select selected value=\"India\" okText=\"Okay\" cancelText=\"Dismiss\">\n            <ion-select-option value={{item.FlagPng}}  *ngFor=\"let item of allCountry\">\n              <ion-img [src]=\"item.FlagPng\"></ion-img>\n            </ion-select-option> -->\n          <!-- </ion-select> -->\n        \n      </ion-col>\n      <ion-col [size]=9 class=\"col\">\n          <ion-input type=\"tel\" [(ngModel)]=\"mobile\"\n          placeholder=\"Mobile Number\" class=\"Input\"></ion-input>\n      </ion-col>\n    </div>      \n  </ion-row>\n\n  <ion-row class=\"row3\" (click)=\"signup1()\">\n    <div class=\"button\">\n      <ion-text class=\"signin\">SIGN IN</ion-text>\n    </div>\n  </ion-row>\n\n  <ion-row class=\"row3\" (click)=\"googleSignIn()\">\n    <div style=\"width: 260px;\n    height: 55px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-evenly;\n    align-items: center;\n    overflow: visible;\n    background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n    box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n    border-radius: 10px;\">\n      <ion-avatar>\n        <img src=\"../../assets/images/google.jpg\">\n      </ion-avatar>\n      <ion-text class=\"signin\">GOOGLE SIGN IN</ion-text>\n    </div>\n  </ion-row>\n</div>\n  \n\n</ion-content>\n\n\n\n\n\n\n\n\n\n\n<!-- My Changes -->\n<!-- <ion-content>\n\n  <ion-row class=\"row1\">\n    <img class=\"img\" src=\"../../assets/images/login/logo (1).svg\">\n  </ion-row>\n\n\n  \n  <div *ngIf=\"!initial && loginMobile\">\n    <ion-row class=\"row\">\n      <ion-text class=\"text\">Enter your phone number</ion-text>\n    </ion-row>\n  \n    <ion-row class=\"row2\">\n      <div class=\"div2\">\n        <ion-col [size]=3 class=\"col1\">\n          <img class=\"img1\" src=\"../../assets/images/login/india.svg\">\n          <ion-icon name=\"chevron-down-outline\"></ion-icon>\n         \n          \n        </ion-col>\n        <ion-col [size]=9 class=\"col\">\n            <ion-input type=\"tel\" [(ngModel)]=\"mobile\"\n            placeholder=\"Mobile Number\" class=\"Input\"></ion-input>\n        </ion-col>\n      </div>      \n    </ion-row>\n  \n    <ion-row class=\"row3\" (click)=\"signup()\">\n      <div class=\"button\">\n        <ion-text class=\"signin\">SIGN IN</ion-text>\n      </div>\n    </ion-row>\n  </div>\n\n\n  <div *ngIf=\"!initial && !loginMobile\">\n\n\n    <ion-row>\n      <ion-input type=\"email\" [(ngModel)]=\"email\"\n      placeholder=\"Email\" class=\"Input\"></ion-input>\n    </ion-row>\n \n\n    <ion-row class=\"row3\" (click)=\"signup()\">\n      <div class=\"button\">\n        <ion-text class=\"signin\">SIGN IN</ion-text>\n      </div>\n    </ion-row>\n\n  </div>\n  \n<div *ngIf=\"initial\">\n\n  <ion-row class=\"row3\" (click)=\"signUpMobile()\">\n    <div class=\"button\">\n      <ion-text class=\"signin\">SIGN IN Via Mobile</ion-text>\n    </div>\n  </ion-row>\n\n  <ion-row class=\"row3\" (click)=\"signUpEmail()\">\n    <div class=\"button\">\n      <ion-text class=\"signin\">SIGN IN Via Email</ion-text>\n    </div>\n  </ion-row>\n</div>\n \n\n\n  \n\n\n\n  \n\n</ion-content> -->\n\n<!-- END -->\n";
      /***/
    },

    /***/
    "./src/app/login/login-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/login/login-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/login/login.module.ts":
    /*!***************************************!*\
      !*** ./src/app/login/login.module.ts ***!
      \***************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/login/login.page.scss":
    /*!***************************************!*\
      !*** ./src/app/login/login.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".row {\n  justify-content: center;\n}\n\n.row1 {\n  justify-content: center;\n  margin-top: 20%;\n}\n\n.row2 {\n  justify-content: center;\n  margin-top: 5%;\n}\n\n.row3 {\n  justify-content: center;\n  margin-top: 7%;\n}\n\n.img {\n  width: 50%;\n  height: 50%;\n}\n\n.text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 18px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n  font-style: normal;\n  text-align: center;\n}\n\n.signin {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 16px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n\nion-input {\n  --backgroundColor:#EBEBEB;\n  font-family: \"Poppins-Bold\";\n  font-size: 15px;\n  --placeholder-color:#333;\n}\n\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 50%;\n}\n\n.img1 {\n  width: 65%;\n}\n\nion-icon {\n  color: #888888;\n  font-size: 35px;\n}\n\n.col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  height: 115%;\n  border: 1px solid #EBEBEB;\n  border-radius: 10px;\n  background: #EBEBEB;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.div2 {\n  width: 260px;\n  height: 45px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n}\n\nion-avatar {\n  width: 30px;\n  height: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdUJBQUE7QUFDSjs7QUFDQTtFQUNJLHVCQUFBO0VBQ0EsZUFBQTtBQUVKOztBQUFBO0VBQ0ksdUJBQUE7RUFDQSxjQUFBO0FBR0o7O0FBREE7RUFDSSx1QkFBQTtFQUNBLGNBQUE7QUFJSjs7QUFGQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0FBS0o7O0FBSEE7RUFDSSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBTUo7O0FBSkE7RUFDSSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFPSjs7QUFMRTtFQUNFLHlCQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0VBQ0Esd0JBQUE7QUFRSjs7QUFORTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FBU047O0FBUEU7RUFDRSxVQUFBO0FBVUo7O0FBUkU7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQVdOOztBQVRFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFZSjs7QUFWRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7QUFhSjs7QUFYRTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FBY0o7O0FBWkU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQWVKIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJvd3tcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5yb3cxe1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDIwJTtcbn1cbi5yb3cye1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDUlO1xufVxuLnJvdzN7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogNyU7XG59XG4uaW1nIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIGhlaWdodDogNTAlO1xufVxuLnRleHQge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgY29sb3I6ICMwMDNDNjk7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbi5zaWduaW4ge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICB9XG4gIGlvbi1pbnB1dCB7XG4gICAgLS1iYWNrZ3JvdW5kQ29sb3I6I0VCRUJFQjtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgLS1wbGFjZWhvbGRlci1jb2xvcjojMzMzO1xuICB9XG4gIC5kaXYge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBoZWlnaHQ6IDUwJTtcbiAgfVxuICAuaW1nMSB7XG4gICAgd2lkdGg6IDY1JTtcbiAgfVxuICBpb24taWNvbiB7XG4gICAgICBjb2xvcjogIzg4ODg4ODtcbiAgICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgfVxuICAuY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDExNSU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VCRUJFQjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6ICNFQkVCRUI7XG4gIH1cbiAgLmNvbDEge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuZGl2MiB7XG4gICAgd2lkdGg6IDI2MHB4O1xuICAgIGhlaWdodDogNDVweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgfVxuICBpb24tYXZhdGFyIHtcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IDMwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/login/login.page.ts":
    /*!*************************************!*\
      !*** ./src/app/login/login.page.ts ***!
      \*************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/fcm/ngx */
      "./node_modules/@ionic-native/fcm/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/google-plus/ngx */
      "./node_modules/@ionic-native/google-plus/__ivy_ngcc__/ngx/index.js");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(authService, storage, fcm, router, route, httpClient, googlePlus, navCtrl) {
          _classCallCheck(this, LoginPage);

          this.authService = authService;
          this.storage = storage;
          this.fcm = fcm;
          this.router = router;
          this.route = route;
          this.httpClient = httpClient;
          this.googlePlus = googlePlus;
          this.navCtrl = navCtrl;
          this.allCountry = [];
          this.userData = {};
          this.initial = true;
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            console.log('hello');
            this.fcm.getToken().then(function (token) {
              _this.deviceKey = token;
              console.log(_this.deviceKey);
              console.log('device key....');
            }); // this.httpClient.get<any>('https://cors-anywhere.herokuapp.com/http://countryapi.gear.host/v1/Country/getCountries').subscribe((res) => {
            //   console.log(res);
            //   this.allCountry=res.Response;
            // });
          }
        }, {
          key: "signup",
          value: function signup() {
            var _this2 = this;

            if (this.mobile.length != 10) {
              this.authService.presentToast('Please Enter Valid Mobile Number');
            } else {
              this.storage.set('isEmailLogin', false).then(function (result) {
                var vc = {
                  mobile: _this2.mobile
                };
                var navigationExtras = {
                  queryParams: {
                    special: JSON.stringify(vc)
                  }
                };

                _this2.router.navigate(['verify'], navigationExtras);
              });
            }
          }
        }, {
          key: "signUpMobile",
          value: function signUpMobile() {
            this.initial = false;
            this.loginMobile = true;
          }
        }, {
          key: "signUpEmail",
          value: function signUpEmail() {
            this.initial = false;
            this.loginMobile = false;
          }
        }, {
          key: "signup1",
          value: function signup1() {
            var _this3 = this;

            console.log('heyyyyyyyyyy');

            if (this.mobile.length != 10) {
              this.authService.presentToast('Please Enter Valid Mobile Number');
            } else {
              var obj = {
                'contactNum': this.mobile,
                'deviceId': this.deviceKey
              };
              this.authService.login(obj).subscribe(function (data) {
                console.log(data);

                if (data.success) {
                  _this3.storage.set('user', data.user);

                  _this3.navCtrl.navigateRoot('tabs/tab4');
                } else {
                  _this3.storage.set("contactNum", _this3.mobile).then(function (res) {
                    console.log(res);

                    if (res != null) {
                      _this3.navCtrl.navigateRoot('hobby');
                    }
                  });
                }
              });
            }
          }
        }, {
          key: "googleSignIn",
          value: function googleSignIn() {
            var _this4 = this;

            this.googlePlus.login({}).then(function (result) {
              _this4.authService.loading('Fetching Your details');

              console.log(result);
              console.log(result.email);
              console.log(result.email);
              var emailId = result.email;
              var firstName = result.givenName;
              var lastName = result.familyName;
              var s = result.familyName;
              var ar = [];
              ar = s.split();
              var fullName = result.givenName + " " + ar[0];
              var obj = {
                emailId: emailId,
                emailIdRegistered: true
              };

              _this4.authService.login(obj).subscribe(function (res) {
                if (res.success == false) {
                  _this4.storage.set('emailId', emailId).then(function (email) {
                    _this4.storage.set('isEmailLogin', true).then(function (response) {
                      _this4.router.navigateByUrl('hobby');
                    });
                  });

                  _this4.authService.dismissLoading();
                } else {
                  _this4.storage.set("user", res.user);

                  console.log(res.user);

                  _this4.authService.dismissLoading();

                  _this4.router.navigateByUrl('tabs/tab4');
                }
              });
            })["catch"](function (err) {
              return _this4.userData = "Error ".concat(JSON.stringify(err));
            });
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
        }, {
          type: _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_6__["FCM"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }, {
          type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_8__["GooglePlus"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-module-es5.js.map