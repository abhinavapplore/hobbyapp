(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~question-question-module~tab1-tab1-module~tab2-tab2-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/question/question.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/question/question.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppQuestionQuestionPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-row style=\"padding-top: 10%;\">\n    <ion-col size=\"3\" style=\"display: flex;flex-direction: column;\n    justify-content: center;align-items: center;\" (click)=\"instagramShare()\">\n     <ion-icon style=\"font-size: 50px;\" name=\"logo-instagram\"></ion-icon>\n     <ion-text>Instagram</ion-text>\n    </ion-col>\n    <ion-col size=\"3\" style=\"display: flex;flex-direction: column;\n    justify-content: center;align-items: center;\" (click)=\"facebookShare()\">\n     <ion-icon style=\"font-size: 50px;\" name=\"logo-facebook\"></ion-icon>\n     <ion-text>Facebook</ion-text>\n    </ion-col>\n    <ion-col size=\"3\" style=\"display: flex;flex-direction: column;\n    justify-content: center;align-items: center;\" (click)=\"twitterShare()\">\n     <ion-icon style=\"font-size: 50px;\" name=\"logo-twitter\"></ion-icon>\n     <ion-text>Twitter</ion-text>\n    </ion-col>\n    <ion-col size=\"3\" style=\"display: flex;flex-direction: column;\n    justify-content: center;align-items: center;\" (click)=\"whatsappShare()\">\n     <ion-icon style=\"font-size: 50px;\" name=\"logo-whatsapp\"></ion-icon>\n     <ion-text>Whatsapp</ion-text>\n    </ion-col>\n  </ion-row>\n \n  <ion-row style=\"justify-content: center;padding-top: 10%;\" (click)=\"closeModal()\">\n    <ion-button class=\"button\" expand='block' >Close</ion-button>\n  </ion-row>\n </ion-content>";
      /***/
    },

    /***/
    "./src/app/question/question.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/question/question.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppQuestionQuestionPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n\n.row ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row1 {\n  width: 90%;\n  justify-content: center;\n}\n\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #4FDC56;\n}\n\n.row2 {\n  width: 276px;\n  height: 75px;\n  overflow: visible;\n  background-color: #FBFBFB;\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n  margin-top: 7%;\n}\n\n.row2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row3 {\n  width: 90%;\n  justify-content: space-evenly;\n  padding-top: 5%;\n}\n\n.row3 .col1 {\n  width: 95px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background-color: #fafdff;\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row3 .col1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #000000;\n}\n\n.row3 .col2 {\n  width: 95px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row3 .col2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n\n.row4 {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n\n.row4 .col {\n  width: 205px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcXVlc3Rpb24vcXVlc3Rpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBO0VBQ0ksVUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQUVKOztBQURJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUdSOztBQUFBO0VBQ0ksVUFBQTtFQUNBLHVCQUFBO0FBR0o7O0FBRkk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBSVI7O0FBREE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSwrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFJSjs7QUFISTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFLUjs7QUFGQTtFQUNJLFVBQUE7RUFDQSw2QkFBQTtFQUNBLGVBQUE7QUFLSjs7QUFKSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxnREFBQTtFQUNBLG1CQUFBO0FBTVI7O0FBTFE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBT1o7O0FBSkk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDZEQUFBO0VBQ0EsZ0RBQUE7RUFDQSxtQkFBQTtBQU1SOztBQUxRO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQU9aOztBQUhBO0VBQ0ksVUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQU1KOztBQUxJO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSw2REFBQTtFQUNBLGdEQUFBO0VBQ0EsbUJBQUE7QUFPUiIsImZpbGUiOiJzcmMvYXBwL3F1ZXN0aW9uL3F1ZXN0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kaXYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnJvdyB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgY29sb3I6ICMwMDNDNjk7XG4gICAgfVxufVxuLnJvdzEge1xuICAgIHdpZHRoOiA5MCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgY29sb3I6ICM0RkRDNTY7XG4gICAgfVxufVxuLnJvdzIge1xuICAgIHdpZHRoOiAyNzZweDtcbiAgICBoZWlnaHQ6IDc1cHg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZCRkJGQjtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDclO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIH1cbn1cbi5yb3czIHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAuY29sMSB7XG4gICAgICAgIHdpZHRoOiA5NXB4O1xuICAgICAgICBoZWlnaHQ6IDU1cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmRmZjtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAuY29sMiB7XG4gICAgICAgIHdpZHRoOiA5NXB4O1xuICAgICAgICBoZWlnaHQ6IDU1cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICAgICAgfVxuICAgIH1cbn1cbi5yb3c0IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAuY29sIHtcbiAgICAgICAgd2lkdGg6IDIwNXB4O1xuICAgICAgICBoZWlnaHQ6IDU1cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIH1cbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/question/question.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/question/question.page.ts ***!
      \*******************************************/

    /*! exports provided: QuestionPage */

    /***/
    function srcAppQuestionQuestionPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "QuestionPage", function () {
        return QuestionPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/social-sharing/ngx */
      "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");

      var QuestionPage = /*#__PURE__*/function () {
        function QuestionPage(modalController, socialSharing, authService) {
          _classCallCheck(this, QuestionPage);

          this.modalController = modalController;
          this.socialSharing = socialSharing;
          this.authService = authService;
        }

        _createClass(QuestionPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.message = this.postData.postTitle + ' \n' + this.postData.postDescription;
            this.postUrl = this.postData.postUrl;
            console.log(this.message);
            console.log(this.postUrl);
          }
        }, {
          key: "facebookShare",
          value: function facebookShare() {
            var _this = this;

            this.socialSharing.shareViaFacebook(this.message, this.postUrl, null).then(function (success) {
              console.log(success);
            })["catch"](function (err) {
              _this.authService.presentToast("Could not share information");

              console.log(err);
            });
          }
        }, {
          key: "twitterShare",
          value: function twitterShare() {
            var _this2 = this;

            this.socialSharing.shareViaTwitter(this.message, this.postUrl, null).then(function (success) {
              console.log(success);
            })["catch"](function (err) {
              _this2.authService.presentToast("Could not share information");

              console.log(err);
            });
          }
        }, {
          key: "whatsappShare",
          value: function whatsappShare() {
            var _this3 = this;

            this.socialSharing.shareViaWhatsApp(this.message, this.postUrl, null).then(function (success) {
              console.log(success);
            })["catch"](function (err) {
              _this3.authService.presentToast("Could not share information");

              console.log(err);
            });
          }
        }, {
          key: "instagramShare",
          value: function instagramShare() {
            var _this4 = this;

            this.socialSharing.shareViaInstagram(this.message, this.postUrl).then(function (success) {
              console.log(success);
            })["catch"](function (err) {
              _this4.authService.presentToast("Could not share information");

              console.log(err);
            });
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalController.dismiss();
          }
        }]);

        return QuestionPage;
      }();

      QuestionPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__["SocialSharing"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }];
      };

      QuestionPage.propDecorators = {
        postData: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
          args: ['postData']
        }]
      };
      QuestionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-question',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./question.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/question/question.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./question.page.scss */
        "./src/app/question/question.page.scss"))["default"]]
      })], QuestionPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~question-question-module~tab1-tab1-module~tab2-tab2-module-es5.js.map