(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~profile1-profile1-module~unlock-contact1-unlock-contact1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/unlock-contact1/unlock-contact1.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/unlock-contact1/unlock-contact1.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n  <div class=\"div\">\n\n    <ion-row style=\"margin-top: 2%;\n  font-size: 30px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  width: 97%;\">\n    <ion-icon (click)=\"closeModal()\" name=\"close-outline\"></ion-icon>\n  </ion-row>\n\n    <ion-row class=\"row\">\n      <ion-text>Connect with <b style=\"text-transform: capitalize;\"> {{hostName}} </b></ion-text>\n    </ion-row>\n    <ion-row class=\"row1\">\n      <ion-text>5 Credits ?</ion-text>\n    </ion-row>\n    <ion-row *ngIf='selected' class=\"row5\" (click)='unSelect()'>\n      <img src=\"../../assets/images/modal/socialdistancing.svg\">\n      <ion-text>5 Credits</ion-text>\n      <ion-text>₹ 10</ion-text>\n    </ion-row>\n    <ion-row *ngIf='!selected' class=\"row2\" (click)='select(\"10\")'>\n      <img src=\"../../assets/images/modal/socialdistancing.svg\">\n      <ion-text>5 Credits</ion-text>\n      <ion-text>₹ 10</ion-text>\n    </ion-row>\n    <ion-row *ngIf='selected1' class=\"row5\" (click)='unSelect1()'>\n      <img src=\"../../assets/images/modal/socialdistancing.svg\">\n      <ion-text>10 Credits</ion-text>\n      <ion-text>₹ 15</ion-text>\n    </ion-row>\n    <ion-row *ngIf='!selected1' class=\"row2\" (click)='select1(\"15\")'>\n      <img src=\"../../assets/images/modal/socialdistancing.svg\">\n      <ion-text>10 Credits</ion-text>\n      <ion-text>₹ 15</ion-text>\n    </ion-row>\n    <ion-row *ngIf='selected || selected1' class=\"row4\" (click)=\"buy()\">\n        <div class=\"col\">\n          <ion-text>BUY</ion-text>\n        </div>\n    </ion-row>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/unlock-contact1/unlock-contact1.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/unlock-contact1/unlock-contact1.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n\n.row ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row1 {\n  width: 90%;\n  justify-content: center;\n}\n\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #4FDC56;\n}\n\n.row2 {\n  width: 276px;\n  height: 75px;\n  overflow: visible;\n  background-color: #FBFBFB;\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n  margin-top: 7%;\n}\n\n.row2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row3 {\n  width: 90%;\n  justify-content: space-evenly;\n  padding-top: 5%;\n}\n\n.row3 .col1 {\n  width: 95px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background-color: #fafdff;\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row3 .col1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #000000;\n}\n\n.row3 .col2 {\n  width: 95px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row3 .col2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n\n.row4 {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n\n.row4 .col {\n  width: 205px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row4 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n\n.row5 {\n  width: 276px;\n  height: 75px;\n  overflow: visible;\n  background-color: #FBFBFB;\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border: 2px solid #59E022;\n  border-radius: 10px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n  margin-top: 7%;\n}\n\n.row5 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdW5sb2NrLWNvbnRhY3QxL3VubG9jay1jb250YWN0MS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBQ0E7RUFDSSxVQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBRUo7O0FBREk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBR1I7O0FBQUE7RUFDSSxVQUFBO0VBQ0EsdUJBQUE7QUFHSjs7QUFGSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFJUjs7QUFEQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLCtDQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUlKOztBQUhJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUtSOztBQUZBO0VBQ0ksVUFBQTtFQUNBLDZCQUFBO0VBQ0EsZUFBQTtBQUtKOztBQUpJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGdEQUFBO0VBQ0EsbUJBQUE7QUFNUjs7QUFMUTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFPWjs7QUFKSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkRBQUE7RUFDQSxnREFBQTtFQUNBLG1CQUFBO0FBTVI7O0FBTFE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBT1o7O0FBSEE7RUFDSSxVQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBTUo7O0FBTEk7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDZEQUFBO0VBQ0EsZ0RBQUE7RUFDQSxtQkFBQTtBQU9SOztBQUxJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQU9SOztBQUpBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsK0NBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFPSjs7QUFOSTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFRUiIsImZpbGUiOiJzcmMvYXBwL3VubG9jay1jb250YWN0MS91bmxvY2stY29udGFjdDEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ucm93IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBjb2xvcjogIzAwM0M2OTtcbiAgICB9XG59XG4ucm93MSB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBjb2xvcjogIzRGREM1NjtcbiAgICB9XG59XG4ucm93MiB7XG4gICAgd2lkdGg6IDI3NnB4O1xuICAgIGhlaWdodDogNzVweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkJGQkZCO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogNyU7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgY29sb3I6ICMwMDNDNjk7XG4gICAgfVxufVxuLnJvdzMge1xuICAgIHdpZHRoOiA5MCU7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIC5jb2wxIHtcbiAgICAgICAgd2lkdGg6IDk1cHg7XG4gICAgICAgIGhlaWdodDogNTVweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmFmZGZmO1xuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIGlvbi10ZXh0IHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5jb2wyIHtcbiAgICAgICAgd2lkdGg6IDk1cHg7XG4gICAgICAgIGhlaWdodDogNTVweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIGlvbi10ZXh0IHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgICAgICB9XG4gICAgfVxufVxuLnJvdzQge1xuICAgIHdpZHRoOiA5MCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIC5jb2wge1xuICAgICAgICB3aWR0aDogMjA1cHg7XG4gICAgICAgIGhlaWdodDogNTVweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgfVxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIH1cbn1cbi5yb3c1IHtcbiAgICB3aWR0aDogMjc2cHg7XG4gICAgaGVpZ2h0OiA3NXB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGQkZCRkI7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYm9yZGVyOiAycHggc29saWQgIzU5RTAyMjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDclO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/unlock-contact1/unlock-contact1.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/unlock-contact1/unlock-contact1.page.ts ***!
  \*********************************************************/
/*! exports provided: UnlockContact1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnlockContact1Page", function() { return UnlockContact1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







let UnlockContact1Page = class UnlockContact1Page {
    constructor(modalController, authService, storage, route, alertController) {
        this.modalController = modalController;
        this.authService = authService;
        this.storage = storage;
        this.route = route;
        this.alertController = alertController;
        this.selected = false;
        this.selected1 = false;
        this.proffessionalUser = {};
        this.user = {};
    }
    ngOnInit() {
        console.log(this.professionalUser);
        console.log('hey.................');
        this.storage.get('user').then((user) => {
            this.user = user;
            this.userId = user._id;
            this.userName = user.fullName;
            this.walletBalance = user.wallet;
            this.hostName = this.professionalUser.fullName;
        });
    }
    select(x) {
        this.selected = true;
        this.selected1 = false;
        this.selectedAmount = parseInt(x);
        this.newAmount = this.selectedAmount * 100;
        if (this.selectedAmount == 10) {
            this.credit = 5;
        }
        else {
            this.credit = 10;
        }
    }
    select1(x) {
        this.selected1 = true;
        this.selected = false;
        this.selectedAmount = parseInt(x);
        this.newAmount = this.selectedAmount * 100;
        if (this.selectedAmount == 15) {
            this.credit = 10;
        }
        else {
            this.credit = 5;
        }
    }
    unSelect() {
        this.selected = false;
        this.selected1 = false;
        this.selectedAmount = '';
        this.credit = 0;
    }
    unSelect1() {
        this.selected = false;
        this.selected1 = false;
        this.selectedAmount = '';
        this.credit = 0;
    }
    buy() {
        this.todayDate = moment__WEBPACK_IMPORTED_MODULE_6__().format('DD/MMM/YYYY');
        console.log(this.todayDate);
        if (this.selectedAmount != '' || this.selectedAmount != undefined || this.selectedAmount != null) {
            var options = {
                description: 'Add money to wallet',
                image: 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
                currency: 'INR',
                key: 'rzp_test_sbuNGT8VxLZqff',
                amount: this.newAmount,
                theme: {
                    color: '#02CBEE',
                },
                modal: {
                    ondismiss: function () {
                        this.presentAlert('Transaction Cancelled');
                    }
                }
            };
            var self = this;
            var successCallback = function (payment_id) {
                var obj = { 'userId': self.userId, 'title': self.professionalUser.fullName,
                    'transactionImage': self.professionalUser.user_img,
                    'amount': self.credit, "deviceId": self.professionalUser.deviceId,
                    'transactionDate': moment__WEBPACK_IMPORTED_MODULE_6__().format('DD/MMM/YYYY'), 'isAddingMoney': true,
                    'transactionTime': moment__WEBPACK_IMPORTED_MODULE_6__().format("h:mm ' A"), 'fullName': self.userName,
                    'addingAmount': self.selectedAmount, 'walletbalance': self.walletBalance,
                    'hostId': self.professionalUser._id,
                    "recieverImg": self.professionalUser.user_img, "senderImg": self.user.user_img
                };
                self.authService.unlockUser(obj).subscribe((res) => {
                    console.log(res);
                    if (res.success) {
                        self.presentAlertSuccess(res.success, 'Transaction Completed.');
                        self.authService.presentToast('User Unlocked.');
                        self.modalController.dismiss(res.success);
                    }
                    else {
                        self.authService.presentToast('Please try in sometime.');
                        self.modalController.dismiss();
                    }
                });
            };
            var cancelCallback = function (error) {
                console.log(error);
                self.presentAlert('Payment Cancelled.');
            };
            RazorpayCheckout.open(options, successCallback, cancelCallback);
            //  
        }
    }
    presentAlertSuccess(success, msg) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'razorpayCancel',
                message: msg,
                buttons: [
                    {
                        text: 'Okay',
                        handler: () => {
                            console.log('Confirm Okay');
                            this.modalController.dismiss(success);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentAlert(msg) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'razorpayCancel',
                message: msg,
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Try Again',
                        handler: () => {
                            console.log('Confirm Okay');
                            this.buy();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    closeModal() {
        this.modalController.dismiss();
    }
};
UnlockContact1Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
UnlockContact1Page.propDecorators = {
    professionalUser: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['professionalUser',] }]
};
UnlockContact1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-unlock-contact1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./unlock-contact1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/unlock-contact1/unlock-contact1.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./unlock-contact1.page.scss */ "./src/app/unlock-contact1/unlock-contact1.page.scss")).default]
    })
], UnlockContact1Page);



/***/ })

}]);
//# sourceMappingURL=default~profile1-profile1-module~unlock-contact1-unlock-contact1-module-es2015.js.map