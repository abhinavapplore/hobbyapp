(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["blocked-blocked-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/blocked/blocked.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/blocked/blocked.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-col [size]=2 (click)=\"back()\">\n        <ion-icon name=\"chevron-back\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text class=\"myTitle\">Blocked Users</ion-text>\n      </ion-col> \n    </ion-row> \n</ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n  <div *ngIf=\"allBlockedUser.length==0\" style=\"text-align: center;\n  margin-top: 5%;\">\n    <ion-text class=\"text\">No Blocked Users.</ion-text>\n  </div>\n\n<div *ngIf=\"allBlockedUser.length!=0\">\n  <!-- <div class=\"div\" > -->\n\n    <div *ngFor=\"let item of allBlockedUser;let z=index\">\n      <ion-card *ngIf=\"!item.isblocked\">\n      \n        <ion-row class=\"row\">\n          <ion-col [size]=3 class=\"col1\">\n            <ion-avatar class=\"container\">     \n              <ion-img class=\"img1\" [src]=\"item.hostId.user_img\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col [size]=9 class=\"col3\">\n            <ion-row class=\"row3\">\n              <ion-col>\n                <ion-text class=\"text\">{{item.hostId.fullName}}</ion-text>\n              </ion-col>\n              <ion-col>\n                <ion-text class=\"text\" (click)=\"unblock(item,z)\">Unblock</ion-text>\n              </ion-col>\n            \n            </ion-row>\n          \n          </ion-col>\n        </ion-row>\n      </ion-card>  \n    </div>\n \n    \n  <!-- </div> -->\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/blocked/blocked-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/blocked/blocked-routing.module.ts ***!
  \***************************************************/
/*! exports provided: BlockedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlockedPageRoutingModule", function() { return BlockedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _blocked_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blocked.page */ "./src/app/blocked/blocked.page.ts");




const routes = [
    {
        path: '',
        component: _blocked_page__WEBPACK_IMPORTED_MODULE_3__["BlockedPage"]
    }
];
let BlockedPageRoutingModule = class BlockedPageRoutingModule {
};
BlockedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BlockedPageRoutingModule);



/***/ }),

/***/ "./src/app/blocked/blocked.module.ts":
/*!*******************************************!*\
  !*** ./src/app/blocked/blocked.module.ts ***!
  \*******************************************/
/*! exports provided: BlockedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlockedPageModule", function() { return BlockedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _blocked_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./blocked-routing.module */ "./src/app/blocked/blocked-routing.module.ts");
/* harmony import */ var _blocked_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./blocked.page */ "./src/app/blocked/blocked.page.ts");







let BlockedPageModule = class BlockedPageModule {
};
BlockedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _blocked_routing_module__WEBPACK_IMPORTED_MODULE_5__["BlockedPageRoutingModule"]
        ],
        declarations: [_blocked_page__WEBPACK_IMPORTED_MODULE_6__["BlockedPage"]]
    })
], BlockedPageModule);



/***/ }),

/***/ "./src/app/blocked/blocked.page.scss":
/*!*******************************************!*\
  !*** ./src/app/blocked/blocked.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-row {\n  margin-top: 0px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n@media (min-height: 568px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 640px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 667px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 731px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 736px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 812px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 823px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n.myTitle {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\nion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmxvY2tlZC9ibG9ja2VkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwrREFBQTtFQUNBLGdEQUFBO0VBQ0EsNEJBQUE7QUFDSjtBQUFJO0VBQ0ksZUFBQTtBQUVSO0FBQUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRVI7QUFHQTtFQUVNO0lBQ0UsZ0JBQUE7RUFETjtBQUNGO0FBS0U7RUFFSTtJQUNFLGVBQUE7RUFKTjtBQUNGO0FBUUU7RUFFSTtJQUNFLGdCQUFBO0VBUE47QUFDRjtBQVdFO0VBRUk7SUFDRSxlQUFBO0VBVk47QUFDRjtBQWNFO0VBRUk7SUFDRSxnQkFBQTtFQWJOO0FBQ0Y7QUFpQkU7RUFFSTtJQUNFLGdCQUFBO0VBaEJOO0FBQ0Y7QUFvQkU7RUFFSTtJQUNFLGVBQUE7RUFuQk47QUFDRjtBQXVCQTtFQUNJLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQXJCSjtBQXdCQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBckJKIiwiZmlsZSI6InNyYy9hcHAvYmxvY2tlZC9ibG9ja2VkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b29sYmFyMSB7XG4gICAgaGVpZ2h0OiA3NHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjVweCAyNXB4O1xuICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICB9XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbn1cblxuXG5AbWVkaWEgKG1pbi1oZWlnaHQgOiA1NjhweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDY0MHB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA2NjdweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDczMXB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA3MzZweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDgxMnB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogODIzcHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4ubXlUaXRsZXtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/blocked/blocked.page.ts":
/*!*****************************************!*\
  !*** ./src/app/blocked/blocked.page.ts ***!
  \*****************************************/
/*! exports provided: BlockedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlockedPage", function() { return BlockedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");






let BlockedPage = class BlockedPage {
    constructor(router, authService, storage, fs) {
        this.router = router;
        this.authService = authService;
        this.storage = storage;
        this.fs = fs;
        this.allBlockedUser = [];
    }
    ngOnInit() {
        this.storage.get('user').then((user) => {
            this.userData = user;
            this.loadData();
        });
    }
    back() {
        this.router.navigateByUrl('tabs/tab1');
    }
    loadData() {
        this.allBlockedUser = [];
        var obj = { "userId": this.userData._id };
        this.authService.blockUserList(obj).subscribe((data) => {
            if (data.success) {
                console.log(data);
                this.allBlockedUser = data.data;
            }
            else {
                console.log(data);
            }
        });
    }
    unblock(item, z) {
        this.allBlockedUser.splice(z, 1);
        var data = { 'blockedId': item._id };
        this.authService.unblockUser(data).subscribe((data) => {
            if (data.success) {
                this.authService.presentToast(item.hostId.fullName + ' unblocked.');
                this.fs.collection('friends').doc(this.userData._id).collection('chats').doc(item.hostId._id).set({
                    isblocked: false
                }, { merge: true });
                this.fs.collection('friends').doc(item.hostId._id).collection('chats').doc(this.userData._id).set({
                    isblocked: false
                }, { merge: true });
            }
            else {
                this.authService.presentToast('Please try in sometime.');
            }
        });
    }
};
BlockedPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"] }
];
BlockedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-blocked',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./blocked.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/blocked/blocked.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./blocked.page.scss */ "./src/app/blocked/blocked.page.scss")).default]
    })
], BlockedPage);



/***/ })

}]);
//# sourceMappingURL=blocked-blocked-module-es2015.js.map