(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~filtermodal-filtermodal-module~tab4-tab4-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/filtermodal/filtermodal.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/filtermodal/filtermodal.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n  <ion-row style=\"margin-top: 2%;\n  font-size: 30px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  width: 97%;\">\n    <ion-icon (click)=\"closeModal()\" name=\"close-outline\"></ion-icon>\n  </ion-row>\n\n  <ion-row class=\"row2\">\n    <ion-text class=\"text\">Filter</ion-text>\n  </ion-row>\n\n<div class=\"div\">\n  <ion-row class=\"row1\">\n    <ion-col class=\"col\" [size]=3>\n      <img class=\"img\" src=\"../../assets/images/modal/exp.svg\">\n      <ion-text class=\"text1\">Distance</ion-text>\n    </ion-col>\n    <ion-col class=\"col2\"  [size]=9>\n      <div class=\"container3\" class=\"col1\">\n        <ion-range mode=\"ios\" (ionChange)=\"skill($event,i)\" min=\"0\" max=\"50\" step=\"10\" \n        value=\"0\" snaps=\"true\" pin=\"true\" ></ion-range>\n      <div class=\"text4\">\n        <ion-text>0</ion-text>\n      </div>\n      <div class=\"text5\">\n        <ion-text>50 kms</ion-text>\n      </div>\n     </div>\n    </ion-col>\n  </ion-row>\n</div>\n  \n  <div class=\"div\">\n    <ion-row class=\"row1\">\n      <ion-col class=\"col\" [size]=3>\n        <img class=\"img\" src=\"../../assets/images/modal/socialdistancing.svg\">\n        <ion-text class=\"text1\">Experience</ion-text>\n      </ion-col>\n      <ion-col class=\"col2\"  [size]=9>\n        <div class=\"container3\" class=\"col1\">\n          <ion-range mode=\"ios\" min=\"0\" max=\"5\" step=\"1\" \n          value=\"0\" snaps=\"true\" pin=\"true\" ></ion-range>\n        <div class=\"text4\">\n          <ion-text>0</ion-text>\n        </div>\n        <div class=\"text5\">\n          <ion-text>5+ Years</ion-text>\n        </div>\n       </div>\n      </ion-col>\n    </ion-row>\n  </div>\n  \n  <!-- <div class=\"div\">\n    <ion-row class=\"row1\">\n      <ion-col class=\"col\" [size]=3>\n        <ion-icon class=\"icon\" name=\"star-outline\"></ion-icon>\n        <ion-text class=\"text1\">Rating</ion-text>\n      </ion-col>\n      <ion-col class=\"col2\"  [size]=9>\n        <div class=\"container3\" class=\"col1\">\n          <ion-range mode=\"ios\" min=\"0\" max=\"5\" step=\"1\" value=\"0\"\n           snaps=\"true\" pin=\"true\" ></ion-range>\n        <div class=\"text4\">\n          <ion-text>0</ion-text>\n        </div>\n        <div class=\"text5\">\n          <ion-text>5</ion-text>\n        </div>\n       </div>\n      </ion-col>\n    </ion-row>\n  </div> -->\n\n  <ion-row class=\"row3\" (click)=\"apply()\">\n    <div class=\"button1\">\n      <ion-text class=\"signin\">APPLY</ion-text>\n    </div>\n  </ion-row>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/filtermodal/filtermodal.page.scss":
/*!***************************************************!*\
  !*** ./src/app/filtermodal/filtermodal.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".text {\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 24px;\n  font-weight: 700;\n  font-style: normal;\n}\n\n.row2 {\n  padding-top: 7%;\n  justify-content: center;\n}\n\n.row1 {\n  width: 276px;\n  height: 99px;\n  overflow: visible;\n  background-color: #FBFBFB;\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.div {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n\n.text1 {\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 10px;\n  margin-top: 15%;\n}\n\n.col {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.container3 {\n  position: relative;\n  text-align: center;\n  color: white;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.text4 {\n  position: absolute;\n  right: 182px;\n  top: 65px;\n  color: #003C69;\n  font-family: \"Poppins-Bold\";\n  font-size: 12px;\n}\n\n.text5 {\n  position: absolute;\n  top: 65px;\n  left: 170px;\n  color: #003C69;\n  font-family: \"Poppins-Bold\";\n  font-size: 12px;\n}\n\n.img {\n  width: 50%;\n  margin-top: 40%;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  height: 40px;\n}\n\n.icon {\n  color: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  font-size: 35px;\n  margin-top: 40%;\n}\n\n.row3 {\n  justify-content: center;\n  padding-top: 7%;\n}\n\n.signin {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 18px;\n}\n\n.button1 {\n  width: 200px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.col2 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmlsdGVybW9kYWwvZmlsdGVybW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDRTtFQUNFLGVBQUE7RUFDQSx1QkFBQTtBQUVKOztBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsK0NBQUE7RUFDQSxtQkFBQTtBQUdKOztBQURBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFJSjs7QUFGQTtFQUNJLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBS0o7O0FBSEE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBTUo7O0FBSkE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQU9KOztBQUxFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7RUFDQSwyQkFBQTtFQUNBLGVBQUE7QUFRSjs7QUFORTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxlQUFBO0FBU0o7O0FBUEU7RUFDRSxVQUFBO0VBQ0EsZUFBQTtBQVVKOztBQVJFO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0FBV0o7O0FBVEU7RUFDSSx3REFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBWU47O0FBVkU7RUFDRSx1QkFBQTtFQUNBLGVBQUE7QUFhSjs7QUFYQTtFQUNJLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQWNKOztBQVpFO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSw2REFBQTtFQUNBLGdEQUFBO0VBQ0EsbUJBQUE7QUFlSjs7QUFiRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBZ0JKIiwiZmlsZSI6InNyYy9hcHAvZmlsdGVybW9kYWwvZmlsdGVybW9kYWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHQge1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgfVxuICAucm93MiB7XG4gICAgcGFkZGluZy10b3A6IDclO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnJvdzEge1xuICAgIHdpZHRoOiAyNzZweDtcbiAgICBoZWlnaHQ6IDk5cHg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZCRkJGQjtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLmRpdiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xufVxuLnRleHQxIHtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBjb2xvcjogIzAwM0M2OTtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbWFyZ2luLXRvcDogMTUlO1xufVxuLmNvbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29udGFpbmVyMyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgLnRleHQ0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDE4MnB4O1xuICAgIHRvcDogNjVweDtcbiAgICBjb2xvcjogIzAwM0M2OTtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gIH1cbiAgLnRleHQ1IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA2NXB4O1xuICAgIGxlZnQ6IDE3MHB4O1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgfVxuICAuaW1nIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIG1hcmdpbi10b3A6IDQwJTtcbiAgfVxuICAuY29sMSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGhlaWdodDogNDBweDtcbiAgfVxuICAuaWNvbiB7XG4gICAgICBjb2xvcjogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICAgIG1hcmdpbi10b3A6IDQwJTtcbiAgfVxuICAucm93M3tcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNyU7XG59XG4uc2lnbmluIHtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAuYnV0dG9uMSB7XG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIGhlaWdodDogNTVweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB9XG4gIC5jb2wyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/filtermodal/filtermodal.page.ts":
/*!*************************************************!*\
  !*** ./src/app/filtermodal/filtermodal.page.ts ***!
  \*************************************************/
/*! exports provided: FiltermodalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltermodalPage", function() { return FiltermodalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let FiltermodalPage = class FiltermodalPage {
    constructor(modalController, authService, storage, router) {
        this.modalController = modalController;
        this.authService = authService;
        this.storage = storage;
        this.router = router;
        this.distance = 0;
        this.filterProfessionalUser = [];
        this.filterPassionistUser = [];
        this.allFilterUser = [];
    }
    ngOnInit() {
        console.log(this.distance);
    }
    skill($event, index) {
        console.log($event);
        this.distance = $event.detail.value;
        console.log(this.distance);
    }
    apply() {
        this.storage.get('user').then((user) => {
            var obj = { 'userId': user._id, 'latitude': JSON.parse(user.userLatitude),
                'longitude': JSON.parse(user.userLongitude), 'distance': this.distance };
            this.authService.filterByDistance(obj).subscribe((data) => {
                if (data.success) {
                    console.log(data);
                    data.data.forEach(element => {
                        if (element._id == user._id) {
                            console.log('user id match');
                        }
                        else if (element._id != user._id && element.userType == "professional") {
                            this.filterProfessionalUser.push(element);
                            // if(this.filterProfessionalUser.length!=0){
                            //   this.storage.set('filterProfessionalUser',this.filterProfessionalUser);
                            // }
                            // let navigationExtras:NavigationExtras = {
                            //   queryParams: {
                            //     special: JSON.stringify(this.filterProfessionalUser),
                            //     userType: "professional"
                            //   }
                            // };
                            // this.router.navigate(['tabs/tab4'],navigationExtras);
                            // this.modalController.dismiss();
                        }
                        else if (element._id != user._id && element.userType == "passionist") {
                            this.filterPassionistUser.push(element);
                            // if(this.filterPassionistUser.length!=0){
                            //   this.storage.set('filterPassionistUser',this.filterPassionistUser);
                            // }
                            // let navigationExtras:NavigationExtras = {
                            //   queryParams: {
                            //     special: JSON.stringify(this.filterPassionistUser),
                            //     userType: "passionists"
                            //   }
                            // };
                            // this.router.navigate(['tabs/tab4'],navigationExtras);
                            // this.modalController.dismiss();
                        }
                    });
                    this.allFilterUser.push({ 'filterProfessionalUser': this.filterProfessionalUser });
                    this.allFilterUser.push({ 'filterPassionistUser': this.filterPassionistUser });
                    this.modalController.dismiss(this.allFilterUser);
                }
                else {
                    this.modalController.dismiss();
                    this.authService.presentToast('No user found');
                }
            });
        });
    }
    closeModal() {
        this.modalController.dismiss();
    }
};
FiltermodalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
FiltermodalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-filtermodal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./filtermodal.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/filtermodal/filtermodal.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./filtermodal.page.scss */ "./src/app/filtermodal/filtermodal.page.scss")).default]
    })
], FiltermodalPage);



/***/ })

}]);
//# sourceMappingURL=default~filtermodal-filtermodal-module~tab4-tab4-module-es2015.js.map