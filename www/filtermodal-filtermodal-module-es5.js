(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["filtermodal-filtermodal-module"], {
    /***/
    "./src/app/filtermodal/filtermodal-routing.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/filtermodal/filtermodal-routing.module.ts ***!
      \***********************************************************/

    /*! exports provided: FiltermodalPageRoutingModule */

    /***/
    function srcAppFiltermodalFiltermodalRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FiltermodalPageRoutingModule", function () {
        return FiltermodalPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _filtermodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./filtermodal.page */
      "./src/app/filtermodal/filtermodal.page.ts");

      var routes = [{
        path: '',
        component: _filtermodal_page__WEBPACK_IMPORTED_MODULE_3__["FiltermodalPage"]
      }];

      var FiltermodalPageRoutingModule = function FiltermodalPageRoutingModule() {
        _classCallCheck(this, FiltermodalPageRoutingModule);
      };

      FiltermodalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FiltermodalPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/filtermodal/filtermodal.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/filtermodal/filtermodal.module.ts ***!
      \***************************************************/

    /*! exports provided: FiltermodalPageModule */

    /***/
    function srcAppFiltermodalFiltermodalModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FiltermodalPageModule", function () {
        return FiltermodalPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _filtermodal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./filtermodal-routing.module */
      "./src/app/filtermodal/filtermodal-routing.module.ts");
      /* harmony import */


      var _filtermodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./filtermodal.page */
      "./src/app/filtermodal/filtermodal.page.ts");

      var FiltermodalPageModule = function FiltermodalPageModule() {
        _classCallCheck(this, FiltermodalPageModule);
      };

      FiltermodalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _filtermodal_routing_module__WEBPACK_IMPORTED_MODULE_5__["FiltermodalPageRoutingModule"]],
        declarations: [_filtermodal_page__WEBPACK_IMPORTED_MODULE_6__["FiltermodalPage"]]
      })], FiltermodalPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=filtermodal-filtermodal-module-es5.js.map