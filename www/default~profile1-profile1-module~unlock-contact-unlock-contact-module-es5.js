(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~profile1-profile1-module~unlock-contact-unlock-contact-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/unlock-contact/unlock-contact.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/unlock-contact/unlock-contact.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUnlockContactUnlockContactPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n  <div class=\"div\">\n\n    <ion-row style=\"margin-top: 2%;\n  font-size: 30px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  width: 97%;\">\n    <ion-icon (click)=\"closeModal()\" name=\"close-outline\"></ion-icon>\n  </ion-row>\n\n    <ion-row class=\"row\">\n      <ion-text>Connect with <b style=\"text-transform: capitalize;\"> {{professionalUser.firstName}} </b></ion-text>\n    </ion-row>\n    <ion-row class=\"row1\">\n      <ion-text>Use 5 Credits ?</ion-text>\n    </ion-row>\n    <ion-row class=\"row2\">\n      <ion-text>Credit Balance</ion-text>\n      <ion-text>20</ion-text>\n    </ion-row>\n    <ion-row class=\"row3\">\n        <div class=\"col1\" (click)=\"no()\">\n          <ion-text>No</ion-text>\n        </div>\n        <div class=\"col2\" (click)=\"yes()\">\n          <ion-text>Yes</ion-text>\n        </div>\n    </ion-row>\n  </div>\n\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/unlock-contact/unlock-contact.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/unlock-contact/unlock-contact.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUnlockContactUnlockContactPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n\n.row ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row1 {\n  width: 90%;\n  justify-content: center;\n}\n\n.row1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #4FDC56;\n}\n\n.row2 {\n  width: 276px;\n  height: 75px;\n  overflow: visible;\n  background-color: #FBFBFB;\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n  margin-top: 7%;\n}\n\n.row2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #003C69;\n}\n\n.row3 {\n  width: 90%;\n  justify-content: space-evenly;\n  padding-top: 5%;\n}\n\n.row3 .col1 {\n  width: 95px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background-color: #fafdff;\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row3 .col1 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #000000;\n}\n\n.row3 .col2 {\n  width: 95px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n\n.row3 .col2 ion-text {\n  font-family: \"Poppins-Bold\";\n  font-size: 18px;\n  color: #FFFFFF;\n}\n\n.row4 {\n  width: 90%;\n  justify-content: center;\n  padding-top: 5%;\n}\n\n.row4 .col {\n  width: 205px;\n  height: 55px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  overflow: visible;\n  background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdW5sb2NrLWNvbnRhY3QvdW5sb2NrLWNvbnRhY3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBO0VBQ0ksVUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQUVKOztBQURJO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUdSOztBQUFBO0VBQ0ksVUFBQTtFQUNBLHVCQUFBO0FBR0o7O0FBRkk7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBSVI7O0FBREE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSwrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFJSjs7QUFISTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFLUjs7QUFGQTtFQUNJLFVBQUE7RUFDQSw2QkFBQTtFQUNBLGVBQUE7QUFLSjs7QUFKSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxnREFBQTtFQUNBLG1CQUFBO0FBTVI7O0FBTFE7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBT1o7O0FBSkk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDZEQUFBO0VBQ0EsZ0RBQUE7RUFDQSxtQkFBQTtBQU1SOztBQUxRO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQU9aOztBQUhBO0VBQ0ksVUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQU1KOztBQUxJO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSw2REFBQTtFQUNBLGdEQUFBO0VBQ0EsbUJBQUE7QUFPUiIsImZpbGUiOiJzcmMvYXBwL3VubG9jay1jb250YWN0L3VubG9jay1jb250YWN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kaXYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnJvdyB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgY29sb3I6ICMwMDNDNjk7XG4gICAgfVxufVxuLnJvdzEge1xuICAgIHdpZHRoOiA5MCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaW9uLXRleHQge1xuICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgY29sb3I6ICM0RkRDNTY7XG4gICAgfVxufVxuLnJvdzIge1xuICAgIHdpZHRoOiAyNzZweDtcbiAgICBoZWlnaHQ6IDc1cHg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZCRkJGQjtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDclO1xuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdQb3BwaW5zLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIH1cbn1cbi5yb3czIHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAuY29sMSB7XG4gICAgICAgIHdpZHRoOiA5NXB4O1xuICAgICAgICBoZWlnaHQ6IDU1cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmRmZjtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAuY29sMiB7XG4gICAgICAgIHdpZHRoOiA5NXB4O1xuICAgICAgICBoZWlnaHQ6IDU1cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtQm9sZCc7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICAgICAgfVxuICAgIH1cbn1cbi5yb3c0IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAuY29sIHtcbiAgICAgICAgd2lkdGg6IDIwNXB4O1xuICAgICAgICBoZWlnaHQ6IDU1cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI3MGRlZywgIzAyQ0JFRSAwJSwgIzU5RTAyMiAxMDAlKTtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCAxMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIH1cbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/unlock-contact/unlock-contact.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/unlock-contact/unlock-contact.page.ts ***!
      \*******************************************************/

    /*! exports provided: UnlockContactPage */

    /***/
    function srcAppUnlockContactUnlockContactPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UnlockContactPage", function () {
        return UnlockContactPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! moment */
      "./node_modules/moment/moment.js");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);

      var UnlockContactPage = /*#__PURE__*/function () {
        function UnlockContactPage(modalController, authService, storage, route, alertController) {
          _classCallCheck(this, UnlockContactPage);

          this.modalController = modalController;
          this.authService = authService;
          this.storage = storage;
          this.route = route;
          this.alertController = alertController;
          this.selected = false;
          this.selected1 = false;
          this.selectedAmount = 5;
          this.user = {};
        }

        _createClass(UnlockContactPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.storage.get('user').then(function (user) {
              _this.user = user;
              _this.userId = user._id;
              _this.walletBalance = user.wallet;
              _this.fullName = user.fullName;
              console.log(_this.professionalUser);
            });
          }
        }, {
          key: "yes",
          value: function yes() {
            var _this3 = this;

            if (this.selectedAmount > this.walletBalance) {
              var options = {
                description: 'Add money to wallet',
                image: 'https://firebasestorage.googleapis.com/v0/b/hobbyit-74e3d.appspot.com/o/Untitled-1-04-04.png?alt=media&token=db9dbbc2-8c8d-448b-980f-8a7a678fe5fd',
                currency: 'INR',
                key: 'rzp_test_sbuNGT8VxLZqff',
                amount: this.selectedAmount * 100,
                theme: {
                  color: '#02CBEE'
                },
                modal: {
                  ondismiss: function ondismiss() {
                    this.presentAlert('Transaction Cancelled');
                  }
                }
              };
              var self = this;

              var successCallback = function successCallback(payment_id) {
                var _this2 = this;

                var obj = {
                  'userId': self.userId,
                  'title': self.professionalUser.fullName,
                  'transactionImage': self.professionalUser.user_img,
                  'amount': self.selectedAmount,
                  "deviceId": self.professionalUser.deviceId,
                  'transactionDate': moment__WEBPACK_IMPORTED_MODULE_6__().format('DD/MMM/YYYY'),
                  'isAddingMoney': false,
                  'transactionTime': moment__WEBPACK_IMPORTED_MODULE_6__().format("h:mm ' A"),
                  'fullName': self.fullName,
                  'walletbalance': self.walletBalance,
                  'hostId': self.professionalUser._id,
                  "recieverImg": self.professionalUser.user_img,
                  "senderImg": self.user.user_img
                };
                self.authService.unlockUser(obj).subscribe(function (data) {
                  console.log(data);

                  if (data.success) {
                    self.presentAlertSuccess(data.success, 'Transaction Completed.');

                    _this2.authService.presentToast('User Unlocked.');

                    _this2.modalController.dismiss(data.success);
                  } else {
                    _this2.authService.presentToast('Please try in sometime.');

                    _this2.modalController.dismiss();
                  }
                });
              };

              var cancelCallback = function cancelCallback(error) {
                self.presentAlert('Transaction Cancelled');
              };

              RazorpayCheckout.open(options, successCallback, cancelCallback);
            } else {
              console.log("goes in else");
              var obj = {
                'userId': this.user._id,
                'title': this.professionalUser.fullName,
                'transactionImage': this.professionalUser.user_img,
                'amount': this.selectedAmount,
                "deviceId": this.professionalUser.deviceId,
                'transactionDate': moment__WEBPACK_IMPORTED_MODULE_6__().format('DD/MMM/YYYY'),
                'isAddingMoney': false,
                'transactionTime': moment__WEBPACK_IMPORTED_MODULE_6__().format("h:mm ' A"),
                'fullName': this.user.fullName,
                'walletbalance': this.user.wallet,
                'hostId': this.professionalUser._id,
                "recieverImg": this.professionalUser.user_img,
                "senderImg": this.user.user_img
              };
              this.authService.unlockUser(obj).subscribe(function (data) {
                console.log(data);

                if (data.success) {
                  _this3.authService.presentToast('User Unlocked.');

                  _this3.modalController.dismiss(data.success);
                } else {
                  _this3.authService.presentToast('Please try in sometime.');

                  _this3.modalController.dismiss();
                }
              });
            }
          }
        }, {
          key: "presentAlertSuccess",
          value: function presentAlertSuccess(success, msg) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this4 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        cssClass: 'razorpayCancel',
                        message: msg,
                        buttons: [{
                          text: 'Okay',
                          handler: function handler() {
                            console.log('Confirm Okay');

                            _this4.modalController.dismiss(success);
                          }
                        }]
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentAlert",
          value: function presentAlert(msg) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this5 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: 'razorpayCancel',
                        message: msg,
                        buttons: [{
                          text: 'Cancel',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler(blah) {
                            console.log('Confirm Cancel: blah');
                          }
                        }, {
                          text: 'Try Again',
                          handler: function handler() {
                            console.log('Confirm Okay');

                            _this5.yes();
                          }
                        }]
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } // yes(){
          //   this.modalController.dismiss();
          // }

        }, {
          key: "no",
          value: function no() {
            this.modalController.dismiss();
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalController.dismiss();
          }
        }]);

        return UnlockContactPage;
      }();

      UnlockContactPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }];
      };

      UnlockContactPage.propDecorators = {
        professionalUser: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
          args: ['professionalUser']
        }]
      };
      UnlockContactPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-unlock-contact',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./unlock-contact.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/unlock-contact/unlock-contact.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./unlock-contact.page.scss */
        "./src/app/unlock-contact/unlock-contact.page.scss"))["default"]]
      })], UnlockContactPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~profile1-profile1-module~unlock-contact-unlock-contact-module-es5.js.map