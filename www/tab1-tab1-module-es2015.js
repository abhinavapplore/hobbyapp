(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar\">\n    <ion-row style=\"margin-top: 15%;\n    margin-bottom: 10%;\">\n      <ion-col [size]=2 (click)='openFirst()'>\n        <img class=\"img\" src=\"../../assets/images/menu.svg\">\n      </ion-col>\n      <ion-col [size]=6>\n        <ion-text>Feed</ion-text>\n      </ion-col>\n      <ion-col [size]=2 (click)=\"searchUser()\">\n        <ion-icon style=\"font-size: 30px;color:#FFFFFF\" name=\"search-outline\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=2 (click)='notification()'>\n        <ion-icon style=\"font-size: 30px;color:#FFFFFF\" name=\"notifications-outline\"></ion-icon>\n      </ion-col>    \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content [fullscreen]=\"true\" [scrollEvents]=\"true\" (ionScroll)=\"didScroll($event)\">\n\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  \n  <ion-row class=\"row1\"> \n    <ion-slides [options]=\"slideOpts\" style=\"margin: 0;\">\n      <ion-col [size]=3>\n        <ion-slide class=\"slide\" (click)=\"addStory(1)\">\n          <ion-avatar class=\"container\">     \n            <ion-img style=\"height: 68px;\n            width: 68px;\" [src]=\"userImg\"></ion-img>\n            <div class=\"bottom-left\">\n              <img src=\"../../assets/images/signup/add.svg\">\n            </div>\n          </ion-avatar> \n          <ion-row class=\"row1\">\n            <ion-text class=\"text\">What's New</ion-text>\n          </ion-row>  \n        </ion-slide>\n      </ion-col>\n      <ion-col [size]=3 *ngFor=\"let item of storyPost\" (click)=\"openStory(item)\">\n        <ion-slide class=\"slide\">\n          <ion-avatar>     \n            <ion-img class=\"img1\" [src]=\"item.userImg\"></ion-img>\n          </ion-avatar> \n          <ion-row class=\"row1\">\n            <ion-text class=\"text\">{{item.userName}}</ion-text>\n          </ion-row>  \n        </ion-slide>\n      </ion-col>\n    </ion-slides>  \n  </ion-row>\n\n  <!-- <div style=\"display: flex;flex-direction: row;justify-content: center;\"> -->\n    <ion-card>\n      <ion-slides pager=true [options]=\"slideOpts1\" #slidewithnav>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/singing.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/gym.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/meditation.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n        <ion-slide>\n            <ion-thumbnail class=\"thumbnail\">\n            <img class=\"img2\" src=\"../../assets/hobbyImage/run.jpg\">\n            </ion-thumbnail>\n        </ion-slide>\n    </ion-slides>\n    </ion-card>     \n  <!-- </div> -->\n\n\n  <!-- <div style=\"display: flex;flex-direction: column;\n  justify-content: center;align-items: center;\"> -->\n    <ion-card *ngFor='let item of imgVidPost; let i=index'>\n      <ion-row style=\"height: 200px;\">\n        <ion-thumbnail (click)=\"viewPhoto(item.postUrl)\" *ngIf=\"item.posttype=='0'\" style=\"height: 200px;\n        width: 410px;\">\n          <ion-img [src]='item.postUrl'></ion-img>\n        </ion-thumbnail>\n        <div *ngIf=\"item.posttype=='1'\">\n          <video #player playsinline preload=\"auto\" controls>\n            <source [src]=\"item.postUrl\" type=\"video/mp4\"/>\n          </video>\n        </div>        \n      </ion-row>\n\n      <ion-row>\n        <ion-col [size]=6 style=\"display: flex;\n        flex-direction: row;\">\n\n        <div style=\"display: flex;\n          flex-direction: row;\n          justify-content: center;\n          align-items: center;\n          font-family: 'Poppins-Bold';\n          font-size: 16px;\n          color: #767474;width: 50px;\">\n            <ion-icon *ngIf='!item.isliked' (click)='likePost(item,i)' class=\"icon1\" \n            name=\"heart-outline\"></ion-icon>\n            <ion-icon *ngIf='item.isliked' (click)='deleteLike(item,i)' class=\"icon3\" \n            name=\"heart\"></ion-icon>\n            <ion-text (click)=\"goToLike(item)\" style=\"margin-left: 15%;\">{{item.likeCount}}</ion-text>\n        </div>\n\n          <div style=\"display: flex;\n          flex-direction: row;\n          justify-content: center;\n          align-items: center;\n          font-family: 'Poppins-Bold';\n          font-size: 16px;margin-left: 2%;\n          color: #767474;width: 50px;\" *ngIf=\"item.posttype=='1'\">\n            <img src=\"../../assets/images/home/eye.svg\">\n            <!-- <ion-text style=\"margin-left: 15%;\">{{item.viewCount}}</ion-text> -->\n        </div>\n        <img style=\"margin-left: 5%;\" (click)=share(item) \n        src=\"../../assets/images/home/share.svg\">\n        </ion-col>\n        <ion-col [size]=6 style=\"display: flex;flex-direction: row;justify-content: flex-end;\">\n          <ion-icon color=black *ngIf='!item.isBookmarked' (click)='bookmark(item,i)' \n          class=\"icon1\" name=\"bookmark-outline\"></ion-icon>\n          <ion-icon color='primary' *ngIf='item.isBookmarked' (click)='deleteBookmark(item,i)' \n          class=\"icon1\" name=\"bookmark\"></ion-icon>\n        </ion-col>    \n      </ion-row>\n\n      <ion-row style=\"margin-top: 2%;\" *ngIf=\"item.posttype=='0'\">\n        <ion-text style=\"margin-left: 2%;\" class=\"text2\">{{item.postTitle}}</ion-text>\n      </ion-row>\n\n      <ion-row style=\"margin-top: 2%;\" *ngIf=\"item.posttype=='1'\">\n        <ion-text style=\"margin-left: 2%;\" class=\"text2\">{{item.postTitle}}</ion-text>\n        <ion-icon class=\"icon2\" name=\"trending-up-outline\"></ion-icon>\n      </ion-row>\n\n      <ion-row>\n        <ion-text style=\"margin-left: 2%;\" class=\"text3\">{{item.postDescription}}</ion-text>\n      </ion-row>\n      <ion-row style=\"margin-bottom: 1%;\n      margin-top: 2%;\n      margin-left: 2%;\n      display: flex;\n      flex-direction: row;\n      align-items: center;\">\n\n          <!-- <ion-avatar style=\"height: 30px;\n          width: 30px;\" (click)=\"gotoUserProfile(item)\"> -->\n          <ion-thumbnail style=\"height: 30px;\n          width: 30px;\" (click)=\"gotoUserProfile(item)\">\n            <ion-img style=\"border-radius: 10px;\" [src]='item.userImg'></ion-img>\n          </ion-thumbnail>\n            \n          <!-- </ion-avatar> -->\n          <div style=\"display: flex;\n          flex-direction: row;\n          justify-content: space-evenly;\n          align-items: center;\n          width: 200px;\n          font-family: Poppins-Bold;\n          font-size: 10px;\n          color: #767474;\n          text-transform: capitalize;\">\n            <ion-text>{{item.userName}}</ion-text>\n            <ion-text>.</ion-text>\n            <ion-text style=\"opacity: 0.6;\">{{item.newDate1}}</ion-text>\n          </div>\n      </ion-row>\n    </ion-card>\n\n  <!-- </div> -->\n\n  <div *ngIf=\"posts.length==0\" class=\"div6\">\n    <ion-text>No Posts</ion-text>\n  </div>\n\n  <ion-row style=\"padding-top: 15%;\">\n    <ion-text style=\"color: #F6F6F6;\">.</ion-text>\n  </ion-row>\n  \n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/tab1/tab1-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function() { return Tab1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"],
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ "./src/app/tab1/tab1.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tab1-routing.module */ "./src/app/tab1/tab1-routing.module.ts");








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"],
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "./src/app/tab1/tab1.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".icon {\n  color: #ffffff;\n  font-size: 35px;\n}\n\n.img {\n  width: 25px;\n}\n\n.container {\n  position: relative;\n  text-align: center;\n  padding-top: 2%;\n  height: 68px;\n  width: 68px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #003C69;\n}\n\n.bottom-left {\n  position: absolute;\n  left: 70%;\n  top: 80%;\n  background: #2196F3;\n  height: 15px;\n  border-radius: 50%;\n  width: 15px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\nion-avatar {\n  height: 70px;\n  width: 70px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #003C69;\n}\n\n.text {\n  font-family: \"Poppins-Regular\";\n  font-size: 10px;\n  color: #000000;\n}\n\n.img1 {\n  height: 80px;\n  width: 80px;\n}\n\n.slide {\n  width: 75px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row1 {\n  padding-top: 5%;\n}\n\n.slide1 {\n  width: 100%;\n  height: 100%;\n  padding: 1%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.div {\n  height: 134px;\n  width: 334px;\n  padding: 1%;\n}\n\n.thumbnail {\n  height: 125px;\n  width: 390px;\n  padding: 1%;\n}\n\n.img2 {\n  height: 125px;\n  width: 390px;\n  border-radius: 5px;\n}\n\n.slide3 {\n  height: 125px;\n  width: 334px;\n}\n\n.col {\n  position: relative;\n  text-align: center;\n  padding: 2%;\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n}\n\n.col3 {\n  display: flex;\n  flex-direction: column;\n  justify-content: space-evenly;\n  width: 157px;\n}\n\n.div1 {\n  position: absolute;\n  top: 5px;\n  left: 5px;\n  padding: 2%;\n  border-radius: 10px;\n  background: #3dd694;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.text1 {\n  font-family: \"Poppins-Medium\";\n  font-size: 14px;\n  color: #FFFFFF;\n  text-transform: uppercase;\n}\n\n.text2 {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #003C69;\n  text-transform: capitalize;\n}\n\n.text3 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #767474;\n}\n\n.img3 {\n  height: 177px;\n  width: 151px;\n}\n\n.thumbnail1 {\n  height: 177px;\n  width: 151px;\n}\n\n.row2 {\n  border-radius: 5px;\n  background: white;\n  width: 90%;\n  padding: 1%;\n}\n\n.row8 {\n  width: 344px;\n  height: 197px;\n  border-radius: 5px;\n  background: white;\n  padding: 1%;\n}\n\n.row3 {\n  padding-top: 2%;\n}\n\n.row4 {\n  padding-top: 17%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n}\n\n.icon1 {\n  color: black;\n  font-size: 25px;\n}\n\n.icon3 {\n  color: #B92D2D;\n  font-size: 25px;\n}\n\n.div2 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 3%;\n}\n\n.div7 {\n  width: 344px;\n  height: 140px;\n  background: white;\n  border-radius: 5px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.slides {\n  background: white;\n  border-radius: 5px;\n}\n\n.div3 {\n  position: absolute;\n  top: 40px;\n  left: 130px;\n  color: white;\n  font-size: 60px;\n}\n\n.icon2 {\n  color: #41A44A;\n  font-size: 25px;\n  padding-left: 2%;\n}\n\n.text4 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #525252;\n}\n\n.div4 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 3%;\n}\n\n.img4 {\n  height: 125px;\n  width: 95%;\n}\n\n.row6 {\n  position: relative;\n  justify-content: center;\n  padding-top: 2%;\n}\n\n.row7 {\n  padding-top: 3%;\n  padding-left: 3%;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n}\n\n.col2 {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n}\n\n.div5 {\n  position: absolute;\n  top: 10px;\n  left: 12px;\n  width: 105px;\n  height: 28px;\n  border-radius: 10px;\n  background: #3dd694;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.div6 {\n  margin-top: 10%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.div6 ion-text {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #000000;\n}\n\nvideo {\n  height: 200px;\n  width: 390px;\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.bar {\n  z-index: 2;\n  transform: translate(450%, -300%);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMS90YWIxLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBQ0U7RUFDRSxXQUFBO0FBRUo7O0FBQUU7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FBR0o7O0FBREU7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUlKOztBQUZFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUFLSjs7QUFIRTtFQUNFLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFNSjs7QUFKRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FBT0o7O0FBTEU7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQVFKOztBQUhBO0VBQ0UsZUFBQTtBQU1GOztBQUpBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQU9GOztBQUxBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBUUY7O0FBTkE7RUFDRSxhQUFBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7QUFTSjs7QUFOQTtFQUNFLGFBQUE7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7QUFTSjs7QUFOQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBU0Y7O0FBUEE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0FBVUo7O0FBUkE7RUFDRSxhQUFBO0VBQ0Usc0JBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7QUFXSjs7QUFUQTtFQUNFLGtCQUFBO0VBR0EsUUFBQTtFQUNFLFNBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFVSjs7QUFSQTtFQUNJLDZCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBQVdKOztBQVRBO0VBQ0UsMkJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0FBWUY7O0FBVkE7RUFDRSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBYUY7O0FBWEE7RUFDRSxhQUFBO0VBQ0EsWUFBQTtBQWNGOztBQVpBO0VBQ0UsYUFBQTtFQUNFLFlBQUE7QUFlSjs7QUFiQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQWdCRjs7QUFkQTtFQUNFLFlBQUE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUFpQko7O0FBZkE7RUFDSSxlQUFBO0FBa0JKOztBQWZBO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FBa0JKOztBQWhCQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FBbUJKOztBQWpCQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBb0JGOztBQWxCQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBcUJGOztBQW5CQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBc0JGOztBQXBCQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUF1Qko7O0FBckJBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBd0JKOztBQXRCQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUF5QkY7O0FBdkJBO0VBQ0UsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQTBCRjs7QUF4QkE7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQTJCRjs7QUF6QkE7RUFDSSxhQUFBO0VBQ0EsVUFBQTtBQTRCSjs7QUExQkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQTZCRjs7QUEzQkE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUE4Qko7O0FBNUJBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtBQStCRjs7QUE3QkE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FBZ0NKOztBQTlCQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBaUNKOztBQS9CQTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBa0NGOztBQWpDRTtFQUNJLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFtQ047O0FBL0JBO0VBR0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0FBZ0NGOztBQTlCQTtFQUNFLFVBQUE7RUFDQSxpQ0FBQTtBQWlDRiIsImZpbGUiOiJzcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaWNvbiB7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1zaXplOiAzNXB4O1xuICB9XG4gIC5pbWcge1xuICAgIHdpZHRoOiAyNXB4O1xuICB9XG4gIC5jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDIlO1xuICAgIGhlaWdodDogNjhweDtcbiAgICB3aWR0aDogNjhweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAzQzY5O1xuICB9XG4gIC5ib3R0b20tbGVmdCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDcwJTtcbiAgICB0b3A6IDgwJTtcbiAgICBiYWNrZ3JvdW5kOiAjMjE5NkYzO1xuICAgIGhlaWdodDogMTVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDE1cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgaW9uLWF2YXRhciB7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIHdpZHRoOiA3MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDNDNjk7XG4gIH1cbiAgLnRleHQge1xuICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgY29sb3I6ICMwMDAwMDA7XG4gIH1cbiAgLmltZzEge1xuICAgIGhlaWdodDogODBweDtcbiAgICB3aWR0aDogODBweDtcbiAgfVxuICAuc2xpZGUge1xuICAgIHdpZHRoOiA3NXB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC8vIGlvbi1zbGlkZXMge1xuICAvLyAgIHdpZHRoOiA5MCU7XG4gIC8vIH1cbi5yb3cxIHtcbiAgcGFkZGluZy10b3A6IDUlO1xufVxuLnNsaWRlMSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBhZGRpbmc6IDElO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5kaXYge1xuICBoZWlnaHQ6IDEzNHB4O1xuICB3aWR0aDogMzM0cHg7XG4gIHBhZGRpbmc6IDElO1xufVxuLnRodW1ibmFpbCB7XG4gIGhlaWdodDogMTI1cHg7XG4gICAgd2lkdGg6IDM5MHB4O1xuICAgIHBhZGRpbmc6IDElO1xuICAvLyBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uaW1nMiB7XG4gIGhlaWdodDogMTI1cHg7XG4gICAgd2lkdGg6IDM5MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAvLyBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uc2xpZGUzIHtcbiAgaGVpZ2h0OiAxMjVweDtcbiAgd2lkdGg6IDMzNHB4O1xufVxuLmNvbCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAyJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xufVxuLmNvbDMge1xuICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgd2lkdGg6MTU3cHg7XG59XG4uZGl2MSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gdG9wOiAxM3B4O1xuICAvLyBsZWZ0OiAxNXB4O1xuICB0b3A6IDVweDtcbiAgICBsZWZ0OiA1cHg7XG4gICAgcGFkZGluZzogMiU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBiYWNrZ3JvdW5kOiAjM2RkNjk0O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnRleHQxIHtcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMtTWVkaXVtJztcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi50ZXh0MiB7XG4gIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1Cb2xkJztcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzAwM0M2OTtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4udGV4dDMge1xuICBmb250LWZhbWlseTogJ1BvcHBpbnMtUmVndWxhcic7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3Njc0NzQ7XG59XG4uaW1nMyB7XG4gIGhlaWdodDogMTc3cHg7XG4gIHdpZHRoOiAxNTFweDtcbn1cbi50aHVtYm5haWwxIHtcbiAgaGVpZ2h0OiAxNzdweDtcbiAgICB3aWR0aDogMTUxcHg7XG59XG4ucm93MiB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiA5MCU7XG4gIHBhZGRpbmc6IDElO1xufVxuLnJvdzgge1xuICB3aWR0aDogMzQ0cHg7XG4gICAgaGVpZ2h0OiAxOTdweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgcGFkZGluZzogMSU7XG59XG4ucm93MyB7XG4gICAgcGFkZGluZy10b3A6IDIlO1xuXG59XG4ucm93NCB7XG4gICAgcGFkZGluZy10b3A6IDE3JTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5pY29uMSB7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbn1cbi5pY29uMyB7XG4gIGNvbG9yOiAjQjkyRDJEO1xuICBmb250LXNpemU6IDI1cHg7XG59XG4uZGl2MiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogMyU7XG59XG4uZGl2NyB7XG4gIHdpZHRoOiAzNDRweDtcbiAgaGVpZ2h0OiAxNDBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uc2xpZGVzIHtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uZGl2MyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNDBweDtcbiAgICBsZWZ0OiAxMzBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiA2MHB4O1xufVxuLmljb24yIHtcbiAgY29sb3I6ICM0MUE0NEE7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgcGFkZGluZy1sZWZ0OiAyJTtcbn1cbi50ZXh0NCB7XG4gIGZvbnQtZmFtaWx5OiAnUG9wcGlucy1SZWd1bGFyJztcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzUyNTI1Mjtcbn1cbi5kaXY0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAzJTtcbn1cbi5pbWc0IHtcbiAgICBoZWlnaHQ6IDEyNXB4O1xuICAgIHdpZHRoOiA5NSU7XG59XG4ucm93NiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAyJTtcbn1cbi5yb3c3IHtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgcGFkZGluZy1sZWZ0OiAzJTtcbn1cbi5jb2wxIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29sMiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZGl2NSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMTBweDtcbiAgICBsZWZ0OiAxMnB4O1xuICAgIHdpZHRoOiAxMDVweDtcbiAgICBoZWlnaHQ6IDI4cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBiYWNrZ3JvdW5kOiAjM2RkNjk0O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmRpdjYge1xuICBtYXJnaW4tdG9wOiAxMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBpb24tdGV4dCB7XG4gICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGNvbG9yOiAjMDAwMDAwO1xuICB9XG59XG5cbnZpZGVvIHtcbiAgLy8gbWF4LXdpZHRoOiAxMDAlO1xuICAvLyBtYXgtaGVpZ2h0OiAxMDAlO1xuICBoZWlnaHQ6IDIwMHB4O1xuICB3aWR0aDogMzkwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cbi5iYXIge1xuICB6LWluZGV4OiAyO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg0NTAlLCAtMzAwJSk7XG59Il19 */");

/***/ }),

/***/ "./src/app/tab1/tab1.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _question_question_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../question/question.page */ "./src/app/question/question.page.ts");
/* harmony import */ var _story_story_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../story/story.page */ "./src/app/story/story.page.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/photo-viewer/ngx */ "./node_modules/@ionic-native/photo-viewer/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");

















let Tab1Page = class Tab1Page {
    constructor(menu, router, camera, storage, authService, photoViewer, modalController, route, actionSheetController, platform, angularstorage, file, filePath, socialSharing) {
        this.menu = menu;
        this.router = router;
        this.camera = camera;
        this.storage = storage;
        this.authService = authService;
        this.photoViewer = photoViewer;
        this.modalController = modalController;
        this.route = route;
        this.actionSheetController = actionSheetController;
        this.platform = platform;
        this.angularstorage = angularstorage;
        this.file = file;
        this.filePath = filePath;
        this.socialSharing = socialSharing;
        this.posts = [];
        this.likeArray = [];
        this.currentPlaying = null;
        this.play = true;
        this.storyPost = [];
        this.userStory = [];
        this.selectedUser = [];
        this.unlockedUser = [];
        this.postArr = [];
        this.imgVidPost = [];
        this.slideOpts = {
            slidesPerView: 4,
            spaceBetween: 5,
            initialSlide: 0,
        };
        this.slideOpts1 = {
            slidesPerView: 1,
            spaceBetween: 5,
            initialSlide: 0,
            slideShow: true,
            autoplay: true
        };
        this.route.queryParams.subscribe(params => {
            console.log(params);
            // if(params.addPost){
            //   this.loadData();
            // }
        });
    }
    ngOnInIt() {
    }
    viewPhoto(x) {
        console.log(x);
        this.photoViewer.show(x);
    }
    ionViewDidEnter() {
        this.loadData();
        // var obj={"userId":this.userId};
        // this.authService.story(obj).subscribe((data:any)=>{
        //   console.log(data);
        // });
    }
    ionViewWillLeave() {
        this.slides.stopAutoplay();
    }
    loadData() {
        this.imgVidPost = [];
        this.storage.get('user').then((user) => {
            this.userData = user;
            this.authService.loading("Loading posts...");
            console.log(user);
            this.userId = user._id;
            this.userName = user.fullName;
            this.userImg = user.user_img;
            var obj = { "userId": this.userData._id };
            this.authService.getUnlockUser(obj).subscribe((data) => {
                console.log(data);
                this.unlockedUser = data.data;
                this.unlockedUser.forEach(element => {
                    this.hostId = element.hostUserId;
                    var obj = { 'userId': this.hostId };
                    this.authService.getPost(obj).subscribe((data) => {
                        console.log(data);
                        this.postArr.push(data.data);
                    });
                    this.postArr.forEach(element => {
                        if (element.posttype == '2') {
                            this.storyPost.push(element);
                        }
                    });
                });
            });
            var obj = { 'userId': this.userId };
            this.authService.getPost(obj).subscribe((data) => {
                console.log(data);
                this.posts = data.data;
                this.posts.forEach(element => {
                    element.newDate1 = moment__WEBPACK_IMPORTED_MODULE_15__(element.createdDate).startOf('hour').fromNow();
                    if (element.posttype == '2' && element.userId == this.userId) {
                        this.userStory.push(element);
                    }
                    else if (element.posttype == '0' || element.posttype == '1') {
                        this.imgVidPost.push(element);
                    }
                });
            });
            this.authService.dismissLoading();
        });
    }
    gotoUserProfile(x) {
        console.log(x);
        if (x.userId == this.userId) {
            this.router.navigateByUrl('profile2');
        }
        else {
            var obj = { "userId": x.userId };
            this.authService.getUserProfileById(obj).subscribe((data) => {
                this.selectedUser = data.data;
                console.log(data);
                let navigationExtras = {
                    queryParams: {
                        pageroute: this.router.url,
                        user_id: x.userId,
                        professionalUser: JSON.stringify(this.selectedUser),
                        userType: data.userType
                    }
                };
                this.router.navigate(['profile1'], navigationExtras);
            });
        }
    }
    bookmark(item, i) {
        this.posts[i].isBookmarked = true;
        console.log(item);
        var obj = { 'postId': item._id, 'postUserId': item.userId, 'postUrl': item.postUrl,
            'userId': this.userId, 'postTitle': item.postTitle, 'postDescription': item.postDescription,
            'posttype': item.posttype, 'postCategory': item.postCategory, 'isBookmkared': true };
        this.authService.bookmark(obj).subscribe((data) => {
            console.log(data);
            if (data.success) {
                this.authService.presentToast('Bookmark Added');
            }
        });
    }
    deleteBookmark(item, i) {
        this.posts[i].isBookmarked = false;
        var obj = { 'userId': this.userId };
        this.authService.getBookmark(obj).subscribe((data) => {
            this.bookmarkedPost = data.bookmarkArray;
            console.log(this.bookmarkedPost);
            this.bookmarkedPost.forEach(element => {
                if (item._id == element.postId) {
                    var obj1 = { 'bookmarkId': element._id, 'userId': this.userId };
                    this.authService.deleteBookmark(obj1).subscribe((res) => {
                        console.log(res);
                        this.authService.presentToast('Bookmark Removed');
                    });
                }
            });
        });
    }
    likePost(item, i) {
        this.posts[i].isliked = true;
        this.posts[i].likeCount = item.likeCount + 1;
        var likeArray = item.likes;
        var likedPostUser = { 'likedPostUserid': this.userId, 'userName': this.userData.firstName,
            'userImage': this.userImg };
        likeArray.push(likedPostUser);
        console.log(likeArray);
        var obj = { 'likeArray': JSON.stringify(likeArray), 'postId': item._id, "likeCount": this.posts[i].likeCount,
            'userName': this.userData.firstName, 'hostId': item.userId, 'userId': this.userData._id, };
        this.authService.like(obj).subscribe((data) => {
            console.log(data);
            if (data.success) {
                // this.loadData();
            }
        });
    }
    deleteLike(item, j) {
        this.posts[j].isliked = false;
        this.posts[j].likeCount = item.likeCount - 1;
        item.isliked = false;
        var likeArray = item.likes;
        var index = 0;
        for (var i = 0; i < likeArray.length; i++) {
            if (likeArray[i].likedPostUser == this.userId) {
                index = i--;
                break;
            }
        }
        likeArray.splice(index, 1);
        console.log(likeArray);
        var obj = { 'likeArray': JSON.stringify(likeArray), 'postId': item._id, "likeCount": this.posts[j].likeCount };
        this.authService.deleteLike(obj).subscribe((res) => {
            console.log(res);
            // this.loadData();
        });
    }
    goToLike(item) {
        console.log(item);
        let navigationExtras = {
            queryParams: {
                special: this.router.url,
                selectedPost: JSON.stringify(item)
            }
        };
        this.router.navigate(['likes'], navigationExtras);
    }
    openStory(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            item.newDate = moment__WEBPACK_IMPORTED_MODULE_15__(item.createdDate).startOf('hour').fromNow();
            const modal = yield this.modalController.create({
                component: _story_story_page__WEBPACK_IMPORTED_MODULE_7__["StoryPage"],
                cssClass: 'storyModal',
                componentProps: { postData: item }
            });
            return yield modal.present();
        });
    }
    addStory(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.userStory.length != 0) {
                this.userStory[0].newDate = moment__WEBPACK_IMPORTED_MODULE_15__(this.userStory[0].createdDate).startOf('hour').fromNow();
                const modal = yield this.modalController.create({
                    component: _story_story_page__WEBPACK_IMPORTED_MODULE_7__["StoryPage"],
                    cssClass: 'storyModal',
                    componentProps: { postData: this.userStory }
                });
                return yield modal.present();
            }
            else if (this.userStory.length == 0) {
                const actionSheet = yield this.actionSheetController.create({
                    header: "Select Image source",
                    buttons: [{
                            text: 'Load from Library',
                            handler: () => {
                                this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, i);
                            }
                        },
                        {
                            text: 'Use Camera',
                            handler: () => {
                                this.takePicture(this.camera.PictureSourceType.CAMERA, i);
                            }
                        },
                        {
                            text: 'Cancel',
                            role: 'cancel'
                        }
                    ]
                });
                yield actionSheet.present();
            }
        });
    }
    takePicture(sourceType, i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.platform.is('ios')) {
                const options = {
                    quality: 100,
                    targetWidth: 900,
                    targetHeight: 600,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: sourceType,
                    encodingType: this.camera.EncodingType.JPEG
                };
                const tempImage = yield this.camera.getPicture(options);
                const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
                // Now, the opposite. Extract the full path, minus filename.
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/
                const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
                // Get the Data directory on the device.
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/
                const newBaseFilesystemPath = this.file.dataDirectory;
                yield this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);
                // Result example: file:///var/mobile/Containers/Data/Application
                // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                const storedPhoto = newBaseFilesystemPath + tempFilename;
                this.file.resolveLocalFilesystemUrl(storedPhoto)
                    .then(entry => {
                    entry.file(file => this.readFile(file, i));
                })
                    .catch(err => {
                    console.log(err);
                    // this.presentToast('Error while reading file.');
                });
            }
            else {
                const options = {
                    quality: 100,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: sourceType,
                    encodingType: this.camera.EncodingType.JPEG,
                };
                this.camera.getPicture(options).then((imageData) => {
                    this.file.resolveLocalFilesystemUrl(imageData).then((entry) => {
                        entry.file(file => {
                            console.log(file);
                            this.readFile(file, i);
                        });
                    });
                }, (err) => {
                    // Handle error
                });
            }
        });
    }
    readFile(file, i) {
        const reader = new FileReader();
        reader.onload = () => {
            // const formData = new FormData();
            const imgBlob = new Blob([reader.result], {
                type: file.type
            });
            // formData.append('file', imgBlob, file.name);
            // this.uploadImageData(formData);
            if (i === 1) {
                this.upload2Firebase(imgBlob);
            }
            else {
                console.log("if second image");
            }
        };
        reader.readAsArrayBuffer(file);
    }
    upload2Firebase(image) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.loading('Loading Image..');
            const file = image;
            const filePath = this.makeid(5);
            const fileRef = this.angularstorage.ref(filePath);
            //const newFile = new File(file);
            // let newFile= file.getURL().getFile();
            const task = this.angularstorage.upload(filePath, file);
            console.log(filePath);
            console.log(file);
            // observe percentage changes
            this.uploadPercent = task.percentageChanges();
            // get notified when the download URL is available
            yield task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(() => fileRef.getDownloadURL().subscribe(value => {
                this.imgUrl = value;
                this.uploaded = true;
                var obj = { 'userId': this.userData._id, 'posttype': '2',
                    'userImg': this.userData.user_img, 'postUrl': this.imgUrl,
                    'userName': this.userData.fullName };
                this.authService.addNewPost(obj).subscribe((data) => {
                    console.log(data);
                    if (data.success) {
                        this.authService.presentToast('Story added Successfully');
                        this.authService.dismissLoading();
                        this.loadData();
                    }
                });
            })))
                .subscribe();
        });
    }
    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    didScroll($event) {
        if (this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
            return;
        }
        else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
            // item is out of view, pause it
            this.currentPlaying.pause();
            this.currentPlaying = null;
        }
        this.videoPlayers.forEach(player => {
            console.log(player);
            if (this.currentPlaying) {
                return;
            }
            const nativeElement = player.nativeElement;
            const inView = this.isElementInViewport(nativeElement);
            if (inView) {
                this.currentPlaying = nativeElement;
                this.currentPlaying.muted = true;
                this.currentPlaying.play();
                console.log(this.currentPlaying);
                console.log(player);
                // var viewPostUser={'userId':this.userId};
                // var obj={'viewArray':JSON.stringify(viewPostUser),'postId':item._id,
                // 'userName':this.userName,'hostId':item.userId};
                // this.authService.view(obj).subscribe((data:any)=>{
                //   console.log(data);
                //   if(data.success){
                //     this.loadData();
                //   }
                // })      
            }
        });
    }
    openFullScreen(elem) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        }
        else if (elem.webkitEnterFullscreen) {
            elem.webkitEnterFullscreen();
            elem.enterFullscreen();
        }
    }
    isElementInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth));
    }
    openFirst() {
        this.menu.enable(true, 'first');
        this.menu.open('first');
    }
    notification() {
        let navigationExtras = {
            queryParams: {
                special: this.router.url
            }
        };
        this.router.navigate(['notification'], navigationExtras);
    }
    searchUser() {
        this.router.navigateByUrl('tabs/tab4');
    }
    doRefresh($event) {
        console.log($event);
        this.loadData();
        setTimeout(() => {
            console.log('Async operation has ended');
            $event.target.complete();
        }, 2000);
    }
    presentModal(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _question_question_page__WEBPACK_IMPORTED_MODULE_6__["QuestionPage"],
                cssClass: 'socialSharingModal',
                componentProps: { postData: item }
            });
            return yield modal.present();
        });
    }
    share(item) {
        var message = item.postTitle + ' \n' + item.postDescription;
        var postUrl = item.postUrl;
        this.socialSharing.share(message, null, null, postUrl);
    }
};
Tab1Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_14__["PhotoViewer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_12__["AngularFireStorage"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] },
    { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_11__["FilePath"] },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_13__["SocialSharing"] }
];
Tab1Page.propDecorators = {
    videoPlayers: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"], args: ['player',] }],
    slides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"], args: ['slidewithnav',] }]
};
Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab1.page.scss */ "./src/app/tab1/tab1.page.scss")).default]
    })
], Tab1Page);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map