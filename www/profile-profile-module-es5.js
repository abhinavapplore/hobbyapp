(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppProfileProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "  <ion-header class=\"ion-no-border\">\n    <ion-toolbar class=\"toolbar\">\n      <ion-row>\n        <ion-icon (click)=\"back()\" name=\"chevron-back\"></ion-icon>\n        <ion-text>Last Step</ion-text>\n      </ion-row>   \n    </ion-toolbar>\n  </ion-header>\n  \n\n<ion-content>\n\n  <ion-row class=\"row2\">\n    <ion-text class=\"text3\">Fill your details to get started</ion-text>\n  </ion-row>\n\n  <div class=\"div1\">\n    <ion-row class=\"row3\">\n      <ion-text class=\"text2\">Add a profile pic</ion-text>\n    </ion-row>\n  </div>\n  \n\n  <ion-row class=\"container\"   (click)=\"presentActionSheet1(1,'profile')\"> \n    <!-- <ion-avatar> -->\n      <ion-thumbnail class=\"thumbnail2\">\n        <ion-icon class=\"icon2\" *ngIf=\"!uploaded1\" name=\"person-add-outline\"></ion-icon>\n        <ion-img *ngIf=\"uploaded1\" [src]=\"imgUrl1\"></ion-img>\n      </ion-thumbnail>\n      \n    <!-- </ion-avatar>    -->\n    <div class=\"bottom-left\">\n      <img src=\"../../assets/images/signup/add.svg\">\n    </div>\n  </ion-row>\n\n  <div class=\"div1\">\n    <ion-row class=\"row3\">\n      <ion-text class=\"text2\">Your name</ion-text>\n    </ion-row>\n  </div>\n\n  <div class=\"div1\">\n    <ion-row class=\"row4\">\n        <ion-col [size]=6>\n          <ion-input class=\"input\" [(ngModel)]=\"firstName\" type='text'\n           placeholder=\"First\"></ion-input>\n        </ion-col>\n        <ion-col [size]=6>\n          <ion-input class=\"input\" [(ngModel)]=\"lastName\" type='text' \n          placeholder=\"Last\"></ion-input>\n        </ion-col>\n    </ion-row>\n  </div>\n\n  <div *ngIf=\"!showContactField\">\n    <div class=\"div1\">\n      <ion-row class=\"row3\">\n        <ion-text class=\"text2\">Email</ion-text>\n      </ion-row>\n    </div>\n  \n    <div style=\"margin-top: 1%;\" class=\"div1\">\n      <ion-row class=\"row4\">\n        <ion-input class=\"input1\" [(ngModel)]=\"email\" placeholder='Enter Your Email'></ion-input>\n      </ion-row>\n    </div>\n  </div>\n\n  <div *ngIf=\"showContactField\">\n    <div class=\"div1\">\n      <ion-row class=\"row3\">\n        <ion-text class=\"text2\">Mobile</ion-text>\n      </ion-row>\n    </div>\n  \n    <div style=\"margin-top: 1%;\" class=\"div1\">\n      <ion-row class=\"row4\">\n        <ion-input class=\"input1\" type=\"tel\" [(ngModel)]=\"mobile\" placeholder='Enter Your Mobile'></ion-input>\n      </ion-row>\n    </div>\n  </div>\n\n\n  \n  \n\n  <div class=\"div1\">\n    <ion-row class=\"row5\">\n        <ion-col [size]=5>\n          <ion-text class=\"text2\">Date of Birth</ion-text>\n        </ion-col>\n        <ion-col [size]=7 class=\"col\">\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" placeholder=\"dd/mm/yyyy\" \n            [(ngModel)]=\"dob\"></ion-datetime>\n          <div class=\"centered\">\n            <ion-icon class=\"icon\" name=\"calendar-outline\"></ion-icon>\n          </div>\n        </ion-col>\n    </ion-row>\n  </div>\n\n  <div class=\"div1\">\n    <ion-row class=\"row5\">\n        <ion-col [size]=5>\n          <ion-text class=\"text2\">Gender</ion-text>\n        </ion-col>\n        <ion-col [size]=7 class=\"col1\">      \n          <label class=\"container1\" (click)=\"userGender('male','x')\">Male\n            <input type=\"radio\" checked=\"checked\" name=\"radio\" value='male'>\n            <span class=\"checkmark\"></span>\n          </label>\n          \n          <label class=\"container1\" (click)=\"userGender('female','y')\">Female\n            <input type=\"radio\" name=\"radio\" value='female'>\n            <span class=\"checkmark\"></span>\n          </label>\n        </ion-col>\n    </ion-row>\n  </div>\n\n  <div class=\"div1\">\n    <ion-row class=\"row5\">\n        <ion-col [size]=5>\n          <ion-text class=\"text2\">Location</ion-text>\n        </ion-col>\n        <ion-col [size]=7 class=\"col2\" (click)=\"presentModal()\">\n          <img class=\"img\" src=\"../../assets/images/signup/pen.svg\">\n        </ion-col>\n    </ion-row>\n  </div>\n  \n  <div class=\"div8\">\n    <div class=\"div9\" id=\"map\"></div>\n  </div>\n\n  <div class=\"div2\">\n    <ion-row class=\"row6\">\n      <ion-text class=\"text3\">Languages you speak</ion-text>\n    </ion-row>\n    <ion-row class=\"row7\">\n      <ion-col [size]=4 class=\"col3\">\n        <ion-row *ngFor=\"let x of languageArray; let i=index\">\n          <label class=\"container2\">{{x.language}}\n            <input type=\"checkbox\">\n            <span class=\"checkmark1\" (click)=\"selectedLang(x,i)\"></span>\n          </label>\n        </ion-row>\n      </ion-col>\n      <ion-col [size]=8 class=\"col4\">      \n        <ion-input class=\"input1\" type='text' placeholder='any language'\n         [(ngModel)]=\"otherLang\"></ion-input>\n      </ion-col>\n    </ion-row>\n  </div>\n  \n  <div class=\"div1\" *ngIf=\"userType=='professional'\">\n    <ion-row class=\"row10\">\n       <ion-text class=\"text3\">Whats your skill level?</ion-text>\n    </ion-row>\n  </div>\n\n  <ion-slides *ngIf=\"userType=='professional'\" class=\"slides\" \n  [options]=\"slideOpts\" #slideWithNav>\n    <ion-slide *ngFor=\"let item of userSkills;let i=index\">\n      <div class=\"div4\">\n        <div class=\"div3\">\n          <ion-row class=\"row9\">\n             <ion-text class=\"text8\">{{item.hobby}}</ion-text>\n             <ion-thumbnail class=\"thumbnail\">\n               <ion-img class=\"img1\" [src]=\"item.hobbyImage\"></ion-img>\n             </ion-thumbnail>\n               <div class=\"container3\">\n                  <ion-range mode=\"ios\" (ionChange)=\"skill($event,i)\" min=\"0\" max=\"5\" step=\"1\" \n                  value=\"0\" snaps=\"true\" pin=\"true\" ></ion-range>\n                <div class=\"text4\">\n                  <ion-text>0</ion-text>\n                </div>\n                <div class=\"text5\">\n                  <ion-text>5</ion-text>\n                </div>\n               </div>\n               <ion-text class=\"text6\">{{level}}</ion-text>\n          </ion-row>\n        </div>\n      </div>\n    </ion-slide>\n  </ion-slides>\n  \n\n  <div class=\"div1\" *ngIf=\"userType=='professional'\">\n    <ion-row class=\"row10\">\n       <ion-text class=\"text3\">Upload documents/certificates</ion-text>\n    </ion-row>\n  </div>\n  \n  <div class=\"div11\" *ngIf=\"userType=='professional'\">\n    <div class=\"div6\">\n      <ion-row class=\"row11\">\n        <ion-col [size]=5 class=\"col5\" (click)=\"presentActionSheet1(1,'doc1')\">\n          <div *ngIf=\"!uploaded2\" >\n           <ion-icon class=\"icon1\" name=\"add-outline\"></ion-icon>\n          </div>\n          <div class=\"centered1\" *ngIf=\"uploaded2\">\n           <ion-thumbnail class=\"thumbnail1\">\n            <ion-img [src]=\"imgUrl2\"></ion-img>\n           </ion-thumbnail>          \n          </div>\n        </ion-col>\n        <ion-col [size]=5 class=\"col5\" (click)=\"presentActionSheet1(1,'doc2')\">\n           <div *ngIf=\"!uploaded3\">\n            <ion-icon class=\"icon1\" name=\"add-outline\"></ion-icon>\n           </div>\n             <div class=\"centered1\"  *ngIf=\"uploaded3\">\n              <ion-thumbnail class=\"thumbnail1\">\n                <ion-img [src]=\"imgUrl3\"></ion-img>\n              </ion-thumbnail>\n             </div>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n\n  <div class=\"div7\">\n    <ion-row class=\"row12\">\n      <ion-checkbox class=\"checkbox\" color=\"primary\" mode=\"md\" \n      color=\"primary\" (ionChange)=\"checkbox($event)\"></ion-checkbox>\n      <label class=\"text9\">I agree to the terms and conditions</label>\n      <!-- <label class=\"container4\">I agree to the terms and conditions\n        <input type=\"checkbox\" id=\"myCheckbox\" onclick=\"termsAndCon()\">\n        <span class=\"checkmark2\"></span>\n      </label> -->\n    </ion-row>\n  </div>\n\n  <div class=\"div10\" (click)=\"submit()\">\n    <ion-row class=\"row12\">\n      <div class=\"button\">\n        <ion-text class=\"text7\">SUBMIT</ion-text>\n      </div>\n    </ion-row>\n  </div>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/profile/profile-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/profile/profile-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: ProfilePageRoutingModule */

    /***/
    function srcAppProfileProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function () {
        return ProfilePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./profile.page */
      "./src/app/profile/profile.page.ts");

      var routes = [{
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
      }];

      var ProfilePageRoutingModule = function ProfilePageRoutingModule() {
        _classCallCheck(this, ProfilePageRoutingModule);
      };

      ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProfilePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/profile/profile.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/profile/profile.module.ts ***!
      \*******************************************/

    /*! exports provided: ProfilePageModule */

    /***/
    function srcAppProfileProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function () {
        return ProfilePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./profile-routing.module */
      "./src/app/profile/profile-routing.module.ts");
      /* harmony import */


      var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./profile.page */
      "./src/app/profile/profile.page.ts");

      var ProfilePageModule = function ProfilePageModule() {
        _classCallCheck(this, ProfilePageModule);
      };

      ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"]],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
      })], ProfilePageModule);
      /***/
    },

    /***/
    "./src/app/profile/profile.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/profile/profile.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppProfileProfilePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n\n.row2 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 10%;\n}\n\n.text1 {\n  width: 211px;\n  height: 28px;\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n}\n\n.text3 {\n  font-family: \"Poppins-Light\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n}\n\n.text2 {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n  font-style: normal;\n  text-align: center;\n}\n\n.row3 {\n  width: 85%;\n  justify-content: flex-start;\n  padding-top: 5%;\n}\n\n.container {\n  position: relative;\n  text-align: center;\n  font-family: \"Poppins-Bold\";\n  font-size: 24px;\n  line-height: 1.5;\n  font-weight: 700;\n  font-style: normal;\n  justify-content: center;\n  padding-top: 2%;\n}\n\n.bottom-left {\n  position: absolute;\n  left: 58%;\n  top: 85%;\n  background: #2196F3;\n  height: 20px;\n  border-radius: 50%;\n  width: 20px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.thumbnail2 {\n  height: 80px;\n  width: 80px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid black;\n}\n\n.input {\n  --placeholder-color: #C9C9C9;\n  font-family: \"Poppins-Regular\";\n  font-size: 18px;\n  background: #FFFFFF;\n  height: 60px;\n  width: 150px;\n  text-align: center;\n  text-transform: capitalize;\n  border-radius: 10px;\n}\n\n.input1 {\n  --placeholder-color: #C9C9C9;\n  font-family: \"Poppins-Regular\";\n  font-size: 18px;\n  background: #FFFFFF;\n  height: 60px;\n  width: 150px;\n  text-align: center;\n}\n\n.row4 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 1%;\n}\n\n.row5 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 10%;\n}\n\n.div1 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.div10 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\nion-datetime {\n  padding: 0;\n  width: 100%;\n  padding-left: 15px;\n}\n\n.col {\n  position: relative;\n  background: #ffffff;\n  height: 60px;\n  border-radius: 10px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.centered {\n  position: absolute;\n  top: 50%;\n  left: 85%;\n  transform: translate(-50%, -50%);\n}\n\n.icon {\n  font-size: 24px;\n  color: black;\n}\n\n.icon2 {\n  font-size: 45px;\n  color: blue;\n}\n\n.container1 {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  cursor: pointer;\n  font-size: 14px;\n  color: #8E8E8E;\n}\n\n.container1 input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n\n.checkmark {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 20px;\n  width: 20px;\n  background-color: #FFFFFF;\n  border-radius: 50%;\n}\n\n.container1:hover input ~ .checkmark {\n  background-color: #ccc;\n}\n\n.container1 input:checked ~ .checkmark {\n  background-color: #1961EB;\n}\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n\n.container1 input:checked ~ .checkmark:after {\n  display: block;\n}\n\n.container1 .checkmark:after {\n  top: 5px;\n  left: 5px;\n  width: 10px;\n  height: 10px;\n  border-radius: 50%;\n  background: white;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n}\n\n.col2 {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  align-items: center;\n}\n\n.img {\n  width: 15%;\n}\n\n.container2 {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 14px;\n  font-family: \"SFProDisplay-Regular\";\n}\n\n.container2 input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0;\n}\n\n.checkmark1 {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 20px;\n  width: 20px;\n  border-radius: 5px;\n  background-color: #FFFFFF;\n}\n\n.container2:hover input ~ .checkmark1 {\n  background-color: #FFFFFF;\n}\n\n.container2 input:checked ~ .checkmark1 {\n  background-color: #1961EB;\n}\n\n.checkmark1:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n\n.container2 input:checked ~ .checkmark1:after {\n  display: block;\n}\n\n.container2 .checkmark1:after {\n  left: 7px;\n  top: 3px;\n  width: 5px;\n  height: 10px;\n  border: 3px solid white;\n  border-width: 0 1px 1px 0;\n  transform: rotate(40deg);\n}\n\n.div2 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.row6 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n  padding-top: 10%;\n}\n\n.row7 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n  padding-top: 5%;\n}\n\n.row8 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.col3 {\n  display: flex;\n  flex-direction: column;\n}\n\n.col4 {\n  height: 150px;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  align-items: flex-end;\n}\n\n.div3 {\n  box-sizing: border-box;\n  width: 315px;\n  height: 220px;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-start;\n  align-items: center;\n  padding: 10px;\n  overflow: visible;\n  background-color: #ffffff;\n  box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 25px;\n}\n\n.row9 {\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  padding-top: 5%;\n  padding-bottom: 5%;\n}\n\n.row10 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  align-items: center;\n  padding-top: 10%;\n}\n\n.div4 {\n  width: 100%;\n  height: 250px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n\n.img1 {\n  width: 80px;\n  height: 80px;\n  border-radius: 10px;\n}\n\n.thumbnail {\n  width: 80px;\n  height: 80px;\n}\n\n.thumbnail1 {\n  width: 110px;\n  height: 100px;\n  border-radius: 10px;\n}\n\nion-range {\n  position: relative;\n  --bar-background: #CCCCCC;\n  --bar-background-active: #CCCCCC;\n  padding: 0;\n  width: 90%;\n  --pin-color: #003C69;\n}\n\n.container3 {\n  position: relative;\n  text-align: center;\n  color: white;\n  width: 90%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.text4 {\n  position: absolute;\n  right: 260px;\n  top: 35px;\n  color: #003C69;\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n}\n\n.text5 {\n  position: absolute;\n  top: 35px;\n  left: 260px;\n  color: #003C69;\n  font-family: \"Poppins-Bold\";\n  font-size: 14px;\n}\n\nion-item {\n  width: 100%;\n}\n\n.div5 {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-around;\n  align-items: center;\n}\n\n.text6 {\n  font-family: \"Poppins-Light\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n}\n\n.div6 {\n  box-sizing: border-box;\n  width: 315px;\n  height: 150px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  overflow: visible;\n  background-color: #ffffff;\n}\n\n.row11 {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-evenly;\n  align-items: center;\n}\n\n.col5 {\n  position: relative;\n  text-align: center;\n  background: #F6F7F9;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 120px;\n  border-radius: 10px;\n}\n\n.centered1 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.centered2 {\n  position: absolute;\n  top: 97%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.icon1 {\n  color: #79797A;\n  font-size: 50px;\n}\n\n.container4 {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 16px;\n  color: #A8A8A8;\n  font-family: \"SFProDisplay-Regular\";\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  user-select: none;\n}\n\n.container4 input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0;\n}\n\n.checkmark2 {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 20px;\n  width: 20px;\n  border-radius: 5px;\n  background-color: #FFFFFF;\n}\n\n.container4:hover input ~ .checkmark2 {\n  background-color: #FFFFFF;\n}\n\n.container4 input:checked ~ .checkmark2 {\n  background-color: #1961EB;\n}\n\n.checkmark2:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n\n.container4 input:checked ~ .checkmark2:after {\n  display: block;\n}\n\n.container4 .checkmark2:after {\n  left: 7px;\n  top: 3px;\n  width: 5px;\n  height: 10px;\n  border: 3px solid white;\n  border-width: 0 1px 1px 0;\n  transform: rotate(40deg);\n}\n\n.div7 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 20%;\n}\n\n.text7 {\n  height: 21px;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 18px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n\n.div8 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.row12 {\n  width: 85%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-top: 10%;\n}\n\n.div9 {\n  height: 100px;\n  width: 85%;\n  position: relative;\n  overflow: hidden;\n}\n\n.div10 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  padding-bottom: 10%;\n}\n\n.text8 {\n  font-family: \"Poppins-Light\";\n  color: #003C69;\n  font-size: 14px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 400;\n  font-style: normal;\n  text-align: center;\n  text-transform: capitalize;\n}\n\n.slides {\n  height: 250pxpx;\n}\n\n.div11 {\n  width: 100%;\n  height: 180px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.text9 {\n  margin-left: 5%;\n  font-family: \"Poppins-Regular\";\n  color: #555555;\n  font-size: 14px;\n}\n\n.checkbox {\n  width: 20px;\n  height: 20px;\n  border: 1px solid #555555;\n  border-radius: 5px;\n  --background-checked: #1961EB;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBT0U7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFpQkE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUFkSjs7QUFnQkU7RUFDRSw0QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQWJKOztBQWVFO0VBQ0UsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQVpKOztBQWNFO0VBQ0ksVUFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQVhOOztBQWFFO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQVlFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFUSjs7QUFvQkU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQWpCSjs7QUFtQkU7RUFDRSw0QkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7QUFoQko7O0FBa0JFO0VBQ0UsNEJBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFmSjs7QUFrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFmSjs7QUFpQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBZEo7O0FBZ0JFO0VBQ0UsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFiSjs7QUFlRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBWko7O0FBY0U7RUFDRSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBWEo7O0FBYUU7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQVZKOztBQVlFO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBVEo7O0FBV0U7RUFDRSxlQUFBO0VBQ0EsWUFBQTtBQVJKOztBQVdFO0VBQ0UsZUFBQTtFQUNBLFdBQUE7QUFSSjs7QUFXRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBUko7O0FBV0U7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FBUko7O0FBVUU7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FBUEo7O0FBU0U7RUFDRSxzQkFBQTtBQU5KOztBQVFFO0VBQ0UseUJBQUE7QUFMSjs7QUFPRTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUFKSjs7QUFNRTtFQUNFLGNBQUE7QUFISjs7QUFLRTtFQUNFLFFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBRko7O0FBSUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FBREo7O0FBR0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBRUE7RUFDSSxVQUFBO0FBQ0o7O0FBQ0E7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxtQ0FBQTtBQUVKOztBQUFFO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0FBR0o7O0FBREU7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FBSUo7O0FBRkU7RUFDRSx5QkFBQTtBQUtKOztBQUhFO0VBQ0UseUJBQUE7QUFNSjs7QUFKRTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUFPSjs7QUFMRTtFQUNFLGNBQUE7QUFRSjs7QUFORTtFQUNFLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7QUFTSjs7QUFQRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBVUo7O0FBUkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBV0o7O0FBVEU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFZSjs7QUFWRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBYUo7O0FBVkU7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7QUFhSjs7QUFYRTtFQUNFLGFBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0FBY0o7O0FBWkU7RUFDTSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsK0NBQUE7RUFDQSxtQkFBQTtBQWVSOztBQWJFO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FBZ0JKOztBQWJFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQWdCSjs7QUFkRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFpQko7O0FBZkU7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBa0JOOztBQWhCRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FBbUJKOztBQWpCRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUFvQko7O0FBbEJFO0VBQ0Usa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxvQkFBQTtBQXFCSjs7QUFuQkU7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQXNCSjs7QUFwQkU7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQXVCSjs7QUFyQkU7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtBQXdCSjs7QUF0QkU7RUFDSSxXQUFBO0FBeUJOOztBQXZCRTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FBMEJOOztBQXhCRTtFQUNFLDRCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBMkJKOztBQXpCRTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtBQTRCSjs7QUExQkE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7QUE2Qko7O0FBM0JBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQThCSjs7QUE1QkU7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUErQko7O0FBN0JFO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBZ0NKOztBQTlCRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBaUNKOztBQS9CRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQ0FBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFFQSxpQkFBQTtBQWtDSjs7QUFoQ0U7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7QUFtQ0o7O0FBakNFO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQW9DSjs7QUFsQ0U7RUFDRSx5QkFBQTtBQXFDSjs7QUFuQ0U7RUFDRSx5QkFBQTtBQXNDSjs7QUFwQ0U7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FBdUNKOztBQXJDRTtFQUNFLGNBQUE7QUF3Q0o7O0FBdENFO0VBQ0UsU0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtBQXlDSjs7QUF2Q0U7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBMENKOztBQXhDRTtFQUNFLFlBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBMkNKOztBQXpDRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBNENKOztBQTFDRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUE2Q0o7O0FBM0NFO0VBQ0UsYUFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBOENKOztBQTVDRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUErQ0o7O0FBN0NFO0VBQ0UsNEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtBQWdESjs7QUE5Q0E7RUFDRSxlQUFBO0FBaURGOztBQS9DQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQWtESjs7QUFoREE7RUFDRSxlQUFBO0VBQ0EsOEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQW1ERjs7QUFqREE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtBQW9ESiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taWNvbiB7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICB9XG4gIC8vIC5yb3cxIHtcbiAgLy8gICBkaXNwbGF5OiBmbGV4O1xuICAvLyAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIC8vICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIC8vICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgLy8gfVxuICAucm93MiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDEwJTtcbiAgfVxuLy8gICAudGV4dCB7XG4vLyAgICAgd2lkdGg6IDI2NnB4O1xuLy8gICAgIGhlaWdodDogMzdweDtcbi8vICAgICBvdmVyZmxvdzogaGlkZGVuO1xuLy8gICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuLy8gICAgIGNvbG9yOiAjZmZmZmZmO1xuLy8gICAgIGZvbnQtc2l6ZTogMjdweDtcbi8vICAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuLy8gICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4vLyAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbi8vIH1cbi50ZXh0MSB7XG4gICAgd2lkdGg6IDIxMXB4O1xuICAgIGhlaWdodDogMjhweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC50ZXh0MyB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1MaWdodFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC50ZXh0MiB7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBjb2xvcjogIzAwM0M2OTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAucm93MyB7XG4gICAgICB3aWR0aDogODUlO1xuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgcGFkZGluZy10b3A6IDUlO1xuICB9XG4gIC5jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDIlO1xuICB9XG4gIC5ib3R0b20tbGVmdCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDU4JTtcbiAgICB0b3A6IDg1JTtcbiAgICBiYWNrZ3JvdW5kOiAjMjE5NkYzO1xuICAgIGhlaWdodDogMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLy8gaW9uLWF2YXRhciB7XG4gIC8vICAgaGVpZ2h0OiA4MHB4O1xuICAvLyAgIHdpZHRoOiA4MHB4O1xuICAvLyAgIGRpc3BsYXk6IGZsZXg7XG4gIC8vICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgLy8gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgLy8gICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAvLyAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xuICAvLyB9XG4gIC50aHVtYm5haWwyIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgd2lkdGg6IDgwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIH1cbiAgLmlucHV0IHtcbiAgICAtLXBsYWNlaG9sZGVyLWNvbG9yOiAjQzlDOUM5O1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGhlaWdodDogNjBweDtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIH1cbiAgLmlucHV0MSB7XG4gICAgLS1wbGFjZWhvbGRlci1jb2xvcjogI0M5QzlDOTtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgd2lkdGg6IDE1MHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5yb3c0IHtcbiAgICB3aWR0aDogODUlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiAxJTtcbiAgfVxuICAucm93NSB7XG4gICAgd2lkdGg6IDg1JTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xuICB9XG4gIC5kaXYxIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuZGl2MTAge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIGlvbi1kYXRldGltZSB7XG4gICAgcGFkZGluZzogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIH1cbiAgLmNvbCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLmNlbnRlcmVkIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogODUlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB9XG4gIC5pY29uIHtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgY29sb3I6IGJsYWNrO1xuICB9XG5cbiAgLmljb24ye1xuICAgIGZvbnQtc2l6ZTogNDVweDtcbiAgICBjb2xvcjogYmx1ZTtcbiAgfVxuXG4gIC5jb250YWluZXIxIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZy1sZWZ0OiAzNXB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICM4RThFOEU7XG5cbiAgfVxuICAuY29udGFpbmVyMSBpbnB1dCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG9wYWNpdHk6IDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG4gIC5jaGVja21hcmsge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIH1cbiAgLmNvbnRhaW5lcjE6aG92ZXIgaW5wdXQgfiAuY2hlY2ttYXJrIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjO1xuICB9XG4gIC5jb250YWluZXIxIGlucHV0OmNoZWNrZWQgfiAuY2hlY2ttYXJrIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTk2MUVCO1xuICB9XG4gIC5jaGVja21hcms6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgLmNvbnRhaW5lcjEgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcms6YWZ0ZXIge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG4gIC5jb250YWluZXIxIC5jaGVja21hcms6YWZ0ZXIge1xuICAgIHRvcDogNXB4O1xuICAgIGxlZnQ6IDVweDtcbiAgICB3aWR0aDogMTBweDtcbiAgICBoZWlnaHQ6IDEwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLmNvbDEge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbDIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uaW1nIHtcbiAgICB3aWR0aDogMTUlO1xufVxuLmNvbnRhaW5lcjIge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBwYWRkaW5nLWxlZnQ6IDM1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtZmFtaWx5OiBcIlNGUHJvRGlzcGxheS1SZWd1bGFyXCI7XG4gIH1cbiAgLmNvbnRhaW5lcjIgaW5wdXQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBvcGFjaXR5OiAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBoZWlnaHQ6IDA7XG4gICAgd2lkdGg6IDA7XG4gIH1cbiAgLmNoZWNrbWFyazEge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG4gIH1cbiAgLmNvbnRhaW5lcjI6aG92ZXIgaW5wdXQgfiAuY2hlY2ttYXJrMSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgfVxuICAuY29udGFpbmVyMiBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyazEge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTYxRUI7XG4gIH1cbiAgLmNoZWNrbWFyazE6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgLmNvbnRhaW5lcjIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsxOmFmdGVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuICAuY29udGFpbmVyMiAuY2hlY2ttYXJrMTphZnRlciB7XG4gICAgbGVmdDogN3B4O1xuICAgIHRvcDogM3B4O1xuICAgIHdpZHRoOiA1cHg7XG4gICAgaGVpZ2h0OiAxMHB4O1xuICAgIGJvcmRlcjogM3B4IHNvbGlkIHdoaXRlO1xuICAgIGJvcmRlci13aWR0aDogMCAxcHggMXB4IDA7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoNDBkZWcpO1xuICB9XG4gIC5kaXYyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAucm93NiB7XG4gICAgd2lkdGg6IDg1JTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDEwJTtcbiAgfVxuICAucm93NyB7XG4gICAgd2lkdGg6IDg1JTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICB9XG4gIC5yb3c4IHtcbiAgICB3aWR0aDogODUlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuIFxuICAuY29sMyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB9XG4gIC5jb2w0IHtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgfVxuICAuZGl2MyB7XG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICAgIHdpZHRoOiAzMTVweDtcbiAgICAgICAgaGVpZ2h0OiAyMjBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIH1cbiAgLnJvdzkge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIHBhZGRpbmctYm90dG9tOiA1JTtcbiAgICAgXG4gIH1cbiAgLnJvdzEwIHtcbiAgICB3aWR0aDogODUlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xuICB9XG4gIC5kaXY0IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDI1MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgfVxuICAuaW1nMSB7XG4gICAgICB3aWR0aDogODBweDtcbiAgICAgIGhlaWdodDogODBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIH1cbiAgLnRodW1ibmFpbCB7XG4gICAgd2lkdGg6IDgwcHg7XG4gICAgaGVpZ2h0OiA4MHB4O1xuICB9XG4gIC50aHVtYm5haWwxIHtcbiAgICB3aWR0aDogMTEwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB9XG4gIGlvbi1yYW5nZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIC0tYmFyLWJhY2tncm91bmQ6ICNDQ0NDQ0M7XG4gICAgLS1iYXItYmFja2dyb3VuZC1hY3RpdmU6ICNDQ0NDQ0M7XG4gICAgcGFkZGluZzogMDtcbiAgICB3aWR0aDogOTAlO1xuICAgIC0tcGluLWNvbG9yOiAjMDAzQzY5O1xuICB9XG4gIC5jb250YWluZXIzIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogOTAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgfVxuICAudGV4dDQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMjYwcHg7XG4gICAgdG9wOiAzNXB4O1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAudGV4dDUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDM1cHg7XG4gICAgbGVmdDogMjYwcHg7XG4gICAgY29sb3I6ICMwMDNDNjk7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG4gIGlvbi1pdGVtIHtcbiAgICAgIHdpZHRoOiAxMDAlOyAgICBcbiAgfVxuICAuZGl2NSB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC50ZXh0NiB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1MaWdodFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5kaXY2IHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHdpZHRoOiAzMTVweDtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG4ucm93MTEge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb2w1IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6ICNGNkY3Rjk7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB9XG4gIC5jZW50ZXJlZDEge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDUwJTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIH1cbiAgLmNlbnRlcmVkMiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogOTclO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgfVxuICAuaWNvbjEge1xuICAgIGNvbG9yOiAjNzk3OTdBO1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgfVxuICAuY29udGFpbmVyNCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmctbGVmdDogMzVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6ICNBOEE4QTg7XG4gICAgZm9udC1mYW1pbHk6IFwiU0ZQcm9EaXNwbGF5LVJlZ3VsYXJcIjtcbiAgICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xuICAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICAgIHVzZXItc2VsZWN0OiBub25lO1xuICB9XG4gIC5jb250YWluZXI0IGlucHV0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgb3BhY2l0eTogMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgaGVpZ2h0OiAwO1xuICAgIHdpZHRoOiAwO1xuICB9XG4gIC5jaGVja21hcmsyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xuICB9XG4gIC5jb250YWluZXI0OmhvdmVyIGlucHV0IH4gLmNoZWNrbWFyazIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG4gIH1cbiAgLmNvbnRhaW5lcjQgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTk2MUVCO1xuICB9XG4gIC5jaGVja21hcmsyOmFmdGVyIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5jb250YWluZXI0IGlucHV0OmNoZWNrZWQgfiAuY2hlY2ttYXJrMjphZnRlciB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbiAgLmNvbnRhaW5lcjQgLmNoZWNrbWFyazI6YWZ0ZXIge1xuICAgIGxlZnQ6IDdweDtcbiAgICB0b3A6IDNweDtcbiAgICB3aWR0aDogNXB4O1xuICAgIGhlaWdodDogMTBweDtcbiAgICBib3JkZXI6IDNweCBzb2xpZCB3aGl0ZTtcbiAgICBib3JkZXItd2lkdGg6IDAgMXB4IDFweCAwO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDQwZGVnKTtcbiAgfVxuICAuZGl2NyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDIwJTtcbiAgfVxuICAudGV4dDcge1xuICAgIGhlaWdodDogMjFweDtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIH1cbiAgLmRpdjgge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5yb3cxMiB7XG4gICAgd2lkdGg6IDg1JTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xuICB9XG4gIC5kaXY5IHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHdpZHRoOiA4NSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbiAgLmRpdjEwIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTAlO1xuICB9XG4gIC50ZXh0OCB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1MaWdodFwiO1xuICAgIGNvbG9yOiAjMDAzQzY5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICB9XG4uc2xpZGVzIHtcbiAgaGVpZ2h0OiAyNTBweHB4O1xufVxuLmRpdjExIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDE4MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnRleHQ5IHtcbiAgbWFyZ2luLWxlZnQ6IDUlO1xuICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgY29sb3I6ICM1NTU1NTU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5jaGVja2JveCB7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1NTU1NTU7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjMTk2MUVCO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/profile/profile.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/profile/profile.page.ts ***!
      \*****************************************/

    /*! exports provided: ProfilePage */

    /***/
    function srcAppProfileProfilePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePage", function () {
        return ProfilePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! moment */
      "./node_modules/moment/moment.js");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/file-path/ngx */
      "./node_modules/@ionic-native/file-path/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/fire/storage */
      "./node_modules/@angular/fire/__ivy_ngcc__/storage/es2015/index.js");
      /* harmony import */


      var firebase_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! firebase/storage */
      "./node_modules/firebase/storage/dist/index.esm.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");
      /* harmony import */


      var firebase__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! firebase */
      "./node_modules/firebase/dist/index.cjs.js");
      /* harmony import */


      var firebase__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_17__);
      /* harmony import */


      var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! @ionic-native/fcm/ngx */
      "./node_modules/@ionic-native/fcm/__ivy_ngcc__/ngx/index.js");

      var ProfilePage = /*#__PURE__*/function () {
        function ProfilePage(geolocation, authService, platform, modalController, angularstorage, file, filePath, httpClient, fcm, actionSheetController, storage, camera, router, route, afa, fs, navCtrl) {
          var _this = this;

          _classCallCheck(this, ProfilePage);

          this.geolocation = geolocation;
          this.authService = authService;
          this.platform = platform;
          this.modalController = modalController;
          this.angularstorage = angularstorage;
          this.file = file;
          this.filePath = filePath;
          this.httpClient = httpClient;
          this.fcm = fcm;
          this.actionSheetController = actionSheetController;
          this.storage = storage;
          this.camera = camera;
          this.router = router;
          this.route = route;
          this.afa = afa;
          this.fs = fs;
          this.navCtrl = navCtrl;
          this.level = 'Beginner';
          this.firstName = "";
          this.lastName = "";
          this.email = "";
          this.gender = 'male';
          this.selectedLanguage = [];
          this.uploaded1 = false;
          this.imgUrl1 = "";
          this.uploaded2 = false;
          this.imgUrl2 = "";
          this.uploaded3 = false;
          this.imgUrl3 = "";
          this.checked = false;
          this.hobbyArray = [];
          this.hobbyArray1 = [];
          this.userImg = '';
          this.base64Img = '';
          this.languageArray = [{
            'language': 'English',
            'isSelected': false
          }, {
            'language': 'Hindi',
            'isSelected': false
          }, {
            'language': 'Spanish',
            'isSelected': false
          }, {
            'language': 'French',
            'isSelected': false
          }, {
            'language': 'other',
            'isSelected': false
          }];
          this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          };
          this.gelleryOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL
          };
          this.slideOpts = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: 0
          };
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              _this.pageRoute = params.special;
            }
          });
        }

        _createClass(ProfilePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.fcm.getToken().then(function (token) {
              _this2.deviceKey = token;
              console.log(_this2.deviceKey);
              console.log('device key....');
            });
            this.storage.get("userType").then(function (data) {
              _this2.storage.get('isEmailLogin').then(function (res) {
                if (res === true) {
                  _this2.storage.get('emailId').then(function (item) {
                    _this2.email = item;
                    _this2.showContactField = true;
                  });
                } else {
                  _this2.storage.get("contactNum").then(function (res) {
                    _this2.showContactField = false;
                    _this2.mobile = res;
                  });
                }
              });

              _this2.userType = data;
              console.log(_this2.userType);
            });
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            var _this3 = this;

            this.storage.get('userHobby').then(function (res) {
              _this3.hobbyArray = res;
            });
            this.storage.get("userSkills").then(function (data) {
              _this3.userSkills = data; // for(var i=0; i<this.userSkills.length;i++){
              //   if(i==this.userSkills.length-1){
              //   }else if(){
              //   }
              // }
              // this.skillString=
            });
            this.geolocation.getCurrentPosition().then(function (resp) {
              // resp.coords.latitude
              _this3.latitude = resp.coords.latitude;
              console.log(_this3.latitude); // resp.coords.longitude

              _this3.longitude = resp.coords.longitude;
              console.log(_this3.longitude);

              _this3.testmap();
            });
          }
        }, {
          key: "skill",
          value: function skill($event, index) {
            console.log($event);
            this.skillLevel = $event.detail.value;
            this.hobbyArray[index].skillLevel = this.skillLevel;
            console.log(this.hobbyArray);
            console.log(this.skillLevel);

            if (this.skillLevel == 0 || this.skillLevel == 1 || this.skillLevel == 2) {
              this.level = 'Beginner';
              this.hobbyArray[index].level = this.level;
            } else if (this.skillLevel == 3 || this.skillLevel == 4) {
              this.level = 'Intermediate';
              this.hobbyArray[index].level = this.level;
            } else {
              this.level = 'Expert';
              this.hobbyArray[index].level = this.level;
            }

            if (this.skillLevel != '' && this.skillLevel != undefined) {
              if (index == this.hobbyArray.length - 1) {
                this.slidesRef.slideTo(index[0]);
              } else {
                index = index + 1;
                console.log(typeof index);
                console.log(index);
                this.slidesRef.slideTo(index);
              }
            }
          }
        }, {
          key: "testmap",
          value: function testmap() {
            var myLatlng = new google.maps.LatLng(this.latitude, this.longitude);
            console.log(myLatlng);
            var mapOptions = {
              zoom: 12,
              center: myLatlng,
              mapTypeControl: false,
              scaleControl: false,
              zoomControl: false,
              streetViewControl: false,
              rotateControl: false,
              fullscreenControl: false,
              styles: [{
                stylers: [{
                  saturation: -100
                }]
              }]
            };
            this.map = new google.maps.Map(document.getElementById("map"), mapOptions); //Add User Location Marker To Map

            var marker = new google.maps.Marker({
              position: myLatlng,
              draggable: true
            }); // To add the marker to the map, call setMap();

            marker.setMap(this.map);
            marker.addListener('dragend', function (event) {
              console.log(event);
              this.latitude = event.latLng.lat();
              console.log(this.latitude);
              this.longitude = event.latLng.lng();
              console.log(this.longitude);
            }); //Map Click Event Listner

            this.map.addListener('click', function () {//add functions here
            });
          }
        }, {
          key: "userGender",
          value: function userGender(x, y) {
            this.gender = x;
            console.log(this.gender);
          }
        }, {
          key: "selectedLang",
          value: function selectedLang(item, i) {
            console.log(item);
            console.log(i);

            for (var j = 0; j < this.languageArray.length; j++) {
              if (!this.languageArray[j].isSelected && i == j) {
                this.languageArray[j].isSelected = true;
                console.log(this.languageArray);
              }
            }
          }
        }, {
          key: "presentActionSheet1",
          value: function presentActionSheet1(i, j) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this4 = this;

              var actionSheet, _actionSheet, _actionSheet2;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (!(j == 'profile')) {
                        _context.next = 8;
                        break;
                      }

                      _context.next = 3;
                      return this.actionSheetController.create({
                        header: "Select Image source",
                        buttons: [{
                          text: 'Load from Library',
                          handler: function handler() {
                            _this4.takePicture1(_this4.camera.PictureSourceType.PHOTOLIBRARY, i);
                          }
                        }, {
                          text: 'Use Camera',
                          handler: function handler() {
                            _this4.takePicture1(_this4.camera.PictureSourceType.CAMERA, i);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 3:
                      actionSheet = _context.sent;
                      _context.next = 6;
                      return actionSheet.present();

                    case 6:
                      _context.next = 21;
                      break;

                    case 8:
                      if (!(j == 'doc1')) {
                        _context.next = 16;
                        break;
                      }

                      _context.next = 11;
                      return this.actionSheetController.create({
                        header: "Select Image source",
                        buttons: [{
                          text: 'Load from Library',
                          handler: function handler() {
                            _this4.takePicture2(_this4.camera.PictureSourceType.PHOTOLIBRARY, i);
                          }
                        }, {
                          text: 'Use Camera',
                          handler: function handler() {
                            _this4.takePicture2(_this4.camera.PictureSourceType.CAMERA, i);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 11:
                      _actionSheet = _context.sent;
                      _context.next = 14;
                      return _actionSheet.present();

                    case 14:
                      _context.next = 21;
                      break;

                    case 16:
                      _context.next = 18;
                      return this.actionSheetController.create({
                        header: "Select Image source",
                        buttons: [{
                          text: 'Load from Library',
                          handler: function handler() {
                            _this4.takePicture3(_this4.camera.PictureSourceType.PHOTOLIBRARY, i);
                          }
                        }, {
                          text: 'Use Camera',
                          handler: function handler() {
                            _this4.takePicture3(_this4.camera.PictureSourceType.CAMERA, i);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 18:
                      _actionSheet2 = _context.sent;
                      _context.next = 21;
                      return _actionSheet2.present();

                    case 21:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "takePicture1",
          value: function takePicture1(sourceType, i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this5 = this;

              var options, tempImage, tempFilename, tempBaseFilesystemPath, newBaseFilesystemPath, storedPhoto, _options;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!this.platform.is('ios')) {
                        _context2.next = 14;
                        break;
                      }

                      options = {
                        quality: 100,
                        targetWidth: 900,
                        targetHeight: 600,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      _context2.next = 4;
                      return this.camera.getPicture(options);

                    case 4:
                      tempImage = _context2.sent;
                      tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1); // Now, the opposite. Extract the full path, minus filename.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/

                      tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1); // Get the Data directory on the device.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/

                      newBaseFilesystemPath = this.file.dataDirectory;
                      _context2.next = 10;
                      return this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);

                    case 10:
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                      storedPhoto = newBaseFilesystemPath + tempFilename;
                      this.file.resolveLocalFilesystemUrl(storedPhoto).then(function (entry) {
                        entry.file(function (file) {
                          return _this5.readFile(file, i);
                        });
                      })["catch"](function (err) {
                        console.log(err); // this.presentToast('Error while reading file.');
                      });
                      _context2.next = 16;
                      break;

                    case 14:
                      _options = {
                        quality: 100,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      this.camera.getPicture(_options).then(function (imageData) {
                        // this._file.resolveLocalFilesystemUrl(
                        //   imageData,
                        //   (entry: FileEntry) => {console.log(entry)},
                        //   err => console.log(err)
                        // );
                        _this5.file.resolveLocalFilesystemUrl(imageData).then(function (entry) {
                          entry.file(function (file) {
                            console.log(file);

                            _this5.readFile(file, i);
                          });
                        });
                      }, function (err) {// Handle error
                      });

                    case 16:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "takePicture2",
          value: function takePicture2(sourceType, i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this6 = this;

              var options, tempImage, tempFilename, tempBaseFilesystemPath, newBaseFilesystemPath, storedPhoto, _options2;

              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (!this.platform.is('ios')) {
                        _context3.next = 14;
                        break;
                      }

                      options = {
                        quality: 100,
                        targetWidth: 900,
                        targetHeight: 600,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      _context3.next = 4;
                      return this.camera.getPicture(options);

                    case 4:
                      tempImage = _context3.sent;
                      tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1); // Now, the opposite. Extract the full path, minus filename.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/

                      tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1); // Get the Data directory on the device.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/

                      newBaseFilesystemPath = this.file.dataDirectory;
                      _context3.next = 10;
                      return this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);

                    case 10:
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                      storedPhoto = newBaseFilesystemPath + tempFilename;
                      this.file.resolveLocalFilesystemUrl(storedPhoto).then(function (entry) {
                        entry.file(function (file) {
                          return _this6.readFile2(file, i);
                        });
                      })["catch"](function (err) {
                        console.log(err); // this.presentToast('Error while reading file.');
                      });
                      _context3.next = 16;
                      break;

                    case 14:
                      _options2 = {
                        quality: 100,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      this.camera.getPicture(_options2).then(function (imageData) {
                        // this._file.resolveLocalFilesystemUrl(
                        //   imageData,
                        //   (entry: FileEntry) => {console.log(entry)},
                        //   err => console.log(err)
                        // );
                        _this6.file.resolveLocalFilesystemUrl(imageData).then(function (entry) {
                          entry.file(function (file) {
                            console.log(file);

                            _this6.readFile2(file, i);
                          });
                        });
                      }, function (err) {// Handle error
                      });

                    case 16:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "takePicture3",
          value: function takePicture3(sourceType, i) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this7 = this;

              var options, tempImage, tempFilename, tempBaseFilesystemPath, newBaseFilesystemPath, storedPhoto, _options3;

              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      if (!this.platform.is('ios')) {
                        _context4.next = 14;
                        break;
                      }

                      options = {
                        quality: 100,
                        targetWidth: 900,
                        targetHeight: 600,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      _context4.next = 4;
                      return this.camera.getPicture(options);

                    case 4:
                      tempImage = _context4.sent;
                      tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1); // Now, the opposite. Extract the full path, minus filename.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/tmp/

                      tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1); // Get the Data directory on the device.
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/

                      newBaseFilesystemPath = this.file.dataDirectory;
                      _context4.next = 10;
                      return this.file.copyFile(tempBaseFilesystemPath, tempFilename, newBaseFilesystemPath, tempFilename);

                    case 10:
                      // Result example: file:///var/mobile/Containers/Data/Application
                      // /E4A79B4A-E5CB-4E0C-A7D9-0603ECD48690/Library/NoCloud/cdv_photo_003.jpg
                      storedPhoto = newBaseFilesystemPath + tempFilename;
                      this.file.resolveLocalFilesystemUrl(storedPhoto).then(function (entry) {
                        entry.file(function (file) {
                          return _this7.readFile3(file, i);
                        });
                      })["catch"](function (err) {
                        console.log(err); // this.presentToast('Error while reading file.');
                      });
                      _context4.next = 16;
                      break;

                    case 14:
                      _options3 = {
                        quality: 100,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: sourceType,
                        encodingType: this.camera.EncodingType.JPEG
                      };
                      this.camera.getPicture(_options3).then(function (imageData) {
                        // this._file.resolveLocalFilesystemUrl(
                        //   imageData,
                        //   (entry: FileEntry) => {console.log(entry)},
                        //   err => console.log(err)
                        // );
                        _this7.file.resolveLocalFilesystemUrl(imageData).then(function (entry) {
                          entry.file(function (file) {
                            console.log(file);

                            _this7.readFile3(file, i);
                          });
                        });
                      }, function (err) {// Handle error
                      });

                    case 16:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "readFile",
          value: function readFile(file, i) {
            var _this8 = this;

            var reader = new FileReader();

            reader.onload = function () {
              // const formData = new FormData();
              var imgBlob = new Blob([reader.result], {
                type: file.type
              }); // formData.append('file', imgBlob, file.name);
              // this.uploadImageData(formData);

              if (i === 1) {
                _this8.upload2Firebase1(imgBlob);
              } else {
                console.log("if second image");
              }
            };

            reader.readAsArrayBuffer(file);
          }
        }, {
          key: "readFile2",
          value: function readFile2(file, i) {
            var _this9 = this;

            var reader = new FileReader();

            reader.onload = function () {
              // const formData = new FormData();
              var imgBlob2 = new Blob([reader.result], {
                type: file.type
              }); // formData.append('file', imgBlob, file.name);
              // this.uploadImageData(formData);

              if (i === 1) {
                _this9.upload2Firebase2(imgBlob2);
              } else {
                console.log("if second image");
              }
            };

            reader.readAsArrayBuffer(file);
          }
        }, {
          key: "readFile3",
          value: function readFile3(file, i) {
            var _this10 = this;

            var reader = new FileReader();

            reader.onload = function () {
              // const formData = new FormData();
              var imgBlob3 = new Blob([reader.result], {
                type: file.type
              }); // formData.append('file', imgBlob, file.name);
              // this.uploadImageData(formData);

              if (i === 1) {
                _this10.upload2Firebase3(imgBlob3);
              } else {
                console.log("if second image");
              }
            };

            reader.readAsArrayBuffer(file);
          }
        }, {
          key: "createFileName",
          value: function createFileName() {
            var d = new Date(),
                n = d.getTime(),
                newFileName = n + ".jpg";
            return newFileName;
          }
        }, {
          key: "makeid",
          value: function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;

            for (var i = 0; i < length; i++) {
              result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            return result;
          } // uploadFile(event) {
          // const file = event.target.files[0];
          // const filePath = this.makeid(5);
          // const fileRef = this.angularstorage.ref(filePath);
          // const task = this.angularstorage.upload(filePath, file);
          // // observe percentage changes
          // this.uploadPercent = task.percentageChanges();
          // // get notified when the download URL is available
          // task.snapshotChanges().pipe(
          // finalize(() => fileRef.getDownloadURL().subscribe(value => {
          // this.imgUrl1 = value;
          // this.uploaded1 = true;
          // }))
          // )
          // .subscribe()
          // }

        }, {
          key: "upload2Firebase1",
          value: function upload2Firebase1(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this11 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      this.authService.loading('Loading Profile..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context5.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this11.imgUrl1 = value;
                          _this11.uploaded1 = true;
                          console.log(_this11.imgUrl1);

                          _this11.authService.dismissLoading();
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "upload2Firebase2",
          value: function upload2Firebase2(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this12 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      this.authService.loading('Loading Document..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context6.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this12.imgUrl2 = value;
                          _this12.uploaded2 = true;
                          console.log(_this12.imgUrl1);

                          _this12.authService.dismissLoading();
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "upload2Firebase3",
          value: function upload2Firebase3(image) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this13 = this;

              var file, filePath, fileRef, task;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      this.authService.loading('Loading Document..');
                      file = image;
                      filePath = this.makeid(5);
                      fileRef = this.angularstorage.ref(filePath); //const newFile = new File(file);
                      // let newFile= file.getURL().getFile();

                      task = this.angularstorage.upload(filePath, file);
                      console.log(filePath);
                      console.log(file); // observe percentage changes

                      this.uploadPercent = task.percentageChanges(); // get notified when the download URL is available

                      _context7.next = 10;
                      return task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () {
                        return fileRef.getDownloadURL().subscribe(function (value) {
                          _this13.imgUrl3 = value;
                          _this13.uploaded3 = true;
                          console.log(_this13.imgUrl1);

                          _this13.authService.dismissLoading();
                        });
                      })).subscribe();

                    case 10:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "checkbox",
          value: function checkbox($event) {
            console.log($event);
            this.checked = $event.detail.checked;
          }
        }, {
          key: "submit",
          value: function submit() {
            var _this14 = this;

            if (this.email == "") {
              this.authService.presentToast("Email is required.");
            } else if (this.firstName == "") {
              this.authService.presentToast("Firstname is required.");
            } else if (this.lastName == "") {
              this.authService.presentToast("Lastname is required.");
            } else if (this.imgUrl1 == "") {
              this.authService.presentToast("Profile picture is required.");
            } else if (!this.checked) {
              this.authService.presentToast("Please Agree To Terms & Conditions.");
            } else {
              if (this.checked) {
                if (moment__WEBPACK_IMPORTED_MODULE_2__(this.dob) > moment__WEBPACK_IMPORTED_MODULE_2__()) {
                  this.authService.presentToast("Dob cannot be future date.");
                } else {
                  this.authService.loading('Creating your profile'); // this.storage.get("userType").then((data)=>{

                  var userName = this.firstName + " " + this.lastName;
                  var lName = this.lastName.charAt(0);
                  var dateOfBirth = this.dob;
                  var gender = this.gender;
                  var latitude = this.latitude;
                  var longitude = this.longitude;
                  var lat = JSON.parse(latitude);
                  var lng = JSON.parse(longitude);
                  var latlng = lat + ',' + lng;
                  console.log(latlng);
                  this.authService.httpClient.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latlng + '&sensor=true&key=AIzaSyApm82MFXMcROWrHaGTj-auUcyOYQwBwsE').subscribe(function (addResponse) {
                    console.log(addResponse);
                    var str = addResponse.plus_code.compound_code;
                    var newstr = str.slice(7);
                    var address = newstr.replace(/\w+[.!?]?$/, '');
                    var language = _this14.languageArray;
                    var hobby = _this14.hobbyArray;
                    var userImage = _this14.imgUrl1;
                    var userDocuents = [{
                      'docUrl': _this14.imgUrl2
                    }, {
                      'docUrl': _this14.imgUrl3
                    }];
                    var obj = {
                      'fullName': userName,
                      "firstName": _this14.firstName,
                      "lastName": lName,
                      'dob': dateOfBirth,
                      'contactNum': _this14.mobile,
                      'gender': gender,
                      'userLatitude': latitude,
                      'userLongitude': longitude,
                      'language': language,
                      'userHobby': hobby,
                      'user_img': userImage,
                      'documents': userDocuents,
                      "userType": _this14.userType,
                      'emailId': _this14.email,
                      emailIdRegistered: true,
                      'userSkills': _this14.userSkills,
                      'address': address,
                      'deviceId': _this14.deviceKey
                    };

                    _this14.authService.register(obj).subscribe(function (res) {
                      if (res.success == false) {
                        console.log('Something went wrong');

                        _this14.authService.dismissLoading();

                        _this14.authService.presentToast("Please try again.");
                      } else {
                        firebase__WEBPACK_IMPORTED_MODULE_17__["auth"]().createUserWithEmailAndPassword(_this14.email, '12345678').then(function (userFire) {
                          console.log(userFire);
                        });

                        _this14.storage.set("ACCESS_TOKEN", res.token);

                        _this14.storage.set("user", res.user);

                        console.log(res.user);

                        _this14.fs.collection('friends').doc(res.user._id).set({
                          userEmail: res.user.emailId,
                          Name: res.user.fullName,
                          displayName: res.user.fullName,
                          photoURL: res.user.user_img,
                          UserID: res.user._id,
                          Timestamp: firebase__WEBPACK_IMPORTED_MODULE_17__["firestore"].FieldValue.serverTimestamp()
                        }, {
                          merge: true
                        });

                        _this14.authService.dismissLoading();

                        _this14.navCtrl.navigateRoot('tabs/tab1');
                      }
                    });
                  }); // });
                }
              } else {
                this.authService.presentToast('Please agree to terms and conditions.');
              }
            }
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl('hobby');
          }
        }, {
          key: "content",
          set: function set(slideWithNav) {
            if (slideWithNav) {
              this.slidesRef = slideWithNav; //Here you can set any properties you would like
            }
          }
        }]);

        return ProfilePage;
      }();

      ProfilePage.ctorParameters = function () {
        return [{
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__["Geolocation"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__["ModalController"]
        }, {
          type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_10__["AngularFireStorage"]
        }, {
          type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"]
        }, {
          type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClient"]
        }, {
          type: _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_18__["FCM"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__["ActionSheetController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }, {
          type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_14__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_14__["ActivatedRoute"]
        }, {
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_15__["AngularFireAuth"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_16__["AngularFirestore"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__["NavController"]
        }];
      };

      ProfilePage.propDecorators = {
        content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['slideWithNav', {
            "static": false
          }]
        }]
      };
      ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./profile.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./profile.page.scss */
        "./src/app/profile/profile.page.scss"))["default"]]
      })], ProfilePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=profile-profile-module-es5.js.map