(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notification-notification-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar1\">\n    <ion-row>\n      <ion-col [size]=2 (click)=\"back()\">\n        <ion-icon name=\"chevron-back\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text>Notifications</ion-text>\n      </ion-col> \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n  <div>\n    <div class=\"div\" *ngFor='let item of notificationData'>\n      <!-- <div > -->\n        <ion-row class=\"row2\" *ngIf=\"item.notificationType=='like'\" >\n          <ion-col [size]=3 class=\"col1\">\n            <ion-avatar>\n              <ion-img class=\"img\" [src]=\"item.transactionImage\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col [size]=9 class=\"col2\">\n            <ion-row class=\"row3\">\n              <ion-text class=\"text2\">Add to wallet</ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text2\">₹ {{item.transactionAmount}}</ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text3\">{{item.transactionDate}}</ion-text>\n              <ion-text class=\"text4\">{{item.transactionTime}}</ion-text>\n            </ion-row>\n            <div class=\"centered\">\n              <img class=\"img\" src=\"../../assets/images/wallet/tick.png\">\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row class=\"row2\" *ngIf=\"item.notificationType=='unlock'\">\n          <ion-col [size]=3 class=\"col1\">\n            <ion-avatar>\n              <ion-img class=\"img\" src=\"../../assets/hobbyImage/singing.jpg\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col [size]=9 class=\"col2\">\n            <ion-row class=\"row3\">\n              <ion-text class=\"text2\">Unlocked <span class=\"text1\">{{item.userName}}</span></ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text2\">₹ {{item.transactionAmount}}</ion-text>\n            </ion-row>\n            <ion-row class=\"row4\">\n              <ion-text class=\"text3\">{{item.transactionDate}}</ion-text>\n              <ion-text class=\"text4\">{{item.transactionTime}}</ion-text>\n            </ion-row>\n            <div class=\"centered\">\n              <img class=\"img\" src=\"../../assets/images/wallet/tick.png\">\n            </div>\n          </ion-col>\n        </ion-row>\n      <!-- </div> -->\n    </div>\n  </div>\n\n  \n\n  <div class=\"div1\" *ngIf='notificationData.length==0'>\n    <ion-row class=\"row5\" style=\"margin-top: 5%;\">\n      <ion-text class=\"text5\">No Notification</ion-text>\n    </ion-row>\n  </div>\n  \n</ion-content>\n");

/***/ }),

/***/ "./src/app/notification/notification-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/notification/notification-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: NotificationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageRoutingModule", function() { return NotificationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _notification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification.page */ "./src/app/notification/notification.page.ts");




const routes = [
    {
        path: '',
        component: _notification_page__WEBPACK_IMPORTED_MODULE_3__["NotificationPage"]
    }
];
let NotificationPageRoutingModule = class NotificationPageRoutingModule {
};
NotificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NotificationPageRoutingModule);



/***/ }),

/***/ "./src/app/notification/notification.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/notification/notification.module.ts ***!
  \*****************************************************/
/*! exports provided: NotificationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function() { return NotificationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notification-routing.module */ "./src/app/notification/notification-routing.module.ts");
/* harmony import */ var _notification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notification.page */ "./src/app/notification/notification.page.ts");







let NotificationPageModule = class NotificationPageModule {
};
NotificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationPageRoutingModule"]
        ],
        declarations: [_notification_page__WEBPACK_IMPORTED_MODULE_6__["NotificationPage"]]
    })
], NotificationPageModule);



/***/ }),

/***/ "./src/app/notification/notification.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/notification/notification.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar1 {\n  height: 74px;\n  display: flex;\n  overflow: visible;\n  --background: linear-gradient(270deg, #02CBEE 0%, #59E022 100%);\n  box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.25);\n  border-radius: 0 0 25px 25px;\n}\n.toolbar1 ion-row {\n  margin-top: 0px;\n}\n.toolbar1 ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.toolbar1 ion-icon {\n  color: #ffffff;\n  font-size: 30px;\n}\n.toolbar1 ion-text {\n  overflow: hidden;\n  font-family: \"Poppins-Bold\";\n  color: #ffffff;\n  font-size: 25px;\n  letter-spacing: 0px;\n  line-height: 1.2;\n  font-weight: 700;\n}\n@media (min-height: 568px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 640px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 667px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 731px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n@media (min-height: 736px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 812px) {\n  .toolbar1 ion-row {\n    margin-top: 20px;\n  }\n}\n@media (min-height: 823px) {\n  .toolbar1 ion-row {\n    margin-top: 0px;\n  }\n}\n.div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-top: 5%;\n}\n.row2 {\n  width: 344px;\n  height: 110px;\n  background: #FFFFFF;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n  margin-top: 5%;\n}\n.row2 .col1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n.row2 .col1 ion-row {\n  width: 100%;\n}\n.row2 .col1 ion-avatar {\n  height: 50px;\n  width: 50px;\n}\n.row2 .col2 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n  position: relative;\n}\n.row2 .col2 .centered {\n  position: absolute;\n  top: 20px;\n  left: 200px;\n}\n.row2 .col2 .centered .img {\n  width: 40px;\n}\n.row2 .col2 .row3 {\n  width: 100%;\n}\n.row2 .col2 .row4 {\n  width: 100%;\n  padding-top: 3%;\n}\n.row2 .col2 .text1 {\n  font-family: \"Poppins-Regular\";\n  color: #000000;\n  font-size: 14px;\n  margin-left: 3%;\n}\n.row2 .col2 .text2 {\n  font-family: \"Poppins-Bold\";\n  color: #000000;\n  font-size: 14px;\n}\n.row2 .col2 .text3 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 12px;\n}\n.row2 .col2 .text4 {\n  font-family: \"Poppins-Regular\";\n  color: #767474;\n  font-size: 12px;\n  margin-left: 10%;\n}\n.text5 {\n  font-family: \"Poppins-Bold\";\n  color: #000000;\n  font-size: 14px;\n}\n.row5 {\n  width: 90%;\n  justify-content: center;\n}\n.div1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsK0RBQUE7RUFDQSxnREFBQTtFQUNBLDRCQUFBO0FBQ0o7QUFBSTtFQUNFLGVBQUE7QUFFTjtBQUFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUVSO0FBQUk7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQUVSO0FBQUk7RUFDRSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFFTjtBQUdFO0VBRUk7SUFDRSxnQkFBQTtFQUROO0FBQ0Y7QUFLRTtFQUVJO0lBQ0UsZUFBQTtFQUpOO0FBQ0Y7QUFRRTtFQUVJO0lBQ0UsZ0JBQUE7RUFQTjtBQUNGO0FBV0U7RUFFSTtJQUNFLGVBQUE7RUFWTjtBQUNGO0FBY0U7RUFFSTtJQUNFLGdCQUFBO0VBYk47QUFDRjtBQWlCRTtFQUVJO0lBQ0UsZ0JBQUE7RUFoQk47QUFDRjtBQW9CRTtFQUVJO0lBQ0UsZUFBQTtFQW5CTjtBQUNGO0FBd0JFO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUF0Qko7QUF3QkE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQXJCSjtBQXNCSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFwQlI7QUFxQlE7RUFDSSxXQUFBO0FBbkJaO0FBcUJRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUFuQlo7QUFzQkk7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUFwQlI7QUFxQlE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBbkJaO0FBb0JZO0VBQ0ksV0FBQTtBQWxCaEI7QUFxQlE7RUFDSSxXQUFBO0FBbkJaO0FBcUJRO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUFuQlo7QUFxQlE7RUFDSSw4QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQW5CWjtBQXFCVTtFQUNJLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFuQmQ7QUFxQlk7RUFDSSw4QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBbkJoQjtBQXFCWTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQW5CaEI7QUF1QkE7RUFDRSwyQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBcEJGO0FBc0JBO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0FBbkJGO0FBcUJBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFsQkYiLCJmaWxlIjoic3JjL2FwcC9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b29sYmFyMSB7XG4gICAgaGVpZ2h0OiA3NHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjcwZGVnLCAjMDJDQkVFIDAlLCAjNTlFMDIyIDEwMCUpO1xuICAgIGJveC1zaGFkb3c6IDBweCA0cHggMTJweCAwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjVweCAyNXB4O1xuICAgIGlvbi1yb3cge1xuICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIH1cbiAgICBpb24tY29sIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgIH1cbiAgICBpb24tdGV4dCB7XG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1Cb2xkXCI7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gICAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICB9XG4gIH1cblxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDU2OHB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNjQwcHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDY2N3B4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogNzMxcHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBtZWRpYSAobWluLWhlaWdodCA6IDczNnB4KSB7XG4gICAgLnRvb2xiYXIxe1xuICAgICAgaW9uLXJvdyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQG1lZGlhIChtaW4taGVpZ2h0IDogODEycHgpIHtcbiAgICAudG9vbGJhcjF7XG4gICAgICBpb24tcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBAbWVkaWEgKG1pbi1oZWlnaHQgOiA4MjNweCkge1xuICAgIC50b29sYmFyMXtcbiAgICAgIGlvbi1yb3cge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG5cblxuICAuZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG4ucm93MiB7XG4gICAgd2lkdGg6IDM0NHB4O1xuICAgIGhlaWdodDogMTEwcHg7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIC5jb2wxIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGlvbi1yb3cge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLWF2YXRhciB7XG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICB9XG4gICAgfVxuICAgIC5jb2wyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC5jZW50ZXJlZCB7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICB0b3A6IDIwcHg7XG4gICAgICAgICAgICBsZWZ0OiAyMDBweDtcbiAgICAgICAgICAgIC5pbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgLnJvdzMge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLnJvdzQge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgICAgIH1cbiAgICAgICAgLnRleHQxIHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtUmVndWxhclwiO1xuICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMyU7XG4gICAgICAgICAgfVxuICAgICAgICAgIC50ZXh0MiB7XG4gICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnRleHQzIHtcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50ZXh0NCB7XG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMCU7XG4gICAgICAgICAgICAgIH1cbiAgICB9ICAgXG59XG4udGV4dDUge1xuICBmb250LWZhbWlseTogXCJQb3BwaW5zLUJvbGRcIjtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5yb3c1IHtcbiAgd2lkdGg6IDkwJTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uZGl2MSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/notification/notification.page.ts":
/*!***************************************************!*\
  !*** ./src/app/notification/notification.page.ts ***!
  \***************************************************/
/*! exports provided: NotificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPage", function() { return NotificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





let NotificationPage = class NotificationPage {
    constructor(router, route, storage, authService) {
        this.router = router;
        this.route = route;
        this.storage = storage;
        this.authService = authService;
        this.notificationData = [];
        this.userData = {};
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                this.pageRoute = params.special;
            }
        });
    }
    ngOnInit() {
        this.storage.get('user').then((user) => {
            this.userData = user;
            this.userId = user._id;
            this.loadData();
        });
    }
    loadData() {
        this.notificationData = [];
        var obj = { "userId": this.userId };
        this.authService.getNotification(obj).subscribe((data) => {
            console.log(data);
            if (data.success) {
                this.walletBalance = data.data.wallet;
                this.notificationData = data.notificationData;
            }
        });
    }
    back() {
        this.router.navigateByUrl(this.pageRoute);
    }
};
NotificationPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
NotificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notification',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./notification.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./notification.page.scss */ "./src/app/notification/notification.page.scss")).default]
    })
], NotificationPage);



/***/ })

}]);
//# sourceMappingURL=notification-notification-module-es2015.js.map