(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["likes-likes-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/likes/likes.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/likes/likes.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLikesLikesPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar\">\n    <ion-row style=\"margin-top: 15%;\n    margin-bottom: 10%;\">\n      <ion-col [size]=2 (click)='back()'>\n        <ion-icon style=\"font-size: 30px;color:#FFFFFF\" name=\"arrow-back-outline\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=10>\n        <ion-text>Likes</ion-text>\n      </ion-col> \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div style=\"margin-top: 8%;\">\n    <div *ngFor=\"let item of likesArray\">\n      <ion-card style=\"margin-top: -3%;\" (click)=\"goToProfile1(item)\">\n        <ion-row>\n          <ion-col [size]=4 class=\"col1\" style=\"padding: 0;\">\n            <ion-thumbnail class=\"container\">\n              <ion-img class=\"img1\" [src]=\"item.userImage\"></ion-img>\n            </ion-thumbnail>            \n          </ion-col>\n          <ion-col [size]=8 class=\"col3\">\n            <ion-row class=\"row3\">\n              <ion-text class=\"text\">{{item.userName}}</ion-text>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>  \n    </div>\n  </div>\n  \n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/likes/likes-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/likes/likes-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: LikesPageRoutingModule */

    /***/
    function srcAppLikesLikesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LikesPageRoutingModule", function () {
        return LikesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _likes_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./likes.page */
      "./src/app/likes/likes.page.ts");

      var routes = [{
        path: '',
        component: _likes_page__WEBPACK_IMPORTED_MODULE_3__["LikesPage"]
      }];

      var LikesPageRoutingModule = function LikesPageRoutingModule() {
        _classCallCheck(this, LikesPageRoutingModule);
      };

      LikesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LikesPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/likes/likes.module.ts":
    /*!***************************************!*\
      !*** ./src/app/likes/likes.module.ts ***!
      \***************************************/

    /*! exports provided: LikesPageModule */

    /***/
    function srcAppLikesLikesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LikesPageModule", function () {
        return LikesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _likes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./likes-routing.module */
      "./src/app/likes/likes-routing.module.ts");
      /* harmony import */


      var _likes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./likes.page */
      "./src/app/likes/likes.page.ts");

      var LikesPageModule = function LikesPageModule() {
        _classCallCheck(this, LikesPageModule);
      };

      LikesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _likes_routing_module__WEBPACK_IMPORTED_MODULE_5__["LikesPageRoutingModule"]],
        declarations: [_likes_page__WEBPACK_IMPORTED_MODULE_6__["LikesPage"]]
      })], LikesPageModule);
      /***/
    },

    /***/
    "./src/app/likes/likes.page.scss":
    /*!***************************************!*\
      !*** ./src/app/likes/likes.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppLikesLikesPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".img {\n  width: 25px;\n}\n\n.img1 {\n  height: 100px;\n  width: 100%;\n}\n\n.text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n\n.container {\n  height: 100px;\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.col3 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n}\n\n.row3 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  text-transform: capitalize;\n  margin-left: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGlrZXMvbGlrZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUNFO0VBQ0UsYUFBQTtFQUNBLFdBQUE7QUFFSjs7QUFBRTtFQUNJLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFHTjs7QUFERTtFQUNFLGFBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUlKOztBQUZFO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUtKOztBQUhBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSx1QkFBQTtBQU1KOztBQUpBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7QUFPSiIsImZpbGUiOiJzcmMvYXBwL2xpa2VzL2xpa2VzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWcge1xuICAgIHdpZHRoOiAyNXB4O1xuICB9XG4gIC5pbWcxIHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC50ZXh0IHtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgY29sb3I6ICMwMDAwMDA7O1xuICB9XG4gIC5jb250YWluZXIge1xuICAgIGhlaWdodDoxMDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuY29sMSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29sMyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuLnJvdzMge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBtYXJnaW4tbGVmdDogNSU7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/likes/likes.page.ts":
    /*!*************************************!*\
      !*** ./src/app/likes/likes.page.ts ***!
      \*************************************/

    /*! exports provided: LikesPage */

    /***/
    function srcAppLikesLikesPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LikesPage", function () {
        return LikesPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");

      var LikesPage = /*#__PURE__*/function () {
        function LikesPage(router, route, authService) {
          var _this = this;

          _classCallCheck(this, LikesPage);

          this.router = router;
          this.route = route;
          this.authService = authService;
          this.selectedPost = {};
          this.likesArray = [];
          this.professionalUser = {};
          this.route.queryParams.subscribe(function (params) {
            console.log(params);
            _this.selectedPost = JSON.parse(params.selectedPost);
            console.log(_this.selectedPost);
            _this.likesArray = _this.selectedPost.likes;
            console.log(_this.likesArray);
            _this.pageroute = params.specical;
          });
        }

        _createClass(LikesPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "goToProfile1",
          value: function goToProfile1(item) {
            var _this2 = this;

            var obj = {
              "userId": item.likedPostUserid
            };
            this.authService.getUserProfileById(obj).subscribe(function (data) {
              console.log(data);
              _this2.professionalUser = data.data;
              var navigationExtras = {
                queryParams: {
                  pageroute: _this2.router.url,
                  user_id: item.likedPostUserid,
                  professionalUser: JSON.stringify(_this2.professionalUser)
                }
              };

              _this2.router.navigate(['profile1'], navigationExtras);
            });
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl(this.pageroute);
          }
        }]);

        return LikesPage;
      }();

      LikesPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }];
      };

      LikesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-likes',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./likes.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/likes/likes.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./likes.page.scss */
        "./src/app/likes/likes.page.scss"))["default"]]
      })], LikesPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=likes-likes-module-es5.js.map