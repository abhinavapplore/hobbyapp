(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"toolbar\">\n    <ion-row style=\"margin-top: 15%;\n    margin-bottom: 10%;\">\n      <ion-col [size]=2 (click)='openFirst()'>\n        <img class=\"img\" src=\"../../assets/images/menu.svg\">\n      </ion-col>\n      <ion-col [size]=8>\n        <ion-text class=\"text3\">Connections</ion-text>\n      </ion-col>\n      <ion-col [size]=2 (click)=\"notification()\">\n        <ion-icon style=\"font-size: 30px;color:#FFFFFF\" name=\"notifications-outline\"></ion-icon>\n      </ion-col>    \n    </ion-row>   \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content [fullscreen]=\"true\">\n\n  <div class=\"div1\">\n    <ion-row style=\"width: 95%;\">\n      <ion-col [size]=12 class=\"col2\">\n        <ion-icon class=\"icon1\" name=\"search\"></ion-icon>\n        <ion-input (ionInput)=\"getUser($event)\" type='text' placeholder='Search...'></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-list *ngIf=\"showList\" class=\"list\">\n      <ion-item *ngFor=\"let users of search\" (click)=\"selectChat(users)\">\n       <ion-text class=\"noti-head\">\n         <b style=\"color: black;\">\n          {{users.recieverName}} \n         </b>\n         \n        </ion-text> \n      \n      </ion-item>\n    </ion-list>\n  </div>\n\n  <div *ngIf=\"openchats.length==0\" style=\"text-align: center;\n  margin-top: 5%;\">\n    <ion-text class=\"text\">No Conversations.</ion-text>\n  </div>\n\n<div style=\"margin-top: 8%;\" *ngIf=\"openchats.length!=0\">\n  <!-- <div class=\"div\" > -->\n\n    <div *ngFor=\"let item of openchats\" (click)=\"gotochat(item)\">\n      <ion-card *ngIf=\"!item.isblocked\">\n        <ion-row>\n          <ion-col size=\"4\">\n            <ion-thumbnail style=\"height: 80px;\n            width: 100px;\">\n              <ion-img style=\"border-radius: 10px;\" [src]=\"item.recieverImage\"></ion-img>\n            </ion-thumbnail>\n          </ion-col>\n          <ion-col size=\"5\">\n  <ion-row style=\"margin-top: 10px;\">\n    <ion-text class=\"text\">{{item.recieverName}}</ion-text>\n  \n  </ion-row>\n  <ion-row>\n  \n    <ion-text class=\"text1\">{{item.lastMsg}}</ion-text>\n  </ion-row>\n          </ion-col>\n          <ion-col size=\"3\">\n            <ion-row style=\"margin-top: 10px;\">\n              <ion-text class=\"text2\">{{item.msgTime}}</ion-text>\n            </ion-row>\n           \n          </ion-col>\n        </ion-row>\n        \n      </ion-card>\n    \n    </div>\n \n    \n  <!-- </div> -->\n</div>\n\n<ion-row style=\"padding-top: 15%;\">\n  <ion-text style=\"color: #F6F6F6;\"></ion-text>\n</ion-row>\n\n</ion-content>\n\n\n");

/***/ }),

/***/ "./src/app/tab3/tab3-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab3/tab3-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageRoutingModule", function() { return Tab3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");




const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_3__["Tab3Page"],
    }
];
let Tab3PageRoutingModule = class Tab3PageRoutingModule {
};
Tab3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab3PageRoutingModule);



/***/ }),

/***/ "./src/app/tab3/tab3.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tab3-routing.module */ "./src/app/tab3/tab3-routing.module.ts");









let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__["ExploreContainerComponentModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }]),
            _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__["Tab3PageRoutingModule"],
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
    })
], Tab3PageModule);



/***/ }),

/***/ "./src/app/tab3/tab3.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".icon {\n  color: #ffffff;\n  font-size: 25px;\n}\n\n.img {\n  width: 25px;\n}\n\n.container {\n  height: 100px;\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.img1 {\n  height: 100px;\n  width: 100%;\n}\n\n.text {\n  font-family: \"Poppins-Bold\";\n  font-size: 16px;\n  color: #000000;\n}\n\n.text1 {\n  font-family: \"Poppins-Regular\";\n  font-size: 16px;\n  color: #767474;\n}\n\n.text2 {\n  font-family: \"Poppins-Regular\";\n  font-size: 14px;\n  color: #767474;\n}\n\n.text3 {\n  padding-left: 5%;\n}\n\n.div {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  margin-top: 3%;\n}\n\n.div1 {\n  padding-top: 5%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.row1 {\n  width: 340px;\n  height: 48px;\n  border-radius: 5px;\n}\n\n.col {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.col1 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.col2 {\n  background: #FFFFFF;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  width: 342px;\n  height: 48px;\n}\n\nion-input {\n  --background: #FFFFFF;\n  --color: #000000;\n  --placeholder-color: #999999;\n  font-size: 18px;\n  margin-left: 5%;\n}\n\n.icon1 {\n  font-size: 22px;\n  margin-left: 5%;\n}\n\n.col3 {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n}\n\n.row2 {\n  padding-top: 5%;\n  width: 100%;\n  text-align: justify;\n}\n\n.row3 {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  text-transform: capitalize;\n}\n\n.div1 {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  margin-top: 3%;\n}\n\n.list {\n  width: 90%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMy90YWIzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBQ0U7RUFDRSxXQUFBO0FBRUo7O0FBQUU7RUFDRSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFHSjs7QUFERTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FBSUo7O0FBRkU7RUFDSSwyQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBS047O0FBSEU7RUFDRSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBTUo7O0FBSkE7RUFDSSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBT0o7O0FBTEE7RUFDSSxnQkFBQTtBQVFKOztBQU5BO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0FBU0o7O0FBUEE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFVSjs7QUFKQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFPSjs7QUFMQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FBUUo7O0FBTkE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBU0o7O0FBUEE7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQVVKOztBQVJBO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUFXSjs7QUFUQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0FBWUo7O0FBVkE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0FBYUo7O0FBWEE7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FBY0o7O0FBWkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0FBZUo7O0FBYkE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFnQko7O0FBZEE7RUFDSSxVQUFBO0FBaUJKIiwiZmlsZSI6InNyYy9hcHAvdGFiMy90YWIzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pY29uIHtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gIH1cbiAgLmltZyB7XG4gICAgd2lkdGg6IDI1cHg7XG4gIH1cbiAgLmNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OjEwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5pbWcxIHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC50ZXh0IHtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnMtQm9sZFwiO1xuICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgY29sb3I6ICMwMDAwMDA7O1xuICB9XG4gIC50ZXh0MSB7XG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGlucy1SZWd1bGFyXCI7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGNvbG9yOiAjNzY3NDc0O1xufVxuLnRleHQyIHtcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zLVJlZ3VsYXJcIjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICM3Njc0NzQ7XG59XG4udGV4dDMge1xuICAgIHBhZGRpbmctbGVmdDogNSU7XG59XG4uZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMyU7XG59XG4uZGl2MSB7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi8vIC5yb3cge1xuXG4vLyAgICAgcGFkZGluZzogMyU7XG4vLyB9XG4ucm93MSB7XG4gICAgd2lkdGg6IDM0MHB4O1xuICAgIGhlaWdodDogNDhweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cbi5jb2wxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb2wyIHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAzNDJweDtcbiAgICBoZWlnaHQ6IDQ4cHg7XG59XG5pb24taW5wdXQge1xuICAgIC0tYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICAtLWNvbG9yOiAjMDAwMDAwO1xuICAgIC0tcGxhY2Vob2xkZXItY29sb3I6ICM5OTk5OTk7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbn1cbi5pY29uMSB7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbn1cbi5jb2wzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4ucm93MiB7XG4gICAgcGFkZGluZy10b3A6IDUlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG4ucm93MyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuLmRpdjEge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAzJTtcbn1cbi5saXN0IHtcbiAgICB3aWR0aDogOTAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/tab3/tab3.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js");









let Tab3Page = class Tab3Page {
    constructor(router, menu, authService, storage, af, fs) {
        this.router = router;
        this.menu = menu;
        this.authService = authService;
        this.storage = storage;
        this.af = af;
        this.fs = fs;
        this.openchats = [];
        this.showList = false;
        this.isSearch = false;
        this.search = [];
    }
    ngOnInIt() {
    }
    ionViewDidEnter() {
        this.storage.get('user').then((user) => {
            this.uid = user._id;
            this.getChats();
            this.senderName = user.senderName;
        });
    }
    getChats() {
        this.authService.loading("Loading chats...");
        this.allUsers = this.fs.collection('friends').doc(this.uid).collection('chats', ref => ref.orderBy('Timestamp')).snapshotChanges();
        // This works:
        console.log(this.allUsers);
        var todayDate = moment__WEBPACK_IMPORTED_MODULE_6__().format("DD-MM-YYYY");
        if (this.allUsers.length != 0) {
            this.allUsers.forEach(user => {
                if (user.length == 0) {
                    this.emptydata = true;
                }
                else {
                    this.openchats = [];
                    user.forEach(userData => {
                        let data = userData.payload.doc.data();
                        let id = userData.payload.doc.id;
                        let time = moment__WEBPACK_IMPORTED_MODULE_6__(userData.payload.doc.Timestamp).format("h:mm A'");
                        console.log(typeof (userData.payload.doc.Timestamp));
                        data.formatTime = time;
                        this.openchats.push(data);
                        console.log("this is openchat array");
                        console.log(this.openchats);
                        console.log("ID: ", id, " Data: ", data);
                        if (this.openchats.length != 0) {
                            this.emptydata = false;
                            for (var i = 0; i < this.openchats.length; i++) {
                                // var msgDate=moment(this.openchats[i].msgDate);
                                if (this.openchats[i].msgDate != todayDate) {
                                    this.openchats[i].showTime = false;
                                }
                                else {
                                    this.openchats[i].showTime = true;
                                    // this.openchats[element].msgTime=this.openchats[element].msgDate;
                                }
                            }
                        }
                        else {
                            this.emptydata = true;
                        }
                    });
                }
            });
        }
        else {
            this.authService.dismissLoading();
            this.emptydata = true;
        }
        this.authService.dismissLoading();
    }
    gotochat(item) {
        let recId = item.recieverId;
        let vc = { recieverId: recId,
            senderId: this.uid,
            recieverName: item.recieverName,
            recieverImg: item.recieverImage,
            senderName: this.senderName };
        let navigationExtras = {
            queryParams: {
                special: JSON.stringify(vc),
            }
        };
        this.router.navigate(['chat'], navigationExtras);
    }
    getUser(ev) {
        var arr = [];
        for (let i = 0; i < this.openchats.length; i++) {
            var input = this.openchats[i];
            arr.push(input);
            let val = ev.target.value;
            if (val && val.trim() != '') {
                this.search = arr.filter(function (item) {
                    return item.recieverName.toLowerCase().indexOf(val.toLowerCase()) > -1;
                });
                // Show the results
                if (this.search.length !== 0) {
                    this.showList = true;
                }
                else {
                    this.showList = false;
                }
            }
            else {
                this.showList = false;
            }
        }
    }
    selectChat(item) {
        this.showList = false;
        this.searchTerm = "";
        this.isSearch = false;
        console.log(item.evn_id);
        console.log(item.evn_name);
        console.log(item);
        this.gotochat(item);
    }
    openFirst() {
        this.menu.enable(true, 'first');
        this.menu.open('first');
    }
    notification() {
        let navigationExtras = {
            queryParams: {
                special: this.router.url
            }
        };
        this.router.navigate(['notification'], navigationExtras);
    }
};
Tab3Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestore"] }
];
Tab3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab3',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab3.page.scss */ "./src/app/tab3/tab3.page.scss")).default]
    })
], Tab3Page);



/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module-es2015.js.map